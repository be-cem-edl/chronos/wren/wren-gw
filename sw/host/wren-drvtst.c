/* Low-level tool to interract with wren using the driver.  */

#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/time.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdint.h>
#include <elf.h>
#include <time.h>

#include "host_map.h"
#include "wren-mb-defs.h"
#include "wrenrx-data.h"

#include "wren-ioctl.h"
#include "../api/app/wrenrx-cond-parser.h"
#include "../api/app/wrenrx-tools.h"
#include "wren/wren-packet.h"
#include "wren/wren-hw.h"
#include "wren/wrentx-table.h"
#include "../api/wren-mb.h"
#include "../api/wrentx-private.h"
#include "../api/wren-util.h"

// #include "pspcie-dma.h"

const char *progname;

static int wren_fd = -1;

static unsigned verbose;

/* MAXSZ in bytes */
static int mb_msg(struct wren_mb_metadata *md, void *data,
		   void *res, unsigned maxsz)
{
    int r;

    if (verbose > 1)
	printf("mb_msg: cmd=%08x len=%u\n", md->cmd, md->len);

    r = wren_mb_msg(wren_fd, md, data, res, maxsz);

    if (verbose > 1)
	printf(" res=%d  cmd=%08x len=%u\n", r, md->cmd, md->len);

    if (md->cmd & CMD_ERROR) {
	printf("error: %08x\n", md->cmd);
	return -1;
    }
    return r;
}

static int
do_ping (char *argv[], int argc)
{
  uint32_t wdata = 0xabba8567;
  struct wren_mb_metadata md;

  uint32_t rdata;

  md.cmd = CMD_PING;
  md.len = 1;

  mb_msg(&md, &wdata, &rdata, sizeof(rdata));

  if (md.len != 0)
      printf ("error ping len\n");

  return 0;
}

static int
do_tx (char *argv[], int argc)
{
  struct wren_mb_metadata wmd;
  int len = 48;
  unsigned i;
  union {
    unsigned char b[60];
    uint32_t w[15];
  } u;
  unsigned char *b = u.b;

  /* Data offset for wr_nic (the first half-word is ignored) */
  b[0] = 0x00;
  b[1] = 0x00;

  /* Dest: broadcast */
  b[2] = 0xf0;
  b[3] = 0xf1;
  b[4] = 0xf2;
  b[5] = 0xf3;
  b[6] = 0xf4;
  b[7] = 0xf5;
  /* Src: xx */
  b[8] = 0xa0;
  b[9] = 0x22;
  b[10] = 0x33;
  b[11] = 0x44;
  b[12] = 0x55;
  b[13] = 0x66;
  /* Proto */
  b[14] = 'W';
  b[15] = 'e';
  /* Content */
  for (i = 16; i < len; i++)
    b[i] = i;

  wmd.cmd = CMD_SEND;
  wmd.len = len;

  mb_msg(&wmd, u.w, NULL, 0);

  return 0;
}

static int
do_link (char *argv[], int argc)
{
  uint32_t v;

  if (ioctl(wren_fd, WREN_IOC_GET_LINK, &v) < 0) {
      fprintf(stderr, "ioctl get_link: %m\n");
      return -1;
  }
  printf("link status: %u\n", v);

  return 0;
}

static void
disp_time (const char *name, unsigned long long sec, unsigned nsec)
{
    time_t t;
    unsigned ns, us, ms;

    ns = nsec;
    ms = ns / 1000000;
    ns -= ms * 1000000;

    us = ns / 1000;
    ns -= us * 1000;

    printf("%s %llu + %ums %uus %uns\n", name, sec, ms, us, ns);
    t = sec;
    printf("%s %s", name, asctime(gmtime(&t)));
}

static int
do_time (char *argv[], int argc)
{
    struct timespec ts;
    struct timeval tv;
    int loop = 0;

    if (argc == 2 && strcmp(argv[1], "-l") == 0)
	loop = 1;

    if (clock_gettime(CLOCK_TAI, &ts) != 0) {
	fprintf(stderr, "cannot get CLOCK_TAI time: %m\n");
    }
    else
	disp_time ("SYS TAI:", ts.tv_sec, ts.tv_nsec);

    if (clock_gettime(CLOCK_REALTIME, &ts) != 0) {
	fprintf(stderr, "cannot get CLOCK_REALTIME time: %m\n");
    }
    else
	disp_time ("SYS RT: ", ts.tv_sec, ts.tv_nsec);

    gettimeofday(&tv, NULL);
    disp_time ("SYS TOD:", tv.tv_sec, tv.tv_usec * 1000);

    do {
	if (wrenctl_get_wr_time(wren_fd, &ts) == 0)
	    disp_time ("WR TAI: ", ts.tv_sec, ts.tv_nsec);
    } while (loop);

  return 0;
}

static int
do_wait (char *argv[], int argc)
{
    struct wren_wr_time res;
    unsigned nsecs, secs;
    unsigned v;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: wait N[s|ms|us]\n");
	return -1;
    }

    v = strtoul(argv[1], &e, 10);
    if (*e == 0 || !strcmp(e, "s")) {
	nsecs = 0;
	secs = v;
    }
    else if (!strcmp(e, "ms")) {
	secs = v / 1000;
	v -= secs * 1000;
	nsecs = v * 1000000;
    }
    else if (!strcmp(e, "us")) {
	secs = v / 1000000;
	v -= secs * 1000000;
	nsecs = v * 1000;
    }
    else {
	fprintf(stderr, "bad wait unit '%s'\n", e);
	return -1;
    }

    if (ioctl(wren_fd, WREN_IOC_GET_TIME, &res) < 0) {
	fprintf(stderr, "ioctl get_time: %m\n");
	return -1;
    }

    res.ns += nsecs;
    if (res.ns > 1000000000) {
	res.ns -= 1000000000;
	res.tai_lo++;
    }
    res.tai_lo += secs;

    if (ioctl(wren_fd, WREN_IOC_WAIT_TIME, &res) < 0) {
	fprintf(stderr, "ioctl wait_time: %m\n");
	return -1;
    }
    return 1;
}

static int
do_read (char *argv[], int argc)
{
    uint32_t cmd[2];
    uint32_t res[128];
    struct wren_mb_metadata md;
    char *e;
    unsigned i;

    if (argc < 2 || argc > 3) {
	fprintf(stderr, "usage: read ADDR [LEN]\n");
	return -1;
    }

    cmd[0] = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for address (%s)\n", argv[1]);
	return -1;
    }

    if (argc > 2) {
	cmd[1] = strtoul(argv[2], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for length (%s)\n", argv[1]);
	    return -1;
	}
    }
    else
	cmd[1] = 1;

    md.cmd = CMD_READ;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, res, sizeof(res)) < 0)
	return -1;

    printf ("0x%08x:", cmd[0]);
    for (i = 0; i < md.len; i++)
	printf (" %08x", res[i]);
    printf("\n");
    return argc <= 3 ? argc : 3;
}

static int
do_write (char *argv[], int argc)
{
    uint32_t cmd[2];
    uint32_t res[128];
    struct wren_mb_metadata md;
    char *e;

    if (argc != 3) {
	fprintf(stderr, "usage: write ADDR VALUE\n");
	return -1;
    }

    cmd[0] = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for address (%s)\n", argv[1]);
	return -1;
    }

    cmd[1] = strtoul(argv[2], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value (%s)\n", argv[2]);
	return -1;
    }

    md.cmd = CMD_WRITE;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, res, sizeof(res)) < 0)
	return -1;
    return 3;
}

static int
do_hw_get_config (char *argv[], int argc)
{
    struct wren_mb_hw_config rep;
    struct wren_mb_metadata md;

    md.cmd = CMD_HW_GET_CONFIG;
    md.len = 0;

    if (mb_msg(&md, NULL,  &rep, sizeof(rep)) < 0)
	return 0;
    printf("model_ident: 0x%08x\n", rep.model_ident);
    printf("fp_id:       0x%08x\n", rep.fp_id);
    printf("pp_id:       0x%08x\n", rep.pp_id);
    printf("pcb_id:      0x%08x\n", rep.pcb_id);
    return 0;
}

static int
do_drv_log_flags (char *argv[], int argc)
{
    uint32_t flags;
    if (argc == 1) {
	if (ioctl(wren_fd, WREN_IOC_GET_DRV_LOG_FLAGS, &flags) < 0) {
	    fprintf(stderr, "ioctl get_drv_log_flags: %m\n");
	    return -1;
	}
	printf("driver log flags: 0x%08x\n", (unsigned)flags);
	return 0;
    }
    else if (argc == 2) {
	char *e;
	flags = strtoul(argv[1], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for flags (%s)\n", argv[1]);
	    return -1;
	}
	if (ioctl(wren_fd, WREN_IOC_SET_DRV_LOG_FLAGS, &flags) < 0) {
	    fprintf(stderr, "ioctl set_drv_log_flags: %m\n");
	    return -1;
	}
	return 2;
    }
    else {
	fprintf(stderr, "usage: %s [FLAGS]\n", argv[0]);
	return -1;
    }
}

static int
do_tx_get_config (char *argv[], int argc)
{
  union {
    struct wren_mb_tx_get_config_reply r;
    uint32_t u32;
  } rep;
  struct wren_mb_metadata md;

  md.cmd = CMD_TX_GET_CONFIG;
  md.len = 0;

  if (mb_msg(&md, NULL,  &rep.u32, sizeof(rep)) < 0)
    return 0;
  printf("sw_version: 0x%08x\n", rep.r.sw_version);
  printf("max_sources: %u\n", rep.r.max_sources);
  printf("nbr_sources: %u\n", rep.r.nbr_sources);
  return 0;
}

static int
do_tx_set_source (char *argv[], int argc)
{
    struct wren_mb_tx_set_source cmd;
    struct wren_mb_metadata md;
    uint32_t rep;
    char *e;

    if (argc != 3) {
	fprintf(stderr, "usage: tx-set-source SRC-IDX SRC-ID\n");
	return -1;
    }

    cmd.source_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    if (parse_protocol (argv + 2, argc - 2, &cmd.proto) < 0)
	return -1;

    md.cmd = CMD_TX_SET_SOURCE;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &rep, sizeof(rep)) < 0)
	return -1;
    return 2;
}

static int
do_tx_get_source (char *argv[], int argc)
{
    struct wren_mb_arg1 cmd;
    struct wren_mb_metadata md;
    struct wren_protocol res;
    unsigned idx;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: tx-get-source SRC-IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    cmd.arg1 = idx;

    md.cmd = CMD_TX_GET_SOURCE;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &res, sizeof(res)) < 0)
	return -1;

    printf ("%u: ", idx);
    disp_protocol(&res);

    return 1;
}

static int
do_tx_get_table (char *argv[], int argc)
{
    struct wren_ioctl_tx_table table;
    int res;
    unsigned idx;
    unsigned off;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: %s TABLE-IDX\n", argv[0]);
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    table.table.table_idx = idx;
    res = ioctl(wren_fd, WREN_IOC_TX_GET_TABLE, &table);
    if (res < 0) {
	printf ("cannot get table: %d\n", table.error);
	return -1;
    }

    printf ("name: %s\n", table.name);
    printf ("source:    %u\n", table.table.src_idx);
    printf ("repeat:    %u\n", table.table.repeat);
    printf ("nbr insns: %u\n", table.table.nbr_insns);
    printf ("nbr data:  %u\n", table.table.nbr_data);

    for (off = 0; off < table.table.nbr_insns; off++) {
	unsigned op0 = table.table.data[off];
	printf ("insn[%02u]: %08x: ", off, op0);


	switch (WREN_TABLE_OP_GET_OP(op0)) {
	case wrentx_table_op_send: {
	    unsigned op_addr = WREN_TABLE_OP_GET_FIELD(op0, SEND_ADDR);
	    unsigned op_len = WREN_TABLE_OP_GET_FIELD(op0, SEND_LEN);
	    printf ("send off=%u len=%u\n", op_addr, op_len);
	    wren_dump_packet_body
		(&table.table.data[table.table.nbr_insns + op_addr],
		 op_len, op_addr);
	}
	    break;
	case wrentx_table_op_wait:
	    printf ("wait %u sec + %u nsec\n",
		    WREN_TABLE_OP_GET_FIELD(op0, WAIT_SEC),
		    table.table.data[++off]);
	    break;
	default:
	    printf ("    unknown\n");
	    break;
	}
    }
    for (off = 0; off < table.table.nbr_data; off++)
	printf ("data[%02u]: %08x\n",
		off, (unsigned)table.table.data[table.table.nbr_insns + off]);

    return 1;
}

static int
do_tx_del_table (char *argv[], int argc)
{
    int res;
    uint32_t idx;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: %s TABLE-IDX\n", argv[0]);
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    res = ioctl(wren_fd, WREN_IOC_TX_DEL_TABLE, &idx);
    if (res < 0) {
	printf ("cannot del table: %d\n", (int)idx);
	return -1;
    }

    return 1;
}

static int
do_tx_play_table (char *argv[], int argc)
{
    struct wren_ioctl_tx_play_table cmd;
    struct wren_wr_time tm;
    int res;
    uint32_t idx;
    char *e;

    if (argc != 2) {
	fprintf(stderr, "usage: %s TABLE-IDX\n", argv[0]);
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }


    if (ioctl(wren_fd, WREN_IOC_GET_TIME, &tm) < 0) {
	fprintf(stderr, "ioctl get_time: %m\n");
	return -1;
    }

    cmd.table_idx = idx;
    cmd.due_ts.sec = tm.tai_lo;
    cmd.due_ts.nsec = tm.ns;
    res = ioctl(wren_fd, WREN_IOC_TX_PLAY_TABLE, &cmd);
    if (res < 0) {
	printf ("cannot play table: %d\n", cmd.error);
	return -1;
    }
    return 1;
}

static int
do_rx_get_config (char *argv[], int argc)
{
    union {
	struct wren_mb_rx_get_config_reply r;
	uint32_t u32;
    } rep;
    struct wren_mb_metadata md;

    md.cmd = CMD_RX_GET_CONFIG;
    md.len = 0;

    if (mb_msg(&md, NULL, &rep.u32, sizeof(rep)) < 0)
	return 0;

    printf("sw_version: 0x%08x\n", rep.r.sw_version);
    printf("max_sources: %u\n", rep.r.max_sources);
    printf("max_conds:   %u\n", rep.r.max_conds);
    printf("max_actions: %u\n", rep.r.max_actions);
    return 0;
}

static void
init_pulser_config (struct wren_mb_pulser_config *trig)
{
    trig->inputs =
	(WRENRX_INPUT_NOSTART << WREN_MB_PULSER_INPUT_START_SH)
	| (WRENRX_INPUT_NOSTOP << WREN_MB_PULSER_INPUT_STOP_SH)
	| (WRENRX_INPUT_CLOCK_1GHZ << WREN_MB_PULSER_INPUT_CLOCK_SH);
    trig->pulser_idx = 0xff;
    trig->width = 8;
    trig->period = 125;
    trig->npulses = 1;
    trig->idelay = 0;
    trig->flags = WREN_MB_PULSER_FLAG_OUT_EN | WREN_MB_PULSER_FLAG_INT_EN;
    trig->load_off_sec = 0;
    trig->load_off_nsec = 0;
}

static int
parse_pulser_clock(const char *opt, const char *arg,
		   uint16_t *input, unsigned sh)
{
    int sig = parse_input_name(arg);
    if (sig < 0) {
	fprintf (stderr, "incorrect value for option %s (%s)\n", opt, arg);
	return -1;
    }
    *input &= ~(WREN_MB_PULSER_INPUT_MASK << sh);
    *input |= sig << sh;
    return 0;
}

static void
parse_pulser_flag(uint8_t *flags, uint8_t flag, long val)
{
    if (val)
	*flags |= flag;
    else
	*flags &= ~flag;
}

static int
parse_pulser_config(struct wren_mb_pulser_config *trig,
		    const char *opt, const char *arg)
{
    long val;
    char *e;

    if (opt[0] != '-') {
	fprintf(stderr, "incorrect option %s\n", opt);
	return -1;
    }

    if (arg == NULL) {
	fprintf(stderr, "missing value for option %s\n", opt);
	return -1;
    }

    if (!strcmp(opt, "-start"))
	return parse_pulser_clock (opt, arg, &trig->inputs,
				   WREN_MB_PULSER_INPUT_START_SH);
    else if (!strcmp(opt, "-stop"))
	return parse_pulser_clock (opt, arg, &trig->inputs,
				   WREN_MB_PULSER_INPUT_STOP_SH);
    else if (!strcmp(opt, "-clock"))
	return parse_pulser_clock (opt, arg, &trig->inputs,
				   WREN_MB_PULSER_INPUT_CLOCK_SH);

    val = strtol(arg, &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value option %s (%s)\n", opt, arg);
	return -1;
    }

    if (!strcmp(opt, "-width"))
	trig->width = val;
    else if (!strcmp(opt, "-period"))
	trig->period = val;
    else if (!strcmp(opt, "-idelay"))
	trig->idelay = val;
    else if (!strcmp(opt, "-n"))
	trig->npulses = val;
    else if (!strcmp(opt, "-r"))
	parse_pulser_flag(&trig->flags, WREN_MB_PULSER_FLAG_REPEAT, val);
    else if (!strcmp(opt, "-immediat"))
	parse_pulser_flag(&trig->flags, WREN_MB_PULSER_FLAG_IMMEDIAT, val);
    else if (!strcmp(opt, "-int"))
	parse_pulser_flag(&trig->flags, WREN_MB_PULSER_FLAG_INT_EN, val);
    else if (!strcmp(opt, "-out"))
	parse_pulser_flag(&trig->flags, WREN_MB_PULSER_FLAG_OUT_EN, val);
    else if (!strcmp(opt, "-sec"))
	trig->load_off_sec = val;
    else if (!strcmp(opt, "-ns"))
	trig->load_off_nsec = val;
    else {
	fprintf(stderr, "unknown option %s\n", opt);
	return -1;
    }

    return 0;
}

static void
help_pulser_config(const struct wren_mb_pulser_config *cfg)
{
    fprintf (stderr,
	     " -start VAL   start input [%s]\n",
	     get_input_name(WREN_MB_PULSER_GET_START(cfg->inputs)));
    fprintf (stderr,
	     " -stop VAL    stop input [%s]\n",
	     get_input_name(WREN_MB_PULSER_GET_STOP(cfg->inputs)));
    fprintf (stderr,
	     " -clock VAL   clock input [%s]\n",
	     get_input_name(WREN_MB_PULSER_GET_CLOCK(cfg->inputs)));
    fprintf (stderr,
	     " -width VAL   pulse width [%u]\n", cfg->width);
    fprintf (stderr,
	     " -period VAL  pulse period [%u]\n", cfg->period);
    fprintf (stderr,
	     " -idelay VAL  initial delay [%u]\n", cfg->idelay);
    fprintf (stderr,
	     " -n VAL       number of pulses [%u]\n", cfg->npulses);
    fprintf (stderr,
	     " -r VAL       repeat mode [%u]\n",
	     cfg->flags & WREN_MB_PULSER_FLAG_REPEAT ? 1 : 0);
    fprintf (stderr,
	     " -immediat VAL   immediat mode [%u]\n",
	     cfg->flags & WREN_MB_PULSER_FLAG_IMMEDIAT ? 1 : 0);
    fprintf (stderr,
	     " -int VAL     interrupt enable [%u]\n",
	     cfg->flags & WREN_MB_PULSER_FLAG_INT_EN ? 1 : 0);
    fprintf (stderr,
	     " -out VAL     output enable [%u]\n",
	     cfg->flags & WREN_MB_PULSER_FLAG_OUT_EN ? 1 : 0);
    fprintf (stderr,
	     " -sec VAL     second part of load time offset\n");
    fprintf (stderr,
	     " -ns VAL      nsecond part of load time offset (def: 0)\n");
}

static int
do_rx_set_comparator (char *argv[], int argc)
{
    struct wren_mb_comparator cfg;
    unsigned idx;
    uint32_t rep;
    char *e;
    unsigned narg;

    cfg.ts.nsec = 0;
    init_pulser_config (&cfg.conf);

    /* Default: 1 sec */
    cfg.conf.load_off_sec = 1;

    if (argc < 3) {
	fprintf(stderr,
		"usage: rx-set-comparator IDX PULSER-IDX [CONFIG]\n");
	help_pulser_config (&cfg.conf);
	return -1;
    }

    narg = 1;

    idx = strtoul(argv[narg], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for comparator index (%s)\n", argv[narg]);
	return -1;
    }
    narg++;

    cfg.conf.pulser_idx = strtoul(argv[narg], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser index (%s)\n", argv[narg]);
	return -1;
    }
    narg++;


    while (narg < argc) {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&cfg.conf, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    /* Adjust time */
    {
        struct wren_wr_time res;

	if (ioctl(wren_fd, WREN_IOC_GET_TIME, &res) < 0) {
	    fprintf(stderr, "ioctl get_time: %m\n");
	    return -1;
	}

	cfg.ts.sec = res.tai_lo + cfg.conf.load_off_sec;
	cfg.ts.nsec = res.ns + cfg.conf.load_off_nsec;
	if (cfg.ts.nsec >= 1000000000) {
	    cfg.ts.nsec -= 1000000000;
	    cfg.ts.sec += 1;
	}
    }

    rep = wrenctl_rx_set_comparator (wren_fd, idx, &cfg);
    if (rep != 0)
	printf("error (0x%x)\n", (unsigned)rep);

    return narg;
}

static int
do_rx_get_comparator (char *argv[], int argc)
{
    union {
	struct wren_mb_comparator cmp;
	uint32_t u32;
    } rep;
    uint32_t idx;
    struct wren_mb_metadata md;
    char *e;
    unsigned inp;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-get-comparator IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for comparator index (%s)\n", argv[1]);
	return -1;
    }

    md.cmd = CMD_RX_GET_COMPARATOR;
    md.len = sizeof(idx) / 4;

    if (mb_msg(&md, &idx, &rep.u32, sizeof(rep)) < 0)
	return -1;
    printf ("sec:      %u\n", rep.cmp.ts.sec);
    printf ("nsec:     %u\n", rep.cmp.ts.nsec);
    disp_time("time:    ", rep.cmp.ts.sec, rep.cmp.ts.nsec);
    inp = WREN_MB_PULSER_GET_START(rep.cmp.conf.inputs);
    printf ("start:    %u (%s)\n", inp, get_input_name(inp));
    inp = WREN_MB_PULSER_GET_STOP(rep.cmp.conf.inputs);
    printf ("stop:     %u (%s)\n", inp, get_input_name(inp));
    inp = WREN_MB_PULSER_GET_CLOCK(rep.cmp.conf.inputs);
    printf ("clock:    %u (%s)\n", inp, get_clock_name(inp));
    printf ("idx:      %u\n", rep.cmp.conf.pulser_idx);
    printf ("idelay:   %u\n", rep.cmp.conf.idelay);
    printf ("width:    %u\n", rep.cmp.conf.width);
    printf ("period:   %u\n", rep.cmp.conf.period);
    printf ("npulses:  %u\n", rep.cmp.conf.npulses);
    printf ("repeat:   %u\n",
	    rep.cmp.conf.flags & WREN_MB_PULSER_FLAG_REPEAT ? 1 : 0);
    printf ("immediat: %u\n",
	    rep.cmp.conf.flags & WREN_MB_PULSER_FLAG_IMMEDIAT ? 1 : 0);
    printf ("out-en:   %u\n",
	    rep.cmp.conf.flags & WREN_MB_PULSER_FLAG_OUT_EN ? 1 : 0);
    printf ("int-en:   %u\n",
	    rep.cmp.conf.flags & WREN_MB_PULSER_FLAG_INT_EN ? 1 : 0);
    printf ("valid:    %u\n",
	    rep.cmp.conf.flags & WREN_MB_PULSER_FLAG_ENABLE ? 1 : 0);
    return 1;
}

static int
do_rx_abort_comparator (char *argv[], int argc)
{
    struct wren_mb_arg2 cmd;
    union {
	struct wren_mb_comparator cmp;
	uint32_t u32;
    } rep;
    uint32_t idx;
    struct wren_mb_metadata md;
    char *e;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-abort-comparator IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for comparator index (%s)\n", argv[1]);
	return -1;
    }

    cmd.arg1 = idx / WREN_COMPARATORS_PER_GROUP;
    cmd.arg2 = 1 << (idx & (WREN_COMPARATORS_PER_GROUP - 1));
    md.cmd = CMD_RX_ABORT_COMPARATOR;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &rep.u32, sizeof(rep)) < 0)
	return -1;
    return 1;
}

static int
do_rx_pulser_status (char *argv[], int argc)
{
    uint32_t idx;
    uint32_t rep[2];
    int len;
    struct wren_mb_metadata md;
    char *e;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-pulser-status GRP\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for group index (%s)\n", argv[1]);
	return -1;
    }

    md.cmd = CMD_RX_PULSERS_STATUS;
    md.len = sizeof(idx) / 4;

    len = mb_msg(&md, &idx, rep, sizeof(rep));
    if (len < 0)
	return -1;
    printf("running: %08x\n", (unsigned)rep[0]);
    printf("loaded:  %08x\n", (unsigned)rep[1]);

    return 1;
}

static int
do_rx_get_pulser (char *argv[], int argc)
{
    uint32_t idx;
    union {
	struct wren_mb_rx_get_pulser_reply r;
	uint32_t u32;
    } rep;
    int len;
    struct wren_mb_metadata md;
    char *e;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-get-pulser IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser index (%s)\n", argv[1]);
	return -1;
    }

    md.cmd = CMD_RX_GET_PULSER;
    md.len = sizeof(idx) / 4;

    len = mb_msg(&md, &idx, &rep.u32, sizeof(rep));
    if (len < 0)
	return -1;
    printf("state:        %08x\n", (unsigned)rep.r.state);
    printf("pulses:       %08x\n", (unsigned)rep.r.pulses);
    printf("ts:           %08x\n", (unsigned)rep.r.ts);
    printf("done_comp:    %08x\n", (unsigned)rep.r.done_comp);
    printf("current_comp: %08x\n", (unsigned)rep.r.current_comp);
    printf("loaded_comp:  %08x\n", (unsigned)rep.r.loaded_comp);
    printf("late_comp:    %08x\n", (unsigned)rep.r.late_comp);
    printf("busy_comp:    %08x\n", (unsigned)rep.r.busy_comp);

    return 1;
}

static int
do_rx_abort_pulser (char *argv[], int argc)
{
    struct wren_mb_arg2 cmd;
    uint32_t idx;
    struct wren_mb_metadata md;
    char *e;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-abort-pulser IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser index (%s)\n", argv[1]);
	return -1;
    }

    cmd.arg1 = idx / WREN_PULSERS_PER_GROUP;
    cmd.arg2 = 1 << (idx & (WREN_PULSERS_PER_GROUP - 1));
    md.cmd = CMD_RX_ABORT_PULSERS;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, NULL, 0) < 0)
	return -1;
    return 1;
}

static int
do_rx_set_action (char *argv[], int argc)
{
    struct wren_mb_rx_set_action cmd;
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&cmd.conf);

    if (argc < 4) {
	fprintf(stderr,
		"usage: rx-set-action IDX COND-IDX PULSER-IDX [CONFIG]\n");
	help_pulser_config (&cmd.conf);
	return -1;
    }

    narg = 1;

    cmd.act_idx = strtoul(argv[narg], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for action index (%s)\n", argv[narg]);
	return -1;
    }
    narg++;

    cmd.cond_idx = strtoul(argv[narg], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for condition index (%s)\n", argv[narg]);
	return -1;
    }
    narg++;

    cmd.conf.pulser_idx = strtoul(argv[narg], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser index (%s)\n", argv[narg]);
	return -1;
    }
    narg++;

    while (narg < argc) {
	const char *opt = argv[narg];

	if (parse_pulser_config(&cmd.conf, opt, argv[narg + 1]) != 0)
	    return -1;

	narg += 2;
    }

    res = wrenctl_rx_set_action(wren_fd, &cmd);
    if (res != 0)
	printf("error (0x%x)\n", res);

    return narg;
}

#if 0
static int
do_rx_set_out_cfg (char *argv[], int argc)
{
    union {
	struct wren_mb_out_cfg cfg;
	uint32_t u32;
    } cmd;
    uint32_t rep;
    struct wren_mb_metadata md;
    char *e;

    if (argc < 3) {
	fprintf(stderr, "usage: rx-set-out-cfg OUT-IDX DRV [MASK INV_IN INV_OUT]\n");
	fprintf(stderr, " DRV is off, on, 50ohm\n");
	return -1;
    }

    cmd.cfg.out_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for output index (%s)\n", argv[1]);
	return -1;
    }

    if (!strcmp(argv[2], "off")) {
	cmd.cfg.oe = 0;
	cmd.cfg.term = 0;
	cmd.cfg.mask = 0;
	cmd.cfg.inv_in = 0;
	cmd.cfg.inv_out = 0;
    }
    else {
	if (argc < 6) {
	    fprintf(stderr, "missing arguments\n");
	    return -1;
	}

	cmd.cfg.oe = 1;
	if (!strcmp(argv[2], "on"))
	    cmd.cfg.term = 0;
	else if (!strcmp(argv[2], "50ohm"))
	    cmd.cfg.term = 1;
	else {
	    fprintf(stderr, "bad value for driver (%s)\n", argv[2]);
	    return -1;
	}

	cmd.cfg.mask = strtoul(argv[3], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for output index (%s)\n", argv[3]);
	    return -1;
	}

	cmd.cfg.inv_in = strtoul(argv[4], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for inv_in (%s)\n", argv[4]);
	    return -1;
	}

	cmd.cfg.inv_out = strtoul(argv[5], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for inv_out (%s)\n", argv[5]);
	    return -1;
	}
    }

    md.cmd = CMD_RX_SET_OUT_CFG;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &rep, sizeof(rep)) < 0)
	return -1;

    return argc;
}
#endif

static int
do_rx_set_out_cfg (char *argv[], int argc)
{
    struct wren_mb_out_cfg cmd;
    uint32_t rep;
    struct wren_mb_metadata md;
    char *e;

    if (argc != 5) {
	fprintf(stderr, "usage: rx-set-out-cfg OUT-IDX INV_IN MASK INV_OUT\n");
	return -1;
    }

    cmd.out_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for output index (%s)\n", argv[1]);
	return -1;
    }

    cmd.inv_in = strtoul(argv[2], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for inv_in (%s)\n", argv[2]);
	return -1;
    }

    cmd.mask = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for mask (%s)\n", argv[3]);
	return -1;
    }

    cmd.inv_out = strtoul(argv[4], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for inv_out (%s)\n", argv[4]);
	return -1;
    }

    md.cmd = CMD_RX_SET_OUT_CFG;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &rep, sizeof(rep)) < 0)
	return -1;

    return 5;
}

static int
get_out_cfg (unsigned idx, struct wren_mb_out_cfg *cfg)
{
    union {
	struct wren_mb_out_cfg cfg;
	uint32_t u32;
    } rep;
    struct wren_mb_metadata md;
    int len;


    md.cmd = CMD_RX_GET_OUT_CFG;
    md.len = sizeof(idx) / 4;

    len = mb_msg(&md, &idx, &rep, sizeof(rep));
    if (len != sizeof(rep) / 4)
	return -1;
    memcpy (cfg, &rep.cfg, sizeof (*cfg));
    return 0;
}

#if 0
static int
do_rx_get_out_cfg (char *argv[], int argc)
{
    union {
	struct wren_mb_out_cfg cfg;
	uint32_t u32;
    } rep;
    uint32_t idx;
    struct wren_mb_metadata md;
    char *e;

    if (argc == 1) {
#define NBR_OUT 8
	unsigned i;

	for (i = 0; i < NBR_OUT; i++) {
	    struct wren_mb_out_cfg cfg;

	    if (get_out_cfg(i, &cfg) != 0)
		continue;
	    printf ("%02u: ", i);
	    printf ("oe=%u, term=%u, mask=0x%02x, inv_in=%u, inv_out=%u\n",
		    cfg.oe, cfg.term, cfg.mask, cfg.inv_in, cfg.inv_out);
	}
	return 0;
    }
    else if (argc == 2) {
        idx = strtoul(argv[1], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for output index (%s)\n", argv[1]);
	    return -1;
	}

	md.cmd = CMD_RX_GET_OUT_CFG;
	md.len = sizeof(idx) / 4;

	if (mb_msg(&md, &idx, &rep, sizeof(rep)) < 0)
	    return -1;
	printf ("oe=%u, term=%u, mask=0x%02x, inv_in=%u, inv_out=%u\n",
		rep.cfg.oe, rep.cfg.term,
		rep.cfg.mask, rep.cfg.inv_in, rep.cfg.inv_out);
        return 1;
    }
    else {
	fprintf(stderr, "usage: rx-get-out-cfg OUT-IDX\n");
	return -1;
    }
}
#endif

static int
do_rx_get_out_cfg (char *argv[], int argc)
{
    union {
	struct wren_mb_out_cfg cfg;
	uint32_t u32;
    } rep;
    uint32_t idx;
    struct wren_mb_metadata md;
    char *e;

    if (argc == 1) {
#define NBR_OUT 8
	unsigned i;

	for (i = 0; i < NBR_OUT; i++) {
	    struct wren_mb_out_cfg cfg;

	    if (get_out_cfg(i, &cfg) != 0)
		continue;
	    printf ("%02u: ", i);
	    printf ("mask=0x%02x, inv_in=%u, inv_out=%u\n",
		    cfg.mask, cfg.inv_in, cfg.inv_out);
	}
	return 0;
    }
    else if (argc == 2) {
        idx = strtoul(argv[1], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for output index (%s)\n", argv[1]);
	    return -1;
	}

	md.cmd = CMD_RX_GET_OUT_CFG;
	md.len = sizeof(idx) / 4;

	if (mb_msg(&md, &idx, &rep, sizeof(rep)) < 0)
	    return -1;
	printf ("mask=0x%02x, inv_in=%u, inv_out=%u\n",
		rep.cfg.mask, rep.cfg.inv_in, rep.cfg.inv_out);
        return 1;
    }
    else {
	fprintf(stderr, "usage: rx-get-out-cfg OUT-IDX\n");
	return -1;
    }
}

static int
do_rx_set_source (char *argv[], int argc)
{
  struct wren_mb_rx_set_source cmd;
  int res;
  char *e;

  if (argc != 3) {
    fprintf(stderr, "usage: rx-set-source IDX SRC-ID\n");
    return -1;
  }

  cmd.idx = strtoul(argv[1], &e, 10);
  if (*e != 0) {
    fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
    return -1;
  }
  cmd.cfg.dest = RX_DEST_WREN;
  cmd.cfg.subsample = 0;

  if (parse_protocol(argv + 2, argc - 2, &cmd.cfg.proto) < 0)
      return -1;

  res = wrenctl_rx_set_source(wren_fd, &cmd);
  if (res != 0)
    printf("error (0x%x)\n", res);

  return 2;
}

static int
do_rx_del_source (char *argv[], int argc)
{
  struct wren_mb_rx_set_source cmd;
  int res;
  char *e;

  if (argc != 2) {
    fprintf(stderr, "usage: rx-del-source IDX\n");
    return -1;
  }

  memset(&cmd, 0, sizeof(cmd));

  cmd.idx = strtoul(argv[1], &e, 10);
  if (*e != 0) {
    fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
    return -1;
  }
  cmd.cfg.dest = RX_DEST_WREN;
  cmd.cfg.subsample = 0;
  cmd.cfg.proto.proto = WREN_PROTO_NONE;

  res = wrenctl_rx_set_source(wren_fd, &cmd);
  if (res != 0)
    printf("error (0x%x)\n", res);

  return 2;
}

static int
do_drv_add_source (char *argv[], int argc)
{
    struct wren_protocol proto;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: drv-add-source SRC-ID\n");
	return -1;
    }

    if (parse_protocol(argv + 1, argc - 1, &proto) < 0)
	return -1;

    res = ioctl(wren_fd, WREN_IOC_RX_ADD_SOURCE, &proto);
    if (res < 0)
	printf("error: %m\n");
    else
	printf("source id: %d\n", res);

    return 2;
}

static int
do_drv_del_source (char *argv[], int argc)
{
    uint32_t idx;
    char *e;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: drv-del-source SRC-IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    res = ioctl(wren_fd, WREN_IOC_RX_DEL_SOURCE, &idx);
    if (res < 0)
	printf("error: %m\n");

    return 2;
}

static int
disp_rx_action(const char *pfx, unsigned act_idx)
{
    struct wren_rx_action act;
    int len;
    uint32_t v;

    len = wrenctl_rx_get_action(wren_fd, act_idx, &act);
    if (len != 0)
	return len;

    printf ("%s%u: ", pfx, act_idx);
    printf ("pulser: %u, off-sec: %d, off-nsec: %d, cond-idx: %u, next: ",
	    act.conf.pulser_idx,
	    act.conf.load_off_sec, act.conf.load_off_nsec,
	    act.cond_idx);
    v = act.next;
    if (v == NO_RX_ACT_IDX)
	printf ("-\n");
    else
	printf ("%u\n", v);
    printf ("%s  start: %u, stop: %u, clock: %u, "
	    "width: %u, period: %u, npulses: %u, idelay: %u\n",
	    pfx,
	    WREN_MB_PULSER_GET_START(act.conf.inputs),
	    WREN_MB_PULSER_GET_STOP(act.conf.inputs),
	    WREN_MB_PULSER_GET_CLOCK(act.conf.inputs),
	    act.conf.width, act.conf.period,
	    act.conf.npulses, act.conf.idelay);
    printf ("%s  repeat: %u, immediat: %u, int_en: %u, out_en: %u\n",
	    pfx,
	    act.conf.flags & WREN_MB_PULSER_FLAG_REPEAT ? 1 : 0,
	    act.conf.flags & WREN_MB_PULSER_FLAG_IMMEDIAT ? 1 : 0,
	    act.conf.flags & WREN_MB_PULSER_FLAG_INT_EN ? 1 : 0,
	    act.conf.flags & WREN_MB_PULSER_FLAG_OUT_EN ? 1 : 0);
    return act.next;
}

static void
disp_rx_cond_ops(const union wren_rx_cond_word *ops, unsigned len)
{
    unsigned idx;
    for (idx = 0; idx < len;) {
	union wren_rx_cond_word op = ops[idx];

	if (op.op.op < WREN_OP_FIRST_BINARY) {
	    char orig_chr;
	    switch(op.op.param_orig) {
	    case WRENRX_PARAM_CONTEXT:
		orig_chr = 'c';
		break;
	    case WRENRX_PARAM_EVENT:
		orig_chr = 'e';
		break;
	    case WRENRX_PARAM_CONTEXT | WRENRX_PARAM_EVENT:
		orig_chr = 'a';
		break;
	    default:
		orig_chr = 'x';
		break;
	    }
	    printf (" %c%u", orig_chr, op.op.param_id);
	    switch (op.op.op) {
	    case WREN_OP_EQ:
		printf("==");
		break;
	    case WREN_OP_NE:
		printf("!=");
		break;
	    case WREN_OP_UGT:
	    case WREN_OP_SGT:
		printf(">");
		break;
	    case WREN_OP_UGE:
	    case WREN_OP_SGE:
		printf(">=");
		break;
	    case WREN_OP_ULT:
	    case WREN_OP_SLT:
		printf("<");
		break;
	    case WREN_OP_ULE:
	    case WREN_OP_SLE:
		printf("<=");
		break;
	    default:
		printf("??");
	    }
	    printf ("%u", ops[idx+1].vu32);

	    idx += 2;
	}
	else {
	    switch (op.op.op) {
	    case WREN_OP_AND:
		printf (" &");
		break;
	    case WREN_OP_OR:
		printf (" |");
		break;
	    case WREN_OP_NOT:
		printf (" !");
		break;
	    default:
		printf (" ?");
	    }
	    idx += 1;
	}
    }
}

static int
disp_cond(const struct wren_mb_cond *cond)
{
    if (cond->evt_id == WREN_EVENT_ID_INVALID) {
	printf ("-\n");
	return 0;
    }
    printf ("ev_id: %u, src_idx: %u, len: %u  ",
	    cond->evt_id, cond->src_idx, cond->len);
    if (cond->len > 0) {
	disp_rx_cond_ops(cond->ops, cond->len);
    }
    putchar ('\n');
    return 1;
}

static int
disp_rx_cond(const char *pfx, unsigned cond_idx, const char *act_pfx)
{
    struct wren_rx_cond cond;
    int len;

    len = wrenctl_rx_get_condition(wren_fd, cond_idx, &cond);
    if (len != 0)
	return -1;

    printf ("%s cond %u: ", pfx, cond_idx);
    if (disp_cond(&cond.cond) == 0)
	return NO_RX_COND_IDX;

    if (act_pfx != NULL) {
	uint16_t act_idx = cond.act_idx;
	while (act_idx != NO_RX_ACT_IDX) {
	    int res;
	    res = disp_rx_action(act_pfx, act_idx);
	    if (res == WRENRX_ERR_ACTIONID_TOO_LARGE)
		break;
	    if (res < 0) {
		fprintf(stderr, "rx-get-action error: %d\n", res);
		break;
	    }
	    act_idx = res;
	}
    }
    return cond.next;
}

static int
drv_disp_source(unsigned idx)
{
    struct wren_ioctl_get_source src;
    int res;

    src.source_idx = idx;
    res = ioctl(wren_fd, WREN_IOC_RX_GET_SOURCE, &src);
    if (res < 0) {
	if (errno == EINVAL)
	    return 0;
	fprintf (stderr, "ioctl error: %m\n");
	return -1;
    }
    printf ("%u: ", idx);
    disp_protocol(&src.proto);
    return 1;
}

static int
do_drv_get_source (char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	    return -1;
	}
	drv_disp_source(idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; idx < 32; idx++)
	    if (drv_disp_source(idx) == 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: drv-get-source [IDX]\n");
	return -1;
    }
}

static int
disp_rx_source(unsigned src_idx, unsigned flag_cond, unsigned flag_act)
{
    struct wren_mb_rx_get_source_reply src;
    int len;
    uint32_t cond_idx;

    len = wrenctl_rx_get_source(wren_fd, src_idx, &src);
    if (len != 0)
	return -1;

    printf ("%u: ", src_idx);
    disp_protocol(&src.cfg.proto);

    cond_idx = src.cond_idx;
    if (flag_cond) {
	while (cond_idx != NO_RX_COND_IDX) {
	    int res;

	    res = disp_rx_cond("  ", cond_idx, flag_act ? "    " : NULL);
	    if (res < 0)
		break;
	    cond_idx = res;
	}
    }
    else {
	if (cond_idx != NO_RX_COND_IDX) {
	    printf ("  cond: %u\n", cond_idx);
	}
    }
    return 0;
}

static int
do_rx_get_source (char *argv[], int argc)
{
    unsigned idx;
    unsigned flag_cond = 0;
    unsigned flag_act = 0;
    unsigned opt;

    for (opt = 1; opt < argc && argv[opt][0] == '-'; opt++) {
	if (!strcmp(argv[opt], "-a")) {
	    flag_act = 1;
	    flag_cond = 1;
	} else if (!strcmp(argv[opt], "-c"))
	    flag_cond = 1;
	else
	    goto usage;
    }


    if (opt == argc) {
	for (idx = 0; ; idx++)
	    if (disp_rx_source(idx, flag_cond, flag_act) < 0)
		break;
	return 1;
    }
    else if (opt == argc - 1) {
	char *e;
	idx = strtoul(argv[opt], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for idx (%s)\n", argv[opt]);
	    return -1;
	}

	disp_rx_source(idx, flag_cond, flag_act);
	return 2;
    }

  usage:
    fprintf(stderr, "usage: rx-get-source [IDX]\n");
    return -1;
}

static int
do_rx_get_cond (char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	    return -1;
	}

	disp_rx_cond("", idx, NULL);
	return 2;
    }
    else {
	fprintf(stderr, "usage: rx-get-cond IDX\n");
	return -1;
    }
}

static int
do_rx_del_cond (char *argv[], int argc)
{
    uint32_t idx;

    if (argc == 2) {
	struct wren_mb_metadata md;
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for condition idx (%s)\n", argv[1]);
	    return -1;
	}
	md.cmd = CMD_RX_DEL_COND;
	md.len = sizeof(idx) / 4;

	if (mb_msg(&md, &idx, NULL, 0) < 0)
	    return -1;
	return 2;
    } else {
	fprintf(stderr, "usage: rx-del-cond IDX\n");
	return -1;
    }
}

static int
do_rx_get_action (char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	int res;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	    return -1;
	}

	res = disp_rx_action("", idx);
	if (res < 0)
	    fprintf(stderr, "rx-get-action error: %d\n", res);
	return 2;
    }
    else {
	for (idx = 0; ; idx++) {
	    int res;

	    res = disp_rx_action("", idx);
	    if (res == WRENRX_ERR_ACTION_UNUSED)
		continue;
	    if (res == WRENRX_ERR_ACTIONID_TOO_LARGE)
		break;
	    if (res < 0)
		fprintf(stderr, "rx-get-action error: %d\n", res);
	}
	return 2;
    }
}

static int
do_rx_del_action (char *argv[], int argc)
{
    char *e;
    unsigned idx;
    int res;

    if (argc != 2) {
	fprintf(stderr, "usage: rx-del-action IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for idx (%s)\n", argv[1]);
	return -1;
    }

    res = wrenctl_rx_del_action(wren_fd, idx);
    if (res < 0)
      fprintf(stderr, "failed to delete action: %d\n", res);

    return 2;
}

static int
do_rx_log_index (char *argv[], int argc)
{
    uint32_t idx;
    uint32_t res;
    char *e;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-log-index IDX\n");
	return -1;
    }

    idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for log index (%s)\n", argv[1]);
	return -1;
    }

    if (wrenctl_rx_pulser_log_index(wren_fd, idx, &res) < 0) {
	fprintf(stderr, "cannot get log entry\n");
	return -1;
    }
    printf("index: %08x\n", (unsigned)res);

    return 1;
}

static int
do_rx_log (char *argv[], int argc)
{
    struct wren_mb_rx_get_log cmd;
    uint32_t rep[128];
    struct wren_mb_metadata md;
    char *e;
    unsigned i;
    struct timespec ts;
    int narg;
    unsigned nentries;
    uint32_t idx;
    unsigned ioff;

    if (argc < 2) {
	fprintf(stderr, "usage: rx-log IDX [NBR] [OFF]\n");
	return -1;
    }

    cmd.log_idx = strtoul(argv[1], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for log index (%s)\n", argv[1]);
	return -1;
    }
    narg = 1;

    if (argc >= 3) {
	nentries = strtoul(argv[2], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for nbr entries (%s)\n", argv[2]);
	    return -1;
	}
	narg++;
    }
    else
	nentries = 16;

    if (argc >= 4) {
	idx = strtoul(argv[3], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for entry index (%s)\n", argv[3]);
	    return -1;
	}
	narg++;
    }
    else {
	if (wrenctl_rx_pulser_log_index(wren_fd, cmd.log_idx, &idx) < 0) {
	    fprintf(stderr, "cannot get log entry\n");
	    return -1;
	}
    }

    printf ("Index: %08x (num: %06x)\n",
	    idx, (idx & 0x00ffffff) >> 2);

    /* Clear overflow bit */
    cmd.entry_idx = idx & ~3U;

    if (wrenctl_get_wr_time(wren_fd, &ts) == 0)
	disp_time ("now is", ts.tv_sec, ts.tv_nsec);

    ioff = 0;
    while (nentries > 0) {
	unsigned num;
	unsigned low = (cmd.entry_idx & 0xffffff) >> 2;
	num = nentries;

	if (num > sizeof(rep) / 4)
	    num = sizeof(rep) / 4;
	if (low > 0 && num > low)
	    num = low;
	cmd.nentries = num;
	md.cmd = CMD_RX_LOG_READ;
	md.len = sizeof(cmd) / 4;

	if (mb_msg(&md, &cmd, rep, num * 4) < 0)
	    return -1;

	cmd.entry_idx -= num * 4;
	for (i = 0; i < num; i++) {
	    unsigned v = rep[i];
	    unsigned us = (v >> 4) & 0xfffff;

	    if ((idx & 0x00ffffff) == 0)
		idx += 0x01000000;
	    idx -= 4;
	    printf("[%-3u]: @%08x %08x ", ioff + i, idx, v);
	    if (us >= 1000000)
		printf ("-\n");
	    else
		printf("now-%03us + %06uus %s %s %s %s\n",
		       (unsigned)((ts.tv_sec - (v >> 24)) & 0xff), us,
		       v & 1 ? "Load" : "    ",
		       v & 2 ? "Start" : "     ",
		       v & 4 ? "Pulse" :  "     ",
		       v & 8 ? "Idle" : "    ");
	}

	ioff += num;
	nentries -= num;
    }

    return narg;
}

static void
disp_evlog_recv(union wren_evlog_entry *e)
{
    struct wren_packet_hdr_v1 hdr;
    unsigned len = e->e.len;
    unsigned off;
    unsigned l;

    printf ("RECV ts=%08x %08x, src=%02x\n", e->w[1], e->w[2], e->e.d[8]);
    printf ("         ");
    off = 9;
    for (l = 0; l < 14; l++) {
	if (l == 0)
	    printf("D=");
	else if (l == 6)
	    printf(" S=");
	else if (l == 12)
	    printf(" T=");
	else if (l < 14)
	    printf(":");
	else
	    printf(" ");
	printf ("%02x", e->e.d[off+l]);
    }

    off += 14;
    if (len - off < sizeof(struct wren_packet_hdr_v1)) {
	printf(" [trunc]\n");
	return;
    }
    memcpy(&hdr, &e->e.d[off], sizeof (struct wren_packet_hdr_v1));
    printf("\n"
	   "         ");
    printf("v:%02x dom:%02x tx:%02x len:%04x seq:%04x ts:%08x\n",
	   hdr.version, hdr.domain, hdr.xmit,
	   hdr.len, hdr.seq_id, hdr.snd_ts);
}

static void
dump_evlog(union wren_evlog_entry *e, unsigned len)
{
    unsigned l;

    if (len >= WREN_EVLOG_DSIZE)
	len = WREN_EVLOG_DSIZE;
    for (l = 0; l < len; l++)
	printf (" %02x", e->e.d[l]);
    printf("\n");
}

static int
do_sw_log (char *argv[], int argc)
{
    struct wren_mb_arg1 cmd;
    union {
	struct wren_mb_arg_reply r;
	uint32_t u32;
    } rep;
    struct wren_mb_metadata md;
    unsigned num, idx, max;
    unsigned i;
    unsigned dlen;

    if (argc > 2) {
	fprintf(stderr, "usage: sw-log [NBR]\n");
	return -1;
    }

    if (argc == 2) {
	char *e;

	num = strtoul(argv[1], &e, 0);
	if (*e != 0) {
	    fprintf(stderr, "bad value for NBR (%s)\n", argv[1]);
	    return -1;
	}
    }
    else
	num = 10;

    md.cmd = CMD_EVLOG_INDEX;
    md.len = 0;

    if (mb_msg(&md, NULL, &rep.u32, sizeof(rep)) < 0)
	return -1;
    idx = rep.r.arg1;
    max = rep.r.arg2;
    printf("next: %u\n", idx);
    printf("nbr:  %u\n", max);

    if (num > max)
	num = max;

    idx -= num;

    dlen = 0;

    for (i = 0; i < num; i++) {
	union wren_evlog_entry e;

	cmd.arg1 = idx;

	md.cmd = CMD_EVLOG_READ;
	md.len = sizeof(cmd) / 4;

	if (mb_msg(&md, &cmd, &e.w, sizeof(e)) < 0)
	    return -1;

	printf("[%6u] %02x %u %3u ", idx, e.e.type, e.e.nentries, e.e.len);

	/* Data len (handle continuation) */
	if (e.e.type != EVLOG_TYPE_CONT)
	    dlen = e.e.len;
	else
	    if (dlen > WREN_EVLOG_DSIZE)
		dlen -= WREN_EVLOG_DSIZE;
	    else
		dlen = 0;

	switch(e.e.type) {
	case EVLOG_TYPE_CONT:
	    printf("CONT ");
	    dump_evlog(&e, dlen);
	    break;
	case EVLOG_TYPE_RECV:
	    disp_evlog_recv(&e);
	    break;
	default:
	    dump_evlog(&e, dlen);
	    break;
	}

	idx++;
    }

    return 1;
}

static int
do_tx_send_pkt (char *argv[], int argc)
{
  struct {
    uint32_t src;
    struct wren_capsule_event_hdr evt;
  } d;
  struct wren_mb_metadata md;
  uint32_t rep;
  char *e;

  if (argc != 2) {
    fprintf(stderr, "usage: send-pkt SRC\n");
    return -1;
  }

  d.src = strtoul(argv[1], &e, 10);
  if (*e != 0) {
    fprintf(stderr, "bad value for SRC (%s)\n", argv[1]);
    return -1;
  }

  d.evt.hdr.typ = PKT_EVENT;
  d.evt.hdr.pad = 0;
  d.evt.hdr.len = sizeof(struct wren_capsule_event_hdr) / 4;

  d.evt.ev_id = 123;
  d.evt.ctxt_id = WREN_CONTEXT_NONE;
  d.evt.ts.sec = 0;
  d.evt.ts.nsec = 0;

  md.cmd = CMD_TX_SEND_PACKET;
  md.len = sizeof(d) / 4;

  if (mb_msg(&md, &d, &rep, sizeof(rep)) < 0)
    return -1;
  return 1;
}

static unsigned
wrenrx_cond_to_words(struct wrenrx_cond_tree *t,
		     union wren_rx_cond_word *data)
{
    switch(t->kind) {
    case T_BINARY: {
	unsigned len;
	struct wren_rx_cond_op op;

	len = wrenrx_cond_to_words(t->u.binary.l, data);
	len += wrenrx_cond_to_words(t->u.binary.r, data + len);

	switch(t->u.binary.op) {
	case TOK_AND:
	    op.op = WREN_OP_AND;
	    break;
	case TOK_OR:
	    op.op = WREN_OP_OR;
	    break;
	default:
	    abort();
	}
	op.param_id = 0;
	op.param_orig = 0;

	data[len++].op = op;
	return len;
    }
    case T_PARAM: {
	struct wren_rx_cond_op op;
	unsigned orig;

	switch(t->u.param.op) {
	case TOK_EQ:
	    op.op = WREN_OP_EQ;
	    break;
	default:
	    abort();
	}
	switch (t->u.param.orig) {
	case TOK_CTXT_PARAM:
	    orig = WRENRX_PARAM_CONTEXT;
	    break;
	case TOK_EVT_PARAM:
	    orig = WRENRX_PARAM_EVENT;
	    break;
	case TOK_ANY_PARAM:
	    orig = WRENRX_PARAM_CONTEXT | WRENRX_PARAM_EVENT;
	    break;
	default:
	    abort();
	}

	op.param_id = t->u.param.id;
	op.param_orig = orig;
	data[0].op = op;
	data[1].vu32 = t->u.param.val;
	return 2;
    }
    default:
	abort();
    }
}

static int
do_drv_add_config (char *argv[], int argc)
{
    struct wren_ioctl_config cmd;
    int res;
    char *e;
    struct wrenrx_cond_tree *expr;
    unsigned narg;

    init_pulser_config (&cmd.config);

    /* Default: 1 sec */
    cmd.config.load_off_sec = 1;

    if (argc < 4) {
	fprintf(stderr, "usage: drv-add-config PULSER-IDX SRC-IDX EVT-ID "
		"[-name NAME] [CONFIG] [COND]\n");
	help_pulser_config (&cmd.config);
	return -1;
    }

    cmd.config.pulser_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    cmd.cond.src_idx = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for SRC-IDX (%s)\n", argv[2]);
	return -1;
    }

    cmd.cond.evt_id = strtoul(argv[3], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for EVT-ID (%s)\n", argv[3]);
	return -1;
    }

    cmd.cond.len = 0;
    narg = 4;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (cmd.name, argv[narg + 1], sizeof(cmd.name));
	narg += 2;
    }
    else
	memset(cmd.name, 0, sizeof(cmd.name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&cmd.config, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    if (narg < argc) {
	expr = wrenrx_cond_parse_expr(argv[narg]);
	if (expr == NULL)
	    return -1;

	cmd.cond.len = wrenrx_cond_to_words(expr, cmd.cond.ops);

	narg++;
    }

    res = ioctl(wren_fd, WREN_IOC_RX_ADD_CONFIG, &cmd);
    if (res < 0)
	printf("error: %m\n");
    else
	printf("config-id: %u\n", res);

    return narg;
}

static int
do_drv_imm_config (char *argv[], int argc)
{
    struct wren_ioctl_imm_config cmd;
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&cmd.config);

    /* Default: 1 sec */
    cmd.config.load_off_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: drv-imm-config PULSER-IDX "
		"[-name NAME] [CONFIG]\n");
	help_pulser_config (&cmd.config);
	return -1;
    }

    cmd.config.pulser_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for pulser IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;

    if (narg + 1 < argc && !strcmp(argv[narg], "-name")) {
	strncpy (cmd.name, argv[narg + 1], sizeof(cmd.name));
	narg += 2;
    }
    else
	memset(cmd.name, 0, sizeof(cmd.name));

    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&cmd.config, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    /* Adjust time */
    {
        struct wren_wr_time res;

	if (ioctl(wren_fd, WREN_IOC_GET_TIME, &res) < 0) {
	    fprintf(stderr, "ioctl get_time: %m\n");
	    return -1;
	}

	cmd.ts.sec = res.tai_lo + cmd.config.load_off_sec;
	cmd.ts.nsec = res.ns + cmd.config.load_off_nsec;
	if (cmd.ts.nsec >= 1000000000) {
	    cmd.ts.nsec -= 1000000000;
	    cmd.ts.sec += 1;
	}
    }

    res = ioctl(wren_fd, WREN_IOC_RX_IMM_CONFIG, &cmd);
    if (res < 0)
	printf("error: %m\n");
    else
	printf("config-id: %u\n", res);

    return narg;
}

static int
do_drv_mod_config (char *argv[], int argc)
{
    struct wren_ioctl_mod_config cmd;
    int res;
    char *e;
    unsigned narg;

    init_pulser_config (&cmd.config);

    /* Default: 1 sec */
    cmd.config.load_off_sec = 1;

    if (argc < 2) {
	fprintf(stderr, "usage: drv-mod-config CONFIG-IDX [CONFIG]\n");
	help_pulser_config (&cmd.config);
	return -1;
    }

    cmd.config_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for config IDX (%s)\n", argv[1]);
	return -1;
    }

    narg = 2;
    while (narg + 1 < argc && argv[narg][0] == '-') {
	const char *opt = argv[narg];
	const char *arg = argv[narg + 1];

	if (parse_pulser_config(&cmd.config, opt, arg) != 0)
	    return -1;

	narg += 2;
    }

    res = ioctl(wren_fd, WREN_IOC_RX_MOD_CONFIG, &cmd);
    if (res < 0)
	printf("error: %m\n");

    return narg;
}

static int
drv_disp_config(unsigned idx)
{
    struct wren_ioctl_get_config cfg;
    int res;

    cfg.config_idx = idx;
    res = ioctl(wren_fd, WREN_IOC_RX_GET_CONFIG, &cfg);
    if (res < 0) {
	if (errno == EINVAL)
	    return 0;
	if (errno != ENOENT)
	    fprintf (stderr, "ioctl error: %m\n");
	return -1;
    }
    printf ("%u: name: %s, ", idx, cfg.config.name);
    disp_cond (&cfg.config.cond);

    return 1;
}

static int
do_drv_get_config (char *argv[], int argc)
{
    unsigned idx;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	drv_disp_config(idx);
	return 2;
    } else if (argc == 1) {
	for (idx = 0; ; idx++)
	    if (drv_disp_config(idx) == 0)
		break;
	return 1;
    } else {
	fprintf(stderr, "usage: drv-get-config [IDX]\n");
	return -1;
    }
}

static int
do_drv_del_config (char *argv[], int argc)
{
    uint32_t idx;
    int res;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	res = ioctl(wren_fd, WREN_IOC_RX_DEL_CONFIG, &idx);
	if (res < 0) {
	    fprintf(stderr, "ioctl error: %m\n");
	    return -1;
	}
	return 2;
    } else {
	fprintf(stderr, "usage: drv-del-config IDX\n");
	return -1;
    }
}

static int
do_drv_subscribe_config (char *argv[], int argc)
{
    uint32_t idx;
    int res;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	res = ioctl(wren_fd, WREN_IOC_RX_SUBSCRIBE_CONFIG, &idx);
	if (res < 0) {
	    fprintf(stderr, "ioctl error: %m\n");
	    return -1;
	}
	return 2;
    } else {
	fprintf(stderr, "usage: drv-subscribe-config IDX\n");
	return -1;
    }
}

static int
do_drv_unsubscribe_config (char *argv[], int argc)
{
    uint32_t idx;
    int res;

    if (argc == 2) {
	char *e;
	idx = strtoul(argv[1], &e, 10);
	if (*e != 0) {
	    fprintf(stderr, "bad value for config idx (%s)\n", argv[1]);
	    return -1;
	}
	res = ioctl(wren_fd, WREN_IOC_RX_UNSUBSCRIBE_CONFIG, &idx);
	if (res < 0) {
	    fprintf(stderr, "ioctl error: %m\n");
	    return -1;
	}
	return 2;
    } else {
	fprintf(stderr, "usage: drv-unsubscribe-config IDX\n");
	return -1;
    }
}

static int
do_rx_set_cond (char *argv[], int argc)
{
    struct wren_mb_rx_set_cond cmd;
    int res;
    char *e;
    struct wrenrx_cond_tree *expr;
    unsigned narg;

    if (argc < 4) {
	fprintf(stderr, "usage: rx-set-cond IDX SRC-ID EVT-ID [COND]\n");
	return -1;
    }

    cmd.cond_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for IDX (%s)\n", argv[1]);
	return -1;
    }

    cmd.cond.src_idx = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for SRC-IDX (%s)\n", argv[2]);
	return -1;
    }

    cmd.cond.evt_id = strtoul(argv[3], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for EVT-ID (%s)\n", argv[3]);
	return -1;
    }

    if (argc > 4) {
	narg = 4;

	expr = wrenrx_cond_parse_expr(argv[4]);
	if (expr == NULL)
	    return -1;

	cmd.cond.len = wrenrx_cond_to_words(expr, cmd.cond.ops);
    }
    else {
	cmd.cond.len = 0;
	narg = 3;
    }

    res = wrenctl_rx_set_cond(wren_fd, &cmd);

    if (res != 0)
	printf("rx-set-cond error: %d\n", res);

    return narg + 1;
}

static int
do_rx_subscribe_unsubscribe (char *argv[], int argc, unsigned cmd)
{
    struct wren_mb_rx_subscribe msg;
    uint32_t rep;
    char *e;

    msg.src_idx = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for source idx (%s)\n", argv[1]);
	return -1;
    }
    msg.ev_id = strtoul(argv[2], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for ev-id (%s)\n", argv[2]);
	return -1;
    }

    if (wrenctl_msg(wren_fd, cmd, &msg, sizeof(msg), &rep, sizeof(rep)) < 0)
	return -1;
    return 2;
}

static int
do_rx_subscribe (char *argv[], int argc)
{
    if (argc != 3) {
	fprintf(stderr, "usage: rx-subscribe SRC-IDX EV-ID\n");
	return -1;
    }

    return do_rx_subscribe_unsubscribe(argv, argc, CMD_RX_SUBSCRIBE);
}

static int
do_rx_unsubscribe (char *argv[], int argc)
{
    if (argc != 3) {
	fprintf(stderr, "usage: rx-subscribe SRC-IDX EV-ID\n");
	return -1;
    }

    return do_rx_subscribe_unsubscribe(argv, argc, CMD_RX_UNSUBSCRIBE);
}

static int
do_rx_get_subscribed (char *argv[], int argc)
{
    struct wren_mb_arg1 cmd;
    struct wren_mb_get_subscribed_reply rep;
    char *e;
    unsigned i, j;

    if (argc != 2) {
	fprintf(stderr, "usage: rx-get-subscribed SRC-IDX\n");
	return -1;
    }

    cmd.arg1 = strtoul(argv[1], &e, 10);
    if (*e != 0) {
	fprintf(stderr, "bad value for source idx (%s)\n", argv[1]);
	return -1;
    }

    if (wrenctl_msg(wren_fd, CMD_RX_GET_SUBSCRIBED,
		    &cmd, sizeof(cmd), &rep, sizeof(rep)) < 0)
	return -1;
    printf ("events:");
    for (i = 0; i < 32; i++) {
	if (rep.map[i] == 0)
	    continue;
	for (j = 0; j < 32; j++)
	    if (rep.map[i] & (1 << j))
		printf (" %u", i * 32 + j);
    }
    printf ("\n");
    return 1;
}

static int
do_cli (char *argv[], int argc)
{
    union {
	char buf[128];
	uint32_t w[128 / 4];
    } u;
    int len;
    unsigned narg;
    uint32_t rep;
    struct wren_mb_metadata md;

    if (argc < 2) {
	fprintf(stderr, "usage: cli CMD [ARGS]\n");
	return -1;
    }

    len = 0;
    for (narg = 1; narg < argc; narg++) {
	size_t alen = strlen(argv[narg]);

	if (alen == 1 && argv[narg][0] == ';') {
	    break;
	}

	if (len + 1 + alen > sizeof(u.buf)) {
	    fprintf(stderr, "command is too long (%u bytes)\n",
		    (unsigned)(len + alen + 1));
	    return -1;
	}
	if (len != 0)
	    u.buf[len++] = ' ';
	memcpy(u.buf + len, argv[narg], alen);
	len += alen;
    }

    while (len & 3)
	u.buf[len++] = 0;

    md.cmd = CMD_CLI;
    md.len = len / 4;

    if (mb_msg(&md, u.w, &rep, sizeof(rep)) < 0)
	return -1;
    return narg;
}

static int
do_tty_log (char *argv[], int argc)
{
    struct wren_mb_arg1 cmd;
    struct wren_mb_tty_log rep;
    struct wren_mb_metadata md;

    if (argc != 1) {
	fprintf(stderr, "usage: tty-log\n");
	return -1;
    }

    cmd.arg1 = 0x800;
    md.cmd = CMD_TTY_LOG;
    md.len = sizeof(cmd) / 4;

    if (mb_msg(&md, &cmd, &rep, sizeof(rep)) < 0)
	return -1;
    printf("len: %u\n", (unsigned)rep.len);
    fwrite(rep.u.c, 1, rep.len, stdout);
    putchar('\n');
    return 0;
}

static int
do_drv_read (char *argv[], int argc)
{
    uint32_t buf[512];
    ssize_t len;
    unsigned i;

    while (1) {
	len = read(wren_fd, buf, sizeof(buf));
	if (len < 0) {
	    fprintf(stderr, "read error: %m\n");
	    return -1;
	}
	if (len == 0)
	    return 1;
	printf ("len: %u\n", (unsigned)len);
	for (i = 0; i < len; i += 16) {
	    unsigned j;
	    printf ("%04x:", i);
	    for (j = i; j < i + 16 && j < len; j += 4)
		printf(" %08x", buf[j / 4]);
	    printf("\n");
	}
    }
}

static int do_help(char *argv[], int argc);

struct command_t {
  const char *name;
  /* Return the number of args consumed, argv[0] is the command name.  */
  int (*func)(char *argv[], int argc);
  const char *help;
};

static const struct command_t commands[] = {
  {"ping", do_ping, "ping the board" },
  {"link", do_link, "disp wr link status" },
  {"time", do_time, "disp wr time" },
  {"wait", do_wait, "wait for a delay" },
  {"read", do_read, "read memory" },
  {"write", do_write, "write memory" },

  {"hw-config", do_hw_get_config, "get wren hw config" },

  {"drv-log-flags", do_drv_log_flags, "get/set driver log flags" },

  {"tx-get-config", do_tx_get_config, "get wren tx config" },
  {"tx-set-source", do_tx_set_source, "set a tx source" },
  {"tx-get-source", do_tx_get_source, "get a tx source" },
  {"tx-send-pkt", do_tx_send_pkt, "send a packet" },

  {"tx-get-table", do_tx_get_table, "get a tx table" },
  {"tx-del-table", do_tx_del_table, "delete a tx table" },
  {"tx-play-table", do_tx_play_table, "play a tx table" },

  {"drv-add-source", do_drv_add_source, "add a new rx source" },
  {"drv-get-source", do_drv_get_source, "get an rx source (from driver)" },
  {"drv-del-source", do_drv_del_source, "delete an rx source" },

  {"drv-add-config", do_drv_add_config, "add a new rx config" },
  {"drv-imm-config", do_drv_imm_config, "add an immediat config" },
  {"drv-get-config", do_drv_get_config, "get an rx config" },
  {"drv-del-config", do_drv_del_config, "delete an rx config" },
  {"drv-mod-config", do_drv_mod_config, "modify an rx config" },
  {"drv-subscribe-config", do_drv_subscribe_config, "subscribe to a config" },
  {"drv-unsubscribe-config", do_drv_unsubscribe_config, "unsubscribe to a config" },

  {"rx-get-config", do_rx_get_config,
   "get wren rx config (version, nbr sources...)" },
  {"rx-set-source", do_rx_set_source, "set an rx source" },
  {"rx-get-source", do_rx_get_source, "get an rx source" },
  {"rx-del-source", do_rx_del_source, "suppress a source" },
  {"rx-set-cond", do_rx_set_cond, "set an rx condition entry" },
  {"rx-get-cond", do_rx_get_cond, "get an rx condition" },
  {"rx-del-cond", do_rx_del_cond, "suppress a condition" },
  {"rx-set-action", do_rx_set_action, "set action" },
  {"rx-get-action", do_rx_get_action, "get action" },
  {"rx-del-action", do_rx_del_action, "delete action" },
  {"rx-set-out-cfg", do_rx_set_out_cfg, "set output config" },
  {"rx-get-out-cfg", do_rx_get_out_cfg, "get output config" },

  {"rx-set-comparator", do_rx_set_comparator,
   "set a comparator (to program a pulser)" },
  {"rx-get-comparator", do_rx_get_comparator, "get a comparator" },
  {"rx-abort-comparator", do_rx_abort_comparator, "abort a comparator" },
  {"rx-pulser-status", do_rx_pulser_status, "get pulsers status" },
  {"rx-abort-pulser", do_rx_abort_pulser, "abort a pulser" },
  {"rx-get-pulser", do_rx_get_pulser, "get pulser state" },
  {"rx-subscribe", do_rx_subscribe, "subscribe to an event" },
  {"rx-unsubscribe", do_rx_unsubscribe, "unsubscribe to an event" },
  {"rx-get-subscribed", do_rx_get_subscribed, "get subscribed events" },
  {"rx-log-index", do_rx_log_index, "get log address for pulser/inputs" },
  {"rx-log", do_rx_log, "display logs for a pulser or an input" },

  {"tty-log", do_tty_log, "display tty output" },
  {"sw-log", do_sw_log, "display event log" },

  {"tx", do_tx, "send a packet" },
  {"drv-read", do_drv_read, "wait for data" },
  {"cli", do_cli, "execute a command" },
  {"help", do_help, "print this help" },
  {NULL, NULL, NULL}
};

static int do_help(char *argv[], int argc)
{
  unsigned i;

  for (i = 0; commands[i].name; i++)
    printf ("%-10s  %s\n", commands[i].name, commands[i].help);

  return 0;
}

static void
usage(void)
{
  printf ("usage: %s -d /dev/wrenX\n", progname);
}

int
main(int argc, char **argv)
{
  char *drv;
  int i;
  unsigned optind;

  progname = argv[0];
  drv = NULL;
  verbose = 0;

  /* Simple option decoding. */
  for (i = 1; i < argc; i++) {
    if (argv[i][0] != '-')
      break;
    if (strcmp (argv[i], "-d") == 0 && i < argc + 1) {
      drv = argv[i + 1];
      i++;
    }
    else if (strcmp (argv[i], "-v") == 0)
      verbose++;
    else {
      usage();
      return 2;
    }
  }
  optind = i;

  if (drv == NULL) {
    usage();
    return 2;
  }

  wren_fd = wrenctl_open(drv);
  if (wren_fd < 0) {
      fprintf (stderr, "cannot open %s: %m\n", drv);
      return 2;
  }

  if (wren_check_version(wren_fd) != 0) {
      return 3;
  }

  for (i = optind; i < argc; i++) {
      const char *argv0 = argv[i];
      const struct command_t *cmd;
      unsigned k;

      for (cmd = commands; cmd->name; cmd++)
	if (strcmp (argv0, cmd->name) == 0)
	  break;

      if (cmd->name == NULL) {
	fprintf(stderr, "unknown command %s, try help\n", argv0);
	exit (1);
      }
      else {
	  for (k = i; k < argc; k++)
	      if (argv[k][0] == ',')
		  break;

	  int res = cmd->func (argv + i, k - i);
	  if (res < 0) {
	      fprintf(stderr, "error in command %s\n", argv0);
	      exit (1);
	  }
	  i += res;
      }
  }

  return 0;
}
