void wrentx_init(void);
void wrentx_table_poll(void);

void cmd_tx_get_config (uint32_t *data, uint32_t len);
void cmd_tx_set_source (uint32_t *data, uint32_t len);
void cmd_tx_get_source (uint32_t *data, uint32_t len);
void cmd_tx_send_packet (uint32_t *data, uint32_t len);
void cmd_tx_set_table (uint32_t *data, uint32_t len);
void cmd_tx_get_table (uint32_t *data, uint32_t len);
void cmd_tx_del_table (uint32_t *data, uint32_t len);
void cmd_tx_play_table (uint32_t *data, uint32_t len);
