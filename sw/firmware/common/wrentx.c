#include <stdint.h>
#include <stddef.h>
#include <string.h>
#include "lib.h"
#include "trace.h"
#include "wren.h"
#include "wrentx-data.h"
#include "wren-hw.h"
#include "wren-mb-defs.h"
#include "nic.h"
#include "wren/wren-packet.h"
#include "board_map.h"
#include "wrentx.h"
#include "versions.h"
#include "wren/wrentx-table.h"

#define WRENTX_NBR_TABLES 16

struct wren_tx_source wren_tx_sources[MAX_TX_SOURCES];

#define TABLE_STATE_UNUSED 0
#define TABLE_STATE_LOADED 1
#define TABLE_STATE_RUN    2

#define NO_TABLE_IDX 0xffff

#define NO_TIMEOUT 0xffffffff

struct wren_tx_table {
    uint8_t pc;
    uint8_t state;
    uint8_t src_idx;
    uint8_t nbr_insns;

    uint16_t nbr_data;

    /* Double linked list of running table */
    uint16_t link_prev;
    uint16_t link_next;

    uint32_t count;
    struct wren_packet_ts due_ts;
    struct wren_packet_ts exec_ts;

    uint32_t repeat;
    uint32_t data[WRENTX_MAX_INSNS + WRENTX_MAX_DATA];
};

static struct wren_tx_table wren_tx_tables[WRENTX_NBR_TABLES];

static uint16_t table_exec_list;
static struct wren_packet_ts table_timeout;

static void
wren_packet_ts_add(struct wren_packet_ts *res, unsigned nsec, unsigned sec)
{
    res->nsec += nsec;
    if (res->nsec > 1000000000) {
	res->nsec -= 1000000000;
	res->sec++;
    }
    res->sec += sec;
}

static int
wren_packet_ts_lte(const struct wren_packet_ts *l, const struct wren_packet_ts *r)
{
    if (l->sec < r->sec)
	return 1;
    if (l->sec == r->sec)
	return l->nsec <= r->nsec;
    return 0;
}

void
get_now_packet_ts(struct wren_packet_ts *now)
{
  volatile struct board_map *regs =
    (volatile struct board_map *)BOARD_MAP_BASE;
  unsigned lo;

  lo = regs->tm_tai_lo;

  do {
    now->sec = lo;
    now->nsec = regs->tm_cycles << 4;
    lo = regs->tm_tai_lo;
  } while (lo != now->sec);
}

void
cmd_tx_get_config (uint32_t *data, uint32_t len)
{
  struct wren_mb_tx_get_config_reply *res;
  unsigned i;
  unsigned nbr;

  res = (struct wren_mb_tx_get_config_reply *)mb_get_reply
    (CMD_TX_GET_CONFIG | CMD_REPLY, sizeof(*res) / 4);
  res->sw_version = WREN_FW_VERSION;
  res->max_sources = MAX_TX_SOURCES;

  nbr = 0;
  for (i = 0; i < MAX_TX_SOURCES; i++)
    if (wren_tx_sources[i].proto.proto != WREN_PROTO_NONE)
      nbr++;
  res->nbr_sources = nbr;

  mb_send_reply();
}

void
cmd_tx_set_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_set_source *arg = (struct wren_mb_tx_set_source *)data;
    uint32_t source_idx = arg->source_idx;
    struct wren_tx_source *src;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    if (arg->proto.proto != WREN_PROTO_ETHERNET) {
	mb_reply_error(WRENTX_ERR_BAD_PROTOCOL);
	return;
    }

    src = &wren_tx_sources[source_idx];
    src->proto = arg->proto;
    src->max_delay_us_p2 = arg->max_delay_us_p2;
    src->seq_id = 0;

    mb_reply_ok();
}

void
cmd_tx_get_source (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *arg = (struct wren_mb_arg1 *)data;
    uint32_t source_idx = arg->arg1;
    struct wren_tx_source *src;
    struct wren_protocol *res;

    if (source_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    src = &wren_tx_sources[source_idx];

    res = (struct wren_protocol *)mb_get_reply (CMD_REPLY, sizeof(*res) / 4);

    w32cpy (res, &src->proto, sizeof(src->proto) / 4);
    //    *res = src->proto;

    mb_send_reply();
}

void
w32cpy(void *dst, void *src, unsigned wlen)
{
  volatile uint32_t *d = (volatile uint32_t *)dst;
  uint32_t *s = (uint32_t *)src;

  while (wlen--)
    *d++ = *s++;
}

/* Return 0 on success, otherwise error code.
   LEN is the total length (in words) of capsules */
static int
prepare_send(unsigned src_idx, unsigned len, uint8_t **pbuf, unsigned *poff, unsigned *pkt_plen)
{
    struct wren_tx_source *src;
    unsigned pkt_len;
    uint8_t *buf;
    unsigned off;
    union {
	uint8_t b[20];
	uint32_t w[5];
    } eth_hdr;
    union {
	struct wren_packet_hdr_v1 p;
	uint32_t w[sizeof(struct wren_packet_hdr_v1) / 4];
    } phdr;
    struct wren_packet_ts now;

    src = &wren_tx_sources[src_idx];
    if (src->proto.proto != WREN_PROTO_ETHERNET) {
	return WRENTX_ERR_BAD_PROTOCOL;
    }

    /* Allocate a nic buffer.
       The first 2 bytes are reserved.  */
    pkt_len = 2 + ETH_HDR_LEN + sizeof(struct wren_packet_hdr_v1) + len * 4;
    if (src->proto.u.eth.flags & WREN_PROTO_ETH_FLAGS_VLAN)
	pkt_len += 4;
    buf = (uint8_t*)nic_get_tx_buf (pkt_len);
    if (buf == NULL) {
	uart_printf("send packet: no tx buf\n");
	return WRENTX_ERR_QUEUE_FULL;
    }

    /* Write protocol header  */
    memcpy (eth_hdr.b + 2, src->proto.u.eth.mac, 6);
    memset (eth_hdr.b + 2 + 6, 0, 6);
    off = 2 + 6 + 6;
    if (src->proto.u.eth.flags & WREN_PROTO_ETH_FLAGS_VLAN) {
	eth_hdr.b[off + 0] = 0x81;
	eth_hdr.b[off + 1] = 0;
	eth_hdr.b[off + 2] = src->proto.u.eth.vlan >> 8;
	eth_hdr.b[off + 3] = src->proto.u.eth.vlan;
	off += 4;
    }

    eth_hdr.b[off + 0] = src->proto.u.eth.ethertype >> 8;
    eth_hdr.b[off + 1] = src->proto.u.eth.ethertype;
    off += 2;

    w32cpy(buf, eth_hdr.w, off / 4);

    /* Write packet header */
    phdr.p.version = PKT_VERSION_v1;
    phdr.p.domain = src->proto.u.eth.domain_id;
    phdr.p.xmit = src->proto.u.eth.xmit_id;
    phdr.p.next = src->max_delay_us_p2;
    phdr.p.len = len + sizeof(struct wren_packet_hdr_v1) / 4;
    phdr.p.seq_id = src->seq_id;
    src->seq_id = (src->seq_id + 4) & PKT_SEQ_MASK;

    get_now_packet_ts(&now);
    phdr.p.snd_ts = ((now.sec & 3) << 22) | (now.nsec >> 8);

    w32cpy(buf + off, phdr.w, sizeof(phdr) / 4);

    *pbuf = buf;
    *poff = off + sizeof(struct wren_packet_hdr_v1);
    *pkt_plen = pkt_len;

    return 0;
}

void
cmd_tx_send_packet (uint32_t *data, uint32_t len)
{
    uint32_t src_idx;
    struct wren_tx_source *src;
    unsigned pkt_len;
    unsigned off;
    uint8_t *buf;
    int res;

    /* Check source */
    if (len < 1) {
	mb_reply_failure(CMD_ERROR_LENGTH);
	return;
    }

    src_idx = *data;
    if (src_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }
    src = &wren_tx_sources[src_idx];

    /* Remove source (it's the first byte of the data)  */
    len--;

    res = prepare_send(src_idx, len, &buf, &off, &pkt_len);
    if (res != 0) {
	mb_reply_error(res);
	return;
    }

    /* Save context id and valid time (if present).
       If there is a context capsule, it is the first one and there can be
       only one context per packet */
    if (src->proto.u.eth.xmit_id == WREN_XMIT_ID_MAIN) {
	union wren_capsule_hdr_un ucap;
	union {
	    struct wren_capsule_ctxt_hdr ctxt;
	    uint32_t w[sizeof(struct wren_capsule_ctxt_hdr) / 4];
	} u;

	ucap.u32 = data[1];
	if (ucap.hdr.typ == PKT_CTXT) {
	    unsigned ctxt_id;
	    struct wren_tx_source_context *sc;
	    u.w[1] = data[2];
	    u.w[2] = data[3];
	    u.w[3] = data[4];
	    ctxt_id = u.ctxt.ctxt_id;
	    sc = &src->contexts[ctxt_id % MAX_TX_CONTEXTS];
	    sc->id = ctxt_id;
	    sc->valid_from = u.ctxt.valid_from;

	    WREN_LOG(LOG_TX_CTXT, "send: src %u, ctxt %u valid: %us+%un\n",
		     (unsigned)src_idx, (unsigned)ctxt_id,
		     (unsigned)u.ctxt.valid_from.sec,
		     (unsigned)u.ctxt.valid_from.nsec);
	}
    }

    /* Copy data */
    w32cpy(buf + off, data + 1, len);

    if (log_flags & LOG_TX) {
	uart_printf("send src=%u, buf=%x, pkt_len=0x%x\n",
		    (unsigned)src_idx, (unsigned)buf, pkt_len);
	dump_w32(buf, pkt_len > 64 ? 64 : pkt_len);
    }

    /* Send packet */
    nic_send_tx_buf();
    mb_reply_ok();
    return;
}

void
cmd_tx_set_table (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_table *arg = (struct wren_mb_tx_table *)data;
    unsigned hdr_len = (sizeof(*arg) - sizeof(arg->data)) / 4;
    struct wren_tx_table *tbl;
    unsigned src_idx;

    /* Check length */
    if (len < hdr_len
	|| arg->nbr_insns > WRENTX_MAX_INSNS
	|| arg->nbr_data > WRENTX_MAX_DATA
	|| len != hdr_len + arg->nbr_insns + arg->nbr_data) {
	mb_reply_error(CMD_ERROR_LENGTH);
	return;
    }

    /* Check source exists */
    src_idx = arg->src_idx;
    if (src_idx >= MAX_TX_SOURCES
	|| wren_tx_sources[src_idx].proto.proto == WREN_PROTO_NONE) {
	mb_reply_error(WRENTX_ERR_BAD_SOURCE_IDX);
	return;
    }

    /* Check table index */
    if (arg->table_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_TABLE_IDX);
	return;
    }

    /* Check table is free */
    tbl = &wren_tx_tables[arg->table_idx];
    if (tbl->state != TABLE_STATE_UNUSED) {
	mb_reply_error(WRENTX_ERR_BUSY_TABLE);
	return;
    }

    /* Copy */
    tbl->src_idx = arg->src_idx;
    tbl->nbr_insns = arg->nbr_insns;
    tbl->nbr_data = arg->nbr_data;
    tbl->repeat = arg->repeat;
    w32cpy(tbl->data, arg->data, tbl->nbr_insns + tbl->nbr_data);

    tbl->state = TABLE_STATE_LOADED;
    tbl->pc = 0;

    /* TODO: link table to source, prevent delete source if table */

    mb_reply_ok();
    return;
}

void
cmd_tx_get_table (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_table *arg = (struct wren_mb_tx_table *)data;
    unsigned hdr_len = (sizeof(*arg) - sizeof(arg->data)) / 4;
    struct wren_mb_tx_table *res;
    unsigned rlen;
    struct wren_tx_table *tbl;

    /* Check length */
    if (len < 1) {
	mb_reply_error(CMD_ERROR_LENGTH);
	return;
    }

    /* Check table index */
    if (arg->table_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_TABLE_IDX);
	return;
    }

    /* Check table is used */
    tbl = &wren_tx_tables[arg->table_idx];
    if (tbl->state == TABLE_STATE_UNUSED) {
	mb_reply_error(WRENTX_ERR_UNUSED_TABLE);
	return;
    }

    /* Copy */
    rlen = hdr_len + tbl->nbr_insns + tbl->nbr_data;
    res = (struct wren_mb_tx_table *)mb_get_reply (CMD_REPLY, rlen);
    res->table_idx = arg->table_idx;
    res->src_idx = tbl->src_idx;
    res->repeat = tbl->repeat;
    res->nbr_insns = tbl->nbr_insns;
    res->nbr_data = tbl->nbr_data;
    w32cpy(res->data, tbl->data, tbl->nbr_insns + tbl->nbr_data);

    mb_send_reply();
    return;
}

void
cmd_tx_del_table (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *arg = (struct wren_mb_arg1 *)data;
    struct wren_tx_table *tbl;
    unsigned table_idx = arg->arg1;

    /* Check length */
    if (len < sizeof(*arg) / 4) {
	mb_reply_error(CMD_ERROR_LENGTH);
	return;
    }

    /* Check table index */
    if (table_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_TABLE_IDX);
	return;
    }

    /* Check table is used */
    tbl = &wren_tx_tables[table_idx];
    if (tbl->state == TABLE_STATE_UNUSED) {
	mb_reply_error(WRENTX_ERR_UNUSED_TABLE);
	return;
    }

    /* Check table is idle */
    if (tbl->state != TABLE_STATE_LOADED) {
	mb_reply_error(WRENTX_ERR_BUSY_TABLE);
	return;
    }

    /* TODO: unlink */

    tbl->state = TABLE_STATE_UNUSED;

    mb_reply_ok();
    return;
}

static wren_context_id
substitute_tx_context_id (unsigned src_idx, const struct wren_packet_ts *ts)
{
    const struct wren_tx_source_context *sc = wren_tx_sources[src_idx].contexts;
    const struct wren_tx_source_context *res;
    unsigned i;

    res = NULL;
    for (i = 0; i < MAX_TX_SOURCES; i++, sc++) {
	/* Consider the context if it is valid and valid time is before the
	   due time. */
	if (sc->id != WREN_CONTEXT_NONE
	    && wren_packet_ts_lte(&sc->valid_from, ts)) {
	    /* Use this context if its valid time is after the best result,
	       or if there is no current best result.  */
	    if (res == NULL
		|| wren_packet_ts_lte(&res->valid_from, &sc->valid_from))
		res = sc;
	}
    }
    if (res == NULL)
	return WREN_CONTEXT_CURRENT;
    else
	return res->id;
}

static int
execute_table_send_insn(struct wren_tx_table *tbl)
{
    unsigned op = tbl->data[tbl->pc];
    unsigned ioff = tbl->nbr_insns + WREN_TABLE_OP_GET_FIELD(op, SEND_ADDR);
    unsigned len = WREN_TABLE_OP_GET_FIELD(op, SEND_LEN);
    uint8_t *buf;
    unsigned off;
    unsigned pkt_len;
    int res;

    res = prepare_send(tbl->src_idx, len, &buf, &off, &pkt_len);
    if (res != 0)
	return res;

    /* Copy event capsules, but update time */
    for (unsigned i = 0; i < len;) {
	union wren_capsule_event_un {
	    struct wren_capsule_event_hdr hdr;
	    uint32_t w[sizeof(struct wren_capsule_event_hdr)/4];
	} *un, nev;

	un = (union wren_capsule_event_un *)&tbl->data[ioff + i];
	if (un->hdr.hdr.typ == PKT_EVENT) {
	    /* For packets: due_time is the offset, adjust ctxt_id */
	    unsigned hlen = sizeof(struct wren_capsule_event_hdr)/4;
	    wren_context_id ctxt_id;

	    nev.hdr.hdr = un->hdr.hdr;/* capsule header */
	    nev.hdr.ev_id = un->hdr.ev_id;

	    nev.hdr.ts = un->hdr.ts;
	    wren_packet_ts_add(&nev.hdr.ts, tbl->due_ts.nsec, tbl->due_ts.sec);

	    ctxt_id = un->hdr.ctxt_id;
	    if (ctxt_id == WREN_CONTEXT_CURRENT)
		ctxt_id = substitute_tx_context_id (tbl->src_idx, &nev.hdr.ts);
	    nev.hdr.ctxt_id = ctxt_id;

	    w32cpy(buf + off, nev.w, hlen);
	    w32cpy(buf + off + hlen * 4, &tbl->data[ioff + i + hlen],
		   nev.hdr.hdr.len - hlen);
	}
	else {
	    w32cpy(buf + off, nev.w, un->hdr.hdr.len);
	}

	i += un->hdr.hdr.len;
	off += un->hdr.hdr.len * 4;
    }

    if (log_flags & LOG_TX) {
	uart_printf("send table src=%u, buf=%x, pkt_len=0x%x\n",
		    (unsigned)tbl->src_idx, (unsigned)buf, pkt_len);
	dump_w32(buf, pkt_len > 64 ? 64 : pkt_len);
    }

    /* Send packet */
    nic_send_tx_buf();

    return 0;
}

static void
execute_table_wait_insn(struct wren_tx_table *tbl)
{
    unsigned op = tbl->data[tbl->pc];
    unsigned sec = WREN_TABLE_OP_GET_FIELD(op, WAIT_SEC);
    unsigned nsec = tbl->data[tbl->pc + 1];

    wren_packet_ts_add(&tbl->due_ts, nsec, sec);
    wren_packet_ts_add(&tbl->exec_ts, nsec, sec);
}

#define TABLE_EXEC_ERROR -1
#define TABLE_EXEC_LOOP 1
#define TABLE_EXEC_WAIT 2

static int
execute_table_insns(struct wren_tx_table *tbl)
{
    unsigned op;

    while (1) {
	if (tbl->pc >= tbl->nbr_insns)
	    return TABLE_EXEC_LOOP;

	op = tbl->data[tbl->pc];

	switch(WREN_TABLE_OP_GET_OP(op)) {
	case wrentx_table_op_send:
	    execute_table_send_insn(tbl);
	    tbl->pc++;
	    break;
	case wrentx_table_op_wait:
	    execute_table_wait_insn(tbl);
	    tbl->pc += 2;
	    return TABLE_EXEC_WAIT;
	default:
	    return TABLE_EXEC_ERROR;
	}
    }
}

void
cmd_tx_play_table (uint32_t *data, uint32_t len)
{
    struct wren_mb_tx_play_table *arg = (struct wren_mb_tx_play_table *)data;
    struct wren_tx_table *tbl;
    unsigned table_idx;
    int res;

    /* Check length */
    if (len < sizeof(*arg) / 4) {
	mb_reply_error(CMD_ERROR_LENGTH);
	return;
    }

    /* Check table index */
    table_idx = arg->table_idx;
    if (table_idx >= MAX_TX_SOURCES) {
	mb_reply_error(WRENTX_ERR_BAD_TABLE_IDX);
	return;
    }

    /* Check table is used */
    tbl = &wren_tx_tables[table_idx];
    if (tbl->state == TABLE_STATE_UNUSED) {
	mb_reply_error(WRENTX_ERR_UNUSED_TABLE);
	return;
    }

    /* Check table is idle */
    if (tbl->state != TABLE_STATE_LOADED) {
	mb_reply_error(WRENTX_ERR_BUSY_TABLE);
	return;
    }

    /* Start execution */
    tbl->pc = 0;
    tbl->due_ts.sec = arg->due_ts.sec;
    tbl->due_ts.nsec = arg->due_ts.nsec;
    get_now_packet_ts(&tbl->exec_ts);
    tbl->count = 0;

    res = execute_table_insns(tbl);
    switch(res) {
    case TABLE_EXEC_WAIT:
	/* Insert it in the queue, update the wakeup time */
	tbl->link_next = table_exec_list;
	tbl->link_prev = NO_TABLE_IDX;
	table_exec_list = table_idx;

	tbl->state = TABLE_STATE_RUN;

	if (table_timeout.sec == NO_TIMEOUT
	    || wren_packet_ts_lte(&tbl->exec_ts, &table_timeout))
	    table_timeout = tbl->exec_ts;

	mb_reply_ok();
	break;

    case TABLE_EXEC_ERROR:
	mb_reply_error(WRENTX_ERR_BAD_TABLE);
	break;

    case TABLE_EXEC_LOOP:
	if (tbl->repeat == 1)
	    mb_reply_ok();
	else {
	    /* Loop without wait.  Not allowed */
	    mb_reply_error(WRENTX_ERR_BAD_TABLE);
	}
	break;

    default:
	mb_reply_error(WRENTX_ERR_BAD_TABLE);
	break;
    }
}

void
wrentx_table_poll(void)
{
}

void
wrentx_init(void)
{
    table_exec_list = NO_TABLE_IDX;
    table_timeout.sec = NO_TIMEOUT;
}
