#include <xparameters.h>
#include "lib.h"

/* Bit D is set in PMCR, so the CPU frequency is divided by 64 */
#define CYC_FREQ ((XPAR_CPU_CORTEXR5_0_CPU_CLK_FREQ_HZ + 32) / 64)

static unsigned get_pmccntr(void)
{
    unsigned res;
    asm volatile ("MRC p15,0,%0,c9,c13,0" : "=r"(res));
    return res;
}

unsigned
get_cpu_cycles(void)
{
  return get_pmccntr();
}

void
usleep(unsigned us)
{
  unsigned delta = us * (CYC_FREQ / 1000000);
  unsigned tend = get_cpu_cycles() + delta;

  while (((signed)(tend - get_cpu_cycles())) > 0)
    ;
}

void
__libc_init_array(void)
{
}

void
__libc_fini_array(void)
{
}

void
exit(int s)
{
  while (1)
    ;
}
