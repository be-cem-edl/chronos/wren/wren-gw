#include <stdint.h>
#include "armr5.h"
#include "wren-hw.h"

#if 0
static uint32_t
read_sctlr(void)
{
  uint32_t res;

  asm volatile ("MRC p15,0,%0,c1,c0,0" : "=r"(res));
  return res;
}

static void
write_sctlr(uint32_t sctlr)
{
  asm volatile ("MCR p15,0,%0,c1,c0,0" : : "r"(sctlr));
}
#endif

static void
write_mpu_addr(uint32_t addr)
{
  asm volatile ("MCR p15,0,%0,c6,c1,0" : : "r"(addr));
}

static void
write_mpu_size(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c1,2" : : "r"(val));
}

static void
write_mpu_acc(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c1,4" : : "r"(val));
}

static void
write_mpu_num(uint32_t val)
{
  asm volatile ("MCR p15,0,%0,c6,c2,0" : : "r"(val));
  cpu_isb();
}

#if 0
static void
enable_mpu(void)
{
  uint32_t sctlr = read_sctlr();

  sctlr |= 1;

  cpu_dsb();

  write_sctlr(sctlr);

  cpu_isb();
}

static void
disable_mpu(void)
{
  uint32_t sctlr = read_sctlr();

  sctlr &= ~1;

  cpu_dsb();

  write_sctlr(sctlr);

  cpu_isb();
}
#endif

/* Called by boot.S to setup MPU */
void
Init_MPU(void)
{
  unsigned n = 0;

  /* TODO: disable 0-4KB to trap NULL access ?  */

  /* 0: 0 - 2GB (ddram) */
  write_mpu_num(n++);
  write_mpu_addr(0x00000000);
  write_mpu_acc(0x30b);
  write_mpu_size((30 << 1) | 1);

  /* 1: 0x80000000 2GB + 1GB (PL) */
  write_mpu_num(n++);
  write_mpu_addr(0x80000000);
  write_mpu_acc(0x310);
  write_mpu_size((29 << 1) | 1);

#if 0
  /* 2: 0xC0000000 + 512M (qspi) */
  write_mpu_num(n++);
  write_mpu_addr(0xC0000000);
  write_mpu_acc(0x310);
  write_mpu_size((28 << 1) | 1);
#endif

  /* 3: 0xE0000000 + 256M (PCIe) */
  write_mpu_num(n++);
  write_mpu_addr(0xE0000000);
  write_mpu_acc(0x310);
  write_mpu_size((27 << 1) | 1);

  /* 4: 0xF0000000 + 2*128K (TCM) */
  write_mpu_num(n++);
  write_mpu_addr(BTCM_MAP_BASE);
  write_mpu_acc(0x310);
  write_mpu_size((17 << 1) | 1);

  /* 6: 0xFD000000 + 32M (FPS + uppser LPS) */
  write_mpu_num(n++);
  write_mpu_addr(0xFD000000);
  write_mpu_acc(0x310);
  write_mpu_size((24 << 1) | 1);

  /* 8: 0xFF000000 + 16M (lower LPS + TCM) */
  write_mpu_num(n++);
  write_mpu_addr(0xFF000000);
  write_mpu_acc(0x310);
  write_mpu_size((23 << 1) | 1);

  /* 9: 0xFFFC0000 + 256K (OCM) */
  write_mpu_num(n++);
  write_mpu_addr(0xFFFC0000);
  write_mpu_acc(0x30b);
  write_mpu_size((17 << 1) | 1);

  /* Disable remaing entries */
  while (n < 16) {
    write_mpu_num(n++);
    write_mpu_size(0);
  }
}
