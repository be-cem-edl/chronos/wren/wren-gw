#include <stdint.h>
#include "lib.h"

void FIQInterrupt(void)
{
    uart_printf("FIQ interrupt\n");
}

void IRQInterrupt(void)
{
    uart_printf("IRQ interrupt\n");
}

void UndefinedException(unsigned pc)
{
  uart_printf("undefined exception from %08x\n", pc);
}

void SWInterrupt(unsigned num, unsigned pc)
{
  uart_printf("sw exception #%x from %08x\n", num, pc);
}

void DataAbortInterrupt(unsigned pc)
{
    unsigned dfsr, adfsr, dfar;

    asm volatile ("MRC p15,0,%0,c5,c0,0" : "=r"(dfsr));
    asm volatile ("MRC p15,0,%0,c5,c1,0" : "=r"(adfsr));
    asm volatile ("MRC p15,0,%0,c6,c0,0" : "=r"(dfar));
    uart_printf("data abort pc=%08x addr=%08x dfsr=%08x adfsr=%08x\n",
		pc, dfar, dfsr, adfsr);
}

void PrefetchAbortInterrupt(unsigned pc)
{
  uart_printf("prefetch abort exception from %08x\n", pc);
}
