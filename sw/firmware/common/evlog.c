#include <string.h>
#include "wrenrx-data.h"
#include "wren-hw.h"
#include "lib.h"
#include "evlog.h"

/* Check the size of evlog_entry */
extern unsigned check_evlog_entry_size[sizeof(union wren_evlog_entry) == 256 ? 1 : -1];

/* Index of the next entry to use */
static unsigned evlog_next;

/* Number of entries filled.  Cannot be more than WREN_EVLOG_MAX */
static unsigned evlog_nbr;

static union wren_evlog_entry * const evlog_base = (union wren_evlog_entry *)0;

static unsigned evlog_cur;
static unsigned evlog_first;
static unsigned evlog_rem;
static unsigned evlog_off;

void
evlog_reset(void)
{
    evlog_next = 0;
    evlog_nbr = 0;
}

void
evlog_hdr(unsigned type, unsigned dlen)
{
    unsigned nchunk = (dlen  + WREN_EVLOG_DSIZE - 1) / WREN_EVLOG_DSIZE;
    union wren_evlog_entry *evptr;

    evlog_cur = evlog_next;
    evlog_first = evlog_cur;
    evlog_rem = dlen;
    evlog_off = 0;

    /* Init first entry */
    evptr = &evlog_base[evlog_cur];
    evptr->e.type = type;
    evptr->e.nentries = nchunk;
    evptr->e.len = dlen;

    /* Number of used entries */
    evlog_nbr += nchunk;
    if (evlog_nbr >= WREN_EVLOG_MAX)
	evlog_nbr = WREN_EVLOG_MAX;

    evlog_next = (evlog_next + nchunk) & (WREN_EVLOG_MAX - 1);
}

void
evlog_hdr_extend(unsigned dlen)
{
    unsigned nchunk = (evlog_rem + dlen) / WREN_EVLOG_DSIZE;
    union wren_evlog_entry *evptr;

    evlog_rem += dlen;

    /* Adjust first entry */
    evptr = &evlog_base[evlog_first];
    evptr->e.nentries += nchunk;
    evptr->e.len += dlen;

    /* Number of used entries */
    evlog_nbr += nchunk;
    if (evlog_nbr >= WREN_EVLOG_MAX)
	evlog_nbr = WREN_EVLOG_MAX;
}

void
evlog_push_ts(void)
{
    union wren_evlog_entry *evptr = &evlog_base[evlog_cur];
    union {
	struct {
	    unsigned sec, nsec;
	} ts;
	unsigned char b[EVLOG_TS_SIZE];
    } u;

    if (evlog_off != 0 || evlog_rem < EVLOG_TS_SIZE)
	return;
    wrenrx_now(&u.ts.sec, &u.ts.nsec);
    memcpy(evptr->e.d, &u.ts, EVLOG_TS_SIZE);

    evlog_off += EVLOG_TS_SIZE;
    evlog_rem -= EVLOG_TS_SIZE;
}

/* Initialize next chunk */
static void
evlog_next_chunk(void)
{
    union wren_evlog_entry *evptr;

    evlog_cur = (evlog_cur + 1) & (WREN_EVLOG_MAX - 1);
    evptr = &evlog_base[evlog_cur];
    evptr->e.type = EVLOG_TYPE_CONT;
    evptr->e.nentries = 0;
    evptr->e.len = 0;

    evlog_off = 0;
}

void
evlog_push_u8(unsigned char v)
{
    union wren_evlog_entry *evptr = &evlog_base[evlog_cur];

    if (evlog_rem == 0)
	return;

    evptr->e.d[evlog_off++] = v;
    evlog_rem--;

    if (evlog_rem == 0)
	return;

    if (evlog_off == WREN_EVLOG_DSIZE)
	evlog_next_chunk();
}

void
evlog_push_u16(uint16_t v)
{
    union wren_evlog_entry *evptr = &evlog_base[evlog_cur];

    if (evlog_rem < 2)
	return;

    evptr->u16[2 + evlog_off / 2] = v;
    evlog_off += 2;
    evlog_rem -= 2;

    if (evlog_rem == 0)
	return;

    if (evlog_off == WREN_EVLOG_DSIZE)
	evlog_next_chunk();
}

static void *
evlog_memcpy(void *dst, const void *src, size_t len)
{
    if ((uintptr_t)dst % sizeof(unsigned) == 0
	&& (uintptr_t)src % sizeof(unsigned) == 0
	&& (uintptr_t)len % sizeof(unsigned) == 0) {
	/* Optimize for word accesses */
	unsigned *d = dst;
	const unsigned *s = src;

	/* Unroll */
	while (len > 4 * sizeof(unsigned)) {
	    d[0] = s[0];
	    d[1] = s[1];
	    d[2] = s[2];
	    d[3] = s[3];
	    d += 4;
	    s += 4;
	    len -= 4 * sizeof(unsigned);
	}
	while (len > 0) {
	    *d++ = *s++;
	    len -= sizeof(unsigned);
	}
    }
    else {
	unsigned char *d = dst;
	const unsigned char *s = src;
	while (len > 0) {
	    *d++ = *s++;
	    len--;
	}
    }

    return dst;
}

void
evlog_push_bytes(const unsigned char *d, unsigned len)
{
    while (1) {
	union wren_evlog_entry *evptr = &evlog_base[evlog_cur];
	unsigned clen;

	if (evlog_off + len > WREN_EVLOG_DSIZE)
	    clen = WREN_EVLOG_DSIZE - evlog_off;
	else
	    clen = len;
	if (clen > evlog_rem)
	    clen = evlog_rem;

	evlog_memcpy(evptr->e.d + evlog_off, d, clen);
	evlog_off += clen;
	evlog_rem -= clen;
	d += clen;
	len -= clen;

	if (len == 0)
	    return;
	
	ASSERT(evlog_rem != 0);
	ASSERT(evlog_off == EVLOG_DSIZE);

	evlog_next_chunk();
    }
}


void
cmd_evlog_index (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg_reply *rep;

    rep = (struct wren_mb_arg_reply *)mb_get_reply
	(CMD_REPLY, sizeof(*rep) / 4);
    rep->arg1 = evlog_next;
    rep->arg2 = evlog_nbr;

    mb_send_reply();
}

void
cmd_evlog_read (uint32_t *data, uint32_t len)
{
    struct wren_mb_arg1 *cmd = (struct wren_mb_arg1 *)data;
    unsigned idx = cmd->arg1;
    uint32_t *rep;
    uint32_t *addr;
    unsigned wlen;

    if (idx >= WREN_EVLOG_MAX) {
	mb_reply_error(WRENRX_ERR_EVLOG_TOO_LARGE);
	return;
    }

    wlen = sizeof(union wren_evlog_entry) / 4;
    rep = (uint32_t *)mb_get_reply(CMD_REPLY, wlen);

    addr = evlog_base[idx].w;

    for (unsigned i = 0; i < wlen; i++)
	*rep++ = *addr++;

    mb_send_reply();
}
