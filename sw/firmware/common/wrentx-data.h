#include <stdint.h>
#include "wren/wren-common.h"
#include "wren/wren-packet.h"

/* TX part */

#define MAX_TX_SOURCES 8

#define MAX_TX_CONTEXTS 4

struct wren_tx_source {
    struct wren_protocol proto;
    uint16_t seq_id;
    uint32_t max_delay_us_p2;

    /* Valid time of recently sent contexts so that the context id of events
       sent through tables can be set.  */
    struct wren_tx_source_context {
	wren_context_id id;
	struct wren_packet_ts valid_from;
    } contexts[MAX_TX_CONTEXTS];
};

extern struct wren_tx_source wren_tx_sources[MAX_TX_SOURCES];
