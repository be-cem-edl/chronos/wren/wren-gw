#include <stdio.h>
#include <string.h>
#include "armr5.h"
#include "lib.h"
#include "qspi.h"
#include "board_map.h"
#include "mb_map.h"
#include "wren-mb-defs.h"
#include "wr_nic.h"
#include "rx_desc.h"
#include "tx_desc.h"
#include "wren-hw.h"
#include "wren.h"
#include "nic.h"
#include "wrentx.h"
#include "wrenrx-core.h"
#include "wrenrx-cmd.h"
#include "board.h"
#include "evlog.h"
#include "trace.h"
#include "versions.h"
#include "build.h"

#define VERBOSE_COMMAND 1

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

static volatile struct nic * const nic = (volatile struct nic *)
    (BOARD_MAP_BASE + BOARD_MAP_WRNIC);

static volatile struct mb_map * const mb =
    (volatile struct mb_map *)BTCM_MAP_BASE;

unsigned log_flags = LOG_ERR | LOG_EVENT_TS;

uint32_t *
mb_get_reply(unsigned cmd, unsigned len)
{
    mb->b2h_cmd = cmd;
    mb->b2h_len = len;
    return (uint32_t *)&mb->b2h_data;
}

void
mb_send_reply(void)
{
    mb->b2h_csr = MB_CSR_READY;
}

#if 0
void
mb_reply_word(unsigned cmd, unsigned data)
{
    uint32_t *ptr;

    ptr = mb_get_reply(cmd, sizeof(*ptr) / 4);
    *ptr = data;
    mb_send_reply();
}
#endif

void mb_reply_failure(unsigned err)
{
    mb_get_reply(err, 0);
    mb_send_reply();
}

void mb_reply_error(int err)
{
    /* Note: ERR is negative, so 'CMD_ERROR - err' is like an logical or
       with the absolute value of ERR */
    mb_get_reply(CMD_ERROR - err, 0);
    mb_send_reply();
}

void mb_reply_ok(void)
{
    mb_get_reply(CMD_REPLY, 0);
    mb_send_reply();
}

void host_msg_doorbell(void)
{
    /* Generate a SW interrupt to the host */
    regs->ifr = BOARD_MAP_IFR_MSG;
}

void host_async_doorbell(void)
{
    /* Generate a SW interrupt to the host */
    regs->ifr = BOARD_MAP_IFR_ASYNC;
}

#ifdef VERBOSE_COMMAND
static const char * const cmd_names[] = {
#define def_cmd(NAME) #NAME,
#include "wren-mb-cmds.def"
#undef def_cmd
};
#define CMD_NAMES_NBR (sizeof(cmd_names)/sizeof(cmd_names[0]))
#endif

void exec_host_command(void)
{
    uint32_t cmd = mb->h2b_cmd;
    uint32_t *data = (uint32_t *)mb->h2b_data;
    unsigned len = mb->h2b_len;

#ifdef VERBOSE_COMMAND
    WREN_LOG(LOG_CMD, "cmd: %s (%08lx)\n",
	     cmd < CMD_NAMES_NBR ? cmd_names[cmd] : "-",
	     cmd);
    if (log_flags & LOG_CMD_ARGS) {
	wren_log("args:");
	for (unsigned i = 0; i < 8 && i < len; i++)
	    wren_log(" %08x", (unsigned)data[i]);
	wren_log("\n");
    }
#else
    WREN_LOG(LOG_CMD, "cmd: %08lx\n", cmd);
#endif

    switch(cmd) {
    case CMD_PING: {
	uart_printf(" ping\n");
	mb_reply_ok();
	break;
    }
    case CMD_WRITE: {
	uint32_t addr = data[0];
	unsigned off;

	if (len < 1 || len > sizeof(mb->h2b_data) / 4) {
	    mb_reply_failure(CMD_ERROR_LENGTH);
	    break;
	}

	// uart_printf(" write @%08lx+%u\n", addr, len);
	for (off = 0; off < len - 1; off++) {
	    volatile unsigned *dest =
		(volatile unsigned *)(addr + off * 4);
	    *dest = data[off + 1];
	}
	cpu_dsb();
	len += addr & 0x1f;
	addr &= ~0x1f;
	for (off = 0; off < len; off += 0x20) {
	    r5_dccmvac(addr + off);
	    cpu_isb();
	    r5_icimvau(addr + off);
	}
	mb_reply_ok();
	break;
    }
    case CMD_READ: {
          uint32_t *rep;
	  uint32_t addr = data[0];
	  unsigned len = data[1];
	  unsigned i;

	  if (len > sizeof(mb->h2b_data) / 4)
	      len = sizeof(mb->h2b_data) / 4;
	  rep = (uint32_t *)mb_get_reply (CMD_REPLY, len);
	  for (i = 0; i < len; i++)
	      rep[i] = *(uint32_t *)(addr + i * 4);
	  mb_send_reply();
	  break;
    }
    case CMD_EXEC: {
	uint32_t addr = data[0];
	void (*fn)(void);

	uart_printf(" exec @%08lx\n", addr);
	fn = (void (*)(void))addr;
	fn();
	break;
    }
    case CMD_SEND: {
	unsigned char *b;

	uart_printf(" send %u bytes\n", len);
	b = (unsigned char *)data;
	if (nic_send(b, len) < 0) {
	    uart_printf("no free descriptor\n");
	    return;
	}
	break;
    }

    case CMD_CLI: {
	union {
	    char buf[64];
	    uint32_t w[64/4];
	} u;
	if (len * 4 >= sizeof(u.buf)) {
	    mb_reply_failure(CMD_ERROR_LENGTH);
	    break;
	}

	w32cpy(u.w, data, len);
	u.w[len] = 0;
	WREN_LOG(LOG_CMD, "execute: %s\n", u.buf);
	cmd_cli(u.buf);
	mb_reply_ok();
	break;
    }

    case CMD_TTY_LOG: {
	uint32_t len = data[0];
	struct wren_mb_tty_log *rep = (struct wren_mb_tty_log *)mb->b2h_data;

	/* We are cheating: we fill the reply before 'allocating' it. */
	len = uart_get_log(len, 1, rep->u.c);
	rep = (struct wren_mb_tty_log *)mb_get_reply
	    (CMD_REPLY, 1 + (len + 3) / 4);
	rep->len = len;
	mb_send_reply();
	break;
    }
    case CMD_FW_VERSION: {
	static const char ver[] =
	    "tag:" BUILD_TAG "\n""date:" __DATE__ " " __TIME__ "\n";
	struct wren_mb_version *rep;

	rep = (struct wren_mb_version *)mb_get_reply
	    (CMD_REPLY, (sizeof(ver) + 3) / 4);
	memcpy(rep->u.c, ver, sizeof(ver));
	mb_send_reply();
	break;
    }
    case CMD_HW_GET_CONFIG: {
	struct wren_mb_hw_config *rep;
	uint32_t ids;
	
	rep = (struct wren_mb_hw_config *)mb_get_reply
	    (CMD_REPLY, sizeof(*rep) / 4);
	rep->model_ident = regs->model_id;
	ids = regs->board_id;
	rep->fp_id = (ids & BOARD_MAP_BOARD_ID_FP_ID_MASK)
	    >> BOARD_MAP_BOARD_ID_FP_ID_SHIFT;
	rep->pp_id = (ids & BOARD_MAP_BOARD_ID_PP_ID_MASK)
	    >> BOARD_MAP_BOARD_ID_PP_ID_SHIFT;
	rep->pcb_id = ~0U; /* TODO */
	mb_send_reply();
	break;
    }
    case CMD_HW_GET_USERIO: {
	struct wren_mb_hw_userio *rep;
	uint32_t val;
	
	rep = (struct wren_mb_hw_userio *)mb_get_reply
	    (CMD_REPLY, sizeof(*rep) / 4);
	val = regs->userio_cfg;
	rep->vme_p2a_out_be = (val & BOARD_MAP_USERIO_CFG_DIR_MASK)
	    & ((val & BOARD_MAP_USERIO_CFG_EN_MASK)
	       >> BOARD_MAP_USERIO_CFG_EN_SHIFT);
	rep->vme_p2a_in_be = (~val & BOARD_MAP_USERIO_CFG_DIR_MASK)
	    & ((val & BOARD_MAP_USERIO_CFG_EN_MASK)
	       >> BOARD_MAP_USERIO_CFG_EN_SHIFT);
	rep->vme_p2c_data = 0; /* TODO */
	rep->patchpanel = val & BOARD_MAP_USERIO_CFG_PP_EN ? 1 : 0;
	mb_send_reply();
	break;
    }

    case CMD_TX_GET_CONFIG:
	cmd_tx_get_config(data, len);
	break;
    case CMD_TX_SET_SOURCE:
	cmd_tx_set_source(data, len);
	break;
    case CMD_TX_GET_SOURCE:
	cmd_tx_get_source (data, len);
	break;
    case CMD_TX_SEND_PACKET:
	cmd_tx_send_packet(data, len);
	break;

    case CMD_TX_SET_TABLE:
	cmd_tx_set_table(data, len);
	break;
    case CMD_TX_GET_TABLE:
	cmd_tx_get_table(data, len);
	break;
    case CMD_TX_DEL_TABLE:
	cmd_tx_del_table(data, len);
	break;
    case CMD_TX_PLAY_TABLE:
	cmd_tx_play_table(data, len);
	break;

    case CMD_RX_GET_CONFIG:
	cmd_rx_get_config(data, len);
	break;
    case CMD_RX_SET_SOURCE:
	cmd_rx_set_source(data, len);
	break;
    case CMD_RX_GET_SOURCE:
	cmd_rx_get_source(data, len);
	break;
    case CMD_RX_SET_COND:
	cmd_rx_set_cond(data, len);
	break;
    case CMD_RX_GET_COND:
	cmd_rx_get_cond(data, len);
	break;
    case CMD_RX_DEL_COND:
	cmd_rx_del_cond(data, len);
	break;

    case CMD_RX_SET_COMPARATOR:
	cmd_rx_set_comparator(data, len);
	break;
    case CMD_RX_GET_COMPARATOR:
	cmd_rx_get_comparator(data, len);
	break;
    case CMD_RX_ABORT_COMPARATOR:
	cmd_rx_abort_comparator(data, len);
	break;

    case CMD_RX_SET_ACTION:
	cmd_rx_set_action(data, len);
	break;
    case CMD_RX_GET_ACTION:
	cmd_rx_get_action(data, len);
	break;
    case CMD_RX_DEL_ACTION:
	cmd_rx_del_action(data, len, 0);
	break;
    case CMD_RX_DEL_ACTION_NOTIFY:
	cmd_rx_del_action(data, len, 1);
	break;
    case CMD_RX_MOD_ACTION:
	cmd_rx_mod_action(data, len);
	break;
    case CMD_RX_IMM_ACTION:
	cmd_rx_imm_action(data, len);
	break;
    case CMD_RX_FORCE_PULSER:
	cmd_rx_force_pulser(data, len);
	break;

    case CMD_RX_SET_OUT_CFG:
	cmd_rx_set_out_cfg(data, len);
	break;
    case CMD_RX_GET_OUT_CFG:
	cmd_rx_get_out_cfg(data, len);
	break;
    case CMD_RX_SET_PIN_CFG:
	cmd_rx_set_pin_cfg(data, len);
	break;
    case CMD_RX_GET_PIN_CFG:
	cmd_rx_get_pin_cfg(data, len);
	break;

    case CMD_RX_PULSERS_STATUS:
	cmd_rx_get_pulsers_status(data, len);
	break;
    case CMD_RX_GET_PULSER:
	cmd_rx_get_pulser(data, len);
	break;
    case CMD_RX_ABORT_PULSERS:
	cmd_rx_abort_pulsers(data, len);
	break;

    case CMD_RX_SUBSCRIBE:
	cmd_rx_subscribe(data, len);
	break;
    case CMD_RX_UNSUBSCRIBE:
	cmd_rx_unsubscribe(data, len);
	break;
    case CMD_RX_GET_SUBSCRIBED:
	cmd_rx_get_subscribed(data, len);
	break;

    case CMD_RX_LOG_READ:
	cmd_rx_log_read(data, len);
	break;

    case CMD_RX_RESET:
	cmd_rx_reset(data, len);
	break;
    case CMD_RX_SET_PROMISC:
	cmd_rx_set_promisc(data, len);
	break;
    case CMD_RX_INJECT_PACKET:
	cmd_rx_inject_packet(data, len);
	break;

    case CMD_EVLOG_INDEX:
	cmd_evlog_index(data, len);
	break;
    case CMD_EVLOG_READ:
	cmd_evlog_read(data, len);
	break;

    case CMD_RX_SET_PATCHPANEL: {
	uint32_t cfg = data[0];
	uint32_t val;
	val = regs->userio_cfg & ~BOARD_MAP_USERIO_CFG_PP_EN;
	if (cfg)
	    val |= BOARD_MAP_USERIO_CFG_PP_EN;
	regs->userio_cfg = val;
	mb_reply_ok();
	break;
    }

#ifdef CONFIG_VME_P2
    case CMD_VME_P2A_SET_OUTPUT: {
	uint32_t cfg = data[0];
	uint32_t val;
	/* Enable and set as output */
	val = cfg & BOARD_MAP_USERIO_CFG_DIR_MASK;
	val = val | (val << BOARD_MAP_USERIO_CFG_EN_SHIFT);
	/* But keep patch panel enable bit */
	val |= regs->userio_cfg & BOARD_MAP_USERIO_CFG_PP_EN;
	regs->userio_cfg = val;
	mb_reply_ok();
	break;
    }
#endif

    default:
	uart_printf ("unknown host command 0x%x\n", (unsigned)cmd);
	mb_reply_failure(CMD_ERROR_COMMAND);
	break;
    }

    WREN_LOG(LOG_CMD, "reply: %08lx\n", mb->b2h_cmd);

    /* Clear ready bit, and send irq to host */
    mb->h2b_csr = 0;
    host_msg_doorbell();
}

static void
main_loop(void)
{
    unsigned isr;
    unsigned csr;
    unsigned pulses;

    regs->pulsers_mask = ~0;

    uart_printf("main-loop, press any key for menu\n");
    while (1) {
	isr = nic->eic.eic_isr;
	if (isr & NIC_EIC_EIC_IER_RCOMP)
	    nic_rx_handler();
	if (isr & (NIC_EIC_EIC_IER_TCOMP | NIC_EIC_EIC_IER_TXERR)) {
	    /* Clear EIC isr */
	    nic->eic.eic_isr = isr & (NIC_EIC_EIC_ISR_TCOMP
				      | NIC_EIC_EIC_ISR_TXERR);
	    nic_tx_handler();
	}

	csr = mb->h2b_csr;
	if (csr & MB_CSR_READY) {
	    exec_host_command();
	}

	pulses = regs->pulsers_int;
	if (pulses != 0)
	    pulses_handler(pulses);

	wrentx_table_poll();

	if (uart_can_read())
	    cli_menu();
    }
}

/* rst_pl_done is emio[0], rst_pl_cont is emio[1] */
#define GPIO_DIRM_3     0x00FF0A02c4
#define GPIO_OEN_3      0x00FF0A02c8
#define MASK_DATA_3_LSW 0x00FF0A0018
#define GPIO_DATA_3_RO  0x00FF0A006c

/* pl_reset0 is gpio 95 */
#define GPIO_DIRM_5     0x00FF0A0344
#define GPIO_OEN_5      0x00FF0A0348
#define MASK_DATA_5_MSW 0x00FF0A002C
#define GPIO_DATA_5     0x00FF0A0054
#define GPIO_DATA_5_RO  0x00FF0A0074

static void
pl_reset_init(void)
{
    unsigned v;

    /* output */
    v = read32(GPIO_DIRM_5);
    write32(GPIO_DIRM_5, v | (1<<31));

    v = read32(GPIO_DIRM_3);
    write32(GPIO_DIRM_3, v | (1<<1));

    /* enable */
    v = read32(GPIO_OEN_5);
    write32(GPIO_OEN_5, v | (1<<31));

    v = read32(GPIO_OEN_3);
    write32(GPIO_OEN_3, v | (1<<1));

}

static int
pl_reset_done(void)
{
    return read32(GPIO_DATA_3_RO) & 1;
}

static void
pl_reset_assert(void)
{
    /* clear rst_cont */
    write32(MASK_DATA_3_LSW, (0xfffd << 16));

    /* clear rst_pl_n */
    write32(MASK_DATA_5_MSW, (0x7fff << 16));
}

static void
pl_reset_deassert(void)
{
    write32(MASK_DATA_5_MSW, (0x7fff << 16) | 0x8000);
}

static void
pl_reset_cont(void)
{
    /* set rst_cont */
    write32(MASK_DATA_3_LSW, (0xfffd << 16) | (1<<1));
}

void
map_tcm(void)
{
    unsigned val;
    unsigned i;
    unsigned en_atcm;
    volatile double *p;

    /* Set BTCM base address */
    asm ("MRC p15,0,%0,c9,c1,0" : "=r"(val));
    val &= 0xffe;
    val |= BTCM_MAP_BASE | 1;
    asm volatile ("MCR p15,0,%0,c9,c1,0" : : "r"(val));

    en_atcm = 0;

    /* Set ATCM base address */
    asm ("MRC p15,0,%0,c9,c1,1" : "=r"(val));
    val &= 0xffe;
    val |= ATCM_MAP_BASE | 1;
    asm volatile ("MCR p15,0,%0,c9,c1,1" : : "r"(val));

    cpu_isb();

    if (en_atcm) {
	/* Clear ATCM - this is needed to initialize ECC */
	p = (volatile double *)ATCM_MAP_BASE;
	for (i = 0; i < (1 << (16 - 3)); i++)
	    p[i] = 0.0;
    }

    /* Clear BTCM - this is needed to initialize ECC.
       Use double to write 64b and therefore a correct ECC */
    p = (volatile double *)BTCM_MAP_BASE;
    for (i = 0; i < (1 << (16 - 3)); i++)
	p[i] = 0.0;

#if 0 /* Already enabled */
    /* Enable BTCM and ATCM ECC check */
    val = r5_read_actlr();
    val |= (1<<27) | (1<<26);
    if (en_atcm)
	val |= (1<<25);
    /* TODO: external error ? */
    r5_write_actlr(val);
#endif
}

#define XXBOARD_FUNC(brd,suff) board_##brd##_##suff
#define XBOARD_FUNC(brd,suff) XXBOARD_FUNC(brd,suff)
#define BOARD_FUNC(suff) XBOARD_FUNC(BOARD,suff)

int main(void)
{
    uart_printf("Start of main_app\n");
    qspi_init();

    pl_reset_init();
    pl_reset_assert();

    BOARD_FUNC(pll_init)();

    pl_reset_deassert();

#if 0
    uart_printf("Press a key to continue reset\n");
    uart_getc();
    uart_printf("Wait for reset done\n");
#endif
    pl_reset_cont();
    /* Wait until end of reset before accessing to the PL */
    while (!pl_reset_done())
	;

    uart_printf("before reading version\n");

    uart_printf("Map version: 0x%08x\n", (unsigned)regs->map_version);

    if (regs->map_version != WREN_BOARD_MAP_VERSION) {
	while (1) {
	    uart_printf("Wrong board map version (read %08x, expect %08x)\n",
			(unsigned)regs->map_version, WREN_BOARD_MAP_VERSION);
	    usleep(1000000);
	}
    }

    evlog_reset();

    regs->fw_version = WREN_FW_VERSION;

    BOARD_FUNC(init)();

    nic_init ();

    /* Clear the mailboxes, just in case */
    while (1) {
	unsigned csr;
    	csr = mb->h2b_csr;
	if (!(csr & MB_CSR_READY))
	    break;
	uart_printf("Discard H2B command\n");
	mb->h2b_csr = 0;
    }

    wrenrx_init();
    wrentx_init();

    /* Empty async memory */
    mb->async_board_off = mb->async_host_off;

    /* Enable patch panel if present */
    {
	unsigned pp_id = regs->board_id;
	unsigned val;
	
	pp_id = (pp_id & BOARD_MAP_BOARD_ID_PP_ID_MASK)
	    >> BOARD_MAP_BOARD_ID_PP_ID_SHIFT;

	/* Always enable fpga pins as outputs */
	val = BOARD_MAP_USERIO_CFG_DIR_MASK;

	if (pp_id != 7)
	    val |= BOARD_MAP_USERIO_CFG_PP_EN;
	regs->userio_cfg = val;
    }

    main_loop();

    return 0;
}
