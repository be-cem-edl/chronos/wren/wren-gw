/* Generate an interrupt to the host */
void host_msg_doorbell(void);
void host_async_doorbell(void);

/* Handle an host command */
void exec_host_command(void);

void pulses_handler(unsigned pulses);

void cmd_cli(char *);

void w32cpy(void *dst, void *src, unsigned wlen);

/* Enter into debugger. */
void cli_menu(void);
