#ifndef __CHEBY__VME_REGS_MAP__H__
#define __CHEBY__VME_REGS_MAP__H__
#define VME_REGS_MAP_SIZE 1024 /* 0x400 = 1KB */

/* Core ID - VMEM */
#define VME_REGS_MAP_ID 0x0UL
#define VME_REGS_MAP_ID_PRESET 0x564d454dUL

/* HW version */
#define VME_REGS_MAP_VERSION 0x4UL

/* Interrupt Id */
#define VME_REGS_MAP_INTID 0x8UL
#define VME_REGS_MAP_INTID_VAL_MASK 0xffUL
#define VME_REGS_MAP_INTID_VAL_SHIFT 0

/* Interrupts status */
#define VME_REGS_MAP_ISTAT 0xcUL
#define VME_REGS_MAP_ISTAT_ACFAIL 0x1UL
#define VME_REGS_MAP_ISTAT_IRQ_MASK 0xfeUL
#define VME_REGS_MAP_ISTAT_IRQ_SHIFT 1

/* Interrupts mask */
#define VME_REGS_MAP_IMASK 0x10UL
#define VME_REGS_MAP_IMASK_ACFAIL 0x1UL
#define VME_REGS_MAP_IMASK_IRQ_MASK 0xfeUL
#define VME_REGS_MAP_IMASK_IRQ_SHIFT 1

/* Control register */
#define VME_REGS_MAP_MSTR 0x14UL
#define VME_REGS_MAP_MSTR_RMWENA 0x1UL
#define VME_REGS_MAP_MSTR_REQ 0x2UL
#define VME_REGS_MAP_MSTR_BERR 0x4UL
#define VME_REGS_MAP_MSTR_IBERR 0x8UL
#define VME_REGS_MAP_MSTR_POSTWR 0x10UL
#define VME_REGS_MAP_MSTR_ADO 0x20UL
#define VME_REGS_MAP_MSTR_FAIRREQ 0x40UL
#define VME_REGS_MAP_MSTR_A16_MODE_MASK 0x300UL
#define VME_REGS_MAP_MSTR_A16_MODE_SHIFT 8
#define VME_REGS_MAP_MSTR_A24_MODE_MASK 0xc00UL
#define VME_REGS_MAP_MSTR_A24_MODE_SHIFT 10
#define VME_REGS_MAP_MSTR_A32_MODE_MASK 0x3000UL
#define VME_REGS_MAP_MSTR_A32_MODE_SHIFT 12

/* System Control register */
#define VME_REGS_MAP_SYSC 0x18UL
#define VME_REGS_MAP_SYSC_SYSC 0x1UL
#define VME_REGS_MAP_SYSC_SYSR 0x2UL
#define VME_REGS_MAP_SYSC_ATO 0x4UL

/* Upper address bits for A32 */
#define VME_REGS_MAP_LONGADD 0x1cUL
#define VME_REGS_MAP_LONGADD_VAL_MASK 0xffUL
#define VME_REGS_MAP_LONGADD_VAL_SHIFT 0

/* DMA status */
#define VME_REGS_MAP_DMASTA 0x2cUL
#define VME_REGS_MAP_DMASTA_EN 0x1UL
#define VME_REGS_MAP_DMASTA_IEN 0x2UL
#define VME_REGS_MAP_DMASTA_IRQ 0x4UL
#define VME_REGS_MAP_DMASTA_ERR 0x8UL
#define VME_REGS_MAP_DMASTA_BD_MASK 0xf0UL
#define VME_REGS_MAP_DMASTA_BD_SHIFT 4

/* Slot and geographic address */
#define VME_REGS_MAP_SLOT 0x50UL
#define VME_REGS_MAP_SLOT_GA_MASK 0x3fUL
#define VME_REGS_MAP_SLOT_GA_SHIFT 0
#define VME_REGS_MAP_SLOT_SLOT_MASK 0x1f00UL
#define VME_REGS_MAP_SLOT_SLOT_SHIFT 8

/* Select which BR line (0-3) is used to request the VME bus */
#define VME_REGS_MAP_BRL 0x54UL
#define VME_REGS_MAP_BRL_VAL_MASK 0x3UL
#define VME_REGS_MAP_BRL_VAL_SHIFT 0

/* Bus error address */
#define VME_REGS_MAP_BERR_ADDR 0x58UL

/* Extra info on berr */
#define VME_REGS_MAP_BERR_EXTRA 0x5cUL
#define VME_REGS_MAP_BERR_EXTRA_VAM_MASK 0x3fUL
#define VME_REGS_MAP_BERR_EXTRA_VAM_SHIFT 0
#define VME_REGS_MAP_BERR_EXTRA_RW 0x40UL
#define VME_REGS_MAP_BERR_EXTRA_IACK 0x80UL

/* None */
#define VME_REGS_MAP_XLAT_LP 0x100UL
#define VME_REGS_MAP_XLAT_LP_SIZE 4 /* 0x4 */

/* None */
#define VME_REGS_MAP_XLAT_LP_VECTOR 0x0UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_VADDR_MASK 0xfffff000UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_VADDR_SHIFT 12
#define VME_REGS_MAP_XLAT_LP_VECTOR_IACK 0x800UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_SWAP 0x400UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_AM_MASK 0x3f0UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_AM_SHIFT 4
#define VME_REGS_MAP_XLAT_LP_VECTOR_WIDTH_MASK 0x3UL
#define VME_REGS_MAP_XLAT_LP_VECTOR_WIDTH_SHIFT 0

/* None */
#define VME_REGS_MAP_DBRAM 0x200UL
#define ADDR_MASK_VME_REGS_MAP_DBRAM 0x200UL
#define VME_REGS_MAP_DBRAM_SIZE 512 /* 0x200 */

#ifndef __ASSEMBLER__
struct vme_regs_map {
  /* [0x0]: REG (ro) Core ID - VMEM */
  uint32_t id;

  /* [0x4]: REG (ro) HW version */
  uint32_t version;

  /* [0x8]: REG (rw) Interrupt Id */
  uint32_t intid;

  /* [0xc]: REG (rw) Interrupts status */
  uint32_t istat;

  /* [0x10]: REG (rw) Interrupts mask */
  uint32_t imask;

  /* [0x14]: REG (rw) Control register */
  uint32_t mstr;

  /* [0x18]: REG (rw) System Control register */
  uint32_t sysc;

  /* [0x1c]: REG (rw) Upper address bits for A32 */
  uint32_t longadd;

  /* padding to: 44 Bytes */
  uint32_t __padding_0[3];

  /* [0x2c]: REG (rw) DMA status */
  uint32_t dmasta;

  /* padding to: 80 Bytes */
  uint32_t __padding_1[8];

  /* [0x50]: REG (ro) Slot and geographic address */
  uint32_t slot;

  /* [0x54]: REG (rw) Select which BR line (0-3) is used to request the VME bus */
  uint32_t brl;

  /* [0x58]: REG (ro) Bus error address */
  uint32_t berr_addr;

  /* [0x5c]: REG (ro) Extra info on berr */
  uint32_t berr_extra;

  /* padding to: 256 Bytes */
  uint32_t __padding_2[40];

  /* [0x100]: MEMORY (no description) */
  struct xlat_lp {
    /* [0x0]: REG (rw) (no description) */
    uint32_t vector;
  } xlat_lp[64];

  /* [0x200]: SUBMAP (no description) */
  uint32_t dbram[128];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__VME_REGS_MAP__H__ */
