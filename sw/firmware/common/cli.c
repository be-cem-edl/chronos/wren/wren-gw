#include <string.h>
#include "i2c.h"
#include "lib.h"
#include "board_map.h"
#include "mb_map.h"
#include "wren-mb-defs.h"
#include "qspi.h"
#include "wr_nic.h"
#include "rx_desc.h"
#include "tx_desc.h"
#include "wren-hw.h"
#include "nic.h"
#include "wren.h"
#include "wrenrx-data.h"
#include "wrenrx-core.h"
#include "board.h"
#include "armr5.h"
#include "trace.h"

struct crf_apb {
    uint32_t ERR_CTRL; //	0x0000000000	SLVERR Error Signal Enable.
    uint32_t IR_STATUS; //	0x0000000004	APB Register Address Decode
    uint32_t IR_MASK; //	0x0000000008	Interrupt Mask.
    uint32_t IR_ENABLE; //	0x000000000C	Interrupt Mask.

    uint32_t IR_DISABLE; //	0x0000000010	Interrupt Disable.
    uint32_t pad_14;
    uint32_t pad_18;
    uint32_t CRF_WPROT; //	0x000000001C	CRF_APB SLCR Write Protection.

    uint32_t APLL_CTRL; //	0x0000000020	APLL Clock Unit Control
    uint32_t APLL_CFG; //	0x0000000024	APLL Integer Helper Data
    uint32_t APLL_FRAC_CFG; //	0x0000000028	Fractional control for the PLL
    uint32_t DPLL_CTRL; //	0x000000002C	DPLL Clock Unit Control

    uint32_t DPLL_CFG; //	0x0000000030	DPLL Integer Helper Data.
    uint32_t DPLL_FRAC_CFG; //	0x0000000034	Fractional control for the PLL
    uint32_t VPLL_CTRL; //	0x0000000038	VPLL Clock Unit Control.
    uint32_t VPLL_CFG; //	0x000000003C	VPLL Integer Helper Data.

    uint32_t VPLL_FRAC_CFG; //	0x0000000040	Fractional control for the PLL.
    uint32_t PLL_STATUS; //	0x0000000044	FPD PLL Clocking Status.
    uint32_t APLL_TO_LPD_CTRL; //0x0000000048	APLL to LPD Clock Divisor.
    uint32_t DPLL_TO_LPD_CTRL; //0x000000004C	DPLL to LPD Clock Divisor.

    uint32_t VPLL_TO_LPD_CTRL; //0x0000000050	VPLL to LPD Clock Divisor.
    uint32_t pad_54;
    uint32_t pad_58;
    uint32_t pad_5c;

    uint32_t ACPU_CTRL; //	0x0000000060	APU MPCore Clock Generator
    uint32_t DBG_TRACE_CTRL; //	0x0000000064	Debug Trace Clock Generator
    uint32_t DBG_FPD_CTRL; //	0x0000000068	Debug in FPD Clock Generator
    uint32_t pad_6c;

    uint32_t DP_VIDEO_REF_CTRL; //0x0000000070	DisplayPort Video Clock Gen
    uint32_t DP_AUDIO_REF_CTRL; //0x0000000074	DisplayPort Audio Clock Gen
    uint32_t pad_78;
    uint32_t DP_STC_REF_CTRL; //  0x000000007C	DisplayPort System Time Clock

    uint32_t DDR_CTRL; //	0x0000000080	DDR Memory Controller Clock
    uint32_t GPU_REF_CTRL; //	0x0000000084	GPU Clock Generator Control.
    uint32_t pad_88;
    uint32_t pad_8c;

    uint32_t pad_90[4];

    uint32_t SATA_REF_CTRL; //	0x00000000A0	SATA Clock Generator Control.
    uint32_t pad_a4;
    uint32_t pad_a8;
    uint32_t pad_ac;

    uint32_t pad_b0;
    uint32_t PCIE_REF_CTRL; //	0x00000000B4	PCIe Clock Generator Control.
    uint32_t FPD_DMA_REF_CTRL; //0x00000000B8	FPD DMA Clock Generator Contr
    uint32_t DPDMA_REF_CTRL; //	0x00000000BC	DisplayPort DMA Clock Gen

    uint32_t TOPSW_MAIN_CTRL; //0x00000000C0	AXI Interconnect Clock Gen
    uint32_t TOPSW_LSBUS_CTRL; //0x00000000C4	APB Clock Generator Config
    uint32_t pad_c8;
    uint32_t pad_cc;

    uint32_t pad_d0[4];
    uint32_t pad_e0[4];

    uint32_t pad_f0[2];
    uint32_t DBG_TSTMP_CTRL; //	0x00000000F8	Debug Time Stamp Clock Gen
    uint32_t pad_fc;

    uint32_t RST_FPD_TOP; //	0x0000000100	Software Controlled FPD Resets.
    uint32_t RST_FPD_APU; //	0x0000000104	Software Controlled APU Resets.
    uint32_t RST_DDR_SS; //	0x0000000108	Software Controlled DDR Resets.
};

struct ams {
    uint32_t misc_ctrl;
    uint32_t _pad0[3];

    uint32_t isr_0;
    uint32_t isr_1;
    uint32_t imr_0;
    uint32_t imr_1;

    /* 0x0020 */
    uint32_t ier_0;
    uint32_t ier_1;
    uint32_t idr_0;
    uint32_t idr_1;

    uint32_t _pad1[4];

    /* 0x0040 */
    uint32_t ps_ctrl_status;
    uint32_t pl_ctrl_status;
    uint32_t _pad2[2];

    uint32_t mon_status;
    uint32_t _pad54[3];

    /* 0x0060 */
    uint32_t vcc_pspll;
    uint32_t _pad64[2];
    uint32_t vcc_psbatt;

    uint32_t _pad70[2];
    uint32_t vccint;
    uint32_t vccbram;

    /* 0x0080 */
    uint32_t vccaux;
    uint32_t vcc_psddr_pll;
    uint32_t _pad88[2];

    uint32_t _pad90[3];
    uint32_t vcc_psintfp_ddr;
};

struct plsysmon {
    uint32_t temperature;
    uint32_t supply1;
    uint32_t supply2;
    uint32_t vp_vn;

    uint32_t vrefp;
    uint32_t vrefn;
    uint32_t supply3;
    uint32_t _pad1c[1];

    uint32_t _pad20[4];

    uint32_t _pad30[1];
    uint32_t supply4;
    uint32_t supply5;
    uint32_t supply6;

    uint32_t vaux[16];

    /* 0x0080 */
    uint32_t max_temperature;
    uint32_t max_supply1;
    uint32_t max_supply2;
    uint32_t max_supply3;

    uint32_t min_temperature;
    uint32_t min_supply1;
    uint32_t min_supply2;
    uint32_t min_supply3;

    uint32_t max_supply4;
    uint32_t max_supply5;
    uint32_t max_supply6;
    uint32_t _padac;

    uint32_t min_supply4;
    uint32_t min_supply5;
    uint32_t min_supply6;
    uint32_t _padbc;

    uint32_t _padc0[12];

    uint32_t _padf0[2];
    uint32_t status_flag_1;
    uint32_t status_flag;

    /* 0x0100 */
    uint32_t config_reg0;
    uint32_t config_reg1;
    uint32_t config_reg2;
    uint32_t config_reg3;

    uint32_t config_reg4;
    uint32_t analog_bus;
    uint32_t seq_channel2;
    uint32_t seq_average2;

    uint32_t seq_channel0;
    uint32_t seq_channel1;
    uint32_t seq_average0;
    uint32_t seq_average1;

    uint32_t seq_input_mode0;
    uint32_t seq_input_mode1;
    uint32_t seq_acq0;
    uint32_t seq_acq1;

    /* 0x0140 */
    uint32_t alarm_temperature_upper;
    uint32_t alarm_supply1_upper;
    uint32_t alarm_supply2_upper;
    uint32_t alarm_ot_upper;

    uint32_t alarm_temperature_lower;
    uint32_t alarm_supply1_lower;
    uint32_t alarm_supply2_lower;
    uint32_t alarm_ot_lower;

    uint32_t alarm_supply3_upper;
    uint32_t alarm_supply4_upper;
    uint32_t alarm_supply5_upper;
    uint32_t alarm_supply6_upper;

    uint32_t alarm_supply3_lower;
    uint32_t alarm_supply4_lower;
    uint32_t alarm_supply5_lower;
    uint32_t alarm_supply6_lower;

    /* 0x0180 */
    uint32_t alarm_supply7_upper;
    uint32_t alarm_supply8_upper;
    uint32_t alarm_supply9_upper;
    uint32_t alarm_supply10_upper;

    uint32_t alarm_vccams_upper;
    uint32_t _pad194[3];

    uint32_t alarm_supply7_lower;
    uint32_t alarm_supply8_lower;
    uint32_t alarm_supply9_lower;
    uint32_t alarm_supply10_lower;

    uint32_t alarm_vccams_lower;
    uint32_t _pad1b4[3];

    uint32_t _pad1c0[4];

    uint32_t _pad1d0[4];

    uint32_t seq_input_mode2;
    uint32_t seq_acq2;
    uint32_t seq_low_rate_channel0;
    uint32_t seq_low_rate_channel1;

    uint32_t seq_low_rate_channel2;
    uint32_t _pad1f4[3];

    /* 0x0200 */
    uint32_t supply7;
    uint32_t supply8;
    uint32_t supply9;
    uint32_t supply10;

    uint32_t vccams;
    uint32_t _pad214[3];

    uint32_t _pad220[4];
    uint32_t _pad230[4];
    uint32_t _pad240[4];
    uint32_t _pad250[4];
    uint32_t _pad260[4];
    uint32_t _pad270[4];

    /* 0x0280 */
    uint32_t max_supply7;
    uint32_t max_supply8;
    uint32_t max_supply9;
    uint32_t max_supply10;

    uint32_t max_vccams;
};

static volatile struct crf_apb * const crf_apb =
    (volatile struct crf_apb *) 0xfd1a0000;

static volatile struct ams * const ams =
    (volatile struct ams *) 0xffa50000;

static volatile struct plsysmon * const plsysmon =
    (volatile struct plsysmon *) 0xffa50c00;

static volatile struct board_map * const regs =
    (volatile struct board_map *)BOARD_MAP_BASE;

static volatile struct mb_map * const mb =
    (volatile struct mb_map *)BTCM_MAP_BASE;

static void
do_mpu (char *line)
{
  unsigned v;
  unsigned nreg;
  unsigned i;

  asm ("MRC p15,0,%0,c0,c0,0" : "=r"(v));
  uart_printf("midr: %08x\n", v);

  asm ("MRC p15,0,%0,c0,c0,4" : "=r"(v));
  nreg = (v >> 8) & 0xff;
  uart_printf("mpuir: %08x, nregions=%u\n", v, nreg);

  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr: %08x\n", v);

  asm ("MRC p15,1,%0,c0,c0,1" : "=r"(v));
  uart_printf("clidr: %08x\n", v);

  asm ("MRC p15,0,%0,c1,c0,0" : "=r"(v));
  uart_printf("sctlr: %08x\n", v);

  /* Note: region 0 has lowest priority, region 15 has highest priority */
  for (i = 0; i < nreg; i++) {
    unsigned sz;
    unsigned acc;
    unsigned addr;
    const char *ca;

    /* Set index.  */
    asm volatile ("MCR p15,0,%0,c6,c2,0" : : "r"(i));
    cpu_isb();

    /* Read size and enable bit. */
    asm volatile ("MRC p15,0,%0,c6,c1,2" : "=r"(sz));
    if ((sz & 1) == 0)
      continue;

    asm volatile ("MRC p15,0,%0,c6,c1,0" : "=r"(addr));
    asm volatile ("MRC p15,0,%0,c6,c1,4" : "=r"(acc));

    uart_printf("reg %02u: base: %08x-%08x [sz=%04x, acc=%08x ",
		i, addr, addr + (2 << ((sz >> 1) & 0x1f)) - 1, sz, acc);
    switch(acc & 0x3f) {
    case 0x00:
	ca = "SO";
	break;
    case 0x10:
	ca = "NC";
	break;
    case 0x0b:
	ca = "WB";
	break;
    default:
	ca = "??";
	break;
    }
    uart_puts(ca);
    static const char * const rights[] = {
	"p:-- u:-- ",
	"p:rw u:-- ",
	"p:rw u:ro ",
	"p:rw u:rw ",

	"p:?? u:?? ",
	"p:ro u:-- ",
	"p:ro u:ro ",
	"p:?? u:?? "
    };
    uart_printf(" %s %s]\n",
		acc & (1 << 12) ? "nx" : "  ",
		rights[(acc >>  8) & 0x7]);
  }
}

static void
do_cache (char *line)
{
  unsigned v;

  asm ("MRC p15,0,%0,c1,c0,0" : "=r"(v));
  uart_printf ("sctlr: %08x", v);
  uart_printf (", mpu:%u", (v >> 0) & 1);
  uart_printf (", align:%u", (v >> 1) & 1);
  uart_printf (", D$:%u", (v >> 2) & 1);
  uart_printf (", bpred:%u", (v >> 11) & 1);
  uart_printf (", I$:%u", (v >> 12) & 1);
  uart_printf (", vect:%u", (v >> 13) & 1);
  uart_printf (", mpu-bg:%u", (v >> 17) & 1);
  uart_printf (", div0:%u", (v >> 19) & 1);
  uart_printf (", fiq0:%u", (v >> 21) & 1);
  uart_printf ("\n");

  /* MRC coproc,op1,Rt,CRn,CRm,op2 */

  asm ("MRC p15,0,%0,c0,c0,1" : "=r"(v));
  uart_printf("cache type: %08x (DMinLine: %u, IMinLine: %u)\n",
	      v, (v >> 16) & 0xf, v & 0x0f);

  asm ("MRC p15,1,%0,c0,c0,1" : "=r"(v));
  uart_printf("clidr: %08x (LoU: %u, LoC: %u CL1: D:%u, I:%u)\n",
	      v, (v >> 27) & 3, (v >> 24) & 3, (v >> 1) & 1, (v >> 0) & 1);

  asm volatile ("mcr p15,2,%0,c0,c0,0" : : "r"(0));
  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr D: %08x\n", v);

  asm volatile ("mcr p15,2,%0,c0,c0,0" : : "r"(1));
  asm ("MRC p15,1,%0,c0,c0,0" : "=r"(v));
  uart_printf("ccsidr I: %08x\n", v);
}

static void
do_tcm (char *line)
{
  unsigned v;

  asm ("MRC p15,0,%0,c0,c0,2" : "=r"(v));
  uart_printf("tcm type: %08x\n", v);

  asm ("MRC p15,0,%0,c11,c0,0" : "=r"(v));
  uart_printf("tcm slave port: %08x\n", v);

  asm ("MRC p15,0,%0,c9,c1,0" : "=r"(v));
  uart_printf("btcm: %08x en=%u sz=%uKB\n",
	      v, v & 1, 4 << (((v >> 2) & 0x1f) - 3));

  asm ("MRC p15,0,%0,c9,c1,1" : "=r"(v));
  uart_printf("atcm: %08x en=%u sz=%uKB\n",
	      v, v & 1, 4 << (((v >> 2) & 0x1f) - 3));

  unsigned actlr = r5_read_actlr();
  uart_printf("actlr: 0x%08x ECCen: %x, ERRen: %x\n",
	      actlr, (actlr >> 25) & 7, (actlr & 7));

  unsigned glbl_cntl_addr = 0xff9a0000;
  unsigned glbl_cntl = *(volatile unsigned *)glbl_cntl_addr;
  uart_printf("rpu_glbl_cntl(@%08x): 0x%08x tcm_comb: %x\n",
	      glbl_cntl_addr, glbl_cntl, (glbl_cntl >> 6) & 1);
}

static void
do_sysmon (char *args)
{
    char *arg;

    arg_next_string(&args, &arg);
    if (arg == NULL) {
	unsigned v;

	v = plsysmon->temperature;
	/* Cf ug580 p41 */
	v = ((v * 50931) >> 16) - 18023;
	uart_printf("pl temp:          %d.%02uC\n", (v / 100) - 100, v % 100);

	v = plsysmon->supply1;
	v = (v * 3000) >> 16;
	uart_printf("pl vccint:        %u.%03uV\n", v / 1000, v % 1000);
	v = plsysmon->supply2;
	v = (v * 3000) >> 16;
	uart_printf("pl vccaux:        %u.%03uV\n", v / 1000, v % 1000);
    }
    else if (strcmp (arg, "-r") == 0) {
	uart_printf("ams:\n");
	uart_printf("misc_ctrl:       %08x\n", (unsigned)ams->misc_ctrl);
	uart_printf("ps_ctrl_status:  %08x\n", (unsigned)ams->ps_ctrl_status);
	uart_printf("pl_ctrl_status:  %08x\n", (unsigned)ams->pl_ctrl_status);
	uart_printf("mon_status:      %08x\n", (unsigned)ams->mon_status);
	uart_printf("vcc_pspll:       %08x\n", (unsigned)ams->vcc_pspll);
	uart_printf("vcc_psbatt:      %08x\n", (unsigned)ams->vcc_psbatt);
	uart_printf("vccaux:          %08x\n", (unsigned)ams->vccaux);
	uart_printf("vcc_psddr_pll:   %08x\n", (unsigned)ams->vcc_psddr_pll);
	uart_printf("vcc_psintfp_ddr: %08x\n", (unsigned)ams->vcc_psintfp_ddr);

	uart_printf("plsysmon:\n");
	uart_printf("pl temp:          %08x\n",
		    (unsigned)plsysmon->temperature);
	uart_printf("pl vccint:        %08x\n", (unsigned)plsysmon->supply1);
	uart_printf("pl vccaux:        %08x\n", (unsigned)plsysmon->supply2);
	uart_printf("pl vccbram:       %08x\n", (unsigned)plsysmon->supply3);
	uart_printf("pl vcc_psintlp:   %08x\n", (unsigned)plsysmon->supply4);
	uart_printf("pl vcc_psintfp:   %08x\n", (unsigned)plsysmon->supply5);
	uart_printf("pl vcc_psaux:     %08x\n", (unsigned)plsysmon->supply6);
	uart_printf("pl status_flag_1: %08x\n",
		    (unsigned)plsysmon->status_flag_1);
	uart_printf("pl status_flag:   %08x\n",
		    (unsigned)plsysmon->status_flag);
	uart_printf("pl config_reg0:   %08x\n",
		    (unsigned)plsysmon->config_reg0);
	uart_printf("pl config_reg1:   %08x\n",
		    (unsigned)plsysmon->config_reg1);
	uart_printf("pl config_reg2:   %08x\n",
		    (unsigned)plsysmon->config_reg2);
	uart_printf("pl config_reg4:   %08x\n",
		    (unsigned)plsysmon->config_reg4);
    }
    else {
	uart_printf("usage: sysmon [-a]\n");
    }
}

static void
do_except (char *args)
{
#define BAD_ADDR 0xf0080000
    char *arg;

    arg_next_string(&args, &arg);
    if (arg == NULL)
      uart_printf("missing type\n");
    else if (strcmp(arg, "undef") == 0)
      asm volatile (".word 0xe1400070");  /* hvc */
    else if (strcmp(arg, "svc") == 0)
      asm volatile ("svc #0");
    else if (strcmp(arg, "pabt") == 0)
      ((void (*)(void))BAD_ADDR)();
    else if (strcmp(arg, "rd") == 0) {
      *(volatile unsigned *)BAD_ADDR;
    }
    else
      uart_printf("unknown type\n");
}

static void
do_csr(char *args)
{
    uint32_t ver;
    uint32_t csr;

    ver = regs->map_version;
    csr = mb->h2b_csr;
    uart_printf("version=%08lx, b2h.csr=%x, h2b.csr=%x, mem_b2h[0]=%x\n",
		ver, (unsigned)mb->b2h_csr,
		(unsigned)csr,
		(unsigned)mb->b2h_data[0]);
}

static void
do_inputs(char *args)
{
    char *arg;

    arg_next_string(&args, &arg);
    if (arg == NULL) {
	unsigned i;
	unsigned j;

	for (j = 0; j < 3; j++) {
	    uart_printf("en[%u]:   0x%08x\n",
			j, (unsigned)regs->inputs.csr[j].en);
	    uart_printf("rise[%u]: 0x%08x\n",
			j, (unsigned)regs->inputs.csr[j].rise_sr);
	    uart_printf("fall[%u]: 0x%08x\n",
			j, (unsigned)regs->inputs.csr[j].fall_sr);
	}
	
	for (i = 0; i < 32; i++) {
	    volatile struct inputs_ts *ts = &regs->inputs.ts[i];
	    uart_printf("TS %2u: R: 0x%08xs 0x%08xns, F: 0x%08xs 0x%08xns\n",
			i,
			(unsigned)ts->rise_sec,
			(unsigned)ts->rise_ns,
			(unsigned)ts->fall_sec,
			(unsigned)ts->fall_ns);
	}
    }
    else if (!strcmp(arg, "clear")) {
	unsigned j;
	for (j = 0; j < 3; j++)
	    regs->inputs.csr[j].rise_sr |= ~0U;
    }
    else if (!strcmp(arg, "en")) {
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else
	    regs->inputs.csr[val/32].en |= 1 << (val % 32);
    }
    else
	uart_printf("usage: inputs [en VAL] | [clear]\n");
}

static unsigned arg_addr = BOARD_MAP_BASE;
static unsigned arg_len = 256;

static int
arg_memory(const char *name, char *args)
{
  if (args != NULL && arg_next_number(&args, &arg_addr) != 0)
    {
      uart_puts ("address expected\n");
      return -1;
    }
  if (args != NULL && arg_next_number(&args, &arg_len) != 0)
    {
      uart_puts ("length expected\n");
      return -1;
    }
  uart_printf ("%s at 0x%x [%u]:\n", name, arg_addr, arg_len);
  return 0;
}

void
dump_w32(void *addr, unsigned len)
{
  unsigned i, j;
  for (i = 0; i < len; i += 16)
    {
      uart_printf("%08x:", (unsigned)addr + i);
      for (j = 0; j < 16 && i + j < len; j += 4)
        {
	    unsigned val = read32((unsigned)addr + i + j);
          uart_printf(" %08x", val);
        }
      uart_putc('\n');
    }
}

static void
do_dump(char *args)
{
  if (arg_memory("dump", args) != 0)
    return;

  dump_w32((void *)arg_addr, arg_len);

  arg_addr += arg_len;
}

static void
do_w32 (char *args)
{
  unsigned addr;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &addr) != 0)
    {
      uart_puts ("address expected\n");
      return;
    }
  if (args != NULL && arg_next_number(&args, &val) != 0)
    {
      uart_puts ("value expected\n");
      return;
    }
  uart_printf("[%08x] <- 0x%08x\n", addr, val);
  write32(addr, val);
}

static void
do_rd (char *args)
{
    unsigned addr;
    unsigned val;
    unsigned wd = 4;
    char *arg;

    arg_next_string(&args, &arg);

    if (arg != NULL && arg[0] == '-') {
	if (strcmp(arg, "-1") == 0)
	    wd = 1;
	else if (strcmp(arg, "-2") == 0)
	    wd = 2;
	else if (strcmp(arg, "-4") == 0)
	    wd = 4;
	else {
	    uart_printf("unknown option '%s'\n", arg);
	    return;
	}
	arg_next_string(&args, &arg);
    }

    if (arg == NULL || strtou(arg, &addr) != 0) {
	uart_puts ("address expected\n");
	return;
    }

    uart_printf("[%08x] -> 0x", addr);

    switch(wd) {
    case 4:
	val = read32(addr);
	uart_printf("%08x\n", val);
	break;
    case 2:
	val = read16(addr);
	uart_printf("%04x\n", val);
	break;
    case 1:
	val = read8(addr);
	uart_printf("%02x\n", val);
	break;
    }
}

static void
do_rio32 (char *args)
{
  unsigned addr;
  unsigned val;

  if (args != NULL && arg_next_number(&args, &addr) != 0)
    {
      uart_puts ("address expected\n");
      return;
    }
  r5_dccimvac(addr);
  cpu_isb();
  val = read32(addr);
  uart_printf("[%08x] -> 0x%08x\n", addr, val);
}

/* Some figure:

   ATCM:
   readspeed at 0x00000000 len=0x00000800 repeat=16
   time: 151 cycles

   ATCM, through LPD:
   readspeed at 0xffe00000 len=0x00000800 repeat=16
   time: 6164 cycles

   DDR (cached):
   readspeed at 0x00040000 len=0x00000800 repeat=16
   time: 158 cycles

   OCM (cached):
   readspeed at 0xffff0000 len=0x00000800 repeat=16
   time: 152 cycles

   FPGA (not cached):
   readspeed at 0x8000a000 len=0x00000800 repeat=16
   time: 19459 cycles
*/
static void
do_readspeed (char *args)
{
    unsigned addr, len, repeat;
    unsigned i, j;
    unsigned t0, t1;
    volatile unsigned *p;

    if (args == NULL || arg_next_number(&args, &addr) != 0) {
	uart_puts ("usage: readspeed ADDR [LEN] [REPEAT]\n");
	return;
    }
    len = 2048;
    if (args != NULL && arg_next_number(&args, &len) != 0) {
	uart_puts ("invalid length\n");
	return;
    }
    repeat = 16;
    if (args != NULL && arg_next_number(&args, &repeat) != 0) {
	uart_puts ("invalid length\n");
	return;
    }
    uart_printf("readspeed at 0x%08x len=0x%08x repeat=%u\n",
		addr, len, repeat);
    p = (volatile unsigned *)addr;
    len /= 8 * 4;
    t0 = get_cpu_cycles();
    for (i = 0; i < repeat; i++)
	for (j = 0; j < len; j++) {
	    (void)p[j + 0];
	    (void)p[j + 1];
	    (void)p[j + 2];
	    (void)p[j + 3];
	    (void)p[j + 4];
	    (void)p[j + 5];
	    (void)p[j + 6];
	    (void)p[j + 7];
	}
    t1 = get_cpu_cycles();
    uart_printf("time: %u cycles\n", t1 - t0);
}

static void
do_led (char *args)
{
    char *s;
    unsigned off;
    unsigned val;

    if (args == NULL || arg_next_string(&args, &s) != 0) {
	for (unsigned i = 0; i < 64; i++)
	    uart_printf("led[%02u] = %06x\n",
			i, (unsigned)regs->leds.colors[i].rgb);
	return;
    }
    if (strcmp (s, "init") == 0) {
	for (unsigned i = 0; i < 16; i++)
	    regs->leds.colors[i].rgb = 0;
	for (unsigned i = 0; i < 16; i++) {
	    regs->leds.colors[i].rgb = 0x404040;
	    usleep(200000);
	    regs->leds.colors[i].rgb = 0;
	}
	return;
    }
    if (strcmp (s, "trail") == 0) {
	for (unsigned i = 0; i < 16; i++)
	    regs->leds.colors[i].rgb = 0;
	for (unsigned i = 0; i < 20; i++) {
	    if (i < 16)
		regs->leds.colors[i].rgb = 0x404040;
	    if (i > 0 && i < 17)
		regs->leds.colors[i-1].rgb = 0x400000;
	    if (i > 1 && i < 18)
		regs->leds.colors[i-2].rgb = 0x004000;
	    if (i > 2 && i < 19)
		regs->leds.colors[i-3].rgb = 0x000040;
	    if (i > 3 && i < 20)
		regs->leds.colors[i-4].rgb = 0x000000;
	    usleep(200000);
	}
	return;
    }
    if (strcmp (s, "smooth") == 0) {
	for (unsigned i = 0; i < 16; i++)
	    regs->leds.colors[i].rgb = 0;
	for (unsigned i = 0; i < 25; i++) {
	    if (i < 16)
		regs->leds.colors[i-0].rgb = 0x000010;
	    if (i > 0 && i < 17)
		regs->leds.colors[i-1].rgb = 0x001020;
	    if (i > 1 && i < 18)
		regs->leds.colors[i-2].rgb = 0x102040;
	    if (i > 2 && i < 19)
		regs->leds.colors[i-3].rgb = 0x204040;
	    if (i > 3 && i < 20)
		regs->leds.colors[i-4].rgb = 0x404040;
	    if (i > 4 && i < 21)
		regs->leds.colors[i-5].rgb = 0x404020;
	    if (i > 5 && i < 22)
		regs->leds.colors[i-6].rgb = 0x402010;
	    if (i > 6 && i < 23)
		regs->leds.colors[i-7].rgb = 0x201000;
	    if (i > 7 && i < 24)
		regs->leds.colors[i-8].rgb = 0x100000;
	    if (i > 8 && i < 25)
		regs->leds.colors[i-9].rgb = 0x000000;
	    usleep(200000);
	}
	return;
    }
    if (strcmp (s, "force") == 0) {
	unsigned val;
	if (args == NULL || arg_next_number(&args, &val) != 0)
	    uart_printf("force: 0x%x\n", (unsigned)regs->leds.force);
	else
	    regs->leds.force = val;
	return;
    }
    if (strtou(s, &off) != 0) {
	uart_puts ("offset expected\n");
	return;
    }
    if (args != NULL && arg_next_number(&args, &val) != 0) {
	uart_puts ("value expected\n");
	return;
    }
    off &= 63;
    regs->leds.colors[off].rgb = val;
}

static void
do_version (char *args)
{
  uart_printf("Software: 0x00000000 built on " __DATE__" " __TIME__ "\n");
  uart_printf("Map version: 0x%08x\n", (unsigned)regs->map_version);
}

static void
do_rx (char *args)
{
    volatile struct nic *nic = (volatile struct nic *)regs->wrnic;
    volatile struct rx_desc *rx_desc =(volatile struct rx_desc *)nic->drx;
    unsigned char *RX_BUF = (unsigned char *)nic->mem;
    unsigned i;
    unsigned rxd;

	/* Initialize descriptors.  */
	uart_puts("init...\n");
	nic->CR = NIC_CR_SW_RST;

	/* 2KB per descriptor (so 16KB in total). */
	for (i = 0; i < 8; i++) {
		rx_desc[i].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
			| (i << 11);
		rx_desc[i].flags = RX_DESC_FLAGS_EMPTY;
	}

	/* Enable RX.  */
	nic->CR = NIC_CR_RX_EN;

	uart_printf ("NIC %08x SR: %08x, CR: %08x\n",
		     (unsigned)nic, (unsigned)nic->SR, (unsigned)nic->CR);

	for (i = 0; i < 8; i++) {
	  uart_printf("RX desc %u: flags: %08x, buf: %08x\n",
		      i, (unsigned)rx_desc[i].flags,
		      (unsigned)rx_desc[i].buf);
	}

	uart_puts("wait for packets...\n");
	rxd = 0;
	while (1 || rxd < 8) {
		unsigned sr;

		/* Wait for packet.  */
		do {
			sr = nic->SR;

			if (uart_can_read()) {
			  uart_raw_getc();
			  uart_puts("Stop\n");
			  return;
			}
		} while ((sr & (NIC_SR_BNA | NIC_SR_REC)) == 0);

		/* Clear status bits.  */
		nic->SR = sr & (NIC_SR_BNA | NIC_SR_REC);

		unsigned cur_rx = (nic->SR & NIC_SR_CUR_RX_DESC_MASK)
		  >> NIC_SR_CUR_RX_DESC_SHIFT;

		/* Disp all received packets.  */
		while (cur_rx != rxd) {
		  /* Disp packet.  */
		  unsigned rxd_buf = rx_desc[rxd].buf;
		  unsigned b =
		      ((unsigned)RX_BUF + (rxd_buf & RX_DESC_BUF_OFFSET_MASK));
		  unsigned l =
		    (rxd_buf & RX_DESC_BUF_LEN_MASK) >> RX_DESC_BUF_LEN_SHIFT;
		  uart_printf ("SR: %08x, rx-desc: %u ", sr, rxd);
		  uart_printf("flags: %08x, buf: %08x (adr=%08x, l=%u)\n ",
			      (unsigned)rx_desc[rxd].flags, rxd_buf,
			      b, l);

		  for (unsigned j = 0; j < 72 && j < l + 2; j += 1) {
		    switch (j) {
		    case 0:
		      break;
		    case 2:  // eth daddr
		    case 2 + 6: // eth saddr
		    case 2 + 6 + 6:  // eth type
		    case 2 + 6 + 6 + 2: // eth payload (ip hdr + frag)
		    case 2 + 14 + 12: // ip daddr
		    case 2 + 14 + 16: // ip saddr
		    case 2 + 14 + 20: // ip payload
		      uart_putc('.');
		      break;
		    case 2 + 14 + 8:  // ip protocol
		    case 48:
		      uart_puts("\n ");
		      break;
		    default:
		      uart_putc(' ');
		      break;
		    }
		    uart_printf ("%02x", *(unsigned char *)(b + j));
		  }
		  uart_putc('\n');

		  /* Reset descriptor.  */
		  rx_desc[rxd].buf = ((1536 + 8) << RX_DESC_BUF_LEN_SHIFT)
		    | (rxd << 11);
		  rx_desc[rxd].flags = RX_DESC_FLAGS_EMPTY;

		  rxd = (rxd + 1) & 7;
		}
	}
}

static void
do_tx (char *args)
{
    volatile struct nic *nic = (volatile struct nic *)regs->wrnic;
    volatile struct tx_desc *tx_desc =(volatile struct tx_desc *)nic->dtx;
    unsigned char *nic_mem = (unsigned char *)nic->mem;
    unsigned i;
    unsigned txd;

	/* Initialize descriptors.  */
	uart_puts("init...\n");
	nic->CR = NIC_CR_SW_RST;

	/* 2KB per descriptor (so 16KB in total). */
	for (i = 0; i < 8; i++) {
		tx_desc[i].flags = 0;
		tx_desc[i].dest = 1;
	}

	uart_printf ("NIC %08x SR: %08x, CR: %08x\n",
		     (unsigned)nic, (unsigned)nic->SR, (unsigned)nic->CR);

	txd = 0;
	while (1) {
		unsigned isr;
		unsigned len = 48;
		unsigned off =  (1 << 14) + (txd << 11);
		volatile unsigned char *b = nic_mem + off;
		unsigned i;

		/* Data offset for wr_nic (the first half-word is ignored) */
		b[0] = 0x00;
		b[1] = 0x00;

		/* Dest: broadcast */
		b[2] = 0xf0;
		b[3] = 0xf1;
		b[4] = 0xf2;
		b[5] = 0xf3;
		b[6] = 0xf4;
		b[7] = 0xf5;
		/* Src: xx */
		b[8] = 0xa0;
		b[9] = 0x22;
		b[10] = 0x33;
		b[11] = 0x44;
		b[12] = 0x55;
		b[13] = 0x66;
		/* Proto */
		b[14] = 'W';
		b[15] = 'e';
		/* Content */
		for (i = 16; i < len; i++)
			b[i] = i;

		/* Write len.  */
		tx_desc[txd].buf = (len << TX_DESC_BUF_LEN_SHIFT)
			| (off << TX_DESC_BUF_OFFSET_SHIFT);
		tx_desc[txd].flags = TX_DESC_FLAGS_READY
			| TX_DESC_FLAGS_PAD_E;

		nic->eic.eic_ier = NIC_EIC_EIC_IER_TCOMP
		    | NIC_EIC_EIC_IER_TXERR;

		/* Enable TX.  */
		nic->CR = NIC_CR_TX_EN;

		/* Wait for packet.  */
		do {
		    isr = nic->eic.eic_isr;
		    uart_printf ("SR: %08x, desc: %u@0x%08x, ",
				 (unsigned)nic->SR,
				 txd, (unsigned)&tx_desc[txd]);
		    uart_printf("flags: %08x, buf: %08x, isr: %02x\n",
				(unsigned)tx_desc[txd].flags,
				(unsigned)tx_desc[txd].buf, isr);


#if 1
		    if (uart_can_read()) {
			uart_raw_getc();
			uart_puts("Stop\n");
			return;
		    }
		    usleep(1000000);
#endif
		} while ((isr & (NIC_EIC_EIC_ISR_TCOMP
				 | NIC_EIC_EIC_ISR_TXERR)) == 0);

		/* Clear status bits.  */
		nic->SR |= (NIC_SR_TX_DONE | NIC_SR_TX_ERROR);
		nic->eic.eic_isr |= NIC_EIC_EIC_ISR_TCOMP
		    | NIC_EIC_EIC_ISR_TXERR;

		txd = (txd + 1) & 7;

		uart_puts("Press 's' to send another packet...\n");

		while (!uart_can_read())
		    ;
		if (uart_raw_getc() != 's') {
		    uart_puts("Stop\n");
		    break;
		}

	}
}

static void
do_gen_int (char *args)
{
    uart_printf("generate interrupt\n");

    host_msg_doorbell();
}

static void
do_wr (char *args)
{
    uart_printf("WR state: %x\n", (unsigned) regs->wr_state);
}

static void
do_time (char *args)
{
    volatile struct host_map *host_regs =
	(volatile struct host_map *)HOST_MAP_BASE;
    uart_printf("tai: %02x %08x + %u cyc\n",
		(unsigned)regs->tm_tai_hi, (unsigned)regs->tm_tai_lo,
		(unsigned)regs->tm_cycles);
    uart_printf("tai compact: %08x\n", (unsigned)host_regs->intc.tm_compact);
    uart_printf("timer:       %08x\n", (unsigned)host_regs->intc.tm_timer);

    uart_printf("pmc cntr:    %u\n", get_cpu_cycles());
}

#define FPD_XMPU_CFG 0x00FD5D0000

static void
do_xmpu (char *args)
{
    volatile unsigned *xmpu =
	(volatile unsigned *)FPD_XMPU_CFG;
    volatile unsigned *xmpu_sink =
	(volatile unsigned *)0x00FD4FFF00;
    unsigned int i;
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	uart_printf ("CTRL: %08x, ISR: %02x, sink: %08x\n",
		     xmpu[0], xmpu[4], *xmpu_sink);
	uart_printf ("ERR AXI_ADDR: %08x, AXI_ID: %03x\n", xmpu[1], xmpu[2]);
	for (i = 0; i < 4; i++) {
	    volatile unsigned *r = &xmpu[0x40 + i * 4];
	    uart_printf ("R%u (@%x): 0x%08x-0x%08x mst: %08x cfg: %02x\n",
			 i, (unsigned)r, r[0], r[1], r[2], r[3]);
	}
	return;
    }

    if (!strcmp(cmd, "dis")) {
	/* Disable pcie main */
	volatile unsigned *r = &xmpu[0x40];
	r[0] = 0xfd0f0000 >> 12;
	r[1] = 0xfd0f0000 >> 12;
	r[2] = 0; /* For all id */
	r[3] = 9; /* NonSecure + EN */
    }
    else if (!strcmp(cmd, "en")) {
	/* Disable pcie main */
	volatile unsigned *r = &xmpu[0x40];
	r[3] = 0;
    }
    else if (!strcmp(cmd, "ack")) {
	unsigned isr = xmpu[4];
	xmpu[4] = isr;
    }
    else
	uart_printf("usage: xmpu [en|dis]\n");
}

static void
do_intc (char *args)
{
    volatile struct host_map *host_regs =
	(volatile struct host_map *)HOST_MAP_BASE;
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	uart_printf("isr_raw: %x, imr: %x, isr: %x\n",
		    (unsigned)host_regs->intc.isr_raw,
		    (unsigned)host_regs->intc.imr,
		    (unsigned)host_regs->intc.isr);
    }
    else if (!strcmp(cmd, "ack")) {
	host_regs->intc.iack = 0xff;
    }
    else
	uart_printf("usage: intc [ack]\n");
}

static void
do_rx_src (char *args)
{
    for (unsigned i = 0; i < MAX_RX_SOURCES; i++) {
	struct wren_rx_source *src = &wren_rx_sources[i];
	uart_printf ("%02u: ", i);
	if (src->proto.proto == WREN_PROTO_NONE)
	    uart_printf("unused\n");
	else if (src->proto.proto == WREN_PROTO_ETHERNET) {
	    uart_printf("mac: %02x:%02x:%02x:%02x:%02x:%02x",
			src->proto.u.eth.mac[5],
			src->proto.u.eth.mac[4],
			src->proto.u.eth.mac[3],
			src->proto.u.eth.mac[2],
			src->proto.u.eth.mac[1],
			src->proto.u.eth.mac[0]);
	    uart_printf (" ethtype: %02x, dom-id: %02x\n",
			 src->proto.u.eth.ethertype,
			 src->proto.u.eth.domain_id);
	    uart_printf ("  nbr_frames: %u, subsample: %u, seqid: %04x\n",
			 (unsigned)src->nbr_frames,
			 (unsigned)src->subsample,
			 (unsigned)src->next_seqid);
	}
    }
}

/* Note: h1 must be set before frev. eg:
   rf1 h1 4620
   rf1 frev 43290
*/
static void
do_rf (unsigned idx, char *args)
{
    char *cmd;

    if (args == NULL || arg_next_string(&args, &cmd) != 0) {
	unsigned v;
	uint64_t ftw;

	v = (unsigned)regs->rf[idx].gth_cfg;
	uart_printf("rf[%u]\n", idx);
	uart_printf("cfg: %08x\n", v);

	v = (unsigned)regs->rf[idx].gth_sta;
	uart_printf("sta: %08x\n", v);
	if (v & (1 << 0))
	    uart_printf (" usr-clk-tx-act");
	if (v & (1 << 1))
	    uart_printf (" usr-clk-rx-act");
	if (v & (1 << 2))
	    uart_printf (" rx-cdr");
	if (v & (1 << 3))
	    uart_printf (" rst-tx-done");
	if (v & (1 << 4))
	    uart_printf (" rst-rx-done");
	if (v & (1 << 5))
	    uart_printf (" gt-pow-good");
	if (v & (1 << 6))
	    uart_printf (" rx-pma-rst-done");
	if (v & (1 << 7))
	    uart_printf (" tx-pma-rst-done");
	uart_printf("\n");

	uart_printf("bst-ctrl: 0x%08x\n", (unsigned)regs->rf[idx].bst_ctrl);

	v = (unsigned)regs->rf[idx].h1;
	uart_printf("h1: %u\n", v);

	ftw = (((uint64_t)regs->rf[idx].ftw_hi) << 32) | regs->rf[idx].ftw_lo;
	uart_printf("ftw: %012llx\n", ftw);
	uart_printf("ftw*h1: %012llx\n", ftw*v);
	uart_printf("ftw freq: %llu\n", (125000000 * (ftw >> 16)) >> 32);

	ftw = (((uint64_t)(regs->rf[idx].rf_ftw
			   & BOARD_MAP_RF_RF_FTW_HI_MASK) << 32)
	       | regs->rf[idx].rf_ftw_lo);
	uart_printf("rf-ftw: %016llx\n", ftw);
	uart_printf("rf-ftw freq: %llu\n", 125000000 * (ftw >> 16) >> 32);

	uart_printf("rf-time: tai: %08x, cyc: %08x\n",
		    (unsigned)regs->rf[idx].rf_rx_tai,
		    (unsigned)regs->rf[idx].rf_rx_cyc);
	uart_printf("rf-sta: %x\n",
		    (unsigned)(regs->rf[idx].rf_rx_sta
			       & BOARD_MAP_RF_RF_RX_STA_VALID));
	return;
    }

    if (!strcmp(cmd, "clk-rst")) {
	regs->rf[idx].gth_cfg = 0x03;
	uart_printf("rx+tx clk reset\n");
	regs->rf[idx].gth_cfg = 0x00;
    }
    else if (!strcmp(cmd, "+rst-tx")) {
	regs->rf[idx].gth_cfg |= (1 << 4);
    }
    else if (!strcmp(cmd, "-rst-tx")) {
	regs->rf[idx].gth_cfg &= ~(1 << 4);
    }
    else if (!strcmp(cmd, "+bst")) {
	regs->rf[idx].bst_ctrl |= BOARD_MAP_RF_BST_CTRL_EN;
    }
    else if (!strcmp(cmd, "-bst")) {
	regs->rf[idx].bst_ctrl &= ~BOARD_MAP_RF_BST_CTRL_EN;
    }
    else if (!strcmp(cmd, "rst")) {
	regs->rf[idx].gth_cfg = 0x04;
	uart_printf("rx+tx all reset\n");
	regs->rf[idx].gth_cfg = 0x00;
    }
    else if (!strcmp(cmd, "frev")) {
	unsigned long long nco, vall;
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else {
	    vall = val;
	    nco = (vall << (48 - 4)) / (125000000 >> 4);
	    regs->rf[idx].ftw_hi = nco >> 32;
	    regs->rf[idx].ftw_lo = nco;
	    regs->rf[idx].ftw_load =
		BOARD_MAP_RF_FTW_LOAD_EN | BOARD_MAP_RF_FTW_LOAD_RESET;
	}
    }
    else if (!strcmp(cmd, "h1")) {
	unsigned val;
	if (arg_next_number(&args, &val) != 0)
	    uart_printf("missing number\n");
	else
	    regs->rf[idx].h1 = val;
    }
    else
	uart_printf("unknown command %s\n", cmd);
}

static void
do_rf1 (char *args)
{
    do_rf(0, args);
}

static void
do_rf2 (char *args)
{
    do_rf(1, args);
}

static void
do_rf3 (char *args)
{
    do_rf(2, args);
}

static void
do_i2c (char *args)
{
    unsigned val;

    if (arg_next_number(&args, &val) != 0) {
	for (unsigned addr = 1; addr < 128; addr++) {
	    if (i2c_recv(NULL, 0, addr) == 0)
		uart_printf("i2c slave at 0x%02x\n", addr);
	}
    }
    else {
	uint8_t buf = 0;
	if (i2c_send(&buf, 1, val) == 0)
	    uart_printf("i2c slave at 0x%02x\n", val);
    }

}

static void
do_pmu (char *args)
{
    struct reg_desc {
	const char *name;
	unsigned addr;
    };
    static const struct reg_desc regs[] = {
	{ "error_status_1", 0x00FFD80530},
	{ "error_status_2", 0x00FFD80540},
	{ "error_por_mask_1", 0x00FFD80550},
	{ "error_por_mask_2", 0x00FFD8055C},
	{ "error_srst_mask_1", 0x00FFD8055C},
	{ "error_srst_mask_2", 0x00FFD80574},
	{ "error_sig_mask_1", 0x00FFD80580},
	{ "error_sig_mask_2", 0x00FFD8058C},
	{ "csu_multi_boot", 0xffca0010},
	{ "csu_br_error", 0xffd80528},
	{ "boot_mode_user", 0xff5e0200},
	{ "boot_mode_por", 0xff5e0204},
	{ "reset_ctrl", 0xff5e0218},
	{ "reset_reason", 0xff5e0220},
	{ NULL, 0}
    };
    const struct reg_desc *r;

    for (r = regs; r->name; r++) {
	uint32_t val;
	val = read32(r->addr);
	uart_printf("%s: 0x%08x\n", r->name, (unsigned)val);
    }
}

static void
disp_pspll(const char *name, unsigned ctrl, unsigned cfg)
{
    uart_printf("%s: ctrl=0x%08x, cfg=0x%08x\n", name, ctrl, cfg);
}

static void
do_crf(char *args)
{
    disp_pspll("apll", crf_apb->APLL_CTRL, crf_apb->APLL_CFG);
    disp_pspll("dpll", crf_apb->DPLL_CTRL, crf_apb->DPLL_CFG);
    disp_pspll("vpll", crf_apb->VPLL_CTRL, crf_apb->VPLL_CFG);
    uart_printf("pll-status:    0x%08x\n", (unsigned)crf_apb->PLL_STATUS);
    uart_printf("pcie_ref_ctrl: 0x%08x\n", (unsigned)crf_apb->PCIE_REF_CTRL);
    uart_printf("rst_fpd_top:   0x%08x\n", (unsigned)crf_apb->RST_FPD_TOP);
}

static void
do_otp(char *args)
{
    qspi_dump();
}

static void
do_cond_log(char *args)
{
    char *arg;
    char act;
    unsigned cond;

    if (args == NULL || arg_next_string(&args, &arg) != 0) {
	return;
    }

    if (arg[0] == '+' || arg[0] == '-') {
	act = arg[0];
	arg++;
    }
    else
	act = ' ';
    if (strtou(arg, &cond) != 0 || cond >= MAX_RX_ACTIONS)
	uart_printf("bad value: %s\n", arg);
    if (act == ' ')
	uart_printf("cond %u: %u\n", cond, wren_rx_conds[cond].log);
    wren_rx_conds[cond].log = (act == '+');
}

static void
do_log(char *args)
{
    char *arg;
    if (args == NULL || arg_next_string(&args, &arg) != 0) {
	uart_printf("log flags: 0x%08x\n", log_flags);
#if 1
#define def_trace(NAME, VAL)					\
	uart_printf("%08x %s\n", 1 << VAL, #NAME);
#include "trace.def"
#undef def_trace
#endif
	return;
    }
    if (arg[0] >= '0' && arg[0] <= '9') {
	unsigned val;
	if (strtou(arg, &val) != 0)
	    uart_printf("bad value: %s\n", arg);
	else
	    log_flags = val;
	return;
    }
}

static void do_help(char *args);

static const struct menu_item top_menu[] =
  {
   {"help", do_help },
   {"dump", do_dump },
   {"w32", do_w32},
   {"rd", do_rd},
   {"rio32", do_rio32},
   {"readspeed", do_readspeed},
   {"led", do_led},
   {"mpu", do_mpu },
   {"cache", do_cache },
   {"tcm", do_tcm },
   {"sysmon", do_sysmon },
   {"except", do_except },
   {"csr", do_csr },
   {"inputs", do_inputs },
   {"gen-int", do_gen_int},
   {"nic", do_nic},
   {"rx", do_rx},
   {"tx", do_tx},
   {"wr", do_wr},
   {"time", do_time},
   {"xmpu", do_xmpu},
   {"pmu", do_pmu},
   {"crf", do_crf},
   {"intc", do_intc},
   {"ver", do_version},
   {"otp", do_otp},
   {"rx-src", do_rx_src},
   {"rf1", do_rf1},
   {"rf2", do_rf2},
   {"rf3", do_rf3},
   {"i2c", do_i2c},
   {"log", do_log},
   {"cond-log", do_cond_log},
#ifdef BOARD_MENU
   BOARD_MENU
#endif
   {NULL, NULL}
  };

static void
do_help(char *args)
{
  const struct menu_item *m;

  uart_puts("commands:\n");
  for (m = top_menu; m->name; m++)
    uart_printf (" %s\n", m->name);
}

void
cmd_cli(char *buf)
{
  menu_execute (buf, top_menu);
}

void
cli_menu(void)
{
    while (1) {
	char buf[32];

	uart_readline("cmd> ", uart_getc, buf, sizeof (buf));
	if (buf[0] == 'q' && buf[1] == 0)
	    break;
	menu_execute (buf, top_menu);
    }
}
