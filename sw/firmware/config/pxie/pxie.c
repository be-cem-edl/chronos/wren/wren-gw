#include <string.h>
#include "lib.h"
#include "board.h"
#include "i2c.h"
#include "pcie-core.h"
#include "si5340-load.h"
#include "board_map.h"
#include "wren-hw.h"

void board_pxie_pll_init(void)
{
    /* Deassert PLL rstn */
    pll_reset();

    si5340_init();
}

void board_pxie_init(void)
{
    i2c_init();
    pcie_init ();
}

static volatile struct board_map * const regs =
  (volatile struct board_map *)BOARD_MAP_BASE;
 
static unsigned char dstarc_map[] =
{
    /*  0 */ (1 << 0) | (1 << 6),
    /*  1 */ (3 << 0) | (1 << 6),
    /*  2 */ (7 << 0) | (1 << 6),
    /*  3 */ (6 << 0) | (1 << 6),
    /*  4 */ (2 << 3) | (2 << 6),
    /*  5 */ (4 << 3) | (2 << 6),
    /*  6 */ (6 << 3) | (2 << 6),
    /*  7 */ (1 << 3) | (2 << 6),
    /*  8 */ (0 << 0) | (1 << 6),
    /*  9 */ (2 << 0) | (1 << 6),
    /* 10 */ (4 << 0) | (1 << 6),
    /* 11 */ (5 << 0) | (1 << 6),
    /* 12 */ (0 << 3) | (2 << 6),
    /* 13 */ (3 << 3) | (2 << 6),
    /* 14 */ (5 << 3) | (2 << 6),
    /* 15 */ (7 << 3) | (2 << 6),
    /* 16 */ (3 << 6)
};

void
do_pxie(char *args)
{
    char *arg;

    arg_next_string(&args, &arg);
    if (arg == NULL) {
       unsigned v;
       unsigned i;

       v = regs->pxie_dstarc;
       uart_printf("dstar-c: 0x%08x (slot ", v);
       for (i = 0; i < 17; i++)
           if (v == dstarc_map[i])
               break;
       if (i < 17)
           uart_printf("%u", i);
       else
           uart_printf("unknown");
       uart_printf(")\n");
       uart_printf("dstar-a: 0x%08x\n", (unsigned)regs->pxie_dstara);
    }
    else if (!strcmp(arg, "dstarc")) {
       unsigned val;
       if (arg_next_number(&args, &val) != 0 || val > 16)
           uart_printf("bad or missing number\n");
       else
           regs->pxie_dstarc = dstarc_map[val];
    }
    else
       uart_printf("usage: pxie [dstarc SLOT]\n");
}
