#include "si5340-load.h"
#include "pcie-core.h"

#define BOARD pxie

extern void do_pxie(char *args);

void board_pxie_pll_init(void);
void board_pxie_init(void);

#define SI5340_USE_I2C

#define MAIN_SI5340_ADDR 0x75
#define MAIN_SI5340_FREQ 10000000
#define MAIN_SI5340_FILE "pxie-main-regs.h"

#define HELPER_SI5340_ADDR 0x74
#define HELPER_SI5340_FREQ 25000000
#define HELPER_SI5340_FILE "pxie-helper-regs.h"

#define BOARD_MENU		      \
  { "pll-main", do_main_si5340 },     \
  { "pll-helper", do_helper_si5340 }, \
  { "pxie", do_pxie },		      \
  PCIE_MENU
