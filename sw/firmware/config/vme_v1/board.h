#include "vme.h"
#include "si5340-load.h"
#include "dco.h"

#define CONFIG_VME_P2

#define BOARD vme

#define SI5340_USE_SPI

#define MAIN_SI5340_ADDR 0
#define MAIN_SI5340_FREQ 48000000
#define MAIN_SI5340_FILE "pcie-main-regs.h"

#define BOARD_MENU			\
  { "pll-main", do_main_si5340 },	\
  { "dco", do_dco },			\
  { "vme", do_vme },

