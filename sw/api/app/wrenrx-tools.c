#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "wrenrx-tools.h"
#include "wren/wren-hw.h"
#include "wren/wren-packet.h"

void
disp_protocol(const struct wren_protocol *proto)
{
    switch (proto->proto) {
    case WREN_PROTO_NONE:
	printf ("(unused)\n");
	break;
    case WREN_PROTO_ETHERNET: {
	const struct wren_proto_eth *peth = &proto->u.eth;
	printf ("eth, flags: 0x%02x", peth->flags);
	printf (", ethertype: 0x%04x", peth->ethertype);
	printf (", domain_id: 0x%02x", peth->domain_id);
	printf (", xmit_id: 0x%02x", peth->xmit_id);
	if (peth->flags & WREN_PROTO_ETH_FLAGS_MAC)
	    printf (", mac: %02x:%02x:%02x:%02x:%02x:%02x",
		    peth->mac[0], peth->mac[1], peth->mac[2],
		    peth->mac[3], peth->mac[4], peth->mac[5]);
	putchar('\n');
	break;
    }
    default:
	printf("?? (%u)\n", proto->proto);
	break;
    }
}

void
disp_params_text(const uint32_t *buf, unsigned len)
{
  unsigned off = 0;
  while (off < len) {
    uint32_t hdr = buf[off++];
    unsigned t = WREN_PACKET_PARAM_GET_DT(hdr);
    unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
    printf (" param 0x%03x", WREN_PACKET_PARAM_GET_TYP(hdr));
    switch (t) {
    case PKT_PARAM_U32:
      printf (" u32 %u", buf[off]);
      break;
    case PKT_PARAM_S32:
      printf (" s32 %d", (int32_t)buf[off]);
      break;
    case PKT_PARAM_S64: {
	union {
	    int64_t u64;
	    uint32_t u32[2];
	} u;
	u.u32[0] = buf[off + 0];
	u.u32[1] = buf[off + 1];
	printf (" s64 %ld", u.u64);
	break;
    }
    default:
	printf (" unknown(%u)", t);
	break;
    }
    printf ("\n");
    off += l - 1;
  }
}

int
parse_protocol(char *argv[], int argc, struct wren_protocol *proto)
{
    char *e;

    if (argc != 1) {
	fprintf(stderr, "missing SRC-ID for protocol\n");
	return -1;
    }

    proto->proto = WREN_PROTO_ETHERNET;
    proto->u.eth.flags = 0;
    memset(proto->u.eth.mac, 0xff, 6);
    proto->u.eth.vlan = 0;
    proto->u.eth.ethertype = ('W' << 8) | 'e';

    proto->u.eth.domain_id = strtoul(argv[0], &e, 0);
    if (*e != 0) {
	fprintf(stderr, "bad value for id (%s)\n", argv[0]);
	return -1;
    }

    proto->u.eth.xmit_id = 0;

    return 0;
}

static const char * const inp_names[] =
{
    "out0",
    "out1",
    "out2",
    "out3",
    "out4",
    "out5",
    "out6",
    "out7",

    "ext1",
    "ext2",
    "ext3",
    "ext4",
    "ext5",
    "ext6",
    "ext7",
    "ext8",

    "ext9",
    "ext10",
    "ext11",
    "ext12",
    "ext13",
    "ext14",
    "pps",
    "1Khz",

    "1Mhz",
    "10Mhz",
    "40Mhz",
    "frev1",
    "bunch1",
    "frev2",
    "bunch2",
    "no"
};

int
parse_input_name(const char *arg)
{
    unsigned i;

    for (i = 0; i < 32; i++)
	if (!strcmp(arg, inp_names[i]))
	    return i;

    if (!strcmp(arg, "1Ghz"))
	return WRENRX_INPUT_CLOCK_1GHZ;
    return -1;
}

const char *get_input_name(unsigned idx)
{
  if (idx < 32)
    return inp_names[idx];
  return NULL;
}

const char *get_clock_name(unsigned idx)
{
  if (idx < 31)
    return inp_names[idx];
  if (idx == WRENRX_INPUT_CLOCK_1GHZ)
    return "1GHz";
  return NULL;
}

struct wrenrx_cond_expr *
wrenrx_cond_tree_to_cond_expr(struct wrenrx_cond *cond,
			      struct wrenrx_cond_tree *t)
{
    if (t == NULL)
	return NULL;

    switch(t->kind) {
    case T_BINARY: {
	struct wrenrx_cond_expr *l, *r;
	enum wrenrx_cond_rel op;

	l = wrenrx_cond_tree_to_cond_expr(cond, t->u.binary.l);
	r = wrenrx_cond_tree_to_cond_expr(cond, t->u.binary.r);

	switch(t->u.binary.op) {
	case TOK_AND:
	    op = WRENRX_AND;
	    break;
	case TOK_OR:
	    op = WRENRX_OR;
	    break;
	default:
	    abort();
	}

	return wrenrx_cond_rel(cond, op, l, r);
    }
    case T_PARAM: {
	enum wrenrx_cond_comp cmp;
	unsigned orig;

	switch(t->u.param.op) {
	case TOK_EQ:
	    cmp = WRENRX_EQ;
	    break;
	default:
	    abort();
	}

	switch (t->u.param.orig) {
	case TOK_CTXT_PARAM:
	    orig = WRENRX_PARAM_CONTEXT;
	    break;
	case TOK_EVT_PARAM:
	    orig = WRENRX_PARAM_EVENT;
	    break;
	case TOK_ANY_PARAM:
	    orig = WRENRX_PARAM_CONTEXT | WRENRX_PARAM_EVENT;
	    break;
	default:
	    abort();
	}

	return wrenrx_cond_param_u32(cond, cmp, orig,
				     t->u.param.id, t->u.param.val);
    }
    default:
	abort();
    }
}
