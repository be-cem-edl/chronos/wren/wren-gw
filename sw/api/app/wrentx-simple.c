#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include "wren/wrentx.h"
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wrentx-private.h"
#include "wren-util.h"
#include "wrentx-util.h"

static unsigned src_idx;

static unsigned flag_dump;
static unsigned flag_hex;
static unsigned flag_inject;

static void
dump_or_send(struct wrentx_handle *handle,
             struct wrentx_frame *frame,
             struct wren_ts *t)
{
    int status;

    if (flag_dump) {
	printf ("### Frame\n");
	wrentx_frame_set_length(frame);
	wrentx_dump_frame(frame);
    }
    else {
	if (flag_hex) {
	    printf ("### Sending frame\n");
	    wrentx_frame_set_length(frame);
	    wren_dump_packet_hex (&frame->pkt);
	}
	if (flag_inject)
	    status = wrentx_inject_frame(handle, src_idx, frame);
	else
	    status = wrentx_send_frame(handle, src_idx, frame);
	if (status != 0) {
	    fprintf (stderr, "%s: cannot send frame: %m\n", progname);
	}
	status = wrentx_wait_until(handle, t);
	if (status != 0) {
	    fprintf (stderr, "%s: interrupted wait: %d\n", progname, status);
	}
    }

    wrentx_clear_frame(handle, frame);
}

static void
play_file(struct wrentx_handle *handle,
          const char *inp_filename,
          unsigned nbr_loops)
{
  FILE *f;
  struct wren_ts now;
  unsigned auto_truncate;
  long capsule_off;
  struct wrentx_table *table;
  struct wrentx_frame *frame;

  filename = inp_filename;

  f = fopen(filename, "r");
  if (f == NULL) {
    fprintf (stderr, "%s: cannot open '%s'\n", progname, filename);
    return;
  }

  if (wrentx_get_time(handle, &now) != 0) {
    fprintf (stderr, "%s: cannot get time\n", progname);
    return;
  }

  frame = wrentx_alloc_frame(handle);
  if (frame == NULL) {
    fprintf (stderr, "%s: cannot get frame\n", progname);
    exit (1);
  }

  table = NULL;
  auto_truncate = 0;
  lineno = 0;
  capsule_off = 0;
  while (1) {
    char buf[128];
    char *b;
    long off;

    /* Offset of the current line, to recover from truncations */
    off = ftell(f);

    if (fgets (buf, sizeof buf, f) != buf) {
      if (nbr_loops && --nbr_loops == 0)
	break;
      rewind (f);
      lineno = 0;
      continue;
    }
    lineno++;

    /* Skip spaces.  */
    b = buf;
    skip_spaces (&b);

    /* Skip comments, empty lines.  */
    if (is_eol (*b))
      continue;

    if (scan_cmp (&b, "auto-truncate")) {
      /* auto-truncate 0|1
	 Set auto truncate behaviour (for too long frames) */
      uint32_t val;

      scan_uint (&b, &val);
      scan_eol (&b);

      auto_truncate = val;
    }
    else if (scan_cmp (&b, "ctxt")) {
	/* ctxt ID TIME(us)
	   Start a context capsule.  */
	capsule_off = off;
	if (parse_ctxt(handle, frame, now, &b) != 0)
	    return;
    }
    else if (scan_cmp (&b, "param")) {
      /* param ID FMT VAL
	 Add a parameter (to current event or context).  */
      int status;

      status = parse_param(frame, &b);
      if (status != 0) {
	if (status == WRENTX_ERR_FRAME_FULL && auto_truncate) {
	    wrentx_truncate_frame(frame);
	    dump_or_send (handle, frame, &now);
	    fseek(f, capsule_off, SEEK_SET);
	}
	else {
	    fprintf (stderr, "%s:%u: failed to add parameter (%d)\n",
		     filename, lineno, status);
	    return;
	}
      }
    }
    else if (scan_cmp (&b, "event")) {
	/* event ID CTXT TIME(us)
	   Start an event capsule.  */
	int status;

	capsule_off = off;

	status = parse_event(handle, frame, now, &b);
	if (status < 0) {
	    if (status == WRENTX_ERR_FRAME_FULL && auto_truncate) {
		dump_or_send (handle, frame, &now);
		fseek(f, capsule_off, SEEK_SET);
	    }
	    else {
		fprintf (stderr, "%s:%u: failed to add event (%d)\n",
			 filename, lineno, status);
		exit (1);
	    }
	}
    }
    else if (scan_cmp (&b, "send")) {
      /* send
	 Send current frame */
      scan_eol (&b);

      dump_or_send (handle, frame, &now);
    }
    else if (scan_cmp (&b, "wait")) {
      /* wait TIME(us)
	 Wait */
      uint32_t tim;
      int status;

      scan_uint (&b, &tim);
      scan_eol (&b);

      wren_add_usec(&now, tim);
      status = wrentx_wait_until(handle, &now);
      if (status != WRENTX_SUCCESS) {
        fprintf(stderr, "wait error: %d\n", status);
      }
    }
    else if (scan_cmp (&b, "table-start")) {
      /* table-start
	 Start creation of a table */
      scan_eol (&b);

      table = wrentx_alloc_table(handle);
    }
    else if (scan_cmp (&b, "table-frame")) {
      /* table-frame
	 Append current frame to the current table */
      int status;

      status = wrentx_table_append_frame(handle, table, frame);
      if (status < 0)
	fprintf(stderr, "table append frame error: %d\n", status);
      wrentx_clear_frame(handle, frame);
    }
    else if (scan_cmp (&b, "table-wait")) {
      /* table-wait TIM(us)
	 Append a wait */
      uint32_t tim;
      int status;
      struct wren_ts t;

      scan_uint (&b, &tim);
      scan_eol (&b);

      t.sec = tim / 1000000;
      tim -= t.sec * 1000000;
      t.nsec = tim * 1000;

      status = wrentx_table_append_wait(handle, table, &t);
      if (status < 0)
	fprintf(stderr, "table append wait error: %d\n", status);
    }
    else if (scan_cmp (&b, "table-load")) {
      /* table-load NAME IDX COUNT
	 Load a table */
      char name[32];
      uint32_t idx;
      uint32_t cnt;
      int status;

      scan_str (&b, name, sizeof(name));
      scan_uint (&b, &idx);
      scan_uint (&b, &cnt);
      scan_eol (&b);

      status = wrentx_load_table(handle, idx, name, src_idx, table, cnt);
      if (status < 0)
	fprintf(stderr, "table load error: %d\n", status);

      wrentx_free_table(table);
      table = NULL;
    }
    else if (scan_cmp (&b, "table-play")) {
      /* table-play IDX TIM(us)
	 Immediately play a table */
      struct wren_ts ts;
      uint32_t idx;
      uint32_t tim;
      int status;

      scan_uint (&b, &idx);
      scan_uint (&b, &tim);
      scan_eol (&b);

      ts = now;
      wren_add_usec(&ts, tim);

      status = wrentx_play_table(handle, idx, &ts);
      if (status < 0)
	fprintf(stderr, "table play error: %d\n", status);
    }
    else if (scan_cmp (&b, "table-play-on-event")) {
      /* table-play-on-event IDX EV
	 Start playing a table when an event is sent */
      uint32_t idx;
      uint32_t ev;
      int status;

      scan_uint (&b, &idx);
      scan_uint (&b, &ev);
      scan_eol (&b);

      status = wrentx_play_table_on_event(handle, src_idx, idx, ev);
      if (status < 0)
	fprintf(stderr, "table play on event error: %d\n", status);
    }
    else if (scan_cmp (&b, "table-play-on-event-param")) {
      /* table-play-on-event-param IDX EV PARAM FMT VAL
	 Start playing a table when an event is sent */
      uint32_t idx;
      uint32_t ev;
      uint32_t param;
      enum param_fmt fmt;
      uint32_t val;
      int status;

      scan_uint (&b, &idx);
      scan_uint (&b, &ev);
      scan_uint (&b, &param);
      fmt = scan_fmt (&b);
      scan_uint (&b, &val);
      scan_eol (&b);

      switch(fmt) {
      case FMT_S32:
	status = wrentx_play_table_on_cond_event_s32(handle, src_idx, idx, ev,
						     param, val);
	break;
      default:
	abort();
      }

      if (status < 0)
	fprintf(stderr, "table play on cond event error: %d\n", status);
    }
    else if (scan_cmp (&b, "table-stop")) {
      /* table-stop IDX
	 Immediately stop playing a table */
      uint32_t idx;
      int status;

      scan_uint (&b, &idx);
      scan_eol (&b);

      status = wrentx_stop_table(handle, src_idx, idx);
      if (status < 0)
	fprintf(stderr, "table stop error: %d\n", status);
    }
    else if (scan_cmp (&b, "pause")) {
      /* pause TIME(ms)
	 Deprecated. Sleep (never virtual). */
      uint32_t t;

      scan_uint (&b, &t);
      scan_eol (&b);

      usleep (t * 1000);
    }
    else {
      fprintf (stderr, "%s: unknown command: %s", filename, b);
      exit (1);
    }
  }

  fclose(f);
  wrentx_free_frame(frame);
}

int
main(int argc, char *argv[])
{
  void *init;
  struct wrentx_handle *handle;
  struct wrentx_frame *frame;
  struct wren_protocol proto;
  uint32_t ctxt_id;
  int status;
  int flag_loop = 1;
  const char *input;
  wren_domain_id dom_id = WREN_DOMAIN_ID_INVALID;
  int c;

  progname = argv[0];
  input = NULL;

  init = wrentx_drv_init(&argc, argv);
  if (init == NULL)
    return 1;

  while ((c = getopt(argc, argv, "Dhf:l:xd:I")) >= 0) {
    switch (c) {
    case 'D':
      flag_dump = 1;
      break;
    case 'x':
      flag_hex = 1;
      break;
    case 'f':
      input = optarg;
      break;
    case 'I':
      flag_inject = 1;
      break;
    case 'l':
      {
        char *e;
        flag_loop = strtoul (optarg, &e, 0);
        if (*e != 0) {
          fprintf (stderr, "%s: incorrect value for option -l\n", progname);
          return 2;
        }
      }
      break;
    case 'd':
      {
        char *e;
        dom_id = strtoul (optarg, &e, 0);
        if (*e != 0) {
          fprintf (stderr, "%s: incorrect value for option -d\n", progname);
          return 2;
        }
      }
      break;
    case '?':
    case 'h':
      fprintf (stderr, "usage: %s DRIVER [driver options] -- [-D] [-x] [-f FILENAME] [-l LOOP]\n",
               progname);
      fprintf (stderr,
	       " -d DOM   specify wren domain id and create the source\n");
      return 2;
    default:
      abort ();
    }
  }

  handle = wrentx_drv_open(init);
  if (handle == NULL)
    return 1;

  if (dom_id != WREN_DOMAIN_ID_INVALID) {
      proto.proto = WREN_PROTO_ETHERNET;
      proto.u.eth.flags = 0;
      proto.u.eth.ethertype = WREN_ETHERTYPE;
      proto.u.eth.domain_id = dom_id;
      proto.u.eth.xmit_id = 0;

      if (wrentx_set_source(handle, src_idx, &proto) < 0) {
	  fprintf (stderr, "%s: cannot set source\n", progname);
	  return 1;
      }
  }

  if (input != NULL) {
    play_file (handle, input, flag_loop);
  }
  else {
    struct wren_ts now;
    unsigned cnt;

    frame = wrentx_alloc_frame(handle);
    if (frame == NULL) {
      fprintf (stderr, "%s: cannot get frame\n", progname);
      return 1;
    }

    cnt = 0;
    ctxt_id = 1;
    while (1) {
      status = wrentx_get_time(handle, &now);
      if (status != 0) {
        fprintf (stderr, "%s: cannot get time\n", progname);
        return 1;
      }

      wren_add_usec(&now, 500000);

#define CTXT(ID) do { if (set_ctxt (handle, frame, ID, &now) != 0) return 1; } while (0)
#define EVENT(ID, CTXT, TIM) add_event(handle, frame, ID, CTXT, TIM)
#define PARAM(ID, VAL) add_param_s32(frame, ID, VAL)
#define SEND() dump_or_send (handle, frame, &now)

      CTXT(ctxt_id);

      PARAM(3, 1456);
      PARAM(4, cnt++);

      SEND();
      usleep (10000);

      EVENT(1, ctxt_id, &now);
      PARAM(4, cnt + 1);
      SEND();

      if (flag_loop && --flag_loop == 0)
        break;

      usleep (800000);

      ctxt_id = (ctxt_id + 1) & 3;
    }
  }

  wrentx_close(handle);
  return 0;
}
