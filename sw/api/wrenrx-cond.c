#include <stdlib.h>
#include <string.h>
#include "wren-mb-defs.h"
#include "wrenrx-cond.h"
#include "wren/wren-packet.h"

struct wrenrx_cond *
wrenrx_cond_alloc(struct wrenrx_handle *handle)
{
  struct wrenrx_cond *res;

  res = malloc (sizeof (struct wrenrx_cond));
  if (res == NULL)
    return NULL;

  wrenrx_cond_reinit (handle, res);
  return res;
}

void
wrenrx_cond_reinit (struct wrenrx_handle *handle, struct wrenrx_cond *cond)
{
  cond->nbr_cond = 0;
};

void
wrenrx_cond_free(struct wrenrx_cond *cond)
{
  free(cond);
}

struct wrenrx_cond_expr *
wrenrx_cond_rel (struct wrenrx_cond *cond,
		 enum wrenrx_cond_rel op,
		 struct wrenrx_cond_expr *l,
		 struct wrenrx_cond_expr *r)
{
  struct wrenrx_cond_expr *res;
  if (cond->nbr_cond == MAX_COND)
    return NULL;

  res = &cond->conds[cond->nbr_cond++];
  res->kind = WRENRX_BINARY;
  res->u.binary.op = op;
  res->u.binary.l = l - cond->conds;
  res->u.binary.r = r - cond->conds;

  return res;
}

struct wrenrx_cond_expr *
wrenrx_cond_param_u32 (struct wrenrx_cond *cond,
		       enum wrenrx_cond_comp op,
		       unsigned orig,
		       uint16_t param, uint32_t val)
{
  struct wrenrx_cond_expr *res;
  if (cond->nbr_cond == MAX_COND)
    return NULL;

  res = &cond->conds[cond->nbr_cond++];
  res->kind = WRENRX_COND;
  res->u.cond.op = op;
  res->u.cond.param = param;
  res->u.cond.orig = orig;
  res->u.cond.val = val;

  return res;
}

int
wrenrx_cond_define (struct wrenrx_cond *cond,
		    unsigned source_idx,
		    uint16_t ev_id,
		    struct wrenrx_cond_expr *expr)
{
  cond->source_idx = source_idx;
  cond->ev_id = ev_id;

  /* TODO: check number of conditions matches, EXPR is the last expr */
  if (expr == NULL)
    cond->nbr_cond = 0;

  return 0;
}

int
wrenrx_cond_expr_to_words (const struct wrenrx_cond *cond,
			   unsigned idx,
			   union wren_rx_cond_word *data,
			   unsigned maxlen)
{
  const struct wrenrx_cond_expr *expr = &cond->conds[idx];

  switch (expr->kind) {
  case WRENRX_COND: {
    if (maxlen < 2)
      return -1;

    switch (expr->u.cond.op) {
    case WRENRX_EQ:
      data[0].op.op = WREN_OP_EQ;
      break;
    case WRENRX_NE:
      data[0].op.op = WREN_OP_NE;
      break;
    case WRENRX_SLT:
      data[0].op.op = WREN_OP_SLT;
      break;
    case WRENRX_SLE:
      data[0].op.op = WREN_OP_SLE;
      break;
    case WRENRX_SGT:
      data[0].op.op = WREN_OP_SGT;
      break;
    case WRENRX_SGE:
      data[0].op.op = WREN_OP_SGE;
      break;
    }

    data[0].op.param_id = expr->u.cond.param;
    data[0].op.param_orig = expr->u.cond.orig;
    data[1].vu32 = expr->u.cond.val;

    return 2;
  }
  case WRENRX_BINARY: {
    int ll, lr;
    ll = wrenrx_cond_expr_to_words(cond, expr->u.binary.l, data, maxlen);
    if (ll < 0)
      return -1;
    if (expr->u.binary.op == WRENRX_NOT) {
	data[ll].op.op = WREN_OP_NOT;
	return ll + 1;
    }
    lr = wrenrx_cond_expr_to_words(cond, expr->u.binary.r,
				   data + ll, maxlen - ll);
    if (lr < 0)
      return -1;
    switch(expr->u.binary.op) {
    case WRENRX_OR:
	data[ll+lr].op.op = WREN_OP_OR;
	break;
    case WRENRX_AND:
	data[ll+lr].op.op = WREN_OP_AND;
	break;
    default:
	abort();
    }
    return ll + lr + 1;
  }
  default:
    abort();
  }
}

unsigned
wrenrx_cond_get_source_idx(const struct wrenrx_cond *cond)
{
    return cond->source_idx;
}

wren_event_id
wrenrx_cond_get_event_id(const struct wrenrx_cond *cond)
{
    return cond->ev_id;
}

const struct wrenrx_cond_expr *
wrenrx_cond_get_expr(const struct wrenrx_cond *cond)
{
    if (cond->nbr_cond == 0)
	return NULL;
    return &cond->conds[cond->nbr_cond - 1];
}

enum wrenrx_cond_expr_kind
wrenrx_cond_expr_get_kind(const struct wrenrx_cond_expr *expr)
{
    return expr->kind;
}

enum wrenrx_cond_comp
wrenrx_cond_expr_cond_get_op(const struct wrenrx_cond_expr *expr)
{
    return expr->u.cond.op;
}

unsigned
wrenrx_cond_expr_cond_get_orig(const struct wrenrx_cond_expr *expr)
{
    return expr->u.cond.orig;
}

wren_param_id
wrenrx_cond_expr_cond_get_param(const struct wrenrx_cond_expr *expr)
{
    return expr->u.cond.param;
}

uint32_t
wrenrx_cond_expr_cond_get_value_u32(const struct wrenrx_cond_expr *expr)
{
    return expr->u.cond.val;
}

enum wrenrx_cond_rel
wrenrx_cond_expr_bin_get_op(const struct wrenrx_cond_expr *expr)
{
    return expr->u.binary.op;
}

const struct wrenrx_cond_expr *
wrenrx_cond_expr_bin_get_left(const struct wrenrx_cond *cond,
			      const struct wrenrx_cond_expr *expr)
{
    return &cond->conds[expr->u.binary.l];
}

const struct wrenrx_cond_expr *
wrenrx_cond_expr_bin_get_right(const struct wrenrx_cond *cond,
			       const struct wrenrx_cond_expr *expr)
{
    return &cond->conds[expr->u.binary.r];
}

enum wrenrx_msg_kind
wrenrx_get_msg(const struct wrenrx_msg *msg)
{
  return msg->kind;
}

unsigned
wrenrx_msg_get_params_length(const struct wrenrx_msg *msg)
{
    switch (msg->kind) {
    case wrenrx_msg_event:
	return msg->u.evt.evt.params_len;
    case wrenrx_msg_context:
	return msg->u.ctxt.ctxt.params_len;
    case wrenrx_msg_pulse:
	return msg->u.pulse.evt.params_len;
    default:
	return 0;
    }
}

unsigned
wrenrx_msg_get_config_id(const struct wrenrx_msg *msg)
{
  if (msg->kind != wrenrx_msg_pulse)
    return 0;
  return msg->u.pulse.config_id;
}

unsigned
wrenrx_msg_get_source_idx(const struct wrenrx_msg *msg)
{
    switch (msg->kind) {
    case wrenrx_msg_event:
	return msg->u.evt.source_idx;
    case wrenrx_msg_context:
	return msg->u.ctxt.source_idx;
    case wrenrx_msg_pulse:
	return msg->u.pulse.source_idx;
    default:
	return ~0U;
    }
}

unsigned
wrenrx_msg_get_event_id(const struct wrenrx_msg *msg)
{
    switch (msg->kind) {
    case wrenrx_msg_event:
	return msg->u.evt.evt.ev_id;
    case wrenrx_msg_pulse:
	return msg->u.pulse.evt.ev_id;
    default:
	return WREN_EVENT_ID_INVALID;
    }
}

unsigned
wrenrx_msg_get_event_evsub_id(const struct wrenrx_msg *msg)
{
    switch (msg->kind) {
    case wrenrx_msg_event:
	return msg->u.evt.evsub_id;
    default:
	return WREN_EVENT_ID_INVALID;
    }
}

int
wrenrx_msg_get_event_ts(const struct wrenrx_msg *msg, struct wren_ts *ts)
{
    switch (msg->kind) {
    case wrenrx_msg_event:
	*ts = msg->u.evt.evt.ts;
	return 0;
    case wrenrx_msg_pulse:
	if (msg->u.pulse.evt.ev_id != WREN_EVENT_ID_INVALID) {
	    *ts = msg->u.pulse.evt.ts;
	    return 0;
	}
	break;
    default:
	break;
    }
    return -1;
}

int
wrenrx_msg_get_pulse_ts(const struct wrenrx_msg *msg, struct wren_ts *ts)
{
    if (msg->kind != wrenrx_msg_pulse)
	return -1;
    *ts = msg->u.pulse.ts;
    return 0;
}

int
wrenrx_msg_get_event_params(const struct wrenrx_msg *msg,
			    struct wrenrx_msg_param *it)
{
    const struct wrenrx_msg_event *ev;

    switch (msg->kind) {
    case wrenrx_msg_event:
	ev = &msg->u.evt.evt;
	break;
    case wrenrx_msg_pulse:
	ev = &msg->u.pulse.evt;
	if (ev->ev_id == WREN_EVENT_ID_INVALID)
	    return -1;
	break;
    default:
	return -1;
    }

    it->data = ev->params;
    it->off = 0;
    it->len = ev->params_len;

    return 0;
}

int
wrenrx_msg_get_context_params(const struct wrenrx_msg *msg,
			      struct wrenrx_msg_param *it)
{
    const struct wrenrx_msg_context *ctxt;

    switch (msg->kind) {
    case wrenrx_msg_context:
	ctxt = &msg->u.ctxt.ctxt;
	break;
    case wrenrx_msg_event:
	ctxt = &msg->u.evt.ctxt;
	break;
    default:
	return -1;
    }

    it->off = 0;
    it->data = ctxt->params;
    it->len = ctxt->params_len;
    return 0;
}

unsigned
wrenrx_msg_get_context_id(const struct wrenrx_msg *msg)
{
    switch (msg->kind) {
    case wrenrx_msg_context:
	return msg->u.ctxt.ctxt_id;
    case wrenrx_msg_event:
	return msg->u.evt.evt.ctxt_id;
    case wrenrx_msg_pulse:
	return msg->u.pulse.evt.ctxt_id;
    default:
	return ~0U;
    }
}

int
wrenrx_msg_get_context_ts(const struct wrenrx_msg *msg, struct wren_ts *ts)
{
  if (msg->kind != wrenrx_msg_context)
    return -1;
  *ts = msg->u.ctxt.ctxt.ts;
  return 0;
}

unsigned
wrenrx_msg_param_is_valid(const struct wrenrx_msg_param *params)
{
  return params->off < params->len;
}

void
wrenrx_msg_param_next(struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    unsigned hdr = params->data[params->off];
    unsigned len = WREN_PACKET_PARAM_GET_LEN(hdr);
    params->off += len;
  }
}

wren_param_id
wrenrx_msg_param_get_id(const struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    unsigned hdr = params->data[params->off];
    return WREN_PACKET_PARAM_GET_TYP(hdr);
  }
  return -1;
}

unsigned
wrenrx_msg_param_get_dt(const struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    unsigned hdr = params->data[params->off];
    return WREN_PACKET_PARAM_GET_DT(hdr);
  }
  return -1;
}


int32_t
wrenrx_msg_param_get_s32(const struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    union {
      uint32_t u32;
      int32_t s32;
    } u;
    u.u32 = params->data[params->off + 1];
    return u.s32;
  }
  return -1;
}

int64_t
wrenrx_msg_param_get_s64(const struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    int64_t res;
    memcpy (&res, &params->data[params->off + 1], 8);
    return res;
  }
  return -1;
}

float
wrenrx_msg_param_get_f32(const struct wrenrx_msg_param *params)
{
  if (params->off < params->len) {
    float res;
    memcpy (&res, &params->data[params->off + 1], 4);
    return res;
  }
  return -1.0;
}

unsigned
wrenrx_drv_msg_get_packet(struct wrenrx_msg *msg,
			  struct wren_packet *pkt)
{
    memcpy(pkt, msg->u.packet.buf, msg->u.packet.len * 4);
    return msg->u.packet.len * 4;
}
