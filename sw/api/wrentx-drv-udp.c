#include <string.h>
#include <stdio.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include "wren-drv.h"
#include "wren-drv-udp.h"
#include "wrentx-drv.h"
#include "wrentx-drv-sim.h"

void *
wrentx_drv_init_udp(int *argc, char *argv[])
{
    return wren_drv_init_udp(argc, argv);
}


struct wrentx_handle *
wrentx_open_udp_by_sockaddr (struct sockaddr_in *addr)
{
  struct wrentx_handle *handle;
  int sock;
  int en;

  sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
  if (sock < 0) {
    perror("socket() failed");
    return NULL;
  }

  /* Enable broadcast (just in case)  */
  if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, &en, sizeof(en))) {
    fprintf(stderr, "%s: warning: broadcast not enabled\n", progname);
  }

  if (connect(sock, (struct sockaddr *)addr, sizeof(*addr)) < 0) {
    perror("connect() failed");
    return NULL;
  }

  handle = wrentx_open_sim_fd(&wrentx_sim_ops_fd_vt, sock);
  if (handle == NULL)
    fprintf (stderr, "%s: cannot open udp\n", progname);

  return handle;
}

struct wrentx_handle *
wrentx_drv_open_udp(void *init)
{
    struct wren_drv_udp_init_arg *arg = init;
    struct wrentx_handle *handle;
    struct sockaddr_in mc_addr;

    memset(&mc_addr, 0, sizeof(mc_addr));
    mc_addr.sin_family  = AF_INET;
    mc_addr.sin_addr    = arg->dst_addr;
    mc_addr.sin_port    = htons(arg->dst_port);

    handle = wrentx_open_udp_by_sockaddr(&mc_addr);
    if (!handle)
	return NULL;

    free (arg);

    return handle;
}

struct wrentx_handle *
wrentx_open_udp_by_port(unsigned port)
{
    struct sockaddr_in mc_addr;

    memset(&mc_addr, 0, sizeof(mc_addr));
    mc_addr.sin_family  = AF_INET;
    mc_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    mc_addr.sin_port = htons(port);

    return wrentx_open_udp_by_sockaddr(&mc_addr);
}

struct wrentx_handle *
wrentx_open_udp_default(void)
{
    return wrentx_open_udp_by_port (1099);
}
