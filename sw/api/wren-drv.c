#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "wren-drv.h"

const char *progname;

struct wren_drv_tuple *
wren_drv_init(const struct wren_drv *drivers, int *argc, char *argv[])
{
    const char *name;
    const struct wren_drv *drv;
    struct wren_drv_tuple *res;

    progname = argv[0];

    if (*argc < 2) {
	fprintf (stderr, "%s: missing driver name\n", progname);
	return NULL;
    }

    name = argv[1];

    if (!strcmp(name, "help")
	|| !strcmp(name, "--help")
	|| !strcmp(name, "-h")) {
	printf ("List of supported drivers:\n");
	for (drv = drivers; drv->name != NULL; drv++) {
	    printf ("%s", drv->help);
	}
	return NULL;
    }

    for (drv = drivers; drv->name != NULL; drv++)
	if (!strcmp(drv->name, name))
	    break;

    if (drv->name == NULL) {
	fprintf (stderr, "%s: unknown driver '%s'\n", progname, name);
	return NULL;
    }

    res = malloc (sizeof(*res));
    if (res == NULL)
	return NULL;

    res->drv = drv;
    res->init = drv->init (argc, argv);
    if (res->init == NULL) {
	free (res);
	return NULL;
    }

    return res;
}

void
wren_drv_remove_args(int *argc, char *argv[], unsigned num)
{
    unsigned j;

    for (j = 1 + num; j < *argc; j++)
	argv[j - num] = argv[j];
    (*argc) -= num;
}
