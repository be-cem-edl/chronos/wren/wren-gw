#ifndef __WRENRX_DRV__H__
#define __WRENRX_DRV__H__

#include "wren/wrenrx.h"
#include "wren/wren-packet.h"
#include "wren-mb-defs.h"

/* Select a wrentx.
   Typically args are:
     DEVICE OPTIONS... -- program options
   or
     DEVICE OPTIONS...

   Options are removed from argc/argc.
   Return NULLin case of error.
*/
struct wren_drv_tuple *wrenrx_drv_init(int *argc, char *argv[]);

/* Open the selected wrenrx.  The argument is the result of wrenrx_drv_init.
   This happens after so that the program can decode the arguments
   even if the board is not present
*/
struct wrenrx_handle *wrenrx_drv_open(struct wren_drv_tuple *init);

/* Read a wren packet from HANDLE.  Possibly remove any lower level headers
   (like ethernet).
   Return the length (in byte). */
int wrenrx_drv_read_packet(struct wrenrx_handle *handle,
			   struct wren_packet *pkt);

unsigned wrenrx_drv_msg_get_packet(struct wrenrx_msg *msg,
				   struct wren_packet *pkt);

int wrenrx_drv_set_promisc(struct wrenrx_handle *handle, unsigned en);

/* Private declarations */

/* For stdin driver */
struct wrenrx_handle *wrenrx_drv_open_fd(int fd);
void *wrenrx_drv_init_stdin(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_stdin(void *);

/* For udp driver */
void *wrenrx_drv_init_udp(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_udp(void *);

/* For eth driver */
void *wrenrx_drv_init_eth(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_open_eth(void *);

/* For wrenctl driver */
void *wrenrx_drv_wren_init(int *argc, char *argv[]);
struct wrenrx_handle *wrenrx_drv_wren_open(void *);

struct wrenrx_operations
{
  int (*get_version)(struct wrenrx_handle *handle,
		     char *version, unsigned maxlen);
  void (*close)(struct wrenrx_handle *handle);
  int (*get_link)(struct wrenrx_handle *handle);
  int (*get_sync)(struct wrenrx_handle *handle);
  int (*set_promisc)(struct wrenrx_handle *handle, unsigned en);
  int (*recv_packet)(struct wrenrx_handle *handle, struct wren_packet *pkt);
  int (*get_time)(struct wrenrx_handle *handle,
		  struct wren_ts *time);
  int (*get_panel_config)(struct wrenrx_handle *handle,
			  struct wren_panel_config *config);

  int (*add_source)(struct wrenrx_handle *handle,
		    struct wren_protocol *proto);
  int (*get_source)(struct wrenrx_handle *handle,
		    unsigned source_idx,
		    struct wren_protocol *proto);
  int (*del_source)(struct wrenrx_handle *handle,
		    unsigned source_idx);

  int (*context_subscribe)(struct wrenrx_handle *handle,
			   unsigned source_idx);
  int (*context_unsubscribe)(struct wrenrx_handle *handle, unsigned source_idx);

  /* Note: internally, the pulser_idx is an index starting from 0 */
  int (*pulser_define)(struct wrenrx_handle *handle,
		       unsigned pulser_idx,
		       const struct wrenrx_cond *cond,
		       const struct wrenrx_pulser_configuration *conf,
		       const char *name);
  int (*pulser_reconfigure)(struct wrenrx_handle *handle,
			    unsigned config_idx,
			    const struct wrenrx_pulser_configuration *conf);
  int (*pulser_force)(struct wrenrx_handle *handle,
		      unsigned config_idx,
		      struct wren_ts *ts,
		      struct wrenrx_pulser_configuration *conf);
  int (*pulser_remove)(struct wrenrx_handle *handle,
		       unsigned config_idx);
  int (*pulser_get_config)(struct wrenrx_handle *handle,
			   unsigned config_idx,
			   unsigned *pulse_num,
			   struct wrenrx_pulser_configuration *conf,
			   struct wrenrx_cond *cond,
			   char *name);
  int (*pulser_program)(struct wrenrx_handle *handle,
			unsigned pulser_num,
			const struct wrenrx_pulser_configuration *conf,
			const struct wren_ts *time,
			const char *name);
  int (*event_subscribe)(struct wrenrx_handle *handle,
			 unsigned source_idx,
			 wren_event_id ev_id,
			 int offset_us);
  int (*event_unsubscribe)(struct wrenrx_handle *handle, unsigned evsub_idx);
  int (*config_subscribe)(struct wrenrx_handle *handle,
			  unsigned config_idx);
  int (*config_unsubscribe)(struct wrenrx_handle *handle,
			    unsigned config_idx);
  int (*output_get) (struct wrenrx_handle *handle,
		     unsigned output_num,
		     struct wrenrx_output_configuration *cfg);
  int (*output_set) (struct wrenrx_handle *handle,
		     unsigned output_num,
		     const struct wrenrx_output_configuration *cfg);
  int (*pin_get)(struct wrenrx_handle *handle,
		 unsigned pin_num,
		 struct wrenrx_pin_configuration *cfg);
  int (*pin_set)(struct wrenrx_handle *handle,
		 unsigned pin_num,
		 const struct wrenrx_pin_configuration *cfg);

  int (*pulser_log_get)(struct wrenrx_handle *handle,
			unsigned pulser_num,
			struct wrenrx_pulser_log *buf,
			unsigned len);

  int (*pin_log_get)(struct wrenrx_handle *handle,
		     unsigned pin_num,
		     struct wrenrx_pin_log *buf,
		     unsigned len);

  int (*sw_log_get)(struct wrenrx_handle *handle,
		    union wren_evlog_entry *ent,
		    unsigned len);

  int (*hw_vme_p2a_set_output)(struct wrenrx_handle *handle, unsigned en);
  int (*hw_patchpanel_set_config)(struct wrenrx_handle *handle, unsigned en);


  int (*get_fd)(struct wrenrx_handle *handle);
  struct wrenrx_msg *(*wait)(struct wrenrx_handle *handle);
};

struct wrenrx_handle_op {
  const struct wrenrx_operations *op;
};

/* Converters */
void
wrenrx_pulser_configuration_to_mb(struct wren_mb_pulser_config *dst,
				  const struct wrenrx_pulser_configuration *src,
				  unsigned pulser_idx);


int wrenrx_cond_to_mb(struct wren_mb_cond *dst,
		      const struct wrenrx_cond *src);

#endif /* __WRENRX_DRV__H__ */
