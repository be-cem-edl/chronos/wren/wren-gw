#include <sys/ioctl.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <stdio.h>
#include "wren-mb.h"
#include "wren-ioctl.h"

int wrenctl_open(const char *pathname)
{
    return open(pathname, O_RDWR);
}

int wren_check_version(int wren_fd)
{
    uint32_t ver;

    if (ioctl(wren_fd, WREN_IOC_GET_VERSION, &ver) != 0)
	return -1;
    if ((ver & WREN_IOCTL_VERSION_MASK)
	!= (WREN_IOCTL_VERSION & WREN_IOCTL_VERSION_MASK)) {
	fprintf (stderr, "wren ioctl version mismatch: driver 0x%08x, library expect 0x%08x\n",
		 ver, WREN_IOCTL_VERSION);
	return -1;
    }
    return 0;
}

int wrenctl_get_wr_time(int wren_fd, struct timespec *ts)
{
    struct wren_wr_time res;

    if (ioctl(wren_fd, WREN_IOC_GET_TIME, &res) < 0) {
	return -1;
    }
    ts->tv_sec = (((unsigned long long)res.tai_hi) << 32) | res.tai_lo;
    ts->tv_nsec = res.ns;
    return 0;
}

int wren_mb_msg(int wren_fd,
		struct wren_mb_metadata *md,
		const void *inp,
		void *res,
		unsigned maxsz)
{
    struct wren_cmd_msg msg;
    int len;

    msg.cmd = md->cmd;
    msg.len = md->len; /* In words */
    if (msg.len > sizeof(msg.data)/sizeof(msg.data[0]))
	return -1;

    memcpy(msg.data, inp, md->len * 4);
    if (ioctl(wren_fd, WREN_IOC_MSG, &msg) < 0) {
	fprintf (stderr, "wren_msb_msg: %m\n");
	return -1;
    }

    md->cmd = msg.cmd;
    md->len = msg.len;

    if (msg.cmd & CMD_ERROR)
	return -(msg.cmd & CMD_MASK);

    len = msg.len;
    if (len * 4 > maxsz) {
	fprintf(stderr, "wren_mb_msg: message too long (%uw, expect %uw)\n",
		msg.len, maxsz / 4);
	len = maxsz / 4;
	return -1;
    }

    memcpy (res, msg.data, len * 4);

    return len;
}

int wrenctl_msg(int wren_fd, unsigned cmd,
		const void *data, unsigned data_len,
		void *res, unsigned maxsz)
{
    struct wren_mb_metadata md;

    md.cmd = cmd;
    md.len = data_len / 4;

    return wren_mb_msg(wren_fd, &md, data, res, maxsz);
}

int
wrenctl_rx_get_source(int wren_fd, unsigned src_idx,
		      struct wren_mb_rx_get_source_reply *dst)
{
    struct wren_mb_metadata wmd;
    int len;
    uint32_t idx;

    idx = src_idx;

    wmd.cmd = CMD_RX_GET_SOURCE;
    wmd.len = sizeof(idx) / 4;

    len = wren_mb_msg(wren_fd, &wmd, &idx, dst, sizeof(*dst));
    if (len != sizeof (*dst) / 4)
	return -1;
    return 0;
}

int
wrenctl_rx_get_action(int wren_fd, unsigned act_idx,
		      struct wren_rx_action *dst)
{
    struct wren_mb_metadata wmd;
    int len;
    uint32_t idx;

    idx = act_idx;

    wmd.cmd = CMD_RX_GET_ACTION;
    wmd.len = sizeof(idx) / 4;

    len = wren_mb_msg(wren_fd, &wmd, &idx, dst, sizeof(*dst));
    if (len < 0)
	return len;
    if (len != sizeof (*dst) / 4)
	return WRENRX_ERR_BAD_MSG_LENGTH;
    return 0;
}

int
wrenctl_rx_del_action(int wren_fd, unsigned act_idx)
{
    struct wren_mb_metadata wmd;
    int len;
    uint32_t idx;

    idx = act_idx;

    wmd.cmd = CMD_RX_DEL_ACTION;
    wmd.len = sizeof(idx) / 4;

    len = wren_mb_msg(wren_fd, &wmd, &idx, NULL, 0);
    if (len < 0)
	return len;
    if (len != 0)
	return WRENRX_ERR_BAD_MSG_LENGTH;
    return 0;
}

int
wrenctl_rx_get_condition(int wren_fd, unsigned cond_idx,
			 struct wren_rx_cond *dst)
{
    struct wren_mb_metadata wmd;
    int len;
    uint32_t idx;

    idx = cond_idx;

    wmd.cmd = CMD_RX_GET_COND;
    wmd.len = sizeof(idx) / 4;

    len = wren_mb_msg(wren_fd, &wmd, &idx, dst, sizeof(*dst));
    if (len != sizeof(*dst) / 4)
	return -1;
    return 0;
}

int
wrenctl_rx_set_source(int wren_fd, struct wren_mb_rx_set_source *cmd)
{
    struct wren_mb_metadata wmd;
    uint32_t rep;

    wmd.cmd = CMD_RX_SET_SOURCE;
    wmd.len = sizeof(*cmd) / 4;

    if (wren_mb_msg(wren_fd, &wmd, cmd, &rep, sizeof(rep)) < 0)
	return -1;
    return rep;
}

int
wrenctl_rx_set_cond(int wren_fd, struct wren_mb_rx_set_cond *cmd)
{
    struct wren_mb_metadata wmd;
    uint32_t rep;
    int res;

    /* Send the packet */
    wmd.cmd = CMD_RX_SET_COND;
    wmd.len = sizeof(*cmd) / 4;

    res = wren_mb_msg(wren_fd, &wmd, cmd, &rep, sizeof(rep));
    if (res < 0)
	return res;
    return rep;
}

int
wrenctl_rx_set_action(int wren_fd, struct wren_mb_rx_set_action *cmd)
{
    struct wren_mb_metadata wmd;
    uint32_t rep;

    wmd.cmd = CMD_RX_SET_ACTION;
    wmd.len = sizeof(*cmd) / 4;

    if (wren_mb_msg(wren_fd, &wmd, cmd, &rep, sizeof(rep)) < 0)
	return -1;
    return rep;

}

int
wrenctl_rx_pulser_log_index(int wren_fd, unsigned pulser_idx, unsigned *res)
{
    uint32_t idx;

    idx = pulser_idx;

    if (ioctl(wren_fd, WREN_IOC_GET_HW_LOG_INDEX, &idx) < 0) {
	return -1;
    }

    return idx;
}

int
wrenctl_rx_read_pulser_log(int wren_fd, unsigned pulser_idx, unsigned num,
			   struct wrenrx_pulser_log_entry *entries)
{
    struct wren_mb_rx_get_log cmd;
    uint32_t rep[128];
    int len;
    struct wren_mb_metadata md;
    unsigned entry_idx;
    struct timespec ts;

    cmd.log_idx = pulser_idx;

    if (wrenctl_rx_pulser_log_index(wren_fd, pulser_idx, &entry_idx) < 0)
	return -1;

    if (wrenctl_get_wr_time(wren_fd, &ts) != 0)
	return -1;

    while (num > 0) {
	unsigned i;
	unsigned l = num < 128 ? num : 128;

	cmd.nentries = l;
	cmd.entry_idx = entry_idx;

	md.cmd = CMD_RX_LOG_READ;
	md.len = sizeof(cmd) / 4;

	len = wren_mb_msg(wren_fd, &md, &cmd, rep, sizeof(rep));
	if (len == 0)
	    return -1;

	for (i = 0; i < l; i++) {
	    unsigned v = rep[i];
	    if (v == ~0U) {
		entries->sec = 0;
		v = 0;
	    }
	    else {
		entries->sec = (ts.tv_sec & ~0xffUL) | (v >> 24);
	    }
	    entries->usec = (v >> 4) & 0xfffff;
	    entries->load = (v & 1) != 0;
	    entries->start = (v & 2) != 0;
	    entries->pulse = (v & 4) != 0;
	    entries->idle = (v & 8) != 0;

	    entries++;
	}

	num -= l;
	entry_idx -= l;
    }
    return 0;
}

int
wrenctl_rx_set_comparator (int wren_fd, unsigned comp_idx,
			   const struct wren_mb_comparator *cfg)
{
    struct wren_mb_rx_set_comparator cmd;
    uint32_t rep;
    struct wren_mb_metadata md;

    cmd.comp_idx = comp_idx;
    memcpy(&cmd.cfg, cfg, sizeof(cmd.cfg));

    md.cmd = CMD_RX_SET_COMPARATOR;
    md.len = sizeof(cmd) / 4;

    if (wren_mb_msg(wren_fd, &md, &cmd, &rep, sizeof(rep)) < 0)
	return -1;
    return rep;
}

int
wrenctl_rx_pulsers_status (int wren_fd, unsigned idx,
			   uint32_t *loaded, uint32_t *running)
{
    uint32_t pulser_idx;
    uint32_t rep[2];
    int len;
    struct wren_mb_metadata md;

    pulser_idx = idx;

    md.cmd = CMD_RX_PULSERS_STATUS;
    md.len = sizeof(pulser_idx) / 4;

    len = wren_mb_msg(wren_fd, &md, &pulser_idx, rep, sizeof(rep));
    if (len < 0)
	return -1;
    if (len == 1)
	return rep[0];

    *running = rep[0];
    *loaded = rep[1];

    return 0;
}

int
wrenctl_rx_abort_pulsers(int wren_fd, unsigned grp_idx, unsigned mask)
{
    struct wren_mb_arg2 arg;
    struct wren_mb_metadata md;
    uint32_t rep;
    int len;

    arg.arg1 = grp_idx;
    arg.arg2 = mask;

    md.cmd = CMD_RX_ABORT_PULSERS;
    md.len = sizeof(arg) / 4;

    len = wren_mb_msg(wren_fd, &md, &arg, &rep, sizeof(rep));
    if (len < 0 || (md.cmd & CMD_ERROR))
	return -1;

    return 0;

}

int
wrenctl_rx_abort_comparator(int wren_fd, unsigned grp_idx, unsigned mask)
{
    struct wren_mb_arg2 arg;
    struct wren_mb_metadata md;
    uint32_t rep;
    int len;

    arg.arg1 = grp_idx;
    arg.arg2 = mask;

    md.cmd = CMD_RX_ABORT_COMPARATOR;
    md.len = sizeof(arg) / 4;

    len = wren_mb_msg(wren_fd, &md, &arg, &rep, sizeof(rep));
    if (len < 0 || (md.cmd & CMD_ERROR))
	return -1;

    return 0;

}
