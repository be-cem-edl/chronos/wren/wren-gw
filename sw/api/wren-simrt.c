#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "wren-simrt.h"

int
wren_simrt_get_time(struct wren_ts *time)
{
  struct timespec res;
  
  if (clock_gettime(CLOCK_TAI, &res) != 0) {
    fprintf(stderr, "cannot get CLOCK_TAI time: %m\n");
    exit(1);
  }

  time->sec = res.tv_sec;
  time->nsec = res.tv_nsec;

  return 0;
}

int
wren_simrt_wait_until(const struct wren_ts *t)
{
    int res;
    struct timespec ts;

    ts.tv_sec = t->sec;
    ts.tv_nsec = t->nsec;

    res = clock_nanosleep(CLOCK_TAI, TIMER_ABSTIME, &ts, NULL);
    if (res == 0)
	return 0;

    /* Interrupted */
    return -1;
}
