#ifndef __WREN_SIMRT__H__
#define __WREN_SIMRT__H__

#include "wren/wren-common.h"

/* wren-drv operations (needs to be wrapped) */
int wren_simrt_get_time(struct wren_ts *time);
int wren_simrt_wait_until(const struct wren_ts *t);

#endif /* __WREN_SIMRT__H__ */
