#ifndef __WREN_WRENTIME__H__
#define __WREN_WRENTIME__H__

#include "wren/wren-common.h"

/* wren-drv operations (needs to be wrapped) */
int wren_wren_get_time(int fd, struct wren_ts *time);
int wren_wren_wait_until(int fd, const struct wren_ts *t);

#endif /* __WREN_WRENTIME__H__ */
