#ifndef __WREN_DRV__H__
#define __WREN_DRV__H__

extern const char *progname;

struct wren_drv_tuple;

typedef void *(*wren_drv_open_t)(struct wren_drv_tuple *init);

/* Common code for rx/tx drivers. */
struct wren_drv {
    const char *name;
    const char *help;
    void *(*init)(int *argc, char *argv[]);
    wren_drv_open_t open;
};

struct wren_drv_tuple {
    const struct wren_drv *drv;
    void *init;
};

/* Extract options from command line to select a driver from drv array.
   Return NULL in case of error */
struct wren_drv_tuple *wren_drv_init(const struct wren_drv *drivers,
				     int *argc, char *argv[]);

/* Remove the first NUM args in ARGV after ARGV[0]. */
void wren_drv_remove_args(int *argc, char *argv[], unsigned num);

#endif /* __WREN_DRV__H__ */
