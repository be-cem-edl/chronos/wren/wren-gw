#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include "wren-drv.h"
#include "wren-drv-udp.h"
#include "wren-simrt.h"
#include "wrenrx-drv.h"
#include "wrenrx-drv-sim.h"

struct wrenrx_handle_udp {
  struct wrenrx_handle_sim base;

  /* File descriptor */
  int fd;
};

static int
wrenrx_drv_udp_read(struct wrenrx_handle *handle,
		    struct wren_packet *packet)
{
    struct wrenrx_handle_udp *he = (struct wrenrx_handle_udp *)handle;

    return read (he->fd, packet, sizeof (*packet));
}

static int
wrenrx_drv_udp_get_time (struct wrenrx_handle *handle,
			 struct wren_ts *time)
{
  return wren_simrt_get_time(time);
}

static void
wrenrx_drv_udp_close(struct wrenrx_handle *handle)
{
    struct wrenrx_handle_udp *he = (struct wrenrx_handle_udp *)handle;
    close(he->fd);
    free(handle);
}

static const struct wrenrx_operations wrenrx_drv_udp_ops = {
    .close = wrenrx_drv_udp_close,
    .recv_packet = wrenrx_drv_udp_read,
    .get_time = wrenrx_drv_udp_get_time,
    .add_source = wrenrx_sim_add_source,
    .pulser_define = wrenrx_sim_pulser_define,
    .wait = wrenrx_sim_wait,
    .event_subscribe = wrenrx_sim_subscribe
};

void *
wrenrx_drv_init_udp(int *argc, char *argv[])
{
    return wren_drv_init_udp(argc, argv);
}


struct wrenrx_handle *
wrenrx_open_udp_by_sockaddr (struct sockaddr_in *addr)
{
    struct wrenrx_handle_udp *handle;
    int sock;

    handle = malloc(sizeof(*handle));
    if (handle == NULL) {
	fprintf (stderr, "%s: allocate handle\n", progname);
	return NULL;
    }

    handle->base.base.op = &wrenrx_drv_udp_ops;

    wrenrx_drv_sim_init(&handle->base);

    sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP);
    if (sock < 0) {
	perror("socket() failed");
	return NULL;
    }

    if (bind(sock, (struct sockaddr *)addr, sizeof(*addr)) < 0) {
	perror("bind() failed");
	return NULL;
    }

    handle->fd = sock;

    return (struct wrenrx_handle *)handle;
}

struct wrenrx_handle *
wrenrx_drv_open_udp(void *init)
{
    struct wren_drv_udp_init_arg *arg = init;
    struct wrenrx_handle *handle;
    struct sockaddr_in mc_addr;

    memset(&mc_addr, 0, sizeof(mc_addr));
    mc_addr.sin_family  = AF_INET;
    mc_addr.sin_addr    = arg->dst_addr;
    mc_addr.sin_port    = htons(arg->dst_port);

    handle = wrenrx_open_udp_by_sockaddr(&mc_addr);
    if (!handle)
	return NULL;

    free (arg);

    return handle;
}

struct wrenrx_handle *
wrenrx_open_udp_by_port(unsigned port)
{
    struct sockaddr_in mc_addr;

    memset(&mc_addr, 0, sizeof(mc_addr));
    mc_addr.sin_family  = AF_INET;
    mc_addr.sin_addr.s_addr = htonl(INADDR_LOOPBACK);
    mc_addr.sin_port = htons(port);

    return wrenrx_open_udp_by_sockaddr(&mc_addr);
}

struct wrenrx_handle *
wrenrx_open_udp_default(void)
{
    return wrenrx_open_udp_by_port (1099);
}
