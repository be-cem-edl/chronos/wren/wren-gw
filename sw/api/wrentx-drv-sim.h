/* Virtual driver based on simulated time + send on file descriptor */
#include "wrentx-drv.h"
#include "wrentx-simvt.h"

struct wrentx_handle_sim_fd;

struct wrentx_sim_operations
{
    int (*send)(struct wrentx_handle_sim_fd *handle,
		struct wrentx_frame *frame);

    int (*wait)(struct wrentx_handle_sim_fd *handle,
		const struct wren_ts *time);
};

int wrentx_sim_send_fd(struct wrentx_handle_sim_fd *handle,
		       struct wrentx_frame *frame);
int wrentx_sim_wait_vt(struct wrentx_handle_sim_fd *handle,
		       const struct wren_ts *time);

extern const struct wrentx_sim_operations wrentx_sim_ops_fd_vt;

/* Open a simulated generator, using simulated time.  */
struct wrentx_handle *
wrentx_open_sim_fd(const struct wrentx_sim_operations *ops, int fd);
