#include <stdio.h>
#include <assert.h>
#include <string.h>
#include <stdlib.h>
#include "wren-drv.h"
#include "wrentx-drv.h"
#include "wren-util.h"

unsigned wrentx_trace_flag = 0;

static const struct wren_drv drivers[] = {
  {
    "stdout",
    "stdout\n"
    "  Send the packets to stdout\n",
    wrentx_drv_init_stdout,
    (wren_drv_open_t)wrentx_drv_open_stdout,
  },
  {
    "dump",
    "dump\n"
    "  Dump the packets (with time) to stdout\n",
    wrentx_drv_init_dump,
    (wren_drv_open_t)wrentx_drv_open_dump,
  },
  {
    "udp",
    "udp [ADDR [PORT]]\n"
    "  Send the packets to udp, default is 127.0.0.1:1099\n",
    wrentx_drv_init_udp,
    (wren_drv_open_t)wrentx_drv_open_udp,
  },
  {
    "eth1",
    "eth1 [MAC-DST [PROTO]]\n"
    "  Send the packets as raw eth packets on interface eth1\n",
    wrentx_drv_init_eth,
    (wren_drv_open_t)wrentx_drv_open_eth,
  },
  {
    "eth1+wrenctl0",
    "eth1+wrenctl0\n" /*  [MAC-DST [PROTO]]\n" */
    "  Send the packets as raw eth packets on interface eth1, "
    "use wrenctl0 for time\n",
    wrentx_drv_init_eth,
    (wren_drv_open_t)wrentx_drv_open_eth_wrenctl,
  },
  {
    "wrenctl0",
    "wrenctl0\n" /* [MAC-DST [PROTO]]\n" */
    "  Use WREN board #0\n",
    wrentx_drv_init_wrenctl,
    (wren_drv_open_t)wrentx_drv_open_wrenctl,
  },
  { NULL }
};

struct wren_drv_tuple *
wrentx_drv_init(int *argc, char *argv[])
{
  return wren_drv_init(drivers, argc, argv);
}

struct wrentx_handle *
wrentx_drv_open(struct wren_drv_tuple *init)
{
    struct wrentx_handle *res;
    res = init->drv->open(init->init);
    free (init);
    return res;
}

void
wrentx_close(struct wrentx_handle *handle)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    base->op->close(handle);
}

int
wrentx_set_source(struct wrentx_handle *handle,
		  unsigned source_idx,
		  struct wren_protocol *proto)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->set_source (handle, source_idx, proto);
}

static int
wrentx_check_params(uint32_t *w, unsigned len)
{
    for (unsigned off = 0; off < len; ) {
	uint32_t hdr = w[off];
	unsigned dt = WREN_PACKET_PARAM_GET_DT(hdr);
	unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
	unsigned tl;

	if (l < 2 || l > len - off)
	    return WRENTX_ERR_FRAME_CONSTRUCT;
	switch (dt) {
	case PKT_PARAM_U32:
	case PKT_PARAM_S32:
	    tl = 2;
	    break;
	case PKT_PARAM_S64:
	    tl = 3;
	    break;
	default:
	    return WRENTX_ERR_FRAME_CONSTRUCT;
	}
	if (l != tl)
	    return WRENTX_ERR_FRAME_CONSTRUCT;
	off += l;
    }
    return WRENTX_SUCCESS;
}

static int
wrentx_check_frame(uint32_t *w, unsigned len)
{
    if (len == 0)
	return WRENTX_ERR_FRAME_CONSTRUCT;

    for (unsigned off = 0; off < len; ) {
	struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)&w[off];

	switch(hdr->typ) {
	case PKT_CTXT:
	{
	    const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;

	    if (off + l > len || hdr->len < l || off + hdr->len > len)
		return WRENTX_ERR_FRAME_CONSTRUCT;
	    if (wrentx_check_params(&w[off + l], hdr->len - l) < 0)
		return WRENTX_ERR_FRAME_CONSTRUCT;
	    off += hdr->len;
	    break;
	}
	case PKT_EVENT:
	{
	    const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;

	    if (off + l > len || hdr->len < l || off + hdr->len > len)
		return WRENTX_ERR_FRAME_CONSTRUCT;
	    if (wrentx_check_params(&w[off + l], hdr->len - l) < 0)
		return WRENTX_ERR_FRAME_CONSTRUCT;
	    off += hdr->len;
	    break;
	}
	default:
	    return WRENTX_ERR_FRAME_CONSTRUCT;
	    break;
	}
    }

    return WRENTX_SUCCESS;
}

int
wrentx_send_frame(struct wrentx_handle *handle,
		  unsigned source_idx,
		  struct wrentx_frame *frame)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    int res;

    if (wrentx_trace_flag) {
	printf ("wrentx_send_frame: src_idx=%u\n", source_idx);
	wren_dump_packet_body(frame->pkt.u.w, frame->len, 0);
    }

    /* At this point only the body is defined, the header is only partially
       set (in particular, the size is not set) */
    res = wrentx_check_frame(frame->pkt.u.w, frame->len);
    if (res != WRENTX_SUCCESS)
	return res;

    return base->op->send_frame (handle, source_idx, frame);
}

int
wrentx_inject_frame(struct wrentx_handle *handle,
		    unsigned source_idx,
		    struct wrentx_frame *frame)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    int res;

    if (wrentx_trace_flag) {
	printf ("wrentx_inject_frame: src_idx=%u\n", source_idx);
	wren_dump_packet_body(frame->pkt.u.w, frame->len, 0);
    }

    /* At this point only the body is defined, the header is only partially
       set (in particular, the size is not set) */
    res = wrentx_check_frame(frame->pkt.u.w, frame->len);
    if (res != WRENTX_SUCCESS)
	return res;

    wrentx_frame_set_length(frame);

    return base->op->inject_frame (handle, source_idx, frame);
}

int
wrentx_get_time(struct wrentx_handle *handle, struct wren_ts *time)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    int res;

    res = base->op->get_time(handle, time);
    if (wrentx_trace_flag)
	printf ("get_time: res=%d, sec=%u, nsec=%u\n",
		res, (unsigned)time->sec, (unsigned)time->nsec);
    return res;
}

int
wrentx_wait_until(struct wrentx_handle *handle, const struct wren_ts *time)
{
  struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;

  if (wrentx_trace_flag)
      printf ("wait_until: sec=%u, nsec=%u\n",
	      (unsigned)time->sec, (unsigned)time->nsec);

  return base->op->wait_until(handle, time);
}

void
wrentx_clear_frame(struct wrentx_handle *handle,
		   struct wrentx_frame *frame)
{
  struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
  wrentx_clear_frame_base(&base->base, frame);
}

int
wrentx_load_table(struct wrentx_handle *handle,
		  unsigned table_idx,
		  const char *name,
		  unsigned source_idx,
		  struct wrentx_table *table,
		  unsigned count)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->load_table (handle, table_idx, name, source_idx,
				 table, count);
}

int
wrentx_unload_table(struct wrentx_handle *handle,
		    unsigned table_idx)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->unload_table (handle, table_idx);
}

int
wrentx_play_table(struct wrentx_handle *handle,
		  unsigned table_idx,
		  const struct wren_ts *ts)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->play_table (handle, table_idx, ts);
}

int
wrentx_play_table_on_event(struct wrentx_handle *handle,
			   unsigned source_idx,
			   unsigned table_idx,
			   wren_event_id ev)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->play_table_on_event (handle, source_idx, table_idx, ev);
}


int
wrentx_play_table_on_cond_event_s32(struct wrentx_handle *handle,
				    unsigned source_idx,
				    unsigned table_idx,
				    wren_event_id ev,
				    wren_param_id param,
				    int32_t val)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->play_table_on_cond_event_s32
	(handle, source_idx, table_idx, ev, param, val);
}

int
wrentx_stop_table(struct wrentx_handle *handle,
		  unsigned source_idx,
		  unsigned table_idx)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->stop_table (handle, source_idx, table_idx);
}

int
wrentx_get_table_name(struct wrentx_handle *handle,
		      unsigned table_idx,
		      char *name)
{
    struct wrentx_handle_op *base = (struct wrentx_handle_op *)handle;
    return base->op->get_table_name (handle, table_idx, name);
}
