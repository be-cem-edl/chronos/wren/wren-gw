#include <time.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <assert.h>
#include <stdio.h>
#include "wren/wrentx.h"
#include "wren/wren-packet.h"
#include "wren/wrentx-table.h"
#include "wrentx-private.h"
#include "wren-util.h"

void
wrentx_init_handle_base (struct wrentx_handle_base *handle)
{
}

int
wrentx_get_utc_tai_offset(struct wrentx_handle *handle, int *res)
{
    /* TODO */
    *res = 37;

    return 0;
}

struct wrentx_frame *
wrentx_alloc_frame(struct wrentx_handle *handle)
{
  struct wrentx_frame *frame;

  frame = malloc (sizeof (*frame));
  if (frame == NULL)
    return NULL;

  wrentx_clear_frame (handle, frame);

  return frame;
}

void
wrentx_free_frame(struct wrentx_frame *frame)
{
    free(frame);
}

void
wrentx_clear_frame_base(struct wrentx_handle_base *handle,
			struct wrentx_frame *frame)
{
  //  frame->handle = handle;
  frame->len = 0;
  frame->hdr = NULL;

  struct wren_packet_hdr_v1 *hdr = &frame->pkt.hdr;

  hdr->version = PKT_VERSION_v1;
  hdr->domain = 0;
  hdr->xmit = 0;

  hdr->seq_id = 0;

  hdr->next = 20;  // 1 sec, FIXME

  hdr->snd_ts = 0;
}

static void
wrentx_copy_ts(struct wren_packet_ts *dst,
	       const struct wren_ts *src)
{
  dst->sec = src->sec;
  dst->nsec = src->nsec;
}

void
wrentx_frame_set_length(struct wrentx_frame *frame)
{
    frame->pkt.hdr.len = frame->len + sizeof (struct wren_packet_hdr_v1) / 4;
}

int
wrentx_frame_fill_header(struct wrentx_handle *handle,
			 struct wrentx_source *src,
			 struct wrentx_frame *frame)
{
  int status;
  struct wren_ts ts;

  frame->pkt.hdr.domain = src->domain_id;
  frame->pkt.hdr.xmit = src->xmit_id;
  frame->pkt.hdr.seq_id = (src->seq_id + PKT_SEQ_INC) & PKT_SEQ_MASK;
  src->seq_id = frame->pkt.hdr.seq_id;

  wrentx_frame_set_length(frame);

  /* Set timestamp. */
  status = wrentx_get_time(handle, &ts);
  if (status != 0)
    return status;
  frame->pkt.hdr.snd_ts = (ts.nsec >> 8) | ((ts.sec & 3) << 22);

  return 0;
}

void
wrentx_dump_frame(struct wrentx_frame *frame)
{
  unsigned len = frame->pkt.hdr.len * 4;
  wren_dump_packet(&frame->pkt, len);
}

void
wrentx_truncate_frame(struct wrentx_frame *frame)
{
  frame->len = frame->capsule_offset;
}

int
wrentx_set_context(struct wrentx_handle *handle,
		   struct wrentx_frame *frame,
		   wren_context_id context_id,
		   struct wren_ts *valid_from)
{
  const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;
  struct wren_capsule_ctxt_hdr *ctxt;

  /* Not enough space.  */
  if (frame->len + l > WREN_PACKET_DATA_MAX_WORDS)
    return WRENTX_ERR_FRAME_FULL;

  /* A context capsule must be the first capsule */
  if (frame->len != 0)
    return WRENTX_ERR_FRAME_CONSTRUCT;

  frame->capsule_offset = frame->len;

  ctxt = (struct wren_capsule_ctxt_hdr *)&frame->pkt.u.b[frame->len * 4];
  frame->hdr = &ctxt->hdr;

  ctxt->hdr.typ = PKT_CTXT;
  ctxt->hdr.pad = 0;
  ctxt->hdr.len = l;

  ctxt->ctxt_id = context_id;
  ctxt->pad = 0;
  wrentx_copy_ts (&ctxt->valid_from, valid_from);

  frame->len += l;

  return 0;
}

int wrentx_add_event(struct wrentx_handle *handle,
                    struct wrentx_frame *frame,
                    wren_event_id id,
                    wren_context_id ctxt_id,
                    struct wren_ts *timestamp)
{
  const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;
  struct wren_capsule_event_hdr *ev;

  /* Not enough space.  */
  if (frame->len + l > WREN_PACKET_DATA_MAX_WORDS)
    return WRENTX_ERR_FRAME_FULL;

  frame->capsule_offset = frame->len;

  ev = (struct wren_capsule_event_hdr *)&frame->pkt.u.b[frame->len * 4];

  frame->hdr = &ev->hdr;

  ev->hdr.typ = PKT_EVENT;
  ev->hdr.pad = 0;
  ev->hdr.len = l;

  ev->ev_id = id;
  ev->ctxt_id = ctxt_id;
  wrentx_copy_ts (&ev->ts, timestamp);

  frame->len += l;

  return 0;
}

static int
wrentx_add_parameter_any(struct wrentx_frame *frame,
			 wren_param_id param, unsigned datatype,
			 unsigned len, const char *buf)
{
  unsigned p = frame->len;
  uint32_t tl;

  /* Need a command before adding parameters.  */
  if (p == 0)
    return WRENTX_ERR_FRAME_CONSTRUCT;

  len += 1;

  /* Not enough space in the packet.  */
  if (frame->len + len > WREN_PACKET_DATA_MAX_WORDS)
    return WRENTX_ERR_FRAME_FULL;

  /* Check length of context capsules */
  if (frame->hdr->typ == PKT_CTXT) {
    unsigned nlen = frame->hdr->len + len;
    unsigned ctxt_hdr_len = sizeof(struct wren_capsule_ctxt_hdr) / 4;
    if (nlen > WREN_CONTEXT_MAX_PARAM_WORDS + ctxt_hdr_len)
      return WRENTX_ERR_CONTEXT_TOO_LONG;
  }

  tl = (param << 16) | (datatype << 10) | len;
  frame->pkt.u.w[p + 0] = tl;
  memcpy (&frame->pkt.u.w[p + 1], buf, (len - 1) * 4);
  frame->len += len;

  frame->hdr->len += len;

  return 0;
}

/* Append a parameter to a cycle or event message.  */
int
wrentx_add_parameter_s32(struct wrentx_frame *frame,
			 wren_param_id param, int32_t value)
{
    char buf[4];

    memcpy (buf, &value, 4);
    return wrentx_add_parameter_any (frame, param, PKT_PARAM_S32, 1, buf);
}

int
wrentx_add_parameter_f32(struct wrentx_frame *frame,
			 wren_param_id param, float value)
{
    char buf[4];

    memcpy (buf, &value, 4);
    return wrentx_add_parameter_any (frame, param, PKT_PARAM_F32, 1, buf);
}

int
wrentx_add_parameter_s64(struct wrentx_frame *frame,
			 wren_param_id param, int64_t value)
{
    char buf[8];

    memcpy (buf, &value, 8);
    return wrentx_add_parameter_any (frame, param, PKT_PARAM_S64, 2, buf);
}

void
wrentx_init_source(struct wrentx_source *src)
{
    src->seq_id = PKT_SEQ_INVALID;
}

struct wrentx_source *
wrentx_check_source(struct wrentx_source *srcs, unsigned nbr_srcs,
		    unsigned source_idx)
{
    struct wrentx_source *res;
    if (source_idx >= nbr_srcs)
	return NULL;

    res = &srcs[source_idx];

    if (res->seq_id == PKT_SEQ_INVALID) {
	/* Not set */
	return NULL;
    }

    return res;
}

struct wrentx_table_exec *
wrentx_check_table(struct wrentx_table_exec *tables, unsigned nbr_tables,
		   unsigned table_idx)
{
    if (table_idx >= nbr_tables)
	return NULL;

    return &tables[table_idx];
}

void
wrentx_recycle_table(struct wrentx_table *table)
{
    table->nbr_insns = 0;
    table->nbr_data = 0;
}

struct wrentx_table *
wrentx_alloc_table(struct wrentx_handle *handle)
{
    struct wrentx_table *res;

    res = malloc (sizeof(*res));

    wrentx_recycle_table(res);
    return res;
}

void
wrentx_free_table(struct wrentx_table *table)
{
  free (table);
}

int
wrentx_table_append_wait(struct wrentx_handle *handle,
			 struct wrentx_table *table,
			 const struct wren_ts *ts)
{
    if (table->nbr_insns + 1 >= WRENTX_MAX_INSNS)
	return WRENTX_ERR_TABLE_FULL;

    table->insns[table->nbr_insns++] =
	(wrentx_table_op_wait << WREN_TABLE_OP_OFF)
      | (ts->sec << WREN_TABLE_OP_WAIT_SEC_OFF);
    table->insns[table->nbr_insns++] = ts->nsec;

    return 0;
}

int
wrentx_table_append_frame(struct wrentx_handle *handle,
			  struct wrentx_table *table,
			  struct wrentx_frame *frame)
{
    if (table->nbr_data + frame->len >= WRENTX_MAX_DATA
	|| table->nbr_insns >= WRENTX_MAX_INSNS)
	return WRENTX_ERR_TABLE_FULL;

    /* Add instruction */
    table->insns[table->nbr_insns++] =
      (wrentx_table_op_send << WREN_TABLE_OP_OFF)
      | (frame->len << WREN_TABLE_OP_SEND_LEN_OFF)
      | (table->nbr_data << WREN_TABLE_OP_SEND_ADDR_OFF);

    /* Copy data */
    memcpy (&table->data[table->nbr_data], frame->pkt.u.b, frame->len * 4);

    /* TODO: check only event capsule (no context) */

    table->nbr_data += frame->len;

    return 0;
}
