#include <stdio.h>
#include <string.h>
#include <assert.h>
#include "wren/wren-packet.h"
#include "wren-util.h"

int
wren_dump_params(unsigned poff, const uint32_t *buf, unsigned len)
{
    unsigned off = 0;

    while (off < len) {
	uint32_t hdr = buf[off];
	unsigned dt = WREN_PACKET_PARAM_GET_DT(hdr);
	unsigned l = WREN_PACKET_PARAM_GET_LEN(hdr);
	
	printf ("(@%02uw)  param: %04x, len: %uw, typ: %u",
		poff, WREN_PACKET_PARAM_GET_TYP(hdr), l, dt);
	if (l < 2 || l > len - off) {
	    printf("  TRUNCATED (rem len=%u)\n", len - off);
	    return -1;
	}
	switch (dt) {
	case PKT_PARAM_U32:
	    if (l != 2) {
		printf ("  BAD LENGTH");
	    }
	    else {
	      printf (" u32: %08x", buf[off + 1]);
	    }
	    break;
	case PKT_PARAM_S32:
	    if (l != 2) {
		printf ("  BAD LENGTH");
	    }
	    else {
	      printf (" s32: %08x", buf[off + 1]);
	    }
	    break;
	case PKT_PARAM_S64: {
	    union {
		int64_t u64;
		uint32_t u32[2];
	    } u;
	    if (l != 3) {
		printf ("  BAD LENGTH");
	    }
	    else {
	      u.u32[0] = buf[off + 1];
	      u.u32[1] = buf[off + 2];
	      printf (" s64: %016lx", u.u64);
	    }
	    break;
	}
	default:
	    printf (" unknown dt=%u", dt);
	}
	printf ("\n");
	off += l;
	poff += l;
    }
    return 0;
}

void
wren_dump_packet_body(const uint32_t *w, unsigned len, unsigned init_off)
{
  unsigned off;

  for (off = 0; off < len; ) {
    struct wren_capsule_hdr *hdr = (struct wren_capsule_hdr *)&w[off];
    unsigned coff = init_off + off;

    switch(hdr->typ) {
    case PKT_CTXT:
      {
        const unsigned l = sizeof (struct wren_capsule_ctxt_hdr) / 4;
        struct wren_capsule_ctxt_hdr *ctxt =
	  (struct wren_capsule_ctxt_hdr *)hdr;

	if (off + l > len) {
	    printf (" TRUNCATED (capsule hdr len=%u, rem len=%u)\n",
		    l, len - off);
	    return;
	}
        printf ("(@%02uw) CONTEXT: id: %u, len: %u\n",
		coff, ctxt->ctxt_id, hdr->len);
        printf ("(@%02uw) from ts: %u+%09uns\n",
		coff + 1, ctxt->valid_from.sec, ctxt->valid_from.nsec);
	if (hdr->len < l || off + hdr->len > len) {
	    printf("  TRUNCATED (rem len=%u)\n", len - off);
	    return;
	}
        if (wren_dump_params (coff + l, &w[off + l], hdr->len - l) < 0)
	    return;
        off += hdr->len;
      }
      break;
    case PKT_EVENT:
      {
        const unsigned l = sizeof (struct wren_capsule_event_hdr) / 4;
        struct wren_capsule_event_hdr *ev =
	    (struct wren_capsule_event_hdr *)hdr;

	if (off + l > len) {
	    printf (" TRUNCATED (capsule hdr len=%u, rem len=%u)\n",
		    l, len - off);
	    return;
	}

        printf ("(@%02uw) EVENT: id: %u, context: %u, len: %u\n",
		coff, ev->ev_id, ev->ctxt_id, hdr->len);
        printf ("(@%02uw) act ts: %u+%09uns\n",
		coff + 2, ev->ts.sec, ev->ts.nsec);

	if (hdr->len < l || off + hdr->len > len) {
	    printf("  TRUNCATED (rem len=%u)\n", len - off);
	    return;
	}

        if (wren_dump_params (coff + l, &w[off + l], hdr->len - l) < 0)
	    return;
        off += hdr->len;
      }
      break;
    default:
	printf("(@%02uw) ???? typ=%u\n", coff, hdr->typ);
	return;
    }
  }
}

void
wren_dump_packet(struct wren_packet *pkt, unsigned len)
{
  const unsigned hlen = sizeof (struct wren_packet_hdr_v1) / 4;

  if (len < sizeof (struct wren_packet_hdr_v1)) {
    printf("...truncated (len=%ub)\n", len);
    return;
  }

  printf ("(@00w) ver: 0x%02x, domain: 0x%02x xmit: 0x%02x next: %u\n",
          pkt->hdr.version,
          pkt->hdr.domain,
	  pkt->hdr.xmit,
	  pkt->hdr.next);
  printf ("(@01w) pkt len: %uw, seq_id: 0x%04x snd ts: %u+%09uns\n",
          pkt->hdr.len,
	  pkt->hdr.seq_id,
	  pkt->hdr.snd_ts >> 22, (pkt->hdr.snd_ts & ((1 << 22) - 1)) << 8);

  if (pkt->hdr.len * 4 > len) {
      printf("  TRUNCATED (len=%ub=%uw)\n", len, len / 4);
      return;
  }
  if (pkt->hdr.len < hlen) {
      printf("  INCORRECT len (hdr len=%us)\n", hlen);
      return;
  }
  wren_dump_packet_body(pkt->u.w, pkt->hdr.len - hlen, hlen);
}


/* Raw dump. */
void
wren_dump_hex (unsigned char *buf, unsigned wlen)
{
  unsigned off;

  for (off = 0; off < wlen; off += 4) {
    unsigned i;

    printf ("%04x:", off);
    for (i = off; i < wlen && i < off + 4; i++) {
      unsigned char *b = buf + i * 4;
      printf ("  %02x %02x %02x %02x", b[0], b[1], b[2], b[3]);
    }
    printf("\n");
  }
}

void
wren_dump_packet_hex (struct wren_packet *pkt)
{
  wren_dump_hex ((unsigned char *)pkt, pkt->hdr.len);
}


void
wren_add_nsec(struct wren_ts *ts, unsigned nsec)
{
  assert (nsec < 1000000000);

  ts->nsec += nsec;
  if (ts->nsec >= 1000000000) {
    ts->nsec -= 1000000000;
    ts->sec++;
  }
}

void
wren_add_ts(struct wren_ts *ts, const struct wren_ts *v)
{
  wren_add_nsec(ts, v->nsec);
  ts->sec += v->sec;
}

int
wren_ts_lt(const struct wren_ts *l, const struct wren_ts *r)
{
    if (l->sec < r->sec)
	return 1;
    if (l->sec == r->sec && l->nsec < r->nsec)
	return 1;
    return 0;
}

int
wren_ts_eq(const struct wren_ts *l, const struct wren_ts *r)
{
    return l->sec == r->sec && l->nsec == r->nsec;
}

void
wren_read_packet_ts(struct wren_ts *dest, const struct wren_packet_ts *ts)
{
    dest->nsec = ts->nsec;
    dest->sec = ts->sec;
}

void
wren_add_packet_ts(struct wren_packet_ts *ts, const struct wren_ts *add)
{
  ts->nsec += add->nsec;
  if (ts->nsec > 1000000000) {
    ts->nsec -= 1000000000;
    ts->sec++;
  }
  ts->sec += add->sec;
}
