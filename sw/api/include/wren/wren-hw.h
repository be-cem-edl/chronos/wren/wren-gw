#ifndef __WREN__WREN_HW__H__
#define __WREN__WREN_HW__H__

/* Constants defined by the hardware */

/* Pulser start/stop/clock values */

#define WRENRX_INPUT_PULSER_0 0	/* Output of pulsers in the same group */
#define WRENRX_INPUT_PULSER_7 7

#define WRENRX_INPUT_EXTERN_1 8	/* External inputs (front-panel) */
#define WRENRX_INPUT_EXTERN_8 15
#define WRENRX_INPUT_EXTERN_14 21

#define WRENRX_INPUT_CLOCK_PPS   22	/* PPS clock */
#define WRENRX_INPUT_CLOCK_1KHZ  23
#define WRENRX_INPUT_CLOCK_1MHZ  24
#define WRENRX_INPUT_CLOCK_10MHZ 25
#define WRENRX_INPUT_CLOCK_40MHZ 26
#define WRENRX_INPUT_CLOCK_FREV1 27
#define WRENRX_INPUT_CLOCK_BUNCH1 28
#define WRENRX_INPUT_CLOCK_FREV2 29
#define WRENRX_INPUT_CLOCK_BUNCH2 30
#define WRENRX_INPUT_CLOCK_1GHZ  31

#define WRENRX_INPUT_NOSTART     31 /* No start input */
#define WRENRX_INPUT_NOSTOP      31 /* No stop input */

/* TODO: RF1-2 Frev, RF1-2 Bunch clock */

/* Sizes defined by the hardware */
/* Number of pulser groups */
#define WREN_NBR_GROUPS 4

#define WREN_PULSERS_PER_GROUP 8
#define WREN_COMPARATORS_PER_GROUP 64

#define WREN_COMPARATORS_PER_PULSER (WREN_COMPARATORS_PER_GROUP / WREN_PULSERS_PER_GROUP)

#define WREN_NBR_PULSERS (WREN_PULSERS_PER_GROUP * WREN_NBR_GROUPS)
#define WREN_NBR_COMPARATORS (WREN_COMPARATORS_PER_GROUP * WREN_NBR_GROUPS)
#define WREN_NBR_PINS (32 + WREN_NBR_PULSERS)

#endif
