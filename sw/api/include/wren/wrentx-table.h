/* Should be considered as private */

#ifndef __WRENTX_TABLE__H__
#define __WRENTX_TABLE__H__

#include <stdint.h>

enum wrentx_table_op {
  /* Encoding: 0x00zz_zzzz */
  wrentx_table_op_nop,

  /* Encoding: 0x01LL_AAAA (LL: length, AAAA: address in data */
  wrentx_table_op_send,
  /* Encoding: 0x02SS_SSSS 0xNNNN_NNNN (SSSSSS: seconds, NNNNNNNN: nsec) */
  wrentx_table_op_wait
};

#define WREN_TABLE_OP_OFF 24
#define WREN_TABLE_OP_SZ   8

#define WREN_TABLE_OP_SEND_LEN_OFF 16
#define WREN_TABLE_OP_SEND_LEN_SZ   8
#define WREN_TABLE_OP_SEND_ADDR_OFF  0
#define WREN_TABLE_OP_SEND_ADDR_SZ  16

#define WREN_TABLE_OP_WAIT_SEC_OFF  0
#define WREN_TABLE_OP_WAIT_SEC_SZ  24

#define WREN_TABLE_OP_GET_FIELD(VAL, FIELD) \
  ((VAL >> WREN_TABLE_OP_##FIELD##_OFF) & ((1 << WREN_TABLE_OP_##FIELD##_SZ) - 1))

#define WREN_TABLE_OP_GET_OP(VAL) \
  ((VAL >> WREN_TABLE_OP_OFF) & ((1 << WREN_TABLE_OP_SZ) - 1))

#endif /* __WRENTX_TABLE__H__ */
