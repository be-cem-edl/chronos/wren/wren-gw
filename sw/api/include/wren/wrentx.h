/* WhiteRabbit Event Node Transmitter.

   The purpose of the event node transmitter (WRENTX) is to distribute
   events over a WhiteRabbit (WR) network to WR event node receivers
   (WRENRX).

   There is no specific hardware for WRENTX.

   An event consits of:
   - An identifier for the kind of event.  This is a user-defined number that
     could be reused for any occurence of the event.
   - A timestamp for the time when the event occurs.  It should be a time
     in the future (the due time).
   - A set of parameters.  A parameter is an identifier and its
     value.  Parameters are opaque and are not interpreted by the generator.

   In order to reduce the amount of parameters per event, there is a notion
   of context.  A context is a super-event: it has a timestamp and parameters.
   Contexts describe properties that stay for a long time.  A context ends
   when the next context starts.
   As events are usually attached to a context, they have an identifier so that
   the event can refer to a specific context.  Once a context ends, it is
   fully discarded and no event can refer to it anymore (TBC).

   The timestamp is always a TAI timestamp as this is the only clock source
   known by WRENTX.

   Multiple transmitters can transmit over the same network, and a receiver can
   be interested in flows from different transmitters.
   It could be possible to distinguish transmitters by ethernet mac address, or
   IP address, or UDP port (if IP or UDP are used), but to simplify filtering
   a domain identifier (a number) is used.  The number of domains handled by
   a receiver can be restricted as well as the number of bits for the
   domain identifier.

   The protocol is not defined by this specification, but the transmitter only
   multicast (or broadcast) messages over the WR network.  It is not expected
   to receive data.
   As a consequence transmitters have no idea about the state of the receivers.

   This file contains the common API.  This API can be executed either from
   the host (see wrentx-host.h) or from a CPU on the board (see wrentx-rt.h) in
   real-time.

   TODO: max period between packets.
   TODO: sync lost
 */

#ifndef __WRENTX__H__
#define __WRENTX__H__

#include <stdint.h>
#include <arpa/inet.h>
#include "wren/wren-common.h"

#ifdef __cplusplus
extern "C" {
#endif

/** \file wren/wrentx.h
    \brief API for the event transmitter

    \example example_wrentx_build.c

    \todo Get version; get configuration (number of inputs, type of board);
    \todo Diagnostics: log the pulses ?
    \todo Reset of WRPC core ?
*/

/** An opaque structure used through the API.  It contains the association of
   the software with the hardware.  */
struct wrentx_handle;

/* See return status in wren-common.h */

#ifndef CTYPESGEN
/* Create an handle using UDP to simulate transport */
struct wrentx_handle *wrentx_open_udp_by_sockaddr (struct sockaddr_in *addr);
#endif

/* Same as before but using localhost for destination address. */
struct wrentx_handle *wrentx_open_udp_by_port(unsigned port);

/* Same as before, but using localhost:1099 */
struct wrentx_handle *wrentx_open_udp_default(void);

/* Create an handle using 'eth1' to send packets and wren0 to get time */
struct wrentx_handle *wrentx_open_eth_wren_default(void);

/* Create an handle using DRV (should be /dev/wrenctlX).
   TODO */
struct wrentx_handle *wrentx_open_wrenctl(const char *drv);

/* Close the handle */
void wrentx_close(struct wrentx_handle *handle);

/* Define a source to transmit on. */
int wrentx_set_source(struct wrentx_handle *handle,
		      unsigned source_idx,
		      struct wren_protocol *proto);

/** \brief Get current TAI time

   @param[in] handle The handle to the hardware
   @param[out] time The result
   @return \ref WRENTX_SUCCESS or an error code
 */
int wrentx_get_time(struct wrentx_handle *handle, struct wren_ts *time);

/** \brief Get current offset from UTC to TAI in seconds.
    TAI = UTC + result
    Currently the offset is 37 sec.
    User is responsible to handle leap second changes.

    @return success in general.
*/
int wrentx_get_utc_tai_offset(struct wrentx_handle *handle, int *res);

/** \brief Abstract type for a frame
 */
struct wrentx_frame;

/** \brief Allocate a frame.

   Create and initialize a frame.  Must be called before any other message
   operations. The second step is to build a frame, adding a context,
   parameters for the context, events and their parameters.

   Once a frame has been built, it can be sent or used for a row in a
   table.

   Finally, you can either free the frame memory or recycle the memory
   to build another frame.

   @param[in] handle The handle to the hardware
   @return The frame; this function should not fail.
  */
struct wrentx_frame *wrentx_alloc_frame(struct wrentx_handle *handle);

/** \brief Free a frame and its resources.

   @param[in] frame The frame to initialize
 */
void wrentx_free_frame(struct wrentx_frame *frame);

/** \brief Clear a frame to re-use it after a send.

   @param[in] handle The handle to the hardware
   @param[in] frame The frame to initialize
 */
void wrentx_clear_frame(struct wrentx_handle *handle,
                       struct wrentx_frame *frame);

/** \brief Truncate a frame by removing last capsule

    Manage frame size by removing the last incomplete capsule from the
    frame. This function can be called when \ref wrentx_add_event or
    \ref wrentx_add_parameter_* functions return \ref WRENTX_ERR_FRAME_FULL

    @param[in] frame The frame to truncate
 */
void wrentx_truncate_frame(struct wrentx_frame *frame);

/** \brief Start a context capsule

   Build a context message identified; parameters can then be appended.
   There can be only one context per frame and it should be the first
   capsule (it should come before any event).

   @param[in] handle The handle to the hardware
   @param[in] frame The frame
   @param[in] context_id The context identifier of the capsule.

    @return \ref WRENTX_SUCCESS
 */
int wrentx_set_context(struct wrentx_handle *handle,
		       struct wrentx_frame *frame,
		       wren_context_id context_id,
		       struct wren_ts *valid_from);

/** \brief Start an event capsule

    Build an event message for event ID.  The event will trigger or has
    triggered at time \p timestamp and is related to context \p context_id
    (or \ref WREN_CONTEXT_NONE if not related to any context).
    There can be multiple event capsules or context capsules in a frame.

    @param[in] handle The handle to the hardware
    @param[in] frame The frame
    @param[in] id The event id
    @param[in] context_id The context identifier
    @param[in] timestamp The event timestamp (due time)

    @return 0 on success, < 0 on failure (eg: not enough space in the frame).
*/
int wrentx_add_event(struct wrentx_handle *handle,
                    struct wrentx_frame *frame,
                    wren_event_id id,
                    wren_context_id context_id,
                    struct wren_ts *timestamp);

/** \brief Append a 32b signed parameter to a capsule

    Append a parameter to a context or event message.

    \todo How can user estimate the size of a frame to avoid error on
    full frame ?  Can the user remove the last but uncomplete capsule ?
    Can the user reuse the incomplete capsule for the next frame ?

    @param[in] frame The frame
    @param[in] param The parameter identifier
    @param[in] value The value of the parameter
*/
int wrentx_add_parameter_s32(struct wrentx_frame *frame,
			     wren_param_id param, int32_t value);

/** \brief Append a 32b float parameter to a capsule
 */
int wrentx_add_parameter_f32(struct wrentx_frame *frame,
			     wren_param_id param, float value);
/** \brief Append a 64b signed parameter to a capsule
 */
int wrentx_add_parameter_s64(struct wrentx_frame *frame,
			     wren_param_id param, int64_t value);

/** \brief Send a frame

   Send a message (which must have been built).
   The message is sent as soon as possible.
   The frame is still usable after this call, but must be re-initialized.

   @param[in] handle The handle to the hardware
   @param[in] frame The frame to send.  Must have been built, and can be
     reused after the call (but it must be re-initialized).
   @return \ref WRENTX_SUCCESS, \ref WRENTX_INPUT_PULSE if a pulse has been
     received, or an error code.
*/
int wrentx_send_frame(struct wrentx_handle *handle,
		      unsigned source_idx,
		      struct wrentx_frame *frame);

/** \brief Wait until absolute time.

    This function is blocking.
*/
int wrentx_wait_until(struct wrentx_handle *handle,
		      const struct wren_ts *time);

/** \brief Opaque structure for a table.

    A table is composed of row (from 0 to N-1), which are instructions.
    The main instruction is sending a frame, but it is also possible to
    do simple loops.  The instructions are executed until the end of the
    table, but execution can be suspended by a wait instruction.  This is
    necessary if the number of events is too high or if the timestamp of
    the events are far apart.

    A table is played on an asynchronous cause, which is either a pulse
    on an input or an API call.

    Once a table is built (in the host memory), it needs to be loaded into
    the board.
*/
struct wrentx_table;

/** \brief Allocate a table.

    It is initially empty.
*/
struct wrentx_table *wrentx_alloc_table(struct wrentx_handle *handle);

/** \brief Free a table.
 */
void wrentx_free_table(struct wrentx_table *table);

/** \brief Append a frame to a table.

    The frame will be send when the table row is reached.
    Any context set to WREN_CONTEXT_CURRENT is replaced by the
    current one (according to the timestamp). Any timestamp in the frame
    will be added with the play timestamp (which is the start timestamp
    added to all wait delays).
*/
int wrentx_table_append_frame(struct wrentx_handle *handle,
			      struct wrentx_table *table,
			      struct wrentx_frame *frame);

/** \brief Suspend execution of the table for a relative delay.

    The delay is relative to the absolute time at the end of the last delay,
    or to the absolute time of the start of the table if there was no
    previous delay.
*/
int wrentx_table_append_wait(struct wrentx_handle *handle,
			     struct wrentx_table *table,
			     const struct wren_ts *ts);

/** \brief Load table in memory

    Replace table \p table_idx with \p table.
    If the table is being executed, it is aborted

    Argument \p count specifies the number of execution of the table once it
    is started.  So usually it is 1 (so the execution stops at the end of the
    table), but could be greather.  0 means infinite repeatition.
*/
int wrentx_load_table(struct wrentx_handle *handle,
		      unsigned table_idx,
		      const char *name,
		      unsigned source_idx,
		      struct wrentx_table *table,
		      unsigned repeat_count);

/** \brief Unload a table.

    After this call, there is no more table associated with \p table_idx
*/
int wrentx_unload_table(struct wrentx_handle *handle,
			unsigned table_idx);

/** \brief Start table execution

    Start execution of a table.  If the table is already started, this is
    a no-op.  The table must have been loaded.
    Timestamp \p ts is the base timestamp for event due time.  However,
    the execution of the table is immediate.
*/
int wrentx_play_table(struct wrentx_handle *handle,
		      unsigned table_idx,
		      const struct wren_ts *ts);

/** \brief Connect an input to a table

    In case of pulse on \p input, the table \p table_idx will be played
    using the pulse timestamp.
    If a table is already being executed, a new pulse or event is a no-op.

    TODO: remove connection ?
    TODO: only one connection ?
*/
int wrentx_play_table_on_input(struct wrentx_handle *handle,
			       unsigned source_idx,
			       unsigned table_idx,
			       unsigned input);

int wrentx_play_table_on_event(struct wrentx_handle *handle,
			       unsigned source_idx,
			       unsigned table_idx,
			       wren_event_id ev);

int wrentx_play_table_on_cond_event_s32(struct wrentx_handle *handle,
					unsigned source_idx,
					unsigned table_idx,
					wren_event_id ev,
					wren_param_id param,
					int32_t val);

/** \brief Stop table execution (abort)

    Stop execution of a table.  If the table is not executed, this is
    a no-op
*/
int wrentx_stop_table(struct wrentx_handle *handle,
		      unsigned source_idx,
		      unsigned table_idx);


/* Get the name of table \p table_idx, stored into \p name whose length is
   WRENTX_TABLE_NAME_MAXLEN (including the NUL).
   Return 0 on success (table exists)
*/
int wrentx_get_table_name(struct wrentx_handle *handle,
			  unsigned table_idx,
			  char *name);

/**
  @{ \name Configuration paramater
*/
/**
  \brief Input Configuration Parameter
*/
/** \brief Enable a pin */
#define WRENTX_PARAM_ENABLE 0
/** \brief Set polarity */
#define WRENTX_PARAM_POLARITY 1
/**
   @}
*/

/** \brief Configure an input */
int wrentx_input_set_config(struct wrentx_handle *handle,
			    unsigned input,
			    unsigned param,
			    unsigned val);
int wrentx_input_get_config(struct wrentx_handle *handle,
			    unsigned input,
			    unsigned param,
			    unsigned *val);

/** \brief Inject a frame

    This is for debugging only.
    The frame is not sent.  It behaves as if the frame was received on
    receiver source.
*/
int wrentx_inject_frame(struct wrentx_handle *handle,
			unsigned source_idx,
			struct wrentx_frame *frame);

#ifdef __cplusplus
}
#endif

#endif /* #define __WRENTX__H__ */
