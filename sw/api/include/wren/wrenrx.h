#ifndef __WREN__WRENRX__H__
#define __WREN__WRENRX__H__
/* WhiteRabbit Timing Receiver.

   The boards contain:
   * a WR PTP core
   * Pulsers
   * A processor.

   So a receiver can receive data and events from the sender and pass them
   (using filtering) to the driver and applications.
   The driver can also generate pulses at a specific time, and thanks to WR,
   the time is absolute.
   Finally, the embedded processor can also receive specific events and
   directly generate pulses according to a policy defined by the driver
   (like events selection, filter on parameters, delay and pulser
   configuration).

   A trigger is defined by a set of conditions on a cycle and optionnaly
   an associated event, when they are received from the network.

   A trigger can configure a pulser for a time relative to the event,
   or schedule an interrupt to the cpu.

   TODO: logs, stats.
*/
#include "wren/wren-common.h"

#ifdef __cplusplus
extern "C" {
#endif

/* An opaque structure used through the API.  It contains the association of
   the software with the hardware.  */
struct wrenrx_handle;

/* Create an association with the hardware.  Returns NULL and set errno in
   case of error.
   NOTE: the arguments will probably change, and it may be possible to have
   multiple functions to open a device.  Will be refined. */
struct wrenrx_handle *wrenrx_open(const char *filename);

/* Open using /dev/wren-lunNNN, which is supposed to be a symlink to the
   real device driver.  The LUN add an indirection for any purpose... */
struct wrenrx_handle *wrenrx_open_by_lun(unsigned lun);

/* For simulation: receive on a UDP socket */
struct wrenrx_handle *wrenrx_open_udp_by_port(unsigned port);
struct wrenrx_handle *wrenrx_open_udp_default(void);

/* Enumerate available LUNs.
   ARR is an array of unsigned, each unsigned is supposed to be 32b, and
   each bit is set if the corresponding LUN exists.
   ARR must be large enough to contain at least NBR bits. */
void wrenrx_enumerate_luns(unsigned *arr, unsigned nbr);

/* Close the association.  */
int wrenrx_close(struct wrenrx_handle *handle);

/* Get current time.  Should return 0. */
int wrenrx_get_time(struct wrenrx_handle *handle, struct wren_ts *time);

/* Return 1 if there is a network link */
int wrenrx_get_link(struct wrenrx_handle *handle);

/* Return 1 if there is a network synchronization */
int wrenrx_get_sync(struct wrenrx_handle *handle);

/* Fill VERSION with a (long) string containing version elements for
   hardware, gateware, driver and software */
int wrenrx_get_version(struct wrenrx_handle *handle,
		       char *version,
		       unsigned maxlen);

/* Fill CONFIG with the configuration of front and patch panels */
int wrenrx_get_panel_config(struct wrenrx_handle *handle,
			    struct wren_panel_config *config);

/* Add a filter for a source.  Once a filter a set, the packets coming
   from this source are considered by the board and are accepted.

   It is recommanded to define the filters once at boot time.

   The number of filters is limited and defined by the hardware.

   Return the source index, or < 0 in case of error.

   Discussion:
   - Do we want to allow filter overwrite ? Probably not
   - Do we want to remove a filter ? Probably useless in operation, but why
     not.
   - Do we want to have symbolic index (so that even if each board handles
     a subset of all sources, the same index designate the same source
     everywhere) ?  For upper software layer.
 */
int wrenrx_add_source(struct wrenrx_handle *handle,
		      struct wren_protocol *proto);

/* Get the configuration of a source. */
int wrenrx_get_source(struct wrenrx_handle *handle,
		      unsigned source_idx,
		      struct wren_protocol *proto);

/* Remove a source.  All associated pulsers are removed.  Loaded pulsers
   are removed, and subscribed events are unsubscribed */
int wrenrx_remove_source(struct wrenrx_handle *handle,
			 unsigned source_idx);

/* Subscribe and unsubscribe to contexts of a source. */
int wrenrx_context_subscribe(struct wrenrx_handle *handle,
			     unsigned source_idx);
int wrenrx_context_unsubscribe(struct wrenrx_handle *handle,
			       unsigned source_idx);


/* TODO: get state, config, stats...
   configure BST clocks
*/

/* Opaque structure.  */
struct wrenrx_cond;

/* Allocate and initialize a condition structure.  */
struct wrenrx_cond *wrenrx_cond_alloc(struct wrenrx_handle *handle);

/* Free a condition structure, after use.  */
void wrenrx_cond_free(struct wrenrx_cond *cond);

/* Recycle a condition structure (to avoid a free+alloc). */
void wrenrx_cond_reinit (struct wrenrx_handle *handle,
			 struct wrenrx_cond *cond);

enum wrenrx_cond_comp {
  WRENRX_EQ,  /* Equal */
  WRENRX_NE,  /* Not equal */
  WRENRX_SLT, /* Signed less than */
  WRENRX_SLE, /* Signed less or equal */
  WRENRX_SGT, /* Signed greater than */
  WRENRX_SGE  /* Signed greater or equal */
};

/* Add an expression to a condition, for PARAM OP VAL.
   ORIG is a bitmask of WRENRX_PARAM_CONTEXT or WRENRX_PARAM_EVENT.  It
   defines where the parameter should be looked for (priority to event).
   if 1, it is looked at from the event. */
struct wrenrx_cond_expr *wrenrx_cond_param_u32 (struct wrenrx_cond *cond,
						enum wrenrx_cond_comp op,
						unsigned orig,
						wren_param_id param,
						uint32_t val);

/* Relations between expressions */
enum wrenrx_cond_rel {
  WRENRX_AND,
  WRENRX_OR,
  WRENRX_NOT /* Right argument is null */
};

/* Add an operation between conditions.  */
struct wrenrx_cond_expr *wrenrx_cond_rel (struct wrenrx_cond *cond,
					  enum wrenrx_cond_rel op,
					  struct wrenrx_cond_expr *l,
					  struct wrenrx_cond_expr *r);


/* Define a condition, which then has to be connected to an interrupt or
   a pulser.
   When an event message from SOURCE_ID is received for event EV_ID, the
   expression EXPR is evaluated.  If true, the action is run.

   Condition can be NULL.  */
int wrenrx_cond_define (struct wrenrx_cond *cond,
			unsigned source_idx,
			wren_event_id ev_id,
			struct wrenrx_cond_expr *expr);

/* Subscribe to a condition.  The event will be delivered to the client
   at its due time + OFFSET_US (which can be negative).

   Return < 0 in case of error,
   or the evsubs id (used to identify the event or to unsubscribe) */
int wrenrx_event_subscribe (struct wrenrx_handle *handle,
			    unsigned source_idx,
			    wren_event_id ev_id,
			    int offset_us);

int wrenrx_event_unsubscribe (struct wrenrx_handle *handle,
			      unsigned evsub_idx);

/* TODO: absolute interrupt (based on time) ?  */

struct wrenrx_pulser_configuration
{
    /* Configuration enabled flag, if 0 this configuration is ignored but
       present */
    unsigned char config_en;

    /* Interrupt enabled flag: if 1, an interrupt is generated on when
       a rising edge is generated (even if outputs are disabled) */
    unsigned char interrupt_en;

    /* Output enabled: if 0, the output stays to 0 */
    unsigned char output_en;

    /* If 1, the configuration is kept in the pulser and can be restarted
       with a start pulse.  A stop pulse does not disable the pulser but
       makes it ready again.  The pulser must be either be reloaded or
       disabled by software to exit from the repeat mode. */
    unsigned char repeat_mode;

    /* Start, stop and clock inputs */
    unsigned start;
    unsigned stop;
    unsigned clock;

    /* Initial delay after the start pulse, in clock cycles
       (used only if the start input is used) */
    unsigned initial_delay;

    /* Duration of the high pulse in ns */
    unsigned pulse_width;
    /* Total duration of the pulse (hi + low) in clock cycles,
       or 0 for level (which is permanent until a stop).  */
    unsigned pulse_period;

    /* Number of pulses, or 0 if infinite. */
    unsigned npulses;

    /* Load offset: time offset on the event due time. */
    int32_t load_offset_sec;
    int32_t load_offset_nsec;
};

/* Add an action for pulser \p pulser_num (between 1 and 32).

   Return a config_idx handle or error if value < 0.
 */
int wrenrx_pulser_define (struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_cond *cond,
			  const struct wrenrx_pulser_configuration *conf,
			  const char *name);

/* Remove a configuration. */
int wrenrx_pulser_remove (struct wrenrx_handle *handle,
			  unsigned config_idx);

/* Modify the configuration of an action
   TODO: add a bitmask to specify which fields change. */
int wrenrx_pulser_reconfigure (struct wrenrx_handle *handle,
			       unsigned config_idx,
			       const struct wrenrx_pulser_configuration *conf);

/* Read configuration (and name).
   If COND is not NULL, it will be filled with the condition. */
int wrenrx_pulser_get_config (struct wrenrx_handle *handle,
			      unsigned config_idx,
			      unsigned *pulse_num,
			      struct wrenrx_pulser_configuration *conf,
			      struct wrenrx_cond *cond,
			      char *name);

/* Force a pulser using temporary configuration CONF.
   User will get a message for each pulse, with invalid event, no context id,
   but the correct source.  TS must be null. */
int wrenrx_pulser_force(struct wrenrx_handle *handle,
			unsigned config_idx,
			struct wren_ts *ts,
			struct wrenrx_pulser_configuration *conf);

enum wrenrx_cond_expr_kind { WRENRX_COND, WRENRX_BINARY };

/* Get fields of a condition or expression */
unsigned wrenrx_cond_get_source_idx(const struct wrenrx_cond *cond);
wren_event_id wrenrx_cond_get_event_id(const struct wrenrx_cond *cond);
const struct wrenrx_cond_expr *wrenrx_cond_get_expr(const struct wrenrx_cond *cond);
enum wrenrx_cond_expr_kind wrenrx_cond_expr_get_kind(const struct wrenrx_cond_expr *expr);
enum wrenrx_cond_comp wrenrx_cond_expr_cond_get_op(const struct wrenrx_cond_expr *expr);
unsigned wrenrx_cond_expr_cond_get_orig(const struct wrenrx_cond_expr *expr);
wren_param_id wrenrx_cond_expr_cond_get_param(const struct wrenrx_cond_expr *expr);
uint32_t wrenrx_cond_expr_cond_get_value_u32(const struct wrenrx_cond_expr *expr);
enum wrenrx_cond_rel wrenrx_cond_expr_bin_get_op(const struct wrenrx_cond_expr *expr);
const struct wrenrx_cond_expr *wrenrx_cond_expr_bin_get_left(const struct wrenrx_cond *cond, const struct wrenrx_cond_expr *expr);
const struct wrenrx_cond_expr *wrenrx_cond_expr_bin_get_right(const struct wrenrx_cond *cond, const struct wrenrx_cond_expr *expr);

/* Subscribe and unsubscribe to a configuration.  Will receive pulses for
   the configuration */
int wrenrx_config_subscribe(struct wrenrx_handle *handle,
			    unsigned config_idx);
int wrenrx_config_unsubscribe(struct wrenrx_handle *handle,
			      unsigned config_idx);

/* TODO: get a list of existing config_id for a pulser_num */

/* Program a pulser (independently of events).

   The pulser \p pulser_num will be programmed at time \p time (if set) or
   immediately (if \p time is NULL).

   TODO:
   * define interraction with pulser_configure.
   * clear (unload) a pulser
   * define what happens if \p time is in the past
*/
int wrenrx_pulser_program(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  const struct wrenrx_pulser_configuration *conf,
			  const struct wren_ts *time,
			  const char *name);

/* Output configuration.  */
struct wrenrx_output_configuration
{
    /* If set, the output of the pulsers are inverted before being
       combined.  Using De Morgan's law, the pulser outputs can be and-ed */
    uint8_t invert_pulsers;
    /* Bit mask of outputs being or-ed.
       If bit N is set, the output N+1 is part of the or-gate. */
    uint8_t mask;
    /* If set, invert the result of the OR gate. */
    uint8_t invert_result;
};

/* Configure \p pin_num (between 1 and 32).  */
int wrenrx_output_set_config (struct wrenrx_handle *handle,
			      unsigned output_num,
			      const struct wrenrx_output_configuration *cfg);
int wrenrx_output_get_config (struct wrenrx_handle *handle,
			      unsigned output_num,
			      struct wrenrx_output_configuration *cfg);

struct wrenrx_pin_configuration
{
    /* If set, the pin is configured as an output */
    uint8_t out_en;

    /* For input: if set the input is terminated with a 50ohm resistor */
    uint8_t term_en;

    /* For input: if set the input is inverted */
    uint8_t inv_inp;
};

int wrenrx_pin_set_config(struct wrenrx_handle *handle,
			  unsigned pin_num,
			  const struct wrenrx_pin_configuration *cfg);
int wrenrx_pin_get_config(struct wrenrx_handle *handle,
			  unsigned pin_num,
			  struct wrenrx_pin_configuration *cfg);

struct wrenrx_msg;

/* Wait for interrupts or data from the receiver.  */
struct wrenrx_msg *wrenrx_wait(struct wrenrx_handle *handle);

/* Get file descriptor to be used for poll/select.  */
int wrenrx_get_fd(struct wrenrx_handle *handle);

enum wrenrx_msg_kind
  {
   wrenrx_msg_network_error,
   wrenrx_msg_pulse,
   wrenrx_msg_event,
   wrenrx_msg_context,

   /* No message for the user.
      User should call wrenrx_wait (maybe after poll(2) or select(2)) */
   wrenrx_msg_none,

   /* Only while debugging */
   wrenrx_msg_packet
  };

/* Get the kind of a message.  */
enum wrenrx_msg_kind wrenrx_get_msg(const struct wrenrx_msg *msg);

/* Get the length of all the parameters (only for statistics) */
unsigned wrenrx_msg_get_params_length(const struct wrenrx_msg *msg);

/* Get config id of a pulse message.  */
unsigned wrenrx_msg_get_config_id(const struct wrenrx_msg *msg);

/* Get time stamp id of a pulse message.  */
int wrenrx_msg_get_pulse_ts(const struct wrenrx_msg *msg, struct wren_ts *ts);

/* Get source index of the event, context or pulse.  */
unsigned wrenrx_msg_get_source_idx(const struct wrenrx_msg *msg);

/* Get the event id of an event message */
unsigned wrenrx_msg_get_event_id(const struct wrenrx_msg *msg);

/* Get the due timestamp of the event */
int wrenrx_msg_get_event_ts(const struct wrenrx_msg *msg, struct wren_ts *ts);

/* Get the subscription id of the event */
unsigned wrenrx_msg_get_event_evsub_id(const struct wrenrx_msg *msg);

/* Get the context id of a pulse, event or context message */
unsigned wrenrx_msg_get_context_id(const struct wrenrx_msg *msg);

/* Get the valid-from timestamp of a context */
int wrenrx_msg_get_context_ts(const struct wrenrx_msg *msg, struct wren_ts *ts);

/* Iterator for parameters.
   Note: the structure is private, user shouldn't modify it */
struct wrenrx_msg_param;

/* Set an iterator for event or context parameters.
   Any number of iterators can be extracted, but its lifetime is limited
   to the lifetime of MSG.
   The user manage the iterator life time. */
int wrenrx_msg_get_event_params(const struct wrenrx_msg *msg,
				struct wrenrx_msg_param *it);
int wrenrx_msg_get_context_params(const struct wrenrx_msg *msg,
				  struct wrenrx_msg_param *it);

/* True if the iterator designate a valid parameter, false if at the end of
   the parameters */
unsigned wrenrx_msg_param_is_valid(const struct wrenrx_msg_param *params);

void wrenrx_msg_param_next(struct wrenrx_msg_param *params);

/* Get current parameter id and length */
wren_param_id wrenrx_msg_param_get_id(const struct wrenrx_msg_param *params);
unsigned wrenrx_msg_param_get_len(const struct wrenrx_msg_param *params);
unsigned wrenrx_msg_param_get_dt(const struct wrenrx_msg_param *params);

int32_t wrenrx_msg_param_get_s32(const struct wrenrx_msg_param *params);
float   wrenrx_msg_param_get_f32(const struct wrenrx_msg_param *params);
int64_t wrenrx_msg_param_get_s64(const struct wrenrx_msg_param *params);

/* A log entry for a pulser */
struct wrenrx_pulser_log {
  /* Timestamp when the event occured.
     Precise to 1us */
  struct wren_ts ts;
  /* Set of events during the last us */
  unsigned flags;
};

/* Flags for a pulse log entry. */
/* The pulser is loaded */
#define WRENRX_PULSER_LOG_FLAG_LOAD (1 << 0)
/* The pulser is started */
#define WRENRX_PULSER_LOG_FLAG_START (1 << 1)
/* The pulser has generated one or more pulses during the 1us time span */
#define WRENRX_PULSER_LOG_FLAG_PULSE (1 << 2)
/* The pulser goes to idlr */
#define WRENRX_PULSER_LOG_FLAG_IDLE (1 << 3)

/* Get logs for a pulser, return the number of entries filled */
int wrenrx_pulser_log_get(struct wrenrx_handle *handle,
			  unsigned pulser_num,
			  struct wrenrx_pulser_log *buf,
			  unsigned len);

struct wrenrx_pin_log {
  /* Timestamp when the event occured.
     Precise to 1us */
  struct wren_ts ts;
  /* Set of events during the last us */
  unsigned flags;
};

/* A positive edge was observed on the pin during the 1us time span */
#define WRENRX_PIN_LOG_FLAG_PEDGE (1 << 2)
/* A negative edge was observed */
#define WRENRX_PIN_LOG_FLAG_NEDGE (1 << 3)

/* Get logs for a pin, return the number of entries filled */
int wrenrx_pin_log_get(struct wrenrx_handle *handle,
		       unsigned pin_num,
		       struct wrenrx_pin_log *buf,
		       unsigned len);


/* Opaque structure for software log */
struct wrenrx_sw_log;

/* Allocate a structure to save at most MAXENT sw log entries */
struct wrenrx_sw_log *wrenrx_sw_log_allocate(unsigned maxent);

/* Free the buffer. */
void wrenrx_sw_log_free(struct wrenrx_sw_log *buf);

/* Get software logs from the board. */
int wrenrx_sw_log_get(struct wrenrx_handle *handle,
		      struct wrenrx_sw_log *buf);

/* Return the number of entries in the log buffer.
   Can be less than the maximum (define on allocate) if there are not
   enough entries */
unsigned wrenrx_sw_log_get_length(struct wrenrx_sw_log *buf);

/* A structure representing a sw log entry */
struct wrenrx_sw_log_entry {
  struct wrenrx_sw_log *log;
  unsigned idx;
  unsigned priv0;
};

/* Extract a the next entry.  ENT is allocated by the user */
int wrenrx_sw_log_get_next_entry(struct wrenrx_sw_log *buf,
				 struct wrenrx_sw_log_entry *ent);

/* Get the type of an entry */
unsigned wrenrx_sw_log_get_type(struct wrenrx_sw_log_entry *ent);

/* Types of a sw log entry.
   These are constants (rather than enums) as new types can be added in the
   future */
#define WRENRX_SW_LOG_TYPE_UNKNOWN 0

/* A packet preceed contexts and events.
   The arrival time and the source index applies to all the following events
   and contexts until the next packet */
#define WRENRX_SW_LOG_TYPE_PACKET 1

/* This corresponds to an event capsule */
#define WRENRX_SW_LOG_TYPE_EVENT 2

/* This corresponds to a context capsule */
#define WRENRX_SW_LOG_TYPE_CONTEXT 3
#define WRENRX_SW_LOG_TYPE_CONDITION 4
/* TODO: user action, errors */

/* Get the absolute time of entry ENT.
   Only for PACKET */
int wrenrx_sw_log_get_time(struct wrenrx_sw_log_entry *ent,
			   struct wren_ts *time);

/* For PACKET: get the source */
unsigned wrenrx_sw_log_packet_get_source_idx(struct wrenrx_sw_log_entry *ent);

/* For EVENT and CONTEXT: create a message from the entry.
   The result must be free */
struct wrenrx_msg *wrenrx_sw_log_get_msg(struct wrenrx_sw_log_entry *ent);

void wrenrx_sw_log_free_msg(struct wrenrx_msg *msg);

/* The not so opaque structure for parameter iterator */
struct wrenrx_msg_param {
  const uint32_t *data;
  unsigned len;
  unsigned off;
};

/* Enable output on vme P2 raw A.  Only the least 4 bits are significant
   and enable the corresponding byte:
    bit 0: outputs 0-7
    bit 1: outputs 8-15
    bit 2: outputs 16-23
    bit 3: outputs 24-31
*/
int wrenrx_hw_vme_p2a_set_output(struct wrenrx_handle *handle, unsigned en);

/* Enable or disable patch panel.  By default it is enabled if present */
int wrenrx_hw_patchpanel_set_config(struct wrenrx_handle *handle, unsigned en);

#ifdef __cplusplus
}
#endif

#endif /* __WREN__WRENRX__H__ */
