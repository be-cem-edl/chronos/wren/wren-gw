#!/bin/bash
echo "run wrpc-sw-build"

git submodule update --init dependencies/wrpc-sw

cd dependencies/wrpc-sw
PATH=/opt/gnu/riscv-11.2-small/bin:$PATH

# For ppsi
git submodule update --init

make wren_defconfig
make

mkdir ../../wrpc-sw
mv wrc.elf wrc.bram ../../wrpc-sw
