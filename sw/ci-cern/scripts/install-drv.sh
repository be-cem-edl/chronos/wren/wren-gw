#!/usr/bin/env bash

set -e

vme_devs=$(awk '$1 == "#+#" && $6 == "VME_WREN" {printf "slot=%u;lun=%d;addr=0x%s;level=%u;vector=%s\n",$20,$7,$11,$22,$23}' /etc/transfer.ref)

pcie_devs=$(awk '$1 == "#+#" && $6 == "PCIe_WREN" {printf "slot=%02u;lun=%d\n",$20,$7}' /etc/transfer.ref)

drv_ver=""

# Configure VME wren
for dev in $vme_devs; do
#  echo $dev
  eval $dev
  # Remove previous registration
  /usr/local/bin/vme-module --slot $slot --remove
  # Register
  /usr/local/bin/vme-register --slot $slot --address-modifier 0x39 --address $addr --size 0x10000 --irq-level $level --irq-vector $vector --name wren
  # Setup ader
  /usr/local/bin/vme-module --slot $slot --disable
  /usr/local/bin/vme-module --slot $slot --ader 1,$addr,0x39,0,0
  /usr/local/bin/vme-module --slot $slot --enable
 
  if [ "$drv_ver" = "" ]; then
    drv_ver=$(./wren-ls driver-version vme $slot)
  fi

  # load WR software
  ./wrpc load -b vme-le -a $addr -o 0x1000 $drv_ver/wrc.elf
done

# Configure PCIe WREN
pcie_devs2=""
for dev in $pcie_devs; do
#  echo $dev
  eval $dev
  pcie_slot=$(awk "\$1 == \"$slot\" {printf \$2}" /var/run/dynpci)
  pcie_devs2="$pcie_devs2 $dev;pcie_slot=$pcie_slot"

  if [ "$drv_ver" = "" ]; then
    drv_ver=$(./wren-ls driver-version pcie $pcie_slot)
  fi

  # Load WR software
  ./wrpc load -b pci -f /sys/bus/pci/devices/0000:$pcie_slot/resource1 -o 0x1000 $drv_ver/wrc.elf
done
pcie_devs="$pcie_devs2"

if [ "$1" = noload ]; then
  exit 0
fi

# Load driver
insmod $drv_ver/wren-core.ko
if [ "$vme_devs" != "" ]; then
  insmod $drv_ver/wren-vme.ko
fi
if [ "$pcie_devs" != "" ]; then
  insmod $drv_ver/wren-pcie.ko
fi

create_links()
{
  # Do not create links for lun < 0
  # (those are used for transmitters)
  if [ $lun -lt 0 ]; then
    return
  fi
  for n in $(ls $1); do
    base=$(echo $n | sed -e 's/[0-9]//g')
    ln -s /dev/$n /dev/${base}-lun${lun}
    if [ "$base" = wren ]; then
      chmod 666 /dev/$n
    fi
  done
}

# create /dev links for lun
rm -f /dev/wren*-lun*
for dev in $vme_devs; do
  eval $dev
  create_links /sys/bus/vme/devices/vme.$slot/wren
done
for dev in $pcie_devs; do
  eval $dev
  create_links /sys/bus/pci/devices/0000:$pcie_slot/wren
done

exit 0
