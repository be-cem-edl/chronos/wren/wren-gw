#!/bin/bash

sed -e 's/REVD_REG_CONFIG/MAIN/' -e 's/revd_registers/main_regs/' < main-Registers.h > ../firmware/pxie/main-regs.h
sed -e 's/REVD_REG_CONFIG/HELPER/' -e 's/revd_registers/helper_regs/' \
    -e '/^typedef struct/,/^} si5341_revd_register_t;/d' < helper-Registers.h > ../firmware/pxie/helper-regs.h

