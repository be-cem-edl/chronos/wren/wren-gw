ctypesgen -I../api/include -I../hw-include -I../firmware/common ../api/wren-mb.h ../api/wren-wrtime.h ../api/include/wren/wren-hw.h -lwren -o wren_mb.py
# ctypesgen -I../api/include -I../hw-include -I../firmware/common ../api/include/wren/wren-hw.h 2> /dev/null | sed -n -e 's/^ *WRENRX/WRENRX/p' > wren_hw.py

ctypesgen -I../api/include ../api/include/wren/wrentx.h ../api/include/wren/wren-common.h -lwren -o wrentx.py
