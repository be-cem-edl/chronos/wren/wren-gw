#ifndef __CHEBY__RX_DESC__H__
#define __CHEBY__RX_DESC__H__
#define RX_DESC_SIZE 16 /* 0x10 */

/* RX Descriptor flags */
#define RX_DESC_FLAGS 0x0UL
#define RX_DESC_FLAGS_EMPTY 0x1UL
#define RX_DESC_FLAGS_ERROR 0x2UL
#define RX_DESC_FLAGS_PORT_MASK 0x3f00UL
#define RX_DESC_FLAGS_PORT_SHIFT 8
#define RX_DESC_FLAGS_GOT_TS 0x4000UL
#define RX_DESC_FLAGS_TS_INCORRECT 0x8000UL

/* RX Descriptor timestamp */
#define RX_DESC_TS 0x4UL
#define RX_DESC_TS_RISE_MASK 0xfffffffUL
#define RX_DESC_TS_RISE_SHIFT 0
#define RX_DESC_TS_FALL_MASK 0xf0000000UL
#define RX_DESC_TS_FALL_SHIFT 28

/* RX Descriptor buffer */
#define RX_DESC_BUF 0x8UL
#define RX_DESC_BUF_OFFSET_MASK 0xffffUL
#define RX_DESC_BUF_OFFSET_SHIFT 0
#define RX_DESC_BUF_LEN_MASK 0xffff0000UL
#define RX_DESC_BUF_LEN_SHIFT 16

/* Unused */
#define RX_DESC_UNUSED 0xcUL

struct rx_desc {
  /* [0x0]: REG (rw) RX Descriptor flags */
  uint32_t flags;

  /* [0x4]: REG (rw) RX Descriptor timestamp */
  uint32_t ts;

  /* [0x8]: REG (rw) RX Descriptor buffer */
  uint32_t buf;

  /* [0xc]: REG (rw) Unused */
  uint32_t unused;
};

#endif /* __CHEBY__RX_DESC__H__ */
