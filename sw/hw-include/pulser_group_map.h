#ifndef __CHEBY__PULSER_GROUP_MAP__H__
#define __CHEBY__PULSER_GROUP_MAP__H__

#define PULSER_GROUP_MAP_SIZE 4096 /* 0x1000 = 4KB */

/* Configuration of comparators */
#define PULSER_GROUP_MAP_COMPARATORS 0x0UL
#define PULSER_GROUP_MAP_COMPARATORS_SIZE 32 /* 0x20 */

/* Load time (seconds) */
#define PULSER_GROUP_MAP_COMPARATORS_TIME_SEC 0x0UL

/* Load time, ns is [8:0], steps is [29:9] */
#define PULSER_GROUP_MAP_COMPARATORS_TIME_NSEC 0x4UL

/* (comment missing) */
#define PULSER_GROUP_MAP_COMPARATORS_CONF1 0x8UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_ENABLE 0x80000000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_ABORT 0x40000000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_OUT_EN 0x80000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_INT_EN 0x40000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT 0x20000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT 0x10000UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_MASK 0x7c00UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_SHIFT 10
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_MASK 0x3e0UL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_SHIFT 5
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_START_MASK 0x1fUL
#define PULSER_GROUP_MAP_COMPARATORS_CONF1_START_SHIFT 0

/* Number of system clock periods for the high part of the pulse */
#define PULSER_GROUP_MAP_COMPARATORS_HIGH 0xcUL

/* Number of clock periods for the pulse */
#define PULSER_GROUP_MAP_COMPARATORS_PERIOD 0x10UL

/* Number of pulses */
#define PULSER_GROUP_MAP_COMPARATORS_NPULSES 0x14UL

/* Initial delay (after start), in clock periods */
#define PULSER_GROUP_MAP_COMPARATORS_IDELAY 0x18UL

/* Pulser configuration */
#define PULSER_GROUP_MAP_PULSERS 0x800UL
#define PULSER_GROUP_MAP_PULSERS_SIZE 32 /* 0x20 */

/* Bit mask of comparators which generated a pulse.  WTC. */
#define PULSER_GROUP_MAP_PULSERS_PULSES 0x0UL

/* Timestamp of the last pulse */
#define PULSER_GROUP_MAP_PULSERS_TS 0x4UL

/* Bit mask of used comparators. */
#define PULSER_GROUP_MAP_PULSERS_DONE_COMP 0x8UL

/* Currently loaded comparator (bit mask) */
#define PULSER_GROUP_MAP_PULSERS_CURRENT_COMP 0xcUL

/* Bit mask of loaded comparators.  WTC. */
#define PULSER_GROUP_MAP_PULSERS_LOADED_COMP 0x10UL

/* Bit is set when corresponding comparator time is in the past.
 */
#define PULSER_GROUP_MAP_PULSERS_COMP_LATE 0x14UL

/* Busy comparators.  Set when comp_status is written. */
#define PULSER_GROUP_MAP_PULSERS_COMP_BUSY 0x18UL

/* Internal state of the pulser */
#define PULSER_GROUP_MAP_PULSERS_STATE 0x1cUL

/* Configuration of the combined outputs */
#define PULSER_GROUP_MAP_OUT_CFG 0x900UL
#define PULSER_GROUP_MAP_OUT_CFG_SIZE 4 /* 0x4 */

/* (comment missing) */
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG 0x0UL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_MASK 0xffUL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_MASK_SHIFT 0
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_IN 0x100UL
#define PULSER_GROUP_MAP_OUT_CFG_CONFIG_INV_OUT 0x200UL

/* events on pulse generators */
#define PULSER_GROUP_MAP_PULSES_EVNT 0x920UL
#define PULSER_GROUP_MAP_PULSES_EVNT_SIZE 32 /* 0x20 */

/* (comment missing) */
#define PULSER_GROUP_MAP_PULSES_EVNT_TM_TAI 0x920UL

/* (comment missing) */
#define PULSER_GROUP_MAP_PULSES_EVNT_TM_CYC 0x924UL

/* Set for events on pulses generators */
#define PULSER_GROUP_MAP_PULSES_EVNT_EVNT 0x928UL

/* control */
#define PULSER_GROUP_MAP_PULSES_EVNT_CTL 0x92cUL
#define PULSER_GROUP_MAP_PULSES_EVNT_CTL_RD 0x1UL

/* status */
#define PULSER_GROUP_MAP_PULSES_EVNT_STS 0x930UL
#define PULSER_GROUP_MAP_PULSES_EVNT_STS_NEMPTY 0x1UL
#define PULSER_GROUP_MAP_PULSES_EVNT_STS_LOST 0x2UL

/* Abort pulsers (select by bit) */
#define PULSER_GROUP_MAP_PULSERS_ABORT 0x940UL

/* pulsers status (1 if running) */
#define PULSER_GROUP_MAP_PULSERS_RUNNING 0x944UL

/* sticky pulsers status (1 if has started) */
#define PULSER_GROUP_MAP_PULSERS_RUN 0x948UL

/* true if a pulser has been loaded */
#define PULSER_GROUP_MAP_PULSERS_LOADED 0x94cUL

#ifndef __ASSEMBLER__
struct pulser_group_map {
  /* [0x0]: MEMORY Configuration of comparators */
  struct comparators {
    /* [0x0]: REG (rw) Load time (seconds) */
    uint32_t time_sec;

    /* [0x4]: REG (rw) Load time, ns is [8:0], steps is [29:9] */
    uint32_t time_nsec;

    /* [0x8]: REG (rw) (comment missing) */
    uint32_t conf1;

    /* [0xc]: REG (rw) Number of system clock periods for the high part of the pulse */
    uint32_t high;

    /* [0x10]: REG (rw) Number of clock periods for the pulse */
    uint32_t period;

    /* [0x14]: REG (rw) Number of pulses */
    uint32_t npulses;

    /* [0x18]: REG (rw) Initial delay (after start), in clock periods */
    uint32_t idelay;

    /* padding to: 32 Bytes */
    uint32_t __padding_0[1];
  } comparators[64];

  /* [0x800]: MEMORY Pulser configuration */
  struct pulsers {
    /* [0x0]: REG (rw) Bit mask of comparators which generated a pulse.  WTC. */
    uint32_t pulses;

    /* [0x4]: REG (ro) Timestamp of the last pulse */
    uint32_t ts;

    /* [0x8]: REG (ro) Bit mask of used comparators. */
    uint32_t done_comp;

    /* [0xc]: REG (ro) Currently loaded comparator (bit mask) */
    uint32_t current_comp;

    /* [0x10]: REG (rw) Bit mask of loaded comparators.  WTC. */
    uint32_t loaded_comp;

    /* [0x14]: REG (ro) Bit is set when corresponding comparator time is in the past.
 */
    uint32_t comp_late;

    /* [0x18]: REG (rw) Busy comparators.  Set when comp_status is written. */
    uint32_t comp_busy;

    /* [0x1c]: REG (ro) Internal state of the pulser */
    uint32_t state;
  } pulsers[8];

  /* [0x900]: MEMORY Configuration of the combined outputs */
  struct out_cfg {
    /* [0x0]: REG (rw) (comment missing) */
    uint32_t config;
  } out_cfg[8];

  /* [0x920]: BLOCK events on pulse generators */
  struct pulses_evnt {
    /* [0x0]: REG (ro) (comment missing) */
    uint32_t tm_tai;

    /* [0x4]: REG (ro) (comment missing) */
    uint32_t tm_cyc;

    /* [0x8]: REG (ro) Set for events on pulses generators */
    uint32_t evnt;

    /* [0xc]: REG (wo) control */
    uint32_t ctl;

    /* [0x10]: REG (ro) status */
    uint32_t sts;

    /* padding to: 2368 Bytes */
    uint32_t __padding_0[3];
  } pulses_evnt;

  /* [0x940]: REG (wo) Abort pulsers (select by bit) */
  uint8_t pulsers_abort;

  /* padding to: 2372 Bytes */
  uint8_t __padding_0[3];

  /* [0x944]: REG (ro) pulsers status (1 if running) */
  uint8_t pulsers_running;

  /* padding to: 2376 Bytes */
  uint8_t __padding_1[3];

  /* [0x948]: REG (rw) sticky pulsers status (1 if has started) */
  uint8_t pulsers_run;

  /* padding to: 2380 Bytes */
  uint8_t __padding_2[3];

  /* [0x94c]: REG (ro) true if a pulser has been loaded */
  uint8_t pulsers_loaded;

  /* padding to: 4096 Bytes */
  uint8_t __padding_3[1715];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__PULSER_GROUP_MAP__H__ */
