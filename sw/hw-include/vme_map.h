#ifndef __CHEBY__VME_MAP__H__
#define __CHEBY__VME_MAP__H__


#include "host_map.h"
#define VME_MAP_SIZE 65536 /* 0x10000 = 64KB */

/* (comment missing) */
#define VME_MAP_HOST 0x0UL
#define ADDR_MASK_VME_MAP_HOST 0xe000UL
#define VME_MAP_HOST_SIZE 8192 /* 0x2000 = 8KB */

/* Extra registers specific to VME */
#define VME_MAP_VME 0x2000UL
#define VME_MAP_VME_SIZE 1024 /* 0x400 = 1KB */

/* (comment missing) */
#define VME_MAP_VME_IRQ 0x2000UL
#define VME_MAP_VME_IRQ_LEVEL_MASK 0x7UL
#define VME_MAP_VME_IRQ_LEVEL_SHIFT 0
#define VME_MAP_VME_IRQ_VECTOR_MASK 0xff00UL
#define VME_MAP_VME_IRQ_VECTOR_SHIFT 8

/* (comment missing) */
#define VME_MAP_VME_GA 0x2004UL
#define VME_MAP_VME_GA_VME_GA_MASK 0x3fUL
#define VME_MAP_VME_GA_VME_GA_SHIFT 0
#define VME_MAP_VME_GA_NOGA_MASK 0x3f00UL
#define VME_MAP_VME_GA_NOGA_SHIFT 8
#define VME_MAP_VME_GA_GA_MASK 0x3f0000UL
#define VME_MAP_VME_GA_GA_SHIFT 16
#define VME_MAP_VME_GA_CST_MASK 0xff000000UL
#define VME_MAP_VME_GA_CST_SHIFT 24

/* (comment missing) */
#define VME_MAP_VME_P2_RS485 0x2008UL
#define VME_MAP_VME_P2_RS485_DAT0 0x1UL
#define VME_MAP_VME_P2_RS485_DAT1 0x2UL
#define VME_MAP_VME_P2_RS485_DIR0 0x10UL
#define VME_MAP_VME_P2_RS485_DIR1 0x20UL

/* (comment missing) */
#define VME_MAP_VME_P2_TTC 0x200cUL
#define VME_MAP_VME_P2_TTC_DAT_MASK 0x3UL
#define VME_MAP_VME_P2_TTC_DAT_SHIFT 0

/* (comment missing) */
#define VME_MAP_VME_HWBYTES_L1 0x2010UL

/* (comment missing) */
#define VME_MAP_VME_HWBYTES_H1 0x2014UL

/* (comment missing) */
#define VME_MAP_VME_HWBYTES_L2 0x2018UL

/* (comment missing) */
#define VME_MAP_VME_HWBYTES_H2 0x201cUL

/* (comment missing) */
#define VME_MAP_VME_HWBYTES_OE 0x2020UL
#define VME_MAP_VME_HWBYTES_OE_L1 0x1UL
#define VME_MAP_VME_HWBYTES_OE_H1 0x2UL
#define VME_MAP_VME_HWBYTES_OE_L2 0x4UL
#define VME_MAP_VME_HWBYTES_OE_H2 0x8UL

/* (comment missing) */
#define VME_MAP_VME_P0_CLK 0x2024UL
#define VME_MAP_VME_P0_CLK_TTL_OE 0x1UL

/* Divisor between bunch and turn clock */
#define VME_MAP_VME_P0_H1 0x2028UL

/* base address of the windows area, 4KB per window */
#define VME_MAP_VME_BASE_ADDR 0x2200UL
#define VME_MAP_VME_BASE_ADDR_SIZE 4 /* 0x4 */

/* (comment missing) */
#define VME_MAP_VME_BASE_ADDR_ADDR 0x0UL

/* windowed access to the board memory (4KB) */
#define VME_MAP_WINDOWS 0x8000UL
#define ADDR_MASK_VME_MAP_WINDOWS 0x8000UL
#define VME_MAP_WINDOWS_SIZE 32768 /* 0x8000 = 32KB */

#ifndef __ASSEMBLER__
struct vme_map {
  /* [0x0]: SUBMAP (comment missing) */
  struct host_map host;

  /* [0x2000]: BLOCK Extra registers specific to VME */
  struct vme {
    /* [0x0]: REG (rw) (comment missing) */
    uint32_t irq;

    /* [0x4]: REG (ro) (comment missing) */
    uint32_t ga;

    /* [0x8]: REG (rw) (comment missing) */
    uint32_t p2_rs485;

    /* [0xc]: REG (rw) (comment missing) */
    uint32_t p2_ttc;

    /* [0x10]: REG (rw) (comment missing) */
    uint8_t hwbytes_l1;

    /* padding to: 20 Bytes */
    uint8_t __padding_0[3];

    /* [0x14]: REG (rw) (comment missing) */
    uint8_t hwbytes_h1;

    /* padding to: 24 Bytes */
    uint8_t __padding_1[3];

    /* [0x18]: REG (rw) (comment missing) */
    uint8_t hwbytes_l2;

    /* padding to: 28 Bytes */
    uint8_t __padding_2[3];

    /* [0x1c]: REG (rw) (comment missing) */
    uint8_t hwbytes_h2;

    /* padding to: 32 Bytes */
    uint8_t __padding_3[3];

    /* [0x20]: REG (rw) (comment missing) */
    uint32_t hwbytes_oe;

    /* [0x24]: REG (rw) (comment missing) */
    uint32_t p0_clk;

    /* [0x28]: REG (rw) Divisor between bunch and turn clock */
    uint16_t p0_h1;

    /* padding to: 512 Bytes */
    uint8_t __padding_4[470];

    /* [0x200]: MEMORY base address of the windows area, 4KB per window */
    struct vme_base_addr {
      /* [0x0]: REG (rw) (comment missing) */
      uint32_t addr;
    } base_addr[8];

    /* padding to: 9216 Bytes */
    uint32_t __padding_5[120];
  } vme;

  /* padding to: 32768 Bytes */
  uint32_t __padding_0[5888];

  /* [0x8000]: SUBMAP windowed access to the board memory (4KB) */
  uint32_t windows[8192];
};
#endif /* !__ASSEMBLER__*/

#endif /* __CHEBY__VME_MAP__H__ */
