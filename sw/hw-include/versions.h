/* If the map version is modified, g_board_map_version of wren-core.vhdl should
   be modified too */
#define WREN_BOARD_MAP_VERSION 0x3510f00d

/* Map version for the host, should be more stable.  Checked by the driver */
#define WREN_HOST_MAP_VERSION 0x3510f009

/* Firmware version, checked by the driver.  Defines the messages between
   the driver and the firmware */
#define WREN_FW_VERSION 0xfb00000d

/* 'WREN' - shouldn't change */
#define WREN_HW_IDENT 0x5752454e
