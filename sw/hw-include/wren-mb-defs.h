/* Mailbox commands */
#ifndef __MB_CMD__H__
#define __MB_CMD__H__

#include "wren/wren-common.h"

#define CMD_REPLY       0x80000000
#define CMD_ERROR       0x40000000
#define CMD_MASK        0x0000ffff

/* Internal error: the length of the message is not correct  */
#define CMD_ERROR_LENGTH (CMD_ERROR | 0x1000)

/* Internal error: data corruption */
#define CMD_ERROR_CORRUPT (CMD_ERROR | 0x1001)

/* Internal error: unknown command */
#define CMD_ERROR_COMMAND (CMD_ERROR | 0x1002)


enum wren_mb_cmd_t {
#define def_cmd(NAME) CMD_##NAME,
#include "wren-mb-cmds.def"
#undef def_cmd
};

/* Async commands */
#define CMD_ASYNC_CONTEXT  0x01
#define CMD_ASYNC_EVENT    0x02
#define CMD_ASYNC_CONFIG   0x03
#define CMD_ASYNC_PULSE    0x04
#define CMD_ASYNC_UNLOAD   0x05
#define CMD_ASYNC_PROMISC  0x06
#define CMD_ASYNC_CONT     0x07
#define CMD_ASYNC_REL_ACT  0x08

/* The low-level structure used by read(2)/write(2) */
struct wren_mb_metadata {
    /* The commands, see CMD_* above. */
    uint32_t cmd;

    /* Length in words. */
    uint32_t len;
};

/* For any command with 1 argument */
struct wren_mb_arg1 {
    uint32_t arg1;
};

struct wren_mb_arg2 {
    uint32_t arg1;
    uint32_t arg2;
};

/* For any reply with 1 or 2 arguments */
struct wren_mb_arg_reply {
    uint32_t arg1;
    uint32_t arg2;
};

struct wren_mb_tx_get_config_reply {
    uint32_t sw_version;
    uint32_t max_sources;
    uint32_t nbr_sources;
};

struct wren_mb_tx_set_source {
    uint32_t source_idx;
    struct wren_protocol proto;

    /* Maximum delay before the next packet */
    uint32_t max_delay_us_p2;
};

struct wren_mb_tx_send_frame {
    uint32_t src_idx;
    uint32_t data[1];
};

struct wren_mb_tx_table {
    uint32_t table_idx;
    /* Outputs in case of get */
    uint32_t src_idx;
    uint32_t repeat;
    uint32_t nbr_insns;
    uint32_t nbr_data;
    uint32_t data[WRENTX_MAX_INSNS + WRENTX_MAX_DATA];
};

struct wren_mb_tx_play_table {
    uint32_t table_idx;
    struct wren_ts due_ts;
};

struct wren_mb_rx_get_config_reply {
    uint32_t sw_version;
    uint32_t max_sources;
    uint32_t max_conds;
    uint32_t max_actions;
};

#define WREN_MB_PULSER_FLAG_REPEAT   (1 << 0)
#define WREN_MB_PULSER_FLAG_IMMEDIAT (1 << 1)
#define WREN_MB_PULSER_FLAG_INT_EN   (1 << 2)
#define WREN_MB_PULSER_FLAG_OUT_EN   (1 << 3)
/* Configuration enable, not present on the board.
   Comparator enable, not present in software. */
#define WREN_MB_PULSER_FLAG_ENABLE   (1 << 7)

#define WREN_MB_PULSER_INPUT_MASK 0x1f
#define WREN_MB_PULSER_INPUT_START_SH 0
#define WREN_MB_PULSER_INPUT_STOP_SH  5
#define WREN_MB_PULSER_INPUT_CLOCK_SH 10
#define WREN_MB_PULSER_GET_CLOCK(INP) \
    (((INP) >> WREN_MB_PULSER_INPUT_CLOCK_SH) & WREN_MB_PULSER_INPUT_MASK)
#define WREN_MB_PULSER_GET_START(INP) \
    (((INP) >> WREN_MB_PULSER_INPUT_START_SH) & WREN_MB_PULSER_INPUT_MASK)
#define WREN_MB_PULSER_GET_STOP(INP) \
    (((INP) >> WREN_MB_PULSER_INPUT_STOP_SH) & WREN_MB_PULSER_INPUT_MASK)
struct wren_mb_pulser_config {
    uint8_t pulser_idx;
    uint8_t flags;
    uint16_t inputs; /* start[0:4], stop[5:9], clock[10:14] */

    uint32_t width;
    uint32_t period;
    uint32_t npulses;
    uint32_t idelay;

    /* Due time offset */
    int32_t load_off_sec;
    int32_t load_off_nsec;
};

struct wren_mb_comparator {
    /* Absolute timestamp */
    struct wren_ts ts;
    /* Pulser config */
    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_set_comparator {
    uint32_t comp_idx;
    struct wren_mb_comparator cfg;
};

struct wren_mb_rx_get_pulser_reply {
    uint32_t pulses;
    uint32_t ts;
    uint32_t done_comp;
    uint32_t current_comp;
    uint32_t loaded_comp;
    uint32_t late_comp;
    uint32_t busy_comp;
    uint32_t state;
};

struct wren_mb_out_cfg {
    uint32_t out_idx;
    uint8_t mask;
    uint8_t inv_in;
    uint8_t inv_out;
};

struct wren_mb_pin_cfg {
    uint32_t pin_idx;
    uint8_t oe;
    uint8_t term; /* 50Ohm terminaison */
    uint8_t inv;
};

/* event subscribe and unsubscribe */
struct wren_mb_rx_subscribe {
    uint32_t src_idx;
    uint32_t ev_id;
};

struct wren_mb_get_subscribed_reply {
    uint32_t map[32];
};

struct wren_mb_rx_set_action {
    uint32_t act_idx;
    uint32_t cond_idx;

    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_mod_action {
    uint32_t act_idx;

    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_imm_action {
    uint32_t act_idx;
    struct wren_mb_comparator cmp;
};

struct wren_mb_rx_force_pulser {
    uint32_t act_idx;

    struct wren_ts ts;
    struct wren_mb_pulser_config conf;
};

struct wren_mb_rx_get_action_reply {
    uint32_t cond_idx;

    /* Due time offset */
    int32_t sec_off;
    int32_t nsec_off;

    struct wren_mb_pulser_config trig;
};

struct wren_mb_rx_source {
    /* 0: events.
       xxx: RF frames */
    uint32_t dest;

    /* Discard N/N+1 frames. */
    uint32_t subsample;

    struct wren_protocol proto;
};

struct wren_mb_rx_set_source {
    /* Source index (from 0 to max_sources - 1) */
    uint32_t idx;

    struct wren_mb_rx_source cfg;
};

struct wren_mb_rx_get_log {
    /* Log index (pulser or input), from 0 to 63 */
    uint32_t log_idx;
    /* Last entry, as returned by log_index command */
    uint32_t entry_idx;
    /* Number of entries (up to 128) */
    uint32_t nentries;
};

#define NO_RX_COND_IDX 0xffff
#define INV_RX_COND_IDX 0xfffe

struct wren_mb_rx_get_source_reply {
    struct wren_mb_rx_source cfg;

    /* First condition */
    uint16_t cond_idx;
};

/* Condition operation.
   Also used for binary operation (in that case, only OP is used). */
struct wren_rx_cond_op {
    uint16_t param_id;
    uint8_t op;
    /* Origin of the parameter (context, event or any) */
    uint8_t param_orig;
};

union wren_rx_cond_word {
    struct wren_rx_cond_op op;
    uint32_t vu32;
    int32_t vs32;
};

#define WREN_COND_MAXLEN 8

struct wren_mb_cond {
    /* Event id.  */
    uint16_t evt_id;

    /* Source index */
    uint8_t src_idx;

    /* Number of words in the conditions.  */
    /* TODO: first parameters id, sorted by source and by id,
       then the expression.  */
    uint8_t len;

    union wren_rx_cond_word ops[WREN_COND_MAXLEN];
};

struct wren_mb_rx_set_cond {
    /* Condition index (from 0 to MAX_RX_CONDS - 1) */
    uint16_t cond_idx;

    struct wren_mb_cond cond;
};

#define WREN_OP_EQ 0
#define WREN_OP_NE 1


#define WREN_OP_UGT 4
#define WREN_OP_UGE 5
#define WREN_OP_ULT 6
#define WREN_OP_ULE 7

#define WREN_OP_SGT 8
#define WREN_OP_SGE 9
#define WREN_OP_SLT 10
#define WREN_OP_SLE 11

#define WREN_OP_FIRST_BINARY 28

#define WREN_OP_AND 28
#define WREN_OP_OR  29
#define WREN_OP_NOT 30

#define WREN_TTY_LOG_MAXLEN 1024

struct wren_mb_tty_log {
    uint32_t len;
    union {
	char c[WREN_TTY_LOG_MAXLEN];
	uint32_t u[WREN_TTY_LOG_MAXLEN / 4];
    } u;
};

/* For versions */
#define WREN_VERSION_MAXLEN 256

struct wren_mb_version {
    union {
	char c[WREN_VERSION_MAXLEN];
	uint32_t u[WREN_VERSION_MAXLEN / 4];
    } u;
};


/* Event log entries */

/* A continuation entry */
#define EVLOG_TYPE_CONT 0x00

/* Header of an ethernet frame */
#define EVLOG_TYPE_RECV 0x01

/* Event capsule */
#define EVLOG_TYPE_EVNT 0x02

/* Context capsule */
#define EVLOG_TYPE_CTXT 0x03

/* Condition evaluation */
#define EVLOG_TYPE_COND 0x04

/* Header size */
#define WREN_EVLOG_HSIZE 0x04

/* Max data size in bytes (252B = 63W) */
#define WREN_EVLOG_DSIZE 0xfc

#define WREN_EVLOG_MAX (1 << (30 - 8))

union wren_evlog_entry {
    unsigned char type;

    struct {
	unsigned char type;
	unsigned char nentries;
	/* Length (in bytes) of the data entry. */
	unsigned short len;
	unsigned char d[WREN_EVLOG_DSIZE];
    } e;

    uint32_t w[WREN_EVLOG_DSIZE / 4 + 1];
    uint16_t u16[WREN_EVLOG_DSIZE / 2 + 2];
};

/* For HW config */
struct wren_mb_hw_config {
    uint32_t model_ident;
    uint32_t fp_id;
    uint32_t pp_id;
    uint32_t pcb_id;
};

/* For HW userio */
struct wren_mb_hw_userio {
    /* Byte enable for VME P2 raw A outputs and inputs */
    uint32_t vme_p2a_out_be;
    uint32_t vme_p2a_in_be;

    uint32_t vme_p2c_data;

    uint32_t patchpanel;
};
#endif /* __MB_CMD__H__ */
