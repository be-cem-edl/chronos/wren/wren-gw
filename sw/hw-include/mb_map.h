struct mb_map {
    /* 0KB-4KB: Board to host */
    uint32_t b2h_csr;
    uint32_t b2h_pad;
    uint32_t b2h_cmd;
    uint32_t b2h_len;
    uint32_t b2h_data[1024 - 4];

    /* 4KB-8KB: Host to board */
    uint32_t h2b_csr;
    uint32_t h2b_pad;
    uint32_t h2b_cmd;
    uint32_t h2b_len;
    uint32_t h2b_data[1024 - 4];

    /* 8-16KB: Async data */
    uint32_t async_data[2048];
    /* 16KB: async regs */
    uint32_t async_board_off;
    uint32_t async_host_off;
    /* TODO: NIC buffers, evlog indexes */
};

#define MB_CSR_READY 0x1
