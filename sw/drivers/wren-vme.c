// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

/*
 * Driver for WREN_VME board.
 */

#include <linux/device.h>
#include <linux/module.h>
#include <linux/mutex.h>
#include <linux/moduleparam.h>
#include <linux/version.h>
#include <linux/firmware.h>
#include <linux/vmalloc.h>
#include <vmebus.h>

#include "wren-core.h"
#include "vme_map.h"

static int wren_vme_irq_handler(void *arg)
{
	struct wren_dev *wren = (struct wren_dev *)arg;
	irqreturn_t res = IRQ_HANDLED;
	
	res = wren_irq_handler(wren);

	/* The vme irq handler is the threaded handler, so execute
	   the thread handle if required. */
	if (res == IRQ_WAKE_THREAD)
		res = wren_irq_thread(0, arg);
	return res;
}

static int wren_vme_probe(struct device *dev, unsigned int ndev)
{
	struct vme_dev *vdev = to_vme_dev(dev);
	struct wren_dev *wren_dev;
	struct vme_map *regs;
	int err = 0;

	wren_dev = vzalloc(sizeof(*wren_dev));
	if (!wren_dev)
		return -ENOMEM;

	dev_set_drvdata(dev, wren_dev);
//	wren_dev->pdev = dev;
	wren_dev->owner = THIS_MODULE;

	dev_notice(dev, "vdev resource %pr\n", &vdev->resource[1]);
	if (vdev->resource[1].start == 0) {
		err = -EINVAL;
		dev_err(dev, "no resource allocated for ader#1\n");
		goto err_remap;
	}
	if (vdev->resource[1].end + 1
	    < vdev->resource[1].start + PAGE_ALIGN(sizeof (struct vme_map))) {
		err = -EINVAL;
		dev_err(dev, "resource size too small for ader#1\n");
		goto err_remap;
	}
	wren_dev->psdma = NULL;
	wren_dev->regs = ioremap(vdev->resource[1].start,
				 PAGE_ALIGN(sizeof (struct vme_map)));
	if (wren_dev->regs == NULL) {
		err = -EINVAL;
		goto err_remap;
	}
	
	/* Map mailboxes (in btcm), use the first 5 windows. */
	regs = (struct vme_map *)wren_dev->regs;
	regs->vme.base_addr[0].addr = 0xffe20000;  /* b2h */
	regs->vme.base_addr[1].addr = 0xffe21000;  /* h2b */
	regs->vme.base_addr[2].addr = 0xffe22000;  /* async */
	regs->vme.base_addr[3].addr = 0xffe23000;  /* async */
	regs->vme.base_addr[4].addr = 0xffe24000;  /* async regs */
	wren_dev->mb = &regs->windows;

	if (wren_register (wren_dev, dev) != 0) {
		err = -EINVAL;
		goto err_register;
	}

	dev_info(dev, "irq vector=%d level=%d\n",
		 vdev->irq_vector, vdev->irq_level);

	iowrite32((vdev->irq_vector << VME_MAP_VME_IRQ_VECTOR_SHIFT)
		  | (vdev->irq_level << VME_MAP_VME_IRQ_LEVEL_SHIFT),
		  &regs->vme.irq);
	err = vme_request_irq(vdev->irq_vector, wren_vme_irq_handler,
			      wren_dev, dev_name(wren_dev->dev_ctl));
	if (err) {
		dev_err(dev, "Can't request IRQ %d (%d)\n",
			vdev->irq_vector, err);
		goto err_irq;
	}

	err = wren_reset(wren_dev);
	if (err)
		dev_err(dev, "Can't reset the board (%d)\n", err);

	dev_info(dev, "wren%u vme carrier driver probed.\n",
		 wren_dev->index);
	return 0;

err_irq:
	wren_unregister(wren_dev);
err_register:
	iounmap(wren_dev->regs);
err_remap:
	vfree(wren_dev);
	return err;
}

static int wren_vme_remove(struct device *dev, unsigned int ndev)
{
	struct wren_dev *wren_dev = dev_get_drvdata(dev);
	struct vme_dev *vdev = to_vme_dev(dev);

	vme_free_irq(vdev->irq_vector);

	wren_unregister(wren_dev);

	iounmap(wren_dev->regs);

	vfree(wren_dev);

	return 0;
}

/* VME manufacturer, board id and revision id */
static const struct vme_device_id wren_vme_id_table[] = {
	{"wren-vme-a24", 0x00080030, 0x000001dc, 0x00000000},
	{"\0", 0, 0, 0},
};


static struct vme_driver wren_vme_driver = {
	.probe = wren_vme_probe,
	.remove = wren_vme_remove,
	.driver = {
		.owner = THIS_MODULE,
		.name = "wren-vme",
	},
	.id_table = wren_vme_id_table,
};


static int __init wren_vme_init(void)
{
	return vme_register_driver(&wren_vme_driver, 0);
}

static void __exit wren_vme_exit(void)
{
	vme_unregister_driver(&wren_vme_driver);
}

module_init(wren_vme_init);
module_exit(wren_vme_exit);

MODULE_AUTHOR("Tristan Gingold <tristan.gingold@cern.ch>");
MODULE_LICENSE("GPL v2");
MODULE_VERSION(VERSION);
MODULE_DESCRIPTION("Driver for the VME WREN");

MODULE_SOFTDEP("pre: wren-core");
