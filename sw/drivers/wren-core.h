// SPDX-FileCopyrightText: 2022 CERN (home.cern)
//
// SPDX-License-Identifier: LGPL-2.1-or-later

#ifndef __WREN_CORE_H__
#define __WREN_CORE_H__
#include <linux/debugfs.h>
#include <linux/device.h>
#include <linux/irqdomain.h>
#include <linux/pci.h>
#include <linux/spinlock.h>
#include <linux/mutex.h>
#include <linux/cdev.h>

#include "wren/wren-common.h"
#include "wren-mb-defs.h"
#include "wren-ioctl.h"

/* Maximum number of sources */
#define WREN_NBR_RX_SOURCES 8

#define WREN_NBR_TX_SOURCES 8

#define WREN_NBR_TX_TABLES 16

/* Size of some buffers */
#define WREN_NBR_CONDS 0x100
#define WREN_NBR_ACTIONS 0x100
#define WREN_MAX_COMPARATORS (8*32)

/* Number of cached context */
#define WREN_NBR_CACHED_CONTEXT 4

/* End of chain marker (cannot be 0 as it is valid)  */
#define WREN_NO_IDX 0xffff
#define WREN_NO_SRC_IDX 0xff
/* Used to mark a condition of an action as not present.  Must be different
   from WREN_NO_IDX which means the action slot is free. */
#define WREN_INV_IDX 0xfffd

/* Condition when a config is dead, ie deleted by the user but not free
   as there might be pulses for it in the async queue */
#define WREN_DEAD_COND 0xfffe

#define WREN_MAX_BUFFER_LEN 128 /* In 32b words! */
#define WREN_MAX_BUFFERS  256
#define WREN_NO_BUFFER 0xffff

#define WREN_MAX_CLIENTS 64

/* Maximum number of messages to be queued for a client.
   Must be a power of 2.  */
#define WREN_MAX_CLIENT_MSG 128

#define WREN_NBR_EVENTID 4096

struct wren_buffer {
	/* Reference counter.  Must be 0 for free buffers.  */
	atomic_t refcnt;
	/* Data length (in words), or link for a free buffer */
	atomic_t len;
	uint32_t data[WREN_MAX_BUFFER_LEN];
};

struct wren_client_msg {
	/* Async command and buffer.  When = 0, the message is not ready */
	atomic_t buf_cmd;
	/* Timestamp for a pulse */
	struct wren_ts ts;
	/* Config id for a pulse */
	uint16_t config_id;
	uint8_t source_idx;
};

struct wren_compact_time {
	/* Number of cycles + low bits of second */
	uint32_t lo;
	/* High bits of second */
	uint32_t hi;
};

/* Entry is valid and in the timeout queue */
#define TIMEOUT_VALID   (1 << 0)
/* Timeout has expired (and not anymore in the timeout queue) */
#define TIMEOUT_EXPIRED (1 << 1)

/* Use linux/timerqueue.h instead ? */
struct wren_timeout_entry {
	struct list_head entry;
	struct wren_compact_time ctime;
	unsigned flags;
	struct task_struct *me;
};

struct wren_client {
	struct wren_dev *wren;
	unsigned idx;

	struct wren_timeout_entry timeout;

	/* Ring of messages for client (process).
	   Head is where messages are read from, so this is the oldest.
	   Both head and tail uses an extra bit to differentiate between full
	   and empty.  So when ring_head == ring_tail, the queue is empty,
	   when ring_head == (ring_tail ^ WREN_MAX_CLIENT_MSG), the queue is
	   full.  You need to & (WREN_MAX_CLIENT_MSG - 1) to get the index.

	   Only the interrupt thread allocates slots from the ring, so there
	   is no need to protect for allocation.
	   Only the user process can remove slots from the ring, but there
	   might be multiple threads! */
	atomic_t ring_head;
	atomic_t ring_tail;
	struct wren_client_msg ring[WREN_MAX_CLIENT_MSG];
	/* Protect the ring */
	wait_queue_head_t wqh;

	struct client_sources {
		/* Ref counters for the sources (for context) */
		atomic_t ref;
		
		/* Event subscription. */
		DECLARE_BITMAP(evsubs, WREN_NBR_EVENTID);
	} sources[WREN_NBR_RX_SOURCES];
};

struct wren_clt_bitmap {
	DECLARE_BITMAP(b, WREN_MAX_CLIENTS);
};

struct wren_dev {
	struct device *dev_ctl;	/* The wren device */
	struct device *dev_usr;
	struct cdev cdev_usr;
	struct cdev cdev_ctl;
	dev_t devid_usr;
	dev_t devid_ctl;
	unsigned index;

	unsigned log_mask;

	/* debugfs */
	struct dentry *dfs_dir;

	/* Hardware version and ident */
	unsigned hw_ident;
	unsigned map_ver;
	unsigned fw_ver;

	/* True when time is valid, set by interrupt handler.  */
	uint8_t wr_synced;

	/* The current msb of tm_compact.  Used only by interrupt
	   handler.  */
	uint8_t tai_msb;

	/* High bits of TAI, set by interrupt handler.
	   Valid only when synced is true */
	uint32_t tai_hi[2];

	/* Mutex to protect message exchanges */
	struct mutex msg_mutex;

	/* Wait queue and lock for reads; protect the b2h mailbox */
	wait_queue_head_t wqh_r;
	/* Set if the reply is discarded */
	uint32_t wqh_r_discard;

	/* Wait queue for async mailbox */
	wait_queue_head_t wqh_a;

	/* Timeouts */
	spinlock_t to_lock;
	struct list_head to_list;

	/* Number of irq vectors (for pcie) */
	int irq_count;

	/* Current interrupt mask. */
	unsigned imr;

	/* Set by the bus specific driver */
	void __iomem *regs;
	struct module *owner;
	void __iomem *mb;

	/* For RX: Sources, contexts, conditions, actions */
	struct mutex data_mutex;
	struct wren_dev_source {
		struct wren_protocol proto;
		/* Simply chained list of conds (use 'link' field,
		   terminated with WREN_NO_IDX. */
		uint16_t conds;
		/* Number of clients subscribed per eventid */
		atomic_t evsubs[WREN_NBR_EVENTID];

		/* Protection of cached_contexts and last_cached_context */
		spinlock_t cached_lock;
		/* Cached contexts (a single wren_buffer) */
		uint16_t cached_contexts[WREN_NBR_CACHED_CONTEXT];
		/* Index of the last filled entry in cached_contexts */
		unsigned last_cached_context;
	} sources[WREN_NBR_RX_SOURCES];

	struct wren_dev_cond {
		struct wren_mb_cond cond;
		/* Next condition for the same source */
		uint16_t link;
		/* Simply chained list of actions */
		uint16_t acts;
		/* Link to the parent */
		uint16_t src_idx;
	} conds[WREN_NBR_CONDS];

	struct wren_dev_action {
		struct wren_mb_pulser_config config;
		/* Condition for the action.  Set to WREN_NO_IDX when the
		   action slot is free. Set to WREN_INV_IDX when there is
		   no condition for this action (immediate action) */
		uint16_t cond_idx;
		/* Next action for the same condition, or WREN_INV_IDX
		   for immediate action. */
		uint16_t link;
		/* Name of the action (no use, except for the user). */
		char name[WRENRX_CONFIGURATION_NAME_MAXLEN];
	} actions[WREN_NBR_ACTIONS];

	uint16_t free_conds;
	atomic_t free_actions;

	rwlock_t client_lock;
	struct wren_clt_bitmap client_used;
	struct wren_client *clients[WREN_MAX_CLIENTS];

	/* Subscription by clients, no protection: set/clear bit atomically */
	struct wren_clt_bitmap client_sources[WREN_NBR_RX_SOURCES];
	struct wren_clt_bitmap client_configs[WREN_NBR_ACTIONS];

	/* -1 or the client which wants all the packets (for debugging) */
	atomic_t promisc;
	
	/* Chained list of free buffers. */
	atomic_t buf_head;
	struct wren_buffer bufs[WREN_MAX_BUFFERS];

	/* Buffer of the last received event for each source.
	   Used only by the interrupt handler */
	unsigned last_evt[WREN_NBR_RX_SOURCES];

	/* Event message which loaded a configuration in a comparator.
	   No protection needed, handled by the interrupt handler */
	struct wren_dev_comparator {
		uint16_t config_id;
		uint8_t src_idx;
		uint16_t buf;
	} comp_buf[WREN_MAX_COMPARATORS];

	/* TX table names, protected by data_mutex */
	struct wren_dev_table {
		char name[WRENTX_TABLE_NAME_MAXLEN];
		/* TX source index or -1 if not assigned */
		int src_idx;
	} tables[WREN_NBR_TX_TABLES];

	/* Only for pci-e */
	struct pci_dev *pdev;
	void __iomem *psdma; /* BAR 0: ingress, dma controller,.. */
};

int wren_register(struct wren_dev *wren, struct device *parent);
void wren_unregister(struct wren_dev *wren);
int wren_reset(struct wren_dev *wren);
irqreturn_t wren_irq_handler(struct wren_dev *wren);
irqreturn_t wren_irq_thread(int irq, void *priv);

#endif /* __WREN_CORE_H__ */
