# Building image for spexi7u


1. Create bitstream and xsa file

Go to hdl/syn/xczu7cg

 hdlmake --fetchto xxx/ohwr
 make project
 vivado
   generate bistream
   export xsa

2. Copy xsa and bitstream
 (still in hdl/syn/xczu7cg)
  cp xczu7cg_top.xsa xczu7cg.runs/impl_1/xczu7cg_top.bit ../../../sw/vitis/

3. Build software

Go to sw/vitis

  xsct spexi7u.xsct

4. Run it

 This script load bitstream, fsbl and main application:

   xsct run-all.xsct

 On the host (cfn-774-cttbwrt0), create a console for the app:

   sudo picocom -b 115200 /dev/ttyUSB1

 The app should wait for pcie scan; run a pcie scan on the host:
   cd pxie-wrt; sh rescan.sh 0f   # where 0f is the slot

5. To modify main application:
  edit it
  make rebuild-app   # it copies sources and rebuild
  xsct run-main.xsct # Load just the app
  
6. Vitis
  vitis -workspace spexi7u-ws
  And close the Welcome tab.

7. Flashing

   xsct run-all.xsct must have been run (to create the elf files)
   make boot.bin
   # On the host:
   sudo ./ht-flasher -a wren-pcie-flash -d SLOT@4 -w boot.bin
