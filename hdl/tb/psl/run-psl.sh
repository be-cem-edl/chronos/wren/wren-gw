#!/bin/bash

testcases="
idelay_nostart_13_1
idelay_nostart_13_1_1
idelay_nostart_13
width_repeat_10
period_7
any_3p
any_3p_start
"

failure=""

if [ $# -ne 0 ]; then
    testcases="$*"
fi

for tc in $testcases; do
    sed -e "s/FILE/$tc.psl/" < run-proto.sby > run.sby
    rm -rf work
    if ! sby --yosys "yosys -m ghdl.so" -fd work run.sby; then
	failure="$failure $tc"
    fi
done

if [ "$failure" != "" ]; then
    echo "failing tests: $failure"
    exit 1
fi
