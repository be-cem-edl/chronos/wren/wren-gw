library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.pulsegen_pkg.all;

entity tb_freq is
end tb_freq;

architecture arch of tb_freq is
  signal clk_125m, clk_1g : std_logic := '0';
  signal pps : std_logic;
  signal clk, clk_d : t_subpulse;
  signal output : std_logic;
begin
  --  Clocking
  p_clk: process
  begin
    clk_125m <= not clk_125m;
    for k in 1 to 8 loop
      clk_1g <= not clk_1g;
      wait for 500 ps;
    end loop;
  end process;

p_out: process
  variable pre_output : t_slv8;
  variable clk_exp : unsigned(15 downto 0);
begin
    wait until rising_edge(clk_125m);
    --report "gen output";
    clk_exp := (15 downto 8 => clk.v, 7 downto 0 => clk_d.v);
    clk_exp := clk_exp sll to_integer (unsigned (clk.dly));
    pre_output := std_logic_vector (clk_exp (15 downto 8));
    clk_d <= clk;
    for i in 0 to 7 loop
      output <= pre_output (0);
      pre_output := '0' & pre_output(7 downto 1);
      if i /= 7 then
        wait until rising_edge(clk_1g);
      end if;
    end loop;
end process;

  pps <= '0', '1' after 12 ns, '0' after 20 ns;

  dut: entity work.freqgen
  generic map (
    g_period => 25
  )
  port map (
    clk_125m_i => clk_125m,
    pps_i => pps,
    clk_o => clk
  );end arch;
