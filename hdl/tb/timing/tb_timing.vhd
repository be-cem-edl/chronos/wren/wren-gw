library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.wishbone_pkg.all;
use work.pulser_pkg.all;
use work.pulser_group_map_consts_pkg.all;

entity tb_timing is
  generic (
    scenario : string := "clk40m"
  );
end tb_timing;

architecture arch of tb_timing is
  signal rst_n, clk_1g, clk_500m, clk_125m, clk_62m5 : std_logic := '0';
  signal wb_in : t_wishbone_slave_in;
  signal wb_out : t_wishbone_slave_out;
  signal inputs : t_subpulse_array(13 downto 0);
  signal comb_outputs : t_slv8_array(7 downto 0);
  signal outputs : std_logic_vector(7 downto 0);

  --  t0 is used for synchro
  constant tm_t0 : std_logic_vector(31 downto 0) := x"0000_0124"; --   t0 mod 4 = 0
  constant tm_t1 : std_logic_vector(31 downto 0) := tm_t0 or x"0000_0001";

  signal tm_tai_raw : std_logic_vector(39 downto 0) := x"00" & tm_t0;
  signal tm_cycles_raw : std_logic_vector(27 downto 0) := std_logic_vector(to_unsigned(62_499_960, 28));

  signal sync_62m5 : std_logic;
  signal tm_tai, tm_load_tai : std_logic_vector(31 downto 0);
  signal tm_cycles : std_logic_vector(25 downto 0);
  signal tm_load_steps : std_logic_vector(25 downto 5);
  signal tm_sync, pre_pps : std_logic;
  signal tm_reset : std_logic := '1';

  signal log_load, log_shift : std_logic;

  signal clocks : t_subpulse_array(8 downto 0);
begin
  rst_n <= '0', '1' after 16 ns;

  --  Clocking
  p_clk: process
  begin
    clk_62m5 <= not clk_62m5;
    for i in 1 to 2 loop
      clk_125m <= not clk_125m;
      for j in 1 to 4 loop
        clk_500m <= not clk_500m;
        for k in 1 to 2 loop
          clk_1g <= not clk_1g;
          wait for 500 ps;
        end loop;
       end loop;
    end loop;
  end process;

  p_tai: process (clk_62m5)
  begin
    if rising_edge(clk_62m5) then
      if tm_reset = '1' then
        tm_tai_raw <= x"00" & tm_t0;
        tm_cycles_raw <= std_logic_vector(to_unsigned(62_499_960, 28));
      else
        if unsigned (tm_cycles_raw) = 62_499_999 then
          tm_cycles_raw <= (others => '0');
          tm_tai_raw <= std_logic_vector(unsigned(tm_tai_raw) + 1);
        else
          tm_cycles_raw <= std_logic_vector(unsigned(tm_cycles_raw) + 1);
        end if;
      end if;
    end if;
  end process;

  p_out: process
    variable pre_outputs : t_slv8_array(7 downto 0);
  begin
    wait until rising_edge(clk_125m);
      --report "gen output";
      pre_outputs := comb_outputs;
      for i in 0 to 7 loop
        for k in 0 to 7 loop
          outputs (k) <= pre_outputs (k)(0);
          pre_outputs (k) := '0' & pre_outputs (k)(7 downto 1);
        end loop;
        if i /= 7 then
          wait until rising_edge(clk_1g);
        end if;
      end loop;
  end process;

  p_measure: process(clk_1g)
    variable cur, prev : std_logic;
    variable t_h : time := 0 ns;
  begin
    if rising_edge(clk_1g) then
      cur := outputs (4);
      if cur = '1' and prev = '0' then
        --  Rising edge.
        report "period=" & time'image(now - t_h);
        t_h := now;
      elsif cur = '0' and prev = '1' then
        --  falling edge
        report "high=" & time'image(now - t_h);
      end if;
      prev := cur;
    end if;
  end process;

  inst_clockgen: entity work.timegen
    port map (
      clk_62m5_i => clk_62m5,
      clk_125m_i => clk_125m,
      rst_n_i => rst_n,
      tm_tai_i => tm_tai_raw,
      tm_cycles_i => tm_cycles_raw (25 downto 0),
      tm_valid_i => '1',
      sync_62m5_o => sync_62m5,
      pre_pps_o => pre_pps,
      tm_tai_o => tm_tai,
      tm_cycles_o => tm_cycles,
      tm_sync_o => tm_sync,
      tm_load_tai_o => tm_load_tai,
      tm_load_steps_o => tm_load_steps
      );

  inst_1mhz: entity work.freqgen
      generic map (
        g_period => 1_000
        )
      port map (
        clk_125m_i => clk_125m,
        pps_i => pre_pps,
        clk_o => clocks (2),
        pulse_o => open
        );

  inst_40mhz: entity work.freqgen
    generic map (
      g_period => 25
      )
    port map (
      clk_125m_i => clk_125m,
      pps_i => pre_pps,
      clk_o => clocks (4),
      pulse_o => open
      );

  inst_timing: entity work.pulser_group
    generic map (
      C_NBR_PULSER => comb_outputs'length
      )
    port map (
      rst_n_i => rst_n,
      clk_pg_i => clk_125m,
      clk_cmp_i => clk_62m5,
      clk_wb_i => clk_62m5,
      sync_62m5_i => sync_62m5,
      wb_i => wb_in,
      wb_o => wb_out,
      tm_tai_i => tm_tai,
      tm_cycles_i => tm_cycles,
      tm_sync_i => tm_sync,
      tm_load_tai_i => tm_load_tai,
      tm_load_steps_i => tm_load_steps,
      inputs_i => inputs,
      clocks_i => clocks,
      comb_outputs_o => comb_outputs,
      log_load_i => log_load,
      log_shift_i => log_shift,
      pulses_int_o => open,
      cur_log_o => open
    );

  clocks (1 downto 0) <= (others => ('0', "000"));
  clocks (3) <= ('0', "000");
  clocks (7 downto 5) <= (others => ('0', "000"));

  inputs <= (others => ('0', "000"));

  p_log: process
  begin
    log_load <= '0';
    log_shift <= '0';

    loop
      wait until rising_edge(clk_125m);
      exit when pre_pps = '1';
    end loop;

    loop
      log_load <= '1';
      log_shift <= '1';

      wait until rising_edge(clk_125m);
      log_load <= '0';

      for i in 1 to 8 loop
        wait until rising_edge(clk_125m);
      end loop;

      log_shift <= '0';

      for i in 1 to 8 loop
        wait until rising_edge(clk_125m);
      end loop;
    end loop;

    wait;
  end process;
  
  p_test: process
    subtype slv32 is std_logic_vector(31 downto 0);
    subtype slv5 is std_logic_vector(4 downto 0);

    procedure write_wb(addr : natural; val : slv32) is
    begin
      wb_in.adr <= std_logic_vector(to_unsigned(addr, 32));
      wb_in.dat <= val;
      wb_in.we <= '1';
      wb_in.cyc <= '1';
      wb_in.stb <= '1';
      wb_in.sel <= "1111";
      loop
        wait until rising_edge (clk_62m5);
        exit when wb_out.ack = '1';
      end loop;
      wb_in.cyc <= '0';
      wb_in.stb <= '0';
    end write_wb;

    procedure read_wb(addr : natural; val : out slv32) is
    begin
      wb_in.adr <= std_logic_vector(to_unsigned(addr, 32));
      wb_in.dat <= (others => 'X');
      wb_in.we <= '0';
      wb_in.cyc <= '1';
      wb_in.stb <= '1';
      wb_in.sel <= "1111";
      loop
        wait until rising_edge (clk_62m5);
        exit when wb_out.ack = '1';
      end loop;
      val := wb_out.dat;
      wb_in.cyc <= '0';
      wb_in.stb <= '0';
    end read_wb;

    variable v, expect : slv32;
    variable t : natural;

    constant input_0  : slv5 := b"01000";
    constant input_1  : slv5 :=
      std_logic_vector(to_unsigned(comb_outputs'length + inputs'length + 1, 5));
    constant no_clock : slv5 := b"11111";
    constant no_start : slv5 := b"11111";
    constant no_stop  : slv5 := b"11111";
    constant clk_1m   : slv5 := b"11000";
    constant clk_40m  : slv5 := b"11010";

    procedure setup_comparator(idx : natural;
                               sec: slv32 := tm_t1;
                               nsec : slv32;
                               start : slv5 := no_start;
                               stop : slv5 := no_stop;
                               clk : slv5 := no_clock;
                               out_en : std_logic := '1';
                               int_en : std_logic := '1';
                               repeat : std_logic := '0';
                               imm : std_logic := '0';
                               high : slv32 := x"0000_0008";
                               period : slv32 := x"0000_007d";
                               npulses : slv32 := x"0000_0001";
                               idelay : slv32 := x"0000_0000")
    is
      constant addr : natural := c_PULSER_GROUP_MAP_COMPARATORS_ADDR + idx*32;
      variable cfg : slv32;
    begin
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_TIME_SEC_ADDR, sec);
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_TIME_NSEC_ADDR, nsec);
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_HIGH_ADDR, high);
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_PERIOD_ADDR, period);
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_NPULSES_ADDR, npulses);
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_IDELAY_ADDR, idelay);
      -- Start, Stop, clock, Pulser = 4
      cfg := (others => 'X');
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_OUT_EN_OFFSET) := out_en;
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_INT_EN_OFFSET) := int_en;
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_IMMEDIAT_OFFSET) := imm;
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_REPEAT_OFFSET) := repeat;
      cfg (c_PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_OFFSET + 4
          downto c_PULSER_GROUP_MAP_COMPARATORS_CONF1_CLOCK_OFFSET) := clk;
      cfg (c_PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_OFFSET + 4
          downto c_PULSER_GROUP_MAP_COMPARATORS_CONF1_STOP_OFFSET) := stop;
      cfg (c_PULSER_GROUP_MAP_COMPARATORS_CONF1_START_OFFSET + 4
          downto c_PULSER_GROUP_MAP_COMPARATORS_CONF1_START_OFFSET) := start;
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_ENABLE_OFFSET) := '1';
      cfg(c_PULSER_GROUP_MAP_COMPARATORS_CONF1_ABORT_OFFSET) := '0';
      write_wb (addr + c_PULSER_GROUP_MAP_COMPARATORS_CONF1_ADDR, cfg);
    end setup_comparator;
  begin
    wb_in.cyc <= '0';
    wb_in.stb <= '0';
    wait until rising_edge(clk_62m5) and rst_n = '1';

    report "configure comb-output";

    --  Configure comb-output 4 (pulser 4)
    write_wb (c_PULSER_GROUP_MAP_OUT_CFG_ADDR + 4 * c_PULSER_GROUP_MAP_OUT_CFG_SIZE,
              x"00_00_00_10");
    --  and 5 (pulser 0)
    write_wb (c_PULSER_GROUP_MAP_OUT_CFG_ADDR + 5 * c_PULSER_GROUP_MAP_OUT_CFG_SIZE,
              x"00_00_00_01");
    --  and 6 (pulser 1)
    write_wb (c_PULSER_GROUP_MAP_OUT_CFG_ADDR + 6 * c_PULSER_GROUP_MAP_OUT_CFG_SIZE,
              x"00_00_00_02");

    if scenario = "simple" or scenario = "all" then
      report "TEST: simple";
      tm_reset <= '1';

      --  Configure comparator 7.
      setup_comparator(7,
        sec => tm_t1,
        nsec => x"000002_12", -- ns (21 + 9 bits)
        start => input_0,
        high => x"0000_0008",
        period => x"0000_007d",
        npulses => x"0000_000a",
        idelay => x"0000_0000");

      --  Configure comparator 3
      setup_comparator(3,
        sec => tm_t1,
        nsec => x"000002_10",
        clk => clk_1m,
        high => x"0000_0014",
        period => x"0000_0001",
        npulses => x"0000_0005");

      report "setup done";
      tm_reset <= '0';

      --  TODO: expect pulses.
      assert comb_outputs(4) = x"00";
      wait until comb_outputs(4) /= x"00";
      report "pulse 4 detected";
      assert tm_tai = x"00" & tm_t1 report "Incorrect tai sec for pulse 4";
      assert tm_cycles = std_logic_vector(to_unsigned(1 * 125 + 16#12# / 2, 26)) report "incorrect tai cyc for pulse 4";
      for i in 1 to 4 loop
        wait until rising_edge(clk_62m5);
      end loop;
      read_wb (c_PULSER_GROUP_MAP_PULSES_EVNT_STS_ADDR, v);
      assert v(c_PULSER_GROUP_MAP_PULSES_EVNT_STS_NEMPTY_OFFSET) = '1';
    end if;

    if scenario = "clk1m+idelay" or scenario = "all" then
      --  Issue: observed delay is off by 1 tick
      report "TEST: clk1m+idelay";
      tm_reset <= '1';

      setup_comparator(2,
        sec => tm_t1,
        nsec => x"000003_e8",
        clk => clk_1m,
        idelay => x"0000_0005");

      tm_reset <= '0';

      --  Wait for the pulse.
      assert comb_outputs(4) = x"00";
      wait until comb_outputs(4) /= x"00";
      report "got a pulse at " & natural'image(to_integer(unsigned(tm_cycles)));

      --  Important: resync for WB.
      wait until rising_edge (clk_62m5);

      read_wb(c_PULSER_GROUP_MAP_PULSERS_ADDR
        + 4 * c_PULSER_GROUP_MAP_PULSERS_SIZE
        + c_PULSER_GROUP_MAP_PULSERS_TS_ADDR, v);
      v := v and x"3fff_ffff"; --  Keep only the ns part of the timestamp
      t := to_integer(unsigned(v));
      report "ts=" & natural'image(t);
      assert t = 6000 report "incorrect timestamp" severity error;
    end if;

    if scenario = "double_done" or scenario = "all" then
      tm_reset <= '1';

      setup_comparator(6,
        sec => tm_t1,
        nsec => x"000002_10");
      setup_comparator(7,
        sec => tm_t1,
        nsec => x"000006_10");

      tm_reset <= '0';

      for i in 0 to 1 loop
        --  Wait for the pulse.
        assert comb_outputs(4) = x"00";
        wait until comb_outputs(4) /= x"00";
        report "got a pulse";

        --  Important: resync for WB.
        wait until rising_edge (clk_62m5);

        read_wb(c_PULSER_GROUP_MAP_PULSERS_ADDR
          + 4 * c_PULSER_GROUP_MAP_PULSERS_SIZE
          + c_PULSER_GROUP_MAP_PULSERS_DONE_COMP_ADDR, v);
        assert v = x"0000_0000" report "done must be set only when the pulser is idle" severity error;
        --  Wait until the end of the pulse.
        wait for 125 ns;

        --  Important: resync for WB.
        wait until rising_edge (clk_62m5);
        read_wb(c_PULSER_GROUP_MAP_PULSERS_ADDR
          + 4 * c_PULSER_GROUP_MAP_PULSERS_SIZE
          + c_PULSER_GROUP_MAP_PULSERS_DONE_COMP_ADDR, v);
        expect := x"0000_0000";
        expect(i) := '1';
        assert v = expect report "done must be set" severity error;

        --  Clear done.
        wait until rising_edge (clk_62m5);
        v := x"0000_0000";
        v(16 + i) := '1';
        --FIXME: write_wb(c_PULSER_GROUP_MAP_COMP_BUSY_ADDR, v);
        wait until rising_edge (clk_62m5);
        read_wb(c_PULSER_GROUP_MAP_PULSERS_ADDR
          + 4 * c_PULSER_GROUP_MAP_PULSERS_SIZE
          + c_PULSER_GROUP_MAP_PULSERS_DONE_COMP_ADDR, v);
        assert v = x"0000_0000" report "done must be cleared" severity error;
      end loop;
    end if;

    if scenario = "clk1m+repeat" or scenario = "all" then
      report "TEST: clk1m+repeat";
      tm_reset <= '1';

      setup_comparator(33,
        sec => tm_t1,
        nsec => x"000003_e8", -- 1000
        clk => clk_1m,
        npulses => x"0000_0002",
        period => x"0000_0003",
        idelay => x"0000_0001");

      setup_comparator(7,
        sec => tm_t1,
        nsec => x"000003_e8", -- 1000
        clk => clk_1m,
        npulses => x"0000_0002",
        period => x"0000_0003",
        idelay => x"0000_0000");

      tm_reset <= '0';

      --  Wait for the pulse.
      assert comb_outputs(4) = x"00";
      wait until comb_outputs(4) /= x"00";
      report "got a pulse at " & natural'image(to_integer(unsigned(tm_cycles)));

      --  Important: resync for WB.
      wait until rising_edge (clk_62m5);

      read_wb(c_PULSER_GROUP_MAP_PULSERS_ADDR
        + 4 * c_PULSER_GROUP_MAP_PULSERS_SIZE
        + c_PULSER_GROUP_MAP_PULSERS_TS_ADDR, v);
      v := v and x"3fff_ffff"; --  Keep only the ns part of the timestamp
      t := to_integer(unsigned(v));
      report "ts=" & natural'image(t);
--      assert t = 6000 report "incorrect timestamp" severity error;
    end if;

    if scenario = "clk40m" or scenario = "all" then
      report "TEST: clock 40";
      tm_reset <= '1';

      --  Configure comparator 7.
      setup_comparator(7,
        sec => tm_t1,
        nsec => x"000002_12", -- ns (21 + 9 bits)
        clk => clk_40m,
        high => x"0000_0010",
        period => x"0000_0001",
        npulses => x"0000_000a",
        idelay => x"0000_0000");

      report "setup done";
      tm_reset <= '0';

      --  TODO: expect pulses.
      assert comb_outputs(4) = x"00";
      wait until comb_outputs(4) /= x"00";
      report "pulse 4 detected";
      assert tm_tai = x"00" & tm_t1 report "Incorrect tai sec for pulse 4";
      assert tm_cycles = std_logic_vector(to_unsigned(1 * 125 + 16#12# / 2, 26)) report "incorrect tai cyc for pulse 4";
      for i in 1 to 4 loop
        wait until rising_edge(clk_62m5);
      end loop;
      read_wb (c_PULSER_GROUP_MAP_PULSES_EVNT_STS_ADDR, v);
      assert v(c_PULSER_GROUP_MAP_PULSES_EVNT_STS_NEMPTY_OFFSET) = '1';
    end if;

    report "end of tests";
    wait;
  end process;
end arch;
