library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.disparity_gen_pkg.all;

entity wr_gthe4_phy_family7_xilinx_ip_pxie is

  generic (
    -- set to non-zero value to speed up the simulation by reducing some delays
    g_simulation         : integer := 0);

  port (
    -- Dedicated reference 125 MHz clock for the GTX transceiver
    clk_gth_i     : in std_logic;

    --  Free running 62.5Mhz (for reset controller and misc).
    clk_freerun_i : in std_logic;

    --  Power-on reset
    gth_reset_i : in std_logic;

    gth_reset_done_o : out std_logic;

    -- txoutclk from TX PCS (250Mhz)
    tx_out_clk_o : out std_logic;

    --  txusrclk for TX PCS (62.5Mhz)
    tx_usr_clk_i : in std_logic;

    -- data input (8 bits, not 8b10b-encoded)
    tx_data_i : in std_logic_vector(15 downto 0);

    -- 1 when tx_data_i contains a control code, 0 when it's a data byte
    tx_k_i : in std_logic_vector(1 downto 0);

    -- disparity of the currently transmitted 8b10b code (1 = plus, 0 = minus).
    -- Necessary for the PCS to generate proper frame termination sequences.
    -- Generated for the 2nd byte (LSB) of tx_data_i.
    tx_disparity_o : out std_logic;

    -- Encoding error indication (1 = error, 0 = no error)
    tx_enc_err_o : out std_logic;

    -- RX path, synchronous to ch0_rx_rbclk_o.

    -- RX recovered clock
    rx_rbclk_o : out std_logic;

    -- 8b10b-decoded data output. The data output must be kept invalid before
    -- the transceiver is locked on the incoming signal to prevent the EP from
    -- detecting a false carrier.
    rx_data_o : out std_logic_vector(15 downto 0);

    -- 1 when the byte on rx_data_o is a control code
    rx_k_o : out std_logic_vector(1 downto 0);

    -- encoding error indication
    rx_enc_err_o : out std_logic;

    -- RX bitslide indication, indicating the delay of the RX path of the
    -- transceiver (in UIs). Must be valid when ch0_rx_data_o is valid.
    rx_bitslide_o : out std_logic_vector(4 downto 0);

    -- reset input, active hi
    rst_i    : in std_logic;

    pad_txn_o : out std_logic;
    pad_txp_o : out std_logic;

    pad_rxn_i : in std_logic := '0';
    pad_rxp_i : in std_logic := '0';

    rdy_o : out std_logic;

    gt_powergood_o : out std_logic;
    clk_mon_o : out std_logic);
end wr_gthe4_phy_family7_xilinx_ip_pxie;

architecture rtl of wr_gthe4_phy_family7_xilinx_ip_pxie is

  COMPONENT gtwizard_ultrascale_pxie
  PORT (
    gtwiz_userclk_tx_active_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_srcclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_usrclk2_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userclk_rx_active_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_tx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_reset_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_start_user_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_buffbypass_rx_error_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_clk_freerun_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_all_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_pll_and_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_datapath_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_cdr_stable_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_tx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_reset_rx_done_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtwiz_userdata_tx_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    gtwiz_userdata_rx_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    gthrxn_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthrxp_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclk0_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxcommadeten_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxmcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxpcommaalignen_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxslide_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    tx8b10ben_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    txctrl0_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txctrl1_in : IN STD_LOGIC_VECTOR(15 DOWNTO 0);
    txctrl2_in : IN STD_LOGIC_VECTOR(7 DOWNTO 0);
    txusrclk_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    txusrclk2_in : IN STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthtxn_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gthtxp_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtpowergood_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    gtrefclkmonitor_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxbyteisaligned_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxbyterealign_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxcommadet_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    rxctrl0_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl1_out : OUT STD_LOGIC_VECTOR(15 DOWNTO 0);
    rxctrl2_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxctrl3_out : OUT STD_LOGIC_VECTOR(7 DOWNTO 0);
    rxpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txoutclk_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0);
    txpmaresetdone_out : OUT STD_LOGIC_VECTOR(0 DOWNTO 0)
  );
END COMPONENT;

--  signal gtwiz_userclk_tx_reset_in     : std_logic;
--  signal gtwiz_userclk_tx_active_out   : std_logic;
  signal gtwiz_userclk_rx_reset_in     : std_logic;
  signal gtwiz_userclk_rx_active_out   : std_logic;
  signal gtwiz_buffbypass_tx_reset_in  : std_logic;
  signal gtwiz_buffbypass_tx_done_out  : std_logic;
  signal gtwiz_buffbypass_tx_error_out : std_logic;
  signal gtwiz_buffbypass_rx_reset_in  : std_logic;
  signal gtwiz_buffbypass_rx_done_out  : std_logic;
  signal gtwiz_buffbypass_rx_error_out : std_logic;
  signal gtwiz_reset_tx_done_out       : std_logic;
  signal gtwiz_reset_rx_done_out       : std_logic;

  signal txctrl0_int        : std_logic_vector(15 downto 0);
  signal txctrl1_int        : std_logic_vector(15 downto 0);
  signal txctrl2_int        : std_logic_vector(7 downto 0);
  signal rxctrl0_int        : std_logic_vector(15 downto 0);
  signal rxctrl1_int        : std_logic_vector(15 downto 0);
  signal rxctrl2_int        : std_logic_vector(7 downto 0);
  signal rxctrl3_int        : std_logic_vector(7 downto 0);
  signal rxpmaresetdone_int : std_logic;
  signal txpmaresetdone_int : std_logic;

  signal rx_clk                                           : std_logic;
  signal serdes_ready_a, serdes_ready_txclk, serdes_ready_rxclk, rx_comma_det, rx_byte_is_aligned, rx_slide : std_logic;
  signal rx_synced, rst_rxclk                                     : std_logic;

  signal rx_data_int : std_logic_vector(15 downto 0);
  signal rx_k_int    : std_logic_vector(1 downto 0);

  signal cur_disp : t_8b10b_disparity;

  signal tx_is_k_swapped : std_logic_vector(1 downto 0);
  signal tx_data_swapped : std_logic_vector(15 downto 0);

  attribute keep                : string;
  attribute keep of rx_data_int : signal is "true";
  attribute keep of rx_k_int    : signal is "true";

  signal rst_n : std_logic;
  signal gtwiz_buffbypass_tx_reset_pre, gtwiz_buffbypass_rx_reset_pre : std_logic;

  signal gtrefclkmonitor : std_logic;
begin

  rst_n <= not rst_i;

  gth_reset_done_o <= gtwiz_reset_tx_done_out;

  gtwiz_buffbypass_tx_reset_pre <= not gtwiz_reset_tx_done_out;

  U_Sync1 : gc_sync_ffs
    port map (
      clk_i    => tx_usr_clk_i,
      rst_n_i  => rst_n,
      data_i   => gtwiz_buffbypass_tx_reset_pre,
      synced_o => gtwiz_buffbypass_tx_reset_in,
      npulse_o => open,
      ppulse_o => open);

  gtwiz_buffbypass_rx_reset_pre <= not gtwiz_userclk_rx_active_out or not gtwiz_buffbypass_tx_done_out;

  U_Sync2 : gc_sync_ffs
    port map (
      clk_i    => rx_clk,
      rst_n_i  => rst_n,
      data_i   => gtwiz_buffbypass_rx_reset_pre,
      synced_o => gtwiz_buffbypass_rx_reset_in,
      npulse_o => open,
      ppulse_o => open);

--  gtwiz_userclk_tx_reset_in <= not txpmaresetdone_int;
  gtwiz_userclk_rx_reset_in <= not rxpmaresetdone_int;

  U_Sync_Reset : gc_sync_ffs
    port map (
      clk_i    => rx_clk,
      rst_n_i  => '1',
      data_i   => rst_i,
      synced_o => rst_rxclk,
      npulse_o => open,
      ppulse_o => open);

  U_Bitslide : entity work.gtp_bitslide
    generic map (
      g_simulation => g_simulation,
      g_target     => "ultrascale")
    port map (
      gtp_rst_i                => rst_i,
      gtp_rx_clk_i             => rx_clk,
      gtp_rx_comma_det_i       => rx_comma_det,
      gtp_rx_byte_is_aligned_i => rx_byte_is_aligned,
      serdes_ready_i           => serdes_ready_rxclk,
      gtp_rx_slide_o           => rx_slide,
      gtp_rx_cdr_rst_o         => open,
      bitslide_o               => rx_bitslide_o,
      synced_o                 => rx_synced);

  tx_is_k_swapped <= tx_k_i(0) & tx_k_i(1);
  tx_data_swapped <= tx_data_i(7 downto 0) & tx_data_i(15 downto 8);

  U_gtwizard_gthe4 : gtwizard_ultrascale_pxie
    port map (
      gthrxn_in(0)                         => pad_rxn_i,
      gthrxp_in(0)                         => pad_rxp_i,
      gthtxn_out(0)                        => pad_txn_o,
      gthtxp_out(0)                        => pad_txp_o,

      gtwiz_reset_clk_freerun_in(0)         => clk_freerun_i,
      gtwiz_reset_all_in(0)                 => gth_reset_i,
      gtwiz_reset_tx_pll_and_datapath_in(0) => '0',
      gtwiz_reset_tx_datapath_in(0)         => '0',
      gtwiz_reset_rx_pll_and_datapath_in(0) => '0',
      gtwiz_reset_rx_datapath_in(0)         => rst_i,
      gtwiz_reset_rx_cdr_stable_out         => open,
      gtwiz_reset_tx_done_out(0)            => gtwiz_reset_tx_done_out,
      gtwiz_reset_rx_done_out(0)            => gtwiz_reset_rx_done_out,

      gtwiz_userclk_tx_active_in(0)        => '1',
      gtwiz_userclk_rx_reset_in(0)         => gtwiz_userclk_rx_reset_in,
      gtwiz_userclk_rx_usrclk_out           => open,
      gtwiz_userclk_rx_srcclk_out           => open,
      gtwiz_userclk_rx_usrclk2_out(0)      => rx_clk,
      gtwiz_userclk_rx_active_out(0)       => gtwiz_userclk_rx_active_out,

      gtwiz_buffbypass_tx_reset_in(0)      => gtwiz_buffbypass_tx_reset_in,
      gtwiz_buffbypass_tx_start_user_in(0) =>'0',
      gtwiz_buffbypass_tx_done_out(0)      => gtwiz_buffbypass_tx_done_out,
      gtwiz_buffbypass_tx_error_out(0)     => gtwiz_buffbypass_tx_error_out,
      gtwiz_buffbypass_rx_reset_in(0)      => gtwiz_buffbypass_rx_reset_in,
      gtwiz_buffbypass_rx_start_user_in(0) => '0',
      gtwiz_buffbypass_rx_done_out(0)      => gtwiz_buffbypass_rx_done_out,
      gtwiz_buffbypass_rx_error_out(0)     => gtwiz_buffbypass_rx_error_out,

      gtrefclk0_in(0) => clk_gth_i,

      txusrclk_in(0) => tx_usr_clk_i,
      txusrclk2_in(0) => tx_usr_clk_i,

      txoutclk_out(0) => tx_out_clk_o,


      gtwiz_userdata_tx_in                  => tx_data_swapped,
      gtwiz_userdata_rx_out                 => rx_data_int,
      rx8b10ben_in(0)                       => '1',
      rxcommadeten_in(0)                    => '1',
      rxmcommaalignen_in(0)                 => '0',
      rxpcommaalignen_in(0)                 => '0',
      rxslide_in(0)                         => rx_slide,
      tx8b10ben_in(0)                       => '1',
      txctrl0_in                            => txctrl0_int,
      txctrl1_in                            => txctrl1_int,
      txctrl2_in                            => txctrl2_int,
      rxbyteisaligned_out(0)                => rx_byte_is_aligned,
      rxbyterealign_out                     => open,
      rxcommadet_out(0)                     => rx_comma_det,
      rxctrl0_out                           => rxctrl0_int,
      rxctrl1_out                           => rxctrl1_int,
      rxctrl2_out                           => rxctrl2_int,
      rxctrl3_out                           => rxctrl3_int,
      rxpmaresetdone_out(0)                 => rxpmaresetdone_int,
      txpmaresetdone_out(0)                 => txpmaresetdone_int,

      gtpowergood_out(0) => gt_powergood_o,
      gtrefclkmonitor_out(0) => gtrefclkmonitor
      );


  serdes_ready_a <=
    not (rst_i or not gtwiz_reset_rx_done_out or not gtwiz_buffbypass_rx_done_out or not gtwiz_buffbypass_tx_done_out);

  U_Sync_Serdes_RDY1 : gc_sync_ffs
    port map (
      clk_i    => rx_clk,
      rst_n_i  => '1',
      data_i   => serdes_ready_a,
      synced_o => serdes_ready_rxclk,
      npulse_o => open,
      ppulse_o => open);

  U_Sync_Serdes_RDY2 : gc_sync_ffs
    port map (
      clk_i    => tx_usr_clk_i,
      rst_n_i  => '1',
      data_i   => serdes_ready_a,
      synced_o => serdes_ready_txclk,
      npulse_o => open,
      ppulse_o => open);

  txctrl0_int <= x"0000";
  txctrl1_int <= x"0000";
  txctrl2_int <= "000000" & tx_is_k_swapped;
  rx_k_int    <= rxctrl0_int(1 downto 0);


  p_gen_rx_outputs : process(rx_clk, rst_rxclk)
  begin
    if(rst_rxclk = '1') then
      rx_data_o    <= (others => '0');
      rx_k_o       <= (others => '0');
      rx_enc_err_o <= '0';
    elsif rising_edge(rx_clk) then
      if(serdes_ready_rxclk = '1' and rx_synced = '1') then
        rx_data_o    <= rx_data_int(7 downto 0) & rx_data_int(15 downto 8);
        rx_k_o       <= rx_k_int(0) & rx_k_int(1);
        rx_enc_err_o <= '0';  --rx_disp_err(0) or rx_disp_err(1) or rx_code_err(0) or rx_code_err(1);
      else
        rx_data_o    <= (others => '1');
        rx_k_o       <= (others => '1');
        rx_enc_err_o <= '1';
      end if;
    end if;
  end process;

  p_gen_tx_disparity : process(tx_usr_clk_i)
  begin
    if rising_edge(tx_usr_clk_i) then
      if serdes_ready_txclk = '0' then
        cur_disp <= RD_MINUS;
      else
        cur_disp <= f_next_8b10b_disparity16(cur_disp, tx_k_i, tx_data_i);
      end if;
    end if;
  end process;

  tx_disparity_o <= to_std_logic(cur_disp);

  rx_rbclk_o   <= rx_clk;

  clk_mon_o <= gtrefclkmonitor;

  rdy_o        <= serdes_ready_rxclk and rx_synced;
  tx_enc_err_o <= '0';

end rtl;
