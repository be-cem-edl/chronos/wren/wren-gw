files = [
    'mpsoc.tcl', 'wren_pxie_top_v0.vhd',
    'gtwizard_ultrascale_pxie/wr_gthe4_phy_family7_xilinx_ip_pxie.vhd',
    'gtwizard_ultrascale_pxie/gtwizard_ultrascale_pxie.xci',
    'gtwizard_ultrascale_rf/gtwizard_ultrascale_rf1.xci',
    ]

gen = [
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gte4_drp_arb.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_rx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_tx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_reset.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gthe4_cal_freqcnt.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_rx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_userdata_rx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_bit_sync.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gthe4_delay_powergood.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_userclk_tx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_reset_inv_sync.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal_tx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_tx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gtwiz_buffbypass_rx.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_reset_sync.v',
#   'gtwizard_ultrascale_pxie/hdl/gtwizard_ultrascale_v1_7_gthe4_cpll_cal.v',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie.v',
#   'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_v1_7_gthe4_channel.v',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie_gtwizard_gthe4.v',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie.xdc',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie_ooc.xdc',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie_gtwizard_top.v',
    'gtwizard_ultrascale_pxie/synth/gtwizard_ultrascale_pxie_gthe4_channel_wrapper.v',

]

modules = {
    'local': ['../../rtl'],
}
