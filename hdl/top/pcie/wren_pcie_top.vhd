--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- White Rabbit Trigger Distribution
-- https://ohwr-gitlab.cern.ch/projects/wrtd
--------------------------------------------------------------------------------
--
-- unit name:   wren_pcie_top
--
-- description: Top entity for GMT over WR playground
--
--------------------------------------------------------------------------------
-- Copyright CERN 2014-2019
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.axi4_pkg.all;
use work.wr_xilinx_pkg.all;
use work.pulser_pkg.all;
use work.wr_fabric_pkg.all;
use work.wrcore_pkg.all;
use work.endpoint_pkg.all;
use work.streamers_pkg.all;

library unisim;
use unisim.vcomponents.all;

entity wren_pcie_top is
  generic (
    --g_WRPC_INITF    : string  :=  "../../../../../dependencies/files/wrc-pxie.bram";
    g_WRPC_INITF    : string  :=  "";
    g_hwbld_date    : std_logic_vector(31 downto 0) := x"0000_0000";
    g_WITH_VME_MASTER : boolean := False;
    g_pcb_version : string := "pcie v0.0";
    -- Simulation-mode enable parameter. Set by default (synthesis) to 0, and
    -- changed to non-zero in the instantiation of the top level DUT in the
    -- testbench. Its purpose is to reduce some internal counters/timeouts
    -- to speed up simulations.
    g_SIMULATION     : integer := 0);
  port (
    ---------------------------------------------------------------------------
    -- Clocks/resets
    ---------------------------------------------------------------------------
    ps_por_i               : in std_logic;

    fpga_pl_clksys_p_i     : in  std_logic;
    fpga_pl_clksys_n_i     : in  std_logic;

    wr_clk_helper_125m_p_i : in  std_logic;
    wr_clk_helper_125m_n_i : in  std_logic;
    wr_clk_main_125m_p_i   : in  std_logic;
    wr_clk_main_125m_n_i   : in  std_logic;
    wr_clk_sfp_125m_p_i    : in  std_logic;
    wr_clk_sfp_125m_n_i    : in  std_logic;

    --  Unused
    wr_clk_aux_p_i         : in  std_logic;
    wr_clk_aux_n_i         : in  std_logic;

    -- clk_helper_25m_i       : in  std_logic;  --  The oscillator before the pll

    fp_i : in std_logic_vector(7 downto 0);
    fp_o : out std_logic_vector(7 downto 0);
    fp_oe_n_o : out std_logic_vector(7 downto 0);
    fp_term_en_o : out std_logic_vector(7 downto 0);
    pp_o : out std_logic_vector(31 downto 0);
    pp_oe_n_o : out std_logic;

    pp_id_i : in std_logic_vector(1 downto 0);
    fp_id_i : in std_logic_vector(1 downto 0);

    fp_led_o : out std_logic;
    pp_led0_o : out std_logic;
    pp_led1_o : out std_logic;
    sfp_led_o : out std_logic;

    wr_led_act_o : out std_logic;
    wr_led_link_o : out std_logic;

    ---------------------------------------------------------------------------
    -- SPI interface to DACs
    ---------------------------------------------------------------------------
    plldac_sclk_o   : out std_logic;
    plldac_din_o    : out std_logic;
    plldac_sync_n_o : out std_logic;

    wr_abscal_o : out std_logic;
    wr_pps_o    : out std_logic;

    --  SPI interface to PLL
    spi_helper_cs_n_o : out std_logic;
    spi_main_cs_n_o   : out std_logic;
    spi_mosi_o        : out std_logic;
    spi_miso_i        : in  std_logic;
    spi_sck_o         : out std_logic;

    ---------------------------------------------------------------------------
    -- SFP I/Os for transceiver
    ---------------------------------------------------------------------------
    sfp0_txp_o         : out std_logic;
    sfp0_txn_o         : out std_logic;
    sfp0_rxp_i         : in  std_logic;
    sfp0_rxn_i         : in  std_logic;
    sfp0_sda_b         : inout std_logic;
    sfp0_scl_b         : inout std_logic;
    sfp0_los_i         : in std_logic;
    sfp0_txdis_o       : out std_logic;
    sfp0_modabs_i      : in  std_logic;

    ---------------------------------------------------------------------------
    -- EEPROM I2C interface for storing configuration and accessing unique ID
    ---------------------------------------------------------------------------
    eeprom_sda_b  : inout std_logic;
    eeprom_scl_b  : inout std_logic;
    ---------------------------------------------------------------------------
    -- UART
    ---------------------------------------------------------------------------
    uart_rxd_i    : in  std_logic;
    uart_txd_o    : out std_logic;

    gth0_txp_o  : out std_logic;
    gth0_txn_o  : out std_logic;
    gth0_rxp_i  : in  std_logic;
    gth0_rxn_i  : in  std_logic;

    gth1_txp_o  : out std_logic;
    gth1_txn_o  : out std_logic;
    gth1_rxp_i  : in  std_logic;
    gth1_rxn_i  : in  std_logic;

    gth_bclk1_p_i : in std_logic;
    gth_bclk1_n_i : in std_logic;
    gth_bclk2_p_i : in std_logic;
    gth_bclk2_n_i : in std_logic
);
end entity wren_pcie_top;

architecture arch of wren_pcie_top is
  -- clock and reset
  signal clk_sys_62m5     : std_logic;
  signal clk_sys_125m     : std_logic;
  signal clk_sys_250m     : std_logic;
  signal clk_sys_500m     : std_logic;
  signal rst_sys_n   : std_logic;

  signal gth_clk_250m : std_logic;

  signal clk_bunch : std_logic_vector(2 downto 1);

  alias clk : std_logic is clk_sys_62m5;
  alias rst_n : std_logic is rst_sys_n;

  attribute keep : string;

  --  WR fabric.
  signal eth_tx_out : t_wrf_source_out;
  signal eth_tx_in  : t_wrf_source_in;
  signal eth_rx_out : t_wrf_sink_out;
  signal eth_rx_in  : t_wrf_sink_in;

  --  mpsoc to pci map
  signal m_axi_out: t_axi4_lite_master_out_32;
  signal m_axi_in : t_axi4_lite_master_in_32;

  signal board_axi_out: t_axi4_lite_master_out_32;
  signal board_axi_in : t_axi4_lite_master_in_32;
  
  signal host_wb_out : t_wishbone_master_out;
  signal host_wb_in  : t_wishbone_master_in;

  signal wr_master_out : t_wishbone_master_out;
  signal wr_master_in  : t_wishbone_master_in;

  signal wb_si5340_out : t_wishbone_master_out;
  signal wb_si5340_in  : t_wishbone_master_in;

  -- WRPC TM interface and aux clocks
  signal tm_link_up         : std_logic;
  signal tm_tai             : std_logic_vector(39 downto 0);
  signal tm_cycles          : std_logic_vector(27 downto 0);
  signal tm_time_valid      : std_logic;

  signal fp_out_unused, fp_oe_unused : std_logic_vector(29 downto 8);
  signal fp_term_unused : std_logic_vector(31 downto 8);
  signal fp_oe : std_logic_vector(7 downto 0);

  signal pp_oe : std_logic_vector(31 downto 0);
  signal pp_en : std_logic;

  -- MT TM interface
--  signal tm : t_mt_timing_if;

signal s_axi_awuser : STD_LOGIC;
signal s_axi_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
signal s_axi_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
signal s_axi_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
signal s_axi_awsize : STD_LOGIC_VECTOR ( 2 downto 0 );
signal s_axi_awburst : STD_LOGIC_VECTOR ( 1 downto 0 );
signal s_axi_awlock : STD_LOGIC;
signal s_axi_awcache : STD_LOGIC_VECTOR ( 3 downto 0 );
signal s_axi_awprot : STD_LOGIC_VECTOR ( 2 downto 0 );
signal s_axi_awvalid : STD_LOGIC;
signal s_axi_awready :  STD_LOGIC;
signal s_axi_wdata : STD_LOGIC_VECTOR ( 31 downto 0 );
signal s_axi_wstrb : STD_LOGIC_VECTOR ( 3 downto 0 );
signal s_axi_wlast : STD_LOGIC;
signal s_axi_wvalid : STD_LOGIC;
signal s_axi_wready :  STD_LOGIC;
signal s_axi_bid :  STD_LOGIC_VECTOR ( 5 downto 0 );
signal s_axi_bresp :  STD_LOGIC_VECTOR ( 1 downto 0 );
signal s_axi_bvalid :  STD_LOGIC;
signal s_axi_bready : STD_LOGIC;

  signal S_AXI_LOG_awid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_awaddr : STD_LOGIC_VECTOR ( 48 downto 0 );
  signal S_AXI_LOG_awlen : STD_LOGIC_VECTOR ( 7 downto 0 );
  signal S_AXI_LOG_awvalid : STD_LOGIC;
  signal S_AXI_LOG_awready : STD_LOGIC;
  signal S_AXI_LOG_wdata : STD_LOGIC_VECTOR ( 127 downto 0 );
  signal S_AXI_LOG_wstrb : STD_LOGIC_VECTOR ( 15 downto 0 );
  signal S_AXI_LOG_wlast : STD_LOGIC;
  signal S_AXI_LOG_wvalid : STD_LOGIC;
  signal S_AXI_LOG_wready : STD_LOGIC;
  signal S_AXI_LOG_bid : STD_LOGIC_VECTOR ( 5 downto 0 );
  signal S_AXI_LOG_bresp : STD_LOGIC_VECTOR ( 1 downto 0 );
  signal S_AXI_LOG_bvalid : STD_LOGIC;
  signal S_AXI_LOG_bready : STD_LOGIC;

  signal gpio_in, gpio_out : std_logic_vector(94 downto 0);

  signal pps_p, pps_valid : std_logic;

  signal irq : std_logic;

  signal clk_pl, rst_pl_n, rst_pl : std_logic;

  signal pps_led : std_logic;

  --  spi
  signal soc_spi_helper_cs_n : std_logic;
  signal soc_spi_main_cs_n   : std_logic;
  signal soc_spi_mosi        : std_logic;
  signal soc_spi_miso        : std_logic;
  signal soc_spi_sck         : std_logic;

  --  From 'view instantiation template'
  component mpsoc is
    port (
      M_AXI_awaddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_awprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_awvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_awready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_wdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_wstrb : out STD_LOGIC_VECTOR ( 3 downto 0 );
      M_AXI_wvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_wready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_bresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_bvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_bready : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_araddr : out STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_arprot : out STD_LOGIC_VECTOR ( 2 downto 0 );
      M_AXI_arvalid : out STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_arready : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_rdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      M_AXI_rresp : in STD_LOGIC_VECTOR ( 1 downto 0 );
      M_AXI_rvalid : in STD_LOGIC_VECTOR ( 0 to 0 );
      M_AXI_rready : out STD_LOGIC_VECTOR ( 0 to 0 );
      GPIO_tri_i : in STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_o : out STD_LOGIC_VECTOR ( 94 downto 0 );
      GPIO_tri_t : out STD_LOGIC_VECTOR ( 94 downto 0 );
      S_AXI_aruser : in STD_LOGIC;
      S_AXI_awuser : in STD_LOGIC;
      S_AXI_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_awlock : in STD_LOGIC;
      S_AXI_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_awvalid : in STD_LOGIC;
      S_AXI_awready : out STD_LOGIC;
      S_AXI_wdata : in STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_wstrb : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_wlast : in STD_LOGIC;
      S_AXI_wvalid : in STD_LOGIC;
      S_AXI_wready : out STD_LOGIC;
      S_AXI_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_bvalid : out STD_LOGIC;
      S_AXI_bready : in STD_LOGIC;
      S_AXI_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_arlock : in STD_LOGIC;
      S_AXI_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_arvalid : in STD_LOGIC;
      S_AXI_arready : out STD_LOGIC;
      S_AXI_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_rdata : out STD_LOGIC_VECTOR ( 31 downto 0 );
      S_AXI_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_rlast : out STD_LOGIC;
      S_AXI_rvalid : out STD_LOGIC;
      S_AXI_rready : in STD_LOGIC;
      S_AXI_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_aruser : in STD_LOGIC;
      S_AXI_LOG_awuser : in STD_LOGIC;
      S_AXI_LOG_awid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_awaddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_awlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_awsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_awlock : in STD_LOGIC;
      S_AXI_LOG_awcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_awprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_awvalid : in STD_LOGIC;
      S_AXI_LOG_awready : out STD_LOGIC;
      S_AXI_LOG_wdata : in STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_wstrb : in STD_LOGIC_VECTOR ( 15 downto 0 );
      S_AXI_LOG_wlast : in STD_LOGIC;
      S_AXI_LOG_wvalid : in STD_LOGIC;
      S_AXI_LOG_wready : out STD_LOGIC;
      S_AXI_LOG_bid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_bresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_bvalid : out STD_LOGIC;
      S_AXI_LOG_bready : in STD_LOGIC;
      S_AXI_LOG_arid : in STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_araddr : in STD_LOGIC_VECTOR ( 48 downto 0 );
      S_AXI_LOG_arlen : in STD_LOGIC_VECTOR ( 7 downto 0 );
      S_AXI_LOG_arsize : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arburst : in STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_arlock : in STD_LOGIC;
      S_AXI_LOG_arcache : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arprot : in STD_LOGIC_VECTOR ( 2 downto 0 );
      S_AXI_LOG_arvalid : in STD_LOGIC;
      S_AXI_LOG_arready : out STD_LOGIC;
      S_AXI_LOG_rid : out STD_LOGIC_VECTOR ( 5 downto 0 );
      S_AXI_LOG_rdata : out STD_LOGIC_VECTOR ( 127 downto 0 );
      S_AXI_LOG_rresp : out STD_LOGIC_VECTOR ( 1 downto 0 );
      S_AXI_LOG_rlast : out STD_LOGIC;
      S_AXI_LOG_rvalid : out STD_LOGIC;
      S_AXI_LOG_rready : in STD_LOGIC;
      S_AXI_LOG_awqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      S_AXI_LOG_arqos : in STD_LOGIC_VECTOR ( 3 downto 0 );
      clk_i : in STD_LOGIC;
      aresetn_i : in STD_LOGIC;
      pl_resetn_o : out STD_LOGIC;
      pl_ps_irq0_i : in STD_LOGIC_VECTOR ( 0 to 0 );
      pl_clk_o : out STD_LOGIC;
      saxi_clk : in STD_LOGIC;
      spi0_sclk_i : in STD_LOGIC;
      spi0_sclk_o : out STD_LOGIC;
      spi0_sclk_t : out STD_LOGIC;
      spi0_m_i : in STD_LOGIC;
      spi0_m_o : out STD_LOGIC;
      spi0_mo_t : out STD_LOGIC;
      spi0_s_i : in STD_LOGIC;
      spi0_s_o : out STD_LOGIC;
      spi0_so_t : out STD_LOGIC;
      spi0_ss_i_n : in STD_LOGIC;
      spi0_ss_o_n : out STD_LOGIC;
      spi0_ss_n_t : out STD_LOGIC;
      spi0_ss1_o_n : out STD_LOGIC;
      saxi_log_clk : in STD_LOGIC
    );
    end component mpsoc;
begin
  inst_mpsoc: mpsoc
    port map (
      --  Master bus (from PS) to give CPU access to the wren memmap.
      M_AXI_araddr(31 downto 0)  => m_axi_out.araddr,
      M_AXI_arprot  => open,
      M_AXI_arready(0) => m_axi_in.arready,
      M_AXI_arvalid(0) => m_axi_out.arvalid,
      M_AXI_awaddr(31 downto 0) => m_axi_out.awaddr,
      M_AXI_awprot  => open,
      M_AXI_awready(0) => m_axi_in.awready,
      M_AXI_awvalid(0) => m_axi_out.awvalid,
      M_AXI_bready(0)  => m_axi_out.bready,
      M_AXI_bresp   => m_axi_in.bresp,
      M_AXI_bvalid(0)  => m_axi_in.bvalid,
      M_AXI_rdata   => m_axi_in.rdata,
      M_AXI_rready(0)  => m_axi_out.rready,
      M_AXI_rresp   => m_axi_in.rresp,
      M_AXI_rvalid(0) => m_axi_in.rvalid,
      M_AXI_wdata   => m_axi_out.wdata,
      M_AXI_wready(0)  => m_axi_in.wready,
      M_AXI_wstrb   => m_axi_out.wstrb,
      M_AXI_wvalid(0)  => m_axi_out.wvalid,

      --  For interrupt generation
      S_AXI_awuser => s_axi_awuser,
      S_AXI_awid => s_axi_awid,
      S_AXI_awaddr => s_axi_awaddr,
      S_AXI_awlen => s_axi_awlen,
      S_AXI_awsize => s_axi_awsize,
      S_AXI_awburst => s_axi_awburst,
      S_AXI_awlock => s_axi_awlock,
      S_AXI_awcache => s_axi_awcache,
      S_AXI_awprot => s_axi_awprot,
      S_AXI_awvalid => s_axi_awvalid,
      S_AXI_awready => s_axi_awready,
      S_AXI_awqos => (others => '0'),
      S_AXI_wdata => s_axi_wdata,
      S_AXI_wstrb => s_axi_wstrb,
      S_AXI_wlast => s_axi_wlast,
      S_AXI_wvalid => s_axi_wvalid,
      S_AXI_wready => s_axi_wready,
      S_AXI_bid => s_axi_bid,
      S_AXI_bresp => s_axi_bresp,
      S_AXI_bvalid => s_axi_bvalid,
      S_AXI_bready => s_axi_bready,
      S_AXI_aruser => 'X',
      S_AXI_arid => (others => 'X'),
      S_AXI_araddr => (others => 'X'),
      S_AXI_arlen => (others => 'X'),
      S_AXI_arsize => (others => 'X'),
      S_AXI_arburst => (others => 'X'),
      S_AXI_arlock => 'X',
      S_AXI_arcache => (others => 'X'),
      S_AXI_arprot => (others => 'X'),
      S_AXI_arvalid => '0',
      S_AXI_arready => open,
      S_AXI_rid => open,
      S_AXI_rdata => open,
      S_AXI_rresp => open,
      S_AXI_rlast => open,
      S_AXI_rvalid => open,
      S_AXI_rready => '0',
      S_AXI_arqos => (others => 'X'),

      saxi_clk => clk,

      saxi_log_clk => clk_sys_250m,

      --  Direct access to DDR for logging.
      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awsize => b"100",  -- 100: 16 bytes, 011: 8 bytes
      S_AXI_LOG_awburst => b"01",  -- 00: FIXED, (01: INCR)
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,
      s_AXI_LOG_awuser => '0',
      s_AXI_LOG_awqos => (others => '0'),
      s_AXI_LOG_awcache => "0001",  -- Set AWCACHE[1] to upsizing ?
      s_AXI_LOG_awlock => '0',
      s_AXI_LOG_awprot => "001",
      s_AXI_LOG_araddr => (others => 'X'),
      s_AXI_LOG_aruser => 'X',
      s_AXI_LOG_arid => (others => 'X'),
      s_AXI_LOG_arlen => (others => 'X'),
      s_AXI_LOG_arsize => (others => 'X'),
      s_AXI_LOG_arburst => (others => 'X'),
      s_AXI_LOG_arcache => (others => 'X'),
      s_AXI_LOG_arlock => 'X',
      s_AXI_LOG_arprot => (others => 'X'),
      s_AXI_LOG_arvalid => '0',
      s_AXI_LOG_arready => open,
      s_AXI_LOG_arqos => (others => 'X'),
      s_AXI_LOG_rready => '0',
      s_AXI_LOG_rid => open,
      s_AXI_LOG_rvalid => open,
      s_AXI_LOG_rlast => open,
      s_AXI_LOG_rresp => open,
      s_AXI_LOG_rdata => open,

      pl_ps_irq0_i  => "0",
      GPIO_tri_i => gpio_in,
      GPIO_tri_o => gpio_out,
      GPIO_tri_t => open,

      spi0_m_i => soc_spi_miso,
      spi0_m_o => soc_spi_mosi,
      spi0_mo_t => open,
      spi0_s_i => '1', -- spi_miso_i,
      spi0_s_o => open,
      spi0_so_t => open,
      spi0_sclk_i => '0',
      spi0_sclk_o => soc_spi_sck,
      spi0_sclk_t => open,
      spi0_ss_i_n => '1',
      spi0_ss_n_t => open,
      spi0_ss_o_n => soc_spi_main_cs_n,
      spi0_ss1_o_n => soc_spi_helper_cs_n,

      aresetn_i     => rst_n,
      clk_i         => clk,
      pl_clk_o      => clk_pl,
      pl_resetn_o   => rst_pl_n);

    rst_pl <= not rst_pl_n;

    inst_int_gen: entity work.mpsoc_int_gen
    port map (
      clk_i => clk,
      rst_n_i => rst_n,
      irq_i => irq,
      s_axi_awaddr => s_axi_awaddr,
      s_axi_awburst => s_axi_awburst,
      s_axi_awcache => s_axi_awcache,
      s_axi_awid => s_axi_awid,
      s_axi_awlen => s_axi_awlen,
      s_axi_awlock => s_axi_awlock,
      s_axi_awprot => s_axi_awprot,
      s_axi_awready => s_axi_awready,
      s_axi_awsize => s_axi_awsize,
      s_axi_awuser => s_axi_awuser,
      s_axi_awvalid => s_axi_awvalid,
      s_axi_wdata => s_axi_wdata,
      s_axi_wlast => s_axi_wlast,
      s_axi_wready => s_axi_wready,
      s_axi_wstrb => s_axi_wstrb,
      s_axi_wvalid => s_axi_wvalid,
      s_axi_bid => s_axi_bid,
      s_axi_bready => s_axi_bready,
      s_axi_bresp => s_axi_bresp,
      s_axi_bvalid => s_axi_bvalid
    );
  
  inst_pci_map: entity work.pci_map
    port map (
      aclk => clk,
      areset_n => rst_n,
      awvalid => m_axi_out.awvalid,
      awready => m_axi_in.awready,
      awaddr => m_axi_out.awaddr(20 downto 2),
      awprot => "000",
      wvalid => m_axi_out.wvalid,
      wready => m_axi_in.wready,
      wdata => m_axi_out.wdata,
      wstrb => m_axi_out.wstrb,
      bvalid => m_axi_in.bvalid,
      bready => m_axi_out.bready,
      bresp => m_axi_in.bresp,
      arvalid => m_axi_out.arvalid,
      arready => m_axi_in.arready,
      araddr => m_axi_out.araddr(20 downto 2),
      arprot => "000",
      rvalid => m_axi_in.rvalid,
      rready => m_axi_out.rready,
      rdata => m_axi_in.rdata,
      rresp => m_axi_in.rresp,

      host_i => host_wb_in,
      host_o => host_wb_out,

      board_awvalid_o => board_axi_out.awvalid,
      board_awready_i => board_axi_in.awready,
      board_awaddr_o => board_axi_out.awaddr (16 downto 2),
      board_awprot_o => open,
      board_wvalid_o => board_axi_out.wvalid,
      board_wready_i => board_axi_in.wready,
      board_wdata_o => board_axi_out.wdata,
      board_wstrb_o => board_axi_out.wstrb,
      board_bvalid_i => board_axi_in.bvalid,
      board_bready_o => board_axi_out.bready,
      board_bresp_i => board_axi_in.bresp,
      board_arvalid_o => board_axi_out.arvalid,
      board_arready_i => board_axi_in.arready,
      board_araddr_o => board_axi_out.araddr (16 downto 2),
      board_arprot_o => open,
      board_rvalid_i => board_axi_in.rvalid,
      board_rready_o => board_axi_out.rready,
      board_rdata_i => board_axi_in.rdata,
      board_rresp_i => board_axi_in.rresp
    );


  fp_oe_n_o <= not fp_oe;
  pp_oe <= (others => pp_en);
  pp_oe_n_o <= not pp_en;

  inst_platform: entity work.wren_platform
    generic map (
      g_simulation => g_simulation,
      g_wrpc_initf => g_wrpc_initf,
      g_hwbld_date => g_hwbld_date,
      g_with_external_clock_input => false
    )
    port map (
      wr_clk_sfp_p_i => wr_clk_sfp_125m_p_i,
      wr_clk_sfp_n_i => wr_clk_sfp_125m_n_i,
      fpga_pl_clksys_p_i => fpga_pl_clksys_p_i,
      fpga_pl_clksys_n_i => fpga_pl_clksys_n_i,
      wr_clk_main_p_i => wr_clk_main_125m_p_i,
      wr_clk_main_n_i => wr_clk_main_125m_n_i,
      wr_clk_helper_p_i => wr_clk_helper_125m_p_i,
      wr_clk_helper_n_i => wr_clk_helper_125m_n_i,
      clk_sys2_125m_o => open,
      clk_pl_i => clk_pl,
      rst_pl_n_i => rst_pl_n,
      rst_pl_cont_i => gpio_out(1),
      rst_pl_done_o => gpio_in(0),
      clk_sys_62m5_o => clk_sys_62m5,
      clk_sys_125m_o => clk_sys_125m,
      clk_sys_250m_o => clk_sys_250m,
      clk_sys_500m_o => clk_sys_500m,
      rst_sys_n_o => rst_sys_n,
      sfp_txp_o => sfp0_txp_o,
      sfp_txn_o => sfp0_txn_o,
      sfp_rxp_i => sfp0_rxp_i,
      sfp_rxn_i => sfp0_rxn_i,
      gth_bclk1_p_i => gth_bclk1_p_i,
      gth_bclk1_n_i => gth_bclk1_n_i,
      gth_bclk2_p_i => gth_bclk2_p_i,
      gth_bclk2_n_i => gth_bclk2_n_i,
      sfp_sda_b => sfp0_sda_b,
      sfp_scl_b => sfp0_scl_b,
      led_act_o => wr_led_act_o,
      led_link_o => wr_led_link_o,
      sfp_los_i => sfp0_los_i,
      sfp_tx_disable_o => sfp0_txdis_o,
      sfp_det_i => sfp0_modabs_i,
      wr_abscal_o => wr_abscal_o,
      tm_link_up_o => tm_link_up,
      tm_time_valid_o => tm_time_valid,
      tm_tai_o => tm_tai,
      tm_cycles_o => tm_cycles,
      spi_main_cs_n_o => spi_main_cs_n_o,
      spi_mosi_o => spi_mosi_o,
      spi_miso_i => spi_miso_i,
      spi_sck_o => spi_sck_o,
      eeprom_sda_b => eeprom_sda_b,
      eeprom_scl_b => eeprom_scl_b,
      uart_rxd_i => uart_rxd_i,
      uart_txd_o => uart_txd_o,
      pps_p_o => pps_p,
      pps_valid_o => pps_valid,
      pps_led_o => pps_led,
      wb_si5340_i => wb_si5340_out,
      wb_si5340_o => wb_si5340_in,
      wr_master_i => wr_master_out,
      wr_master_o => wr_master_in,
      eth_tx_i => eth_tx_out,
      eth_tx_o => eth_tx_in,
      eth_rx_i => eth_rx_out,
      eth_rx_o => eth_rx_in,
      soc_spi_helper_cs_n_i => soc_spi_helper_cs_n,
      soc_spi_main_cs_n_i => soc_spi_main_cs_n,
      soc_spi_mosi_i => soc_spi_mosi,
      soc_spi_miso_o => soc_spi_miso,
      soc_spi_sck_i => soc_spi_sck,
      clk_bunch_o => clk_bunch,
      gth_clk_250m_o => gth_clk_250m
    );

  inst_wren: entity work.wren_core
    generic map (
      g_with_serdes => true,
      g_nbr_rf => 2,
      g_nbr_pulser_group => 4,
      g_nbr_outputs => 30,
      g_with_patch_panel => true,
      g_pcb_version => g_pcb_version,
      g_model_ident => x"70_63_69_31" -- pci1
      )
    port map (
      clk_sys_62m5_i => clk_sys_62m5,
      clk_sys_125m_i => clk_sys_125m,
      clk_sys_250m_i => clk_sys_250m,
      clk_sys_500m_i => clk_sys_500m,
      rst_sys_62m5_n_i => rst_sys_n,
      board_axi_o => board_axi_in,
      board_axi_i => board_axi_out,
      host_wb_o => host_wb_in,
      host_wb_i => host_wb_out,
      tm_link_up_i => tm_link_up,
      tm_tai_i => tm_tai,
      tm_cycles_i => tm_cycles,
      tm_time_valid_i => tm_time_valid,
      wrpc_master_o => wr_master_out,
      wrpc_master_i => wr_master_in,
      irq_o => irq,
      eth_tx_o => eth_tx_out,
      eth_tx_i => eth_tx_in,
      eth_rx_o => eth_rx_out,
      eth_rx_i => eth_rx_in,

      pxie_dstara_mux_o => open,
      pxie_dstarc_mux_o => open,

      S_AXI_LOG_awid => S_AXI_LOG_awid,
      S_AXI_LOG_awaddr => S_AXI_LOG_awaddr,
      S_AXI_LOG_awlen => S_AXI_LOG_awlen,
      S_AXI_LOG_awvalid => S_AXI_LOG_awvalid,
      S_AXI_LOG_awready => S_AXI_LOG_awready,
      S_AXI_LOG_wdata => S_AXI_LOG_wdata,
      S_AXI_LOG_wstrb => S_AXI_LOG_wstrb,
      S_AXI_LOG_wlast => S_AXI_LOG_wlast,
      S_AXI_LOG_wvalid => S_AXI_LOG_wvalid,
      S_AXI_LOG_wready => S_AXI_LOG_wready,
      S_AXI_LOG_bid => S_AXI_LOG_bid,
      S_AXI_LOG_bresp => S_AXI_LOG_bresp,
      S_AXI_LOG_bvalid => S_AXI_LOG_bvalid,
      S_AXI_LOG_bready => S_AXI_LOG_bready,

      wb_si5340_i => wb_si5340_in,
      wb_si5340_o => wb_si5340_out,

      fp_led0_o => fp_led_o,
      fp_led1_o => open,
      pp_led0_o => pp_led0_o,
      pp_led1_o => pp_led1_o,

      switches_i => (others => '0'),
      fp_id_i (1 downto 0) => fp_id_i,
      fp_id_i (2) => '0',
      pp_id_i (1 downto 0) => pp_id_i,
      pp_id_i (2) => '0',
      vme_ga_i => (others => '0'),

      pad_oe_o (7 downto 0) => fp_oe,
      pad_oe_o (29 downto 8) => fp_oe_unused,
      pad_term_o (7 downto 0) => fp_term_en_o,
      pad_term_o (31 downto 8) => fp_term_unused,
      pad_o (7 downto 0) => fp_o,
      pad_o (29 downto 8) => fp_out_unused,
      pad_i (7 downto 0) => fp_i,
      pad_i (31 downto 8) => x"000000",
      pp_oe_i => pp_oe,
      pp_en_o => pp_en,
      pp_o => pp_o,
      vme_userio_i => (others => '0'),
      vme_userio_dir_o => open,
      vme_userio_en_o => open,
      pps_p_i => pps_p,
      pps_valid_i => pps_valid,

      rf1_rxn_i => gth0_rxn_i,
      rf1_rxp_i => gth0_rxp_i,
      rf1_txn_o => gth0_txn_o,
      rf1_txp_o => gth0_txp_o,

      rf2_rxn_i => gth1_rxn_i,
      rf2_rxp_i => gth1_rxp_i,
      rf2_txn_o => gth1_txn_o,
      rf2_txp_o => gth1_txp_o,

      rf3_rxn_i => '0',
      rf3_rxp_i => '0',
      rf3_txn_o => open,
      rf3_txp_o => open,

      gth_clk_250m_i => gth_clk_250m
      );

  wr_pps_o <= pps_p;

  b_leds: block
    constant clk_freq : natural := 62_500_000;
  begin
    inst_sfp: entity work.sfp_argb_leds
      generic map (
        g_clk_freq => clk_freq
      )
      port map (
        clk_i => clk,
        rst_n_i => rst_n,
        sfp_act_i => '0',
        sfp_link_i => '0',
        pps_led_i => pps_led,
        pps_valid_i => pps_valid,
        dout_o => sfp_led_o
      );
  end block;
end architecture arch;
