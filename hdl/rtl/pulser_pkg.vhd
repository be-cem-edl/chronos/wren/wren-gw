library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

package pulser_pkg is
  type t_pulser_config is record
    --  Extra ns delay after load_en
    time_ns : std_logic_vector(9 downto 0);

    -- Start signal
    start    : std_logic_vector(4 downto 0);

    -- Stop signal
    stop     : std_logic_vector(4 downto 0);

    -- Clock enable signal
    clk_en   : std_logic_vector(4 downto 0);

    --  Repeat mode
    repeat   : std_logic;

    --  Enable software interrupt
    int_en : std_logic;

    --  Enable output
    out_en : std_logic;

    --  Initial delay before the first pulse and after the start.
    idelay   : std_logic_vector(31 downto 0);

    -- Number of system clock periods for the high part of the pulse
    high     : std_logic_vector(31 downto 0);

    -- Number of clock periods for the pulse
    period   : std_logic_vector(31 downto 0);

    -- Number of pulses
    npulses  : std_logic_vector(31 downto 0);
  end record;

  constant c_NO_START : std_logic_vector(4 downto 0) := "11111";
  constant c_NO_CLOCK : std_logic_vector(4 downto 0) := "11111";

  --  The internal clock is 125Mhz, but the granularity is 1ns.
  type t_subpulse is record
    --  The value of the pulse at the end of the 8ns.
    v : std_logic;

    --  The edge position.
    --  If 0, there is no edge during the 8ns (but there might
    --  be an edge at the start).
    dly : std_logic_vector(2 downto 0);
  end record;

  type t_subpulse_array is array (natural range <>) of t_subpulse;

  --  Extended pulse (8b, 1b per ns).
  subtype t_slv8 is std_logic_vector(7 downto 0);
  type t_slv8_array is array(natural range <>) of t_slv8;

  function extend_subpulse(s : t_subpulse; prev : std_logic) return t_slv8;

  type t_pulser_log_array is array (natural range <>) of std_logic_vector(3 downto 0);
end pulser_pkg;

package body pulser_pkg is
  function extend_subpulse(s : t_subpulse; prev : std_logic) return t_slv8
  is
    variable tmp : std_logic_vector(15 downto 0);
  begin
    tmp := (15 downto 8 => s.v) & (7 downto 0 => prev);
    tmp := std_logic_vector (shift_left (unsigned (tmp), to_integer(unsigned (s.dly))));
    --  Assume output left to right.
    return tmp (15 downto 8);
  end extend_subpulse;
end pulser_pkg;
