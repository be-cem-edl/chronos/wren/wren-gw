library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use work.gencores_pkg.all;
use work.wishbone_pkg.all;
use work.pulser_pkg.all;
use work.pulser_group_map_consts_pkg.all;

entity pulser_group is
  generic (
    --  Number of pulse generators.
    C_NBR_PULSER : natural := 8;
    --  True for pulse events register
    C_WITH_PULSE_EVNTS : boolean := False
  );
  port (
    rst_n_i     : in  std_logic;
    clk_pg_i    : in  std_logic; -- 125M
    clk_cmp_i   : in  std_logic;
    clk_wb_i    : in  std_logic;
    sync_62m5_i : in  std_logic;
    wb_i        : in  t_wishbone_slave_in;
    wb_o        : out t_wishbone_slave_out;

    --  tm_* uses 62.5Mhz (16ns) and is in advance.
    tm_tai_i    : in  std_logic_vector(31 downto 0);
    tm_cycles_i : in  std_logic_vector(25 downto 0);
    tm_sync_i  : in  std_logic;

    tm_load_tai_i    : in  std_logic_vector(31 downto 0);
    tm_load_steps_i : in  std_logic_vector(25 downto 5);

    --  Inputs
    inputs_i    : in t_subpulse_array(13 downto 0);
    clocks_i    : in t_subpulse_array(8 downto 0);

    --  Outputs
    comb_outputs_o : out t_slv8_array(C_NBR_PULSER - 1 downto 0);

    --  Pulse when a pulser generates a raising edge (delayed).
    --  Used to generate interrupts (so masked if int_en of a pulser is set to 0)
    pulses_int_o : out std_logic_vector(C_NBR_PULSER - 1 downto 0);

    --  Logging
    --  Capture current logs.  The first log (pulser #0) is output on cur_log_o
    log_load_i :  in  std_logic;

    --  Shift logs: next pulser log is output on cur_log_o
    log_shift_i : in  std_logic;

    --  Pulser log
    cur_log_o :   out std_logic_vector(3 downto 0)
  );
end pulser_group;

architecture arch of pulser_group is
  alias clk_125m : std_logic is clk_pg_i;
  alias clk_62m5 : std_logic is clk_cmp_i;

  signal pulsers_loaded, pulsers_running, pulsers_start  : std_logic_vector(C_NBR_PULSER - 1 downto 0) := (others => '0');
  signal pulsers_abort, pulsers_int_en, pulsers_out_en : std_logic_vector(C_NBR_PULSER - 1 downto 0);

  type t_comparator_time_config is record
    sec  : std_logic_vector(31 downto 0);

    -- steps of 512 ns (21 bits)
    steps : std_logic_vector(29 downto 9);
  end record;

  --  Log2 of nbr comparators
  constant C_LOG2_COMPARATORS : natural := 6;
  constant C_NBR_COMPARATORS : natural := 2**C_LOG2_COMPARATORS;
  constant C_COMPARATORS_PER_PULSER : natural := C_NBR_COMPARATORS / C_NBR_PULSER;

  --  The comparators in the SRAM (or subgroup) must be scanned every 0.5us, and they are
  --  scanned using the 62.5Mhz.  So the maximum number of comparators is 2**4=16.
  constant C_SRAM_SIZE : natural := 16;
  constant C_LOG2_SUBGROUPS : natural := C_LOG2_COMPARATORS - 4;
  constant C_NBR_SUBGROUPS : natural := 2**C_LOG2_SUBGROUPS;

  constant C_PULSERS_PER_SUBGROUP : natural := C_NBR_PULSER / C_NBR_SUBGROUPS;

  subtype t_subgroup is natural range C_NBR_SUBGROUPS - 1 downto 0;
  subtype t_pulser is natural range C_NBR_PULSER - 1 downto 0;

  type t_comparator_ram_el is record
    cfg   : t_pulser_config;
    --  Absolute load time.
    tim   : t_comparator_time_config;
    --  If set, load immediately
    immediat : std_logic;
  end record;

  type t_out_cfg is record
    mask : std_logic_vector(C_NBR_PULSER -1 downto 0);
    inv_in  : std_logic;
    inv_out : std_logic;
  end record;


  type t_out_cfg_array is array (0 to C_NBR_PULSER - 1) of t_out_cfg;

  signal comp_icnt, comp_icnt_d : natural range 0 to C_SRAM_SIZE - 1;

  signal comparators_addr : std_logic_vector (10 downto 2);
  signal comparators_data_out : std_logic_vector (31 downto 0);
  signal comparators_wr_out : std_logic;

  type t_slv32_array is array(natural range <>) of std_logic_vector(31 downto 0);
  signal comparators_data_in_arr : t_slv32_array(t_subgroup);
  signal comparators_cpu_data_in : std_logic_vector (31 downto 0);

  signal comparators_en, comparators_busy : std_logic_vector(C_NBR_COMPARATORS - 1 downto 0);
  signal comparators_abort, comparators_late : std_logic_vector(C_NBR_COMPARATORS - 1 downto 0);

  --  Pulse when the comparator is enabled.
  signal comparators_en_wr : std_logic_vector(C_NBR_COMPARATORS - 1 downto 0);

  --  Pulse when the user write the busy bit.
  signal comparators_busy_wr : std_logic_vector(C_NBR_COMPARATORS - 1 downto 0);

  signal out_cfg_addr : std_logic_vector(4 downto 2);
  signal out_cfg_data_in, out_cfg_data_out : std_logic_vector(31 downto 0);
  signal out_cfg_wr : std_logic;

  type t_boolean_array is array (natural range <>) of boolean;

  signal load, load_int_en : t_boolean_array(t_subgroup);
  signal load_en : std_logic;

  signal pg_out_raw, pg_out : t_subpulse_array (t_pulser);
  signal pg_out_d : std_logic_vector (t_pulser);
  signal inputs : t_subpulse_array(31 downto 0);

  signal pulses_evnt_tm_tai : std_logic_vector (31 downto 0);
  signal pulses_evnt_tm_cyc : std_logic_vector (31 downto 0);
  signal pulses_evnt_lost, pulses_evnt_nempty : std_logic;
  signal pulses_evnt_ctl_wr, pulses_evnt_ctl_rd : std_logic;
  signal pg_redge_ext, pulses_evnt_redge : std_logic_vector(63 downto 0);

  type t_pulser_state_array is array(natural range <>) of std_logic_vector(3 downto 0);
  signal pulser_state : t_pulser_state_array(t_pulser);

  type t_pulser_ts_ram is array(t_pulser) of std_logic_vector(31 downto 0);
  type t_pulser_comp_id_ram is array(t_pulser) of std_logic_vector(C_COMPARATORS_PER_PULSER - 1 downto 0);

  --  Per pulser timestamp of the last output raising edge.
  signal pulser_ts_ram : t_pulser_ts_ram;

  --  Per pulser currently loaded comparator.
  signal current_comp_ram : t_pulser_comp_id_ram;

  --  Per pulser set of used comparators.
  signal done_comp_ram : t_pulser_comp_id_ram;

  --  Per pulser sticky bits of loaded comparators (cleared by user).
  signal loaded_comp_ram : t_pulser_comp_id_ram;

  --  Per pulser sticky bits of comparators which generated a pulse (cleared by user)
  signal pulser_pulses_ram : t_pulser_comp_id_ram;

  --  Pulsers state
  signal pulsers_adr  : std_logic_vector(7 downto 2);
  signal pulsers_dato : std_logic_vector(31 downto 0);
  signal pulsers_dati : std_logic_vector(31 downto 0);
  signal pulsers_rd   : std_logic;
  signal pulsers_wr   : std_logic;
  signal pulsers_rack : std_logic;
  signal pulsers_wack : std_logic;

  signal pulsers_idx : t_pulser;
  signal pulsers_reg : natural range 0 to 7;

  signal log_chain : t_pulser_log_array(7 downto 0);

  subtype t_ts_compact is std_logic_vector(31 downto 3);
  signal ts_compact, ts_compact_d, ts_compact_in : t_ts_compact;
begin
  --  Implement counter comp_cnt that starts every steps (512ns)
  --  all the comparators.
  p_cnt_62m5: process (clk_62m5)
  begin
    if rising_edge(clk_62m5) then
      comp_icnt_d <= comp_icnt;

      if tm_cycles_i(4 downto 0) = "11111" then
        comp_icnt <= 0;
      else
        if comp_icnt /= C_SRAM_SIZE - 1 then
          --  Avoid overflow.
          comp_icnt <= comp_icnt + 1;
        end if;
      end if;
    end if;
  end process;

  --  Generate the pulse to load the pulser from their config.
  p_cnt_125m: process (clk_125m)
  begin
    if rising_edge(clk_125m) then
      load_en <= '0';
      if sync_62m5_i = '1' and tm_cycles_i (4 downto 0) = "11111" and tm_sync_i = '1' then
        load_en <= '1';
      end if;
    end if;
  end process;

  --  Delay tm_tai/tm_cycles by 2 + 1 125mhz cycles for timestamping.
  --    2 as tm is in advance by 2 cycles
  --  + 1 as we timestamp at the end of the cycle.
  ts_compact_in(3) <= not sync_62m5_i;
  ts_compact_in(29 downto 4) <= tm_cycles_i;
  ts_compact_in(31 downto 30) <= tm_tai_i (1 downto 0);

  p_ts_compact: process(clk_125m)
  begin
    if rising_edge(clk_125m) then
      -- ts_compact <= ts_compact_d(0);
      -- ts_compact_d(0) <= ts_compact_d(1);

      ts_compact <= ts_compact_d;
      ts_compact_d <= ts_compact_in;
    end if;
  end process;

  --  Comparators address (from cpu).

  --  Two blocks of 16 entry sram.
  g_sram: for i in t_subgroup generate
    signal comp_mem_cpu0_idx : natural range 0 to C_SRAM_SIZE - 1;
    signal comparator_late_shift : std_logic_vector(C_SRAM_SIZE - 1 downto 0);
    signal subgroup_abort, subgroup_en_wr : std_logic_vector(C_SRAM_SIZE - 1 downto 0);
    signal comparators_out, comparators_cpu_out : t_comparator_ram_el;
    signal comparators_cpu_en_out : std_logic;
    signal comp_sec_eq, comp_steps_eq : boolean;
    signal comp_sec_less, comp_usec_less : boolean;
    signal comparators_wr : boolean;
    signal comparator_en : std_logic;
    signal comparators_data_in : std_logic_vector (31 downto 0);
  begin
    comparators_wr <= comparators_wr_out = '1'
      and unsigned (comparators_addr(9 + C_LOG2_SUBGROUPS - 1 downto 9)) = i;
    comp_mem_cpu0_idx <= to_integer (unsigned (comparators_addr (8 downto 5)));

    --  SRAM for comparators.
    p_comp_mem_sec: process (clk_62m5)
    is
      type t_sec_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram_sec : t_sec_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.tim.sec <= ram_sec (comp_icnt);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.tim.sec <= ram_sec (comp_mem_cpu0_idx);

        if comparators_wr and comparators_addr(4 downto 2) = "000" then
          ram_sec (comp_mem_cpu0_idx) := comparators_data_out;
        end if;
      end if;
    end process;

    p_comp_mem_nsec: process (clk_62m5)
    is
      type t_time_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(29 downto 0);
      variable ram : t_time_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.tim.steps <= ram (comp_icnt)(29 downto 9);
        comparators_out.cfg.time_ns <= '0' & ram (comp_icnt)(8 downto 0);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.tim.steps <= ram (comp_mem_cpu0_idx)(29 downto 9);
        comparators_cpu_out.cfg.time_ns <= '0' & ram (comp_mem_cpu0_idx)(8 downto 0);
        if comparators_wr and comparators_addr(4 downto 2) = "001" then
          ram (comp_mem_cpu0_idx) := comparators_data_out (29 downto 0);
        end if;
      end if;
    end process;

    p_comp_mem_cfg: process (clk_62m5)
    is
      type t_cfg_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram : t_cfg_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.cfg.out_en <= ram (comp_icnt)(19);
        comparators_out.cfg.int_en <= ram (comp_icnt)(18);
        comparators_out.immediat   <= ram (comp_icnt)(17);
        comparators_out.cfg.repeat <= ram (comp_icnt)(16);
        comparators_out.cfg.clk_en <= ram (comp_icnt)(14 downto 10);
        comparators_out.cfg.stop   <= ram (comp_icnt)(9 downto 5);
        comparators_out.cfg.start  <= ram (comp_icnt)(4 downto 0);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.cfg.out_en <= ram (comp_mem_cpu0_idx)(19);
        comparators_cpu_out.cfg.int_en <= ram (comp_mem_cpu0_idx)(18);
        comparators_cpu_out.immediat   <= ram (comp_mem_cpu0_idx)(17);
        comparators_cpu_out.cfg.repeat <= ram (comp_mem_cpu0_idx)(16);
        comparators_cpu_out.cfg.clk_en <= ram (comp_mem_cpu0_idx)(14 downto 10);
        comparators_cpu_out.cfg.stop   <= ram (comp_mem_cpu0_idx)(9 downto 5);
        comparators_cpu_out.cfg.start  <= ram (comp_mem_cpu0_idx)(4 downto 0);

        comparators_cpu_en_out <= comparators_en (comp_icnt + i * C_SRAM_SIZE);
        subgroup_abort <= (others => '0');
        subgroup_en_wr <= (others => '0');

        if comparators_wr and comparators_addr(4 downto 2) = "010" then
          ram (comp_mem_cpu0_idx) := comparators_data_out;
          subgroup_abort (comp_mem_cpu0_idx) <= comparators_data_out(30);
          subgroup_en_wr (comp_mem_cpu0_idx) <= comparators_data_out(31);
        end if;
      end if;
    end process;

    comparators_abort (C_SRAM_SIZE * (i + 1) - 1 downto C_SRAM_SIZE * i) <= subgroup_abort;
    comparators_en_wr (C_SRAM_SIZE * (i + 1) - 1 downto C_SRAM_SIZE * i) <= subgroup_en_wr;

    p_comp_mem_high: process (clk_62m5)
    is
      type t_high_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram : t_high_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.cfg.high <= ram (comp_icnt);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.cfg.high <= ram (comp_mem_cpu0_idx);

        if comparators_wr and comparators_addr(4 downto 2) = "011" then
          ram (comp_mem_cpu0_idx) := comparators_data_out;
        end if;
      end if;
    end process;

    p_comp_mem_period: process (clk_62m5)
    is
      type t_period_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram : t_period_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.cfg.period <= ram (comp_icnt);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.cfg.period <= ram (comp_mem_cpu0_idx);

        if comparators_wr and comparators_addr(4 downto 2) = "100" then
          ram (comp_mem_cpu0_idx) := comparators_data_out;
        end if;
      end if;
    end process;

    p_comp_mem_npulses: process (clk_62m5)
    is
      type t_npulses_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram : t_npulses_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.cfg.npulses <= ram (comp_icnt);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.cfg.npulses <= ram (comp_mem_cpu0_idx);

        if comparators_wr and comparators_addr(4 downto 2) = "101" then
          ram (comp_mem_cpu0_idx) := comparators_data_out;
        end if;
      end if;
    end process;

    p_comp_mem_idelay: process (clk_62m5)
    is
      type t_idelay_array is array (0 to C_SRAM_SIZE - 1) of std_logic_vector(31 downto 0);
      variable ram : t_idelay_array;
    begin
      if rising_edge(clk_62m5) then
        --  First port (RO): read values for pulse gen.
        comparators_out.cfg.idelay <= ram (comp_icnt);

        --  Second port (RW): cpu, split over many addresses.
        comparators_cpu_out.cfg.idelay <= ram (comp_mem_cpu0_idx);

        if comparators_wr and comparators_addr(4 downto 2) = "110" then
          ram (comp_mem_cpu0_idx) := comparators_data_out;
        end if;
      end if;
    end process;

    p_comp_din: process (comparators_addr, comparators_cpu_out, comparators_cpu_en_out)
    begin
      case comparators_addr(4 downto 2) is
        when "000" =>
          comparators_data_in <= comparators_cpu_out.tim.sec;
        when "001" =>
          comparators_data_in (8 downto 0) <= comparators_cpu_out.cfg.time_ns(8 downto 0);
          comparators_data_in (29 downto 9) <= comparators_cpu_out.tim.steps;
          comparators_data_in (31 downto 30) <= "00";
        when "010" =>
          comparators_data_in (31)           <= comparators_cpu_en_out;
          comparators_data_in (30 downto 20) <= (others => '0');
          comparators_data_in (19)           <= comparators_cpu_out.cfg.out_en;
          comparators_data_in (18)           <= comparators_cpu_out.cfg.int_en;
          comparators_data_in (17)           <= comparators_cpu_out.immediat;
          comparators_data_in (16)           <= comparators_cpu_out.cfg.repeat;
          comparators_data_in (15)           <= '0';
          comparators_data_in (14 downto 10) <= comparators_cpu_out.cfg.clk_en;
          comparators_data_in (9  downto  5) <= comparators_cpu_out.cfg.stop;
          comparators_data_in (4  downto  0) <= comparators_cpu_out.cfg.start;
        when "011" =>
          comparators_data_in <= comparators_cpu_out.cfg.high;
        when "100" =>
          comparators_data_in <= comparators_cpu_out.cfg.period;
        when "101" =>
          comparators_data_in <= comparators_cpu_out.cfg.npulses;
        when "110" =>
          comparators_data_in <= comparators_cpu_out.cfg.idelay;
        when others =>
          comparators_data_in <= (others => 'X');
      end case;
    end process;

    comparators_data_in_arr(i) <= comparators_data_in;

    p_comp_en: process (clk_62m5)
    begin
      if rising_edge(clk_62m5) then
        if rst_n_i = '0' then
          comparator_en <= '0';
        else
          comparator_en <= comparators_en (comp_icnt + i * C_SRAM_SIZE);
        end if;
      end if;
    end process;
    comp_sec_eq <= comparators_out.tim.sec = tm_load_tai_i;
    comp_steps_eq <= comparators_out.tim.steps = tm_load_steps_i;

    comp_sec_less <= unsigned (comparators_out.tim.sec) < unsigned (tm_load_tai_i);
    comp_usec_less <= unsigned (comparators_out.tim.steps) < unsigned (tm_load_steps_i);

    --  Load a pulser when a comparator entry is valid and triggers.
    load(i) <= (comparators_out.immediat = '1' or (comp_sec_eq and comp_steps_eq)) and comparator_en = '1';
    load_int_en(i) <= comparators_out.cfg.int_en = '1';

    --  Comparators late (set when the time is in the past and therefore will never load)
    p_comp_late: process (clk_62m5)
      alias comparators_late_part is comparators_late((i + 1) * C_SRAM_SIZE - 1 downto i *C_SRAM_SIZE);
      variable late : std_logic;
    begin
      if rising_edge(clk_62m5) then
        if rst_n_i = '0' then
          comparators_late_part <= (others => '0');
        else
          late := f_to_std_logic (comp_sec_less or (comp_sec_eq and comp_usec_less)) and comparator_en;
          if comp_icnt_d = C_SRAM_SIZE - 1 then
            --  End of counter iterations, assign.
            --  As the counter stays on C_SRAM_SIZE-1 value for many cycles, comparators_last_shift
            --  should not be updated on the last counter value.
            comparators_late_part <= late & comparator_late_shift (C_SRAM_SIZE - 1 downto 1);
          else
            comparator_late_shift <= late & comparator_late_shift (C_SRAM_SIZE - 1 downto 1);
          end if;
        end if;
      end if;
    end process;

    --  Pulses generators (2 pulsers by sub-group)
    gen_pulsers: for k in i*C_PULSERS_PER_SUBGROUP to (i+1) * C_PULSERS_PER_SUBGROUP - 1 generate
      signal load_r2, load_r3, load_r2_125 : std_logic;
      signal config_r, config_r2 : t_pulser_config;
      signal load_r, pg_eq_r : boolean;
      signal log_raw, log_p, log, log_d: std_logic_vector(3 downto 0);
      signal int_en, int_en_d1, int_en_d2 : std_logic;
      signal pulse_p : std_logic;
      signal done, done_d : std_logic;
      signal comp_id_mask_r, comp_id_mask_r2, comp_id_mask_r3 : unsigned (C_COMPARATORS_PER_PULSER - 1 downto 0);
      constant c_one : unsigned (C_COMPARATORS_PER_PULSER - 1 downto 0) := resize("1", C_COMPARATORS_PER_PULSER);
    begin
      process (clk_62m5)
        --  Bitmask of comparators which have been loaded to pulser c_pulser.
        variable pulser_load_bits, done_comp : std_logic_vector(C_COMPARATORS_PER_PULSER - 1 downto 0);
      begin
        if rising_edge (clk_62m5) then
          load_r2 <= '0';

          if rst_n_i = '0' then
            load_r <= false;
            loaded_comp_ram (k) <= (others => '0');
            done_comp_ram (k) <= (others => '0');
            current_comp_ram(k) <= (others => '0');
          else
            --  Loaded comparators:
            pulser_load_bits := loaded_comp_ram (k);

            --  WTC (from user).
            if pulsers_wr = '1'
              and pulsers_reg = c_PULSER_GROUP_MAP_PULSERS_LOADED_COMP_ADDR / 4
              and pulsers_idx = k
            then
              pulser_load_bits := pulser_load_bits
                and not pulsers_dati(C_COMPARATORS_PER_PULSER - 1 downto 0);
            end if;

            --  done_comp
            done_comp := done_comp_ram (k);
            --  Cleared on comp_busy WTC.
            done_comp := done_comp
              and not comparators_busy_wr ((k+1)*C_COMPARATORS_PER_PULSER -1 downto k*C_COMPARATORS_PER_PULSER);
            --  Set by the pulser (need to 'or' with a delayed signal to cross the 62.5/125 clock domain).
            if (done or done_d) = '1' then
              done_comp := done_comp or current_comp_ram (k);
            end if;
            done_comp_ram (k) <= done_comp;

            --  Load pulser if current comparator triggered and
            --  target this pulser.
            if load_r and pg_eq_r then
              load_r2 <= '1';
              config_r2 <= config_r;
              --  Current comparator for pulser k.
              comp_id_mask_r2 <= comp_id_mask_r;
              --  Bitmask of loaded comparators for pulser k.
              pulser_load_bits := pulser_load_bits or std_logic_vector(comp_id_mask_r);
            end if;
            loaded_comp_ram (k) <= pulser_load_bits;

            if load_r3 = '1' then
              --  One extra pipeline so that current_comp_ram is set exactly when the pulser
              --  is configured.
              current_comp_ram (k) <= std_logic_vector(comp_id_mask_r3);
            end if;

            load_r3 <= load_r2;
            comp_id_mask_r3 <= comp_id_mask_r2;

            load_r <= load (i);
            pg_eq_r <= comp_icnt_d / C_COMPARATORS_PER_PULSER = (k mod C_PULSERS_PER_SUBGROUP);
            config_r <= comparators_out.cfg;
            comp_id_mask_r <= c_one sll (comp_icnt_d mod C_COMPARATORS_PER_PULSER);
          end if;
        end if;
      end process;

      log_raw(0) <= pulsers_loaded (k);      --  When pulser is loaded
      log_raw(1) <= pulsers_start (k);       --  When pulser is started
      log_raw(2) <= pg_out_raw(k).v;         --  On a pulse
      log_raw(3) <= not pulsers_loaded (k);  --  When it becomes idle

      int_en <= pulsers_int_en(k);

      --  Detect rising edges
      log_p <= log_raw and not log_d;
      int_en_d1 <= int_en;

      pulse_p <= log_p (2);
      int_en_d2 <= int_en_d1;

      --  Log and interrupts.
      p_log: process (clk_125m)
        variable pulser_pulses : std_logic_vector(C_COMPARATORS_PER_PULSER - 1 downto 0);
      begin
        if rising_edge (clk_125m) then
          if rst_n_i = '0' then
            log <= (others => '0');
            --  As rising edges are detected, initialize to '1' so that there is no
            --  spure pikes at reset.
            log_d <= (others => '1');
            log_chain (k) <= (others => '0');

            pulses_int_o (k) <= '0';
          else
            log_d <= log_raw;
            done_d <= done;

            if log_load_i = '1' then
              log_chain (k) <= log;
            elsif log_shift_i = '1' then
              if k /= log_chain'left then
                log_chain (k) <= log_chain (k + 1);
              else
                log_chain (k) <= (others => '0');
              end if;
            end if;

            if log_load_i = '1' then
              --  New acquisition
              log <= log_p;
            else
              --  Accumulate log.
              log <= log or log_p;
            end if;

            pulser_pulses := pulser_pulses_ram (k);

            --  Timestamp & pulse (only if interrupts are enabled)
            if pulse_p = '1' and int_en_d2 = '1' then
              pulser_ts_ram (k) <= ts_compact & pg_out (k).dly;

              pulser_pulses := pulser_pulses or current_comp_ram (k);
            end if;

            --  WTC for pulsers_pulses
            if pulsers_wr = '1' and sync_62m5_i = '1'
              and pulsers_reg = c_PULSER_GROUP_MAP_PULSERS_PULSES_ADDR / 4
              and pulsers_idx = k
            then
              pulser_pulses := pulser_pulses and not pulsers_dati(C_COMPARATORS_PER_PULSER - 1 downto 0);
            end if;

            --  Update RAM.
            pulser_pulses_ram (k) <= pulser_pulses;
            pulses_int_o (k) <= pulse_p and int_en_d2;
          end if;
        end if;
      end process;

      load_r2_125 <= load_r2 and sync_62m5_i;

      inst_pulser: entity work.pulser
        port map (
          clk_i => clk_125m,
          rst_n_i => rst_n_i,
          abort_i => pulsers_abort(k),
          config_i => config_r2,
          load_i => load_r2_125,
          load_en_i => load_en,
          inputs_i => inputs,
          loaded_o => pulsers_loaded(k),
          running_o => pulsers_running(k),
          start_o => pulsers_start(k),
          done_o => done,
          state_o   => pulser_state(k),
          out_o => pg_out_raw(k),
          int_en_o => pulsers_int_en(k),
          out_en_o => pulsers_out_en(k)
          );
      --  The one with output enabled or disabled.
      pg_out(k) <= (pg_out_raw(k).v and pulsers_out_en(k), pg_out_raw(k).dly);
    end generate;
  end generate;

  cur_log_o <= log_chain (0);

  --  Comparators status and busy.
  p_comp_sta: process (clk_62m5)
  begin
    if rising_edge(clk_62m5) then
      if rst_n_i = '0' then
        comparators_en <= (others => '0');
        comparators_busy <= (others => '0');
      else
        --  Write from CPU: enable a comparator.
        --  Write from CPU: abort a comparator.
        comparators_en <= (comparators_en or comparators_en_wr) and not comparators_abort;
        comparators_busy <= (comparators_busy or comparators_en_wr)
         and not (comparators_busy_wr or comparators_abort);
        for i in t_subgroup loop
          if load (i) then
            --  Clear if a comparator triggered.
            comparators_en (comp_icnt_d + C_SRAM_SIZE * i) <= '0';
          end if;
          if load (i) and not load_int_en(i) then
            --  Clear if a comparator triggered and no interrupt
            comparators_busy (comp_icnt_d + C_SRAM_SIZE * i) <= '0';
          end if;
        end loop;
      end if;
    end if;
  end process;

  comparators_cpu_data_in <= comparators_data_in_arr (to_integer(unsigned (comparators_addr(10 downto 9))));

  pulsers_idx <= to_integer(unsigned(pulsers_adr(7 downto 5)));
  pulsers_reg <= to_integer(unsigned(pulsers_adr(4 downto 2)));

  --  Pulsers status
  p_pulser_state: process (clk_125m, sync_62m5_i)
  begin
    if rising_edge(clk_125m) and sync_62m5_i = '1' then
      --  Bit 1-0: word, bit 4-2: data sel, bit 7-5: pulser idx
      --  Write are ignored.
      --  data sel:   0: pulser internal fsm state
      pulsers_dato <= (others => '0');
      case pulsers_reg is
        when c_PULSER_GROUP_MAP_PULSERS_PULSES_ADDR / 4 =>
          --  Comparators which generated a pulse
          --  NOTE: WTC is implemented above
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0) <= pulser_pulses_ram (pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_TS_ADDR / 4 =>
          --  Timestamp
          pulsers_dato <= pulser_ts_ram (pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_DONE_COMP_ADDR / 4 =>
          --  Completed comparator
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0) <= done_comp_ram (pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_CURRENT_COMP_ADDR / 4 =>
          --  Currently loaded comparator
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0) <= current_comp_ram (pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_LOADED_COMP_ADDR / 4 =>
          --  Loaded comparators
          --  NOTE: WTC is implemented above
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0) <= loaded_comp_ram (pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_COMP_LATE_ADDR / 4 =>
          --  Late comparators
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0 ) <=
            comparators_late(C_COMPARATORS_PER_PULSER * (pulsers_idx + 1) - 1
              downto C_COMPARATORS_PER_PULSER * pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_COMP_BUSY_ADDR / 4 =>
          --  Busy comparators
          pulsers_dato (C_COMPARATORS_PER_PULSER - 1 downto 0 ) <=
            comparators_busy(C_COMPARATORS_PER_PULSER * (pulsers_idx + 1) - 1
                downto C_COMPARATORS_PER_PULSER * pulsers_idx);
        when c_PULSER_GROUP_MAP_PULSERS_STATE_ADDR / 4 =>
          --  Internal FSM state
          pulsers_dato (3 downto 0) <= pulser_state (pulsers_idx);
        when others =>
          pulsers_dato <= (others => 'X');
      end case;

      comparators_busy_wr <= (others => '0');
      if pulsers_wr = '1'
        and pulsers_reg = c_PULSER_GROUP_MAP_PULSERS_COMP_BUSY_ADDR / 4
      then
        comparators_busy_wr (C_COMPARATORS_PER_PULSER * (pulsers_idx + 1) - 1
          downto C_COMPARATORS_PER_PULSER * pulsers_idx) <= pulsers_dati(C_COMPARATORS_PER_PULSER - 1 downto 0);
      end if;

      pulsers_rack <= pulsers_rd;
      pulsers_wack <= pulsers_wr;
    end if;
  end process;

  --  Register map
  inst_map: entity work.pulser_group_map
    port map (
      rst_n_i => rst_n_i,
      clk_i => clk_wb_i,
      wb_i => wb_i,
      wb_o => wb_o,
      comparators_addr_o => comparators_addr,
      comparators_data_i => comparators_cpu_data_in,
      comparators_data_o => comparators_data_out,
      comparators_wr_o => comparators_wr_out,
      pulsers_adr_o   => pulsers_adr,
      pulsers_dato_i => pulsers_dato,
      pulsers_dati_o => pulsers_dati,
      pulsers_rd_o => pulsers_rd,
      pulsers_wr_o => pulsers_wr,
      pulsers_rack_i => pulsers_rack,
      pulsers_wack_i => pulsers_wack,
      out_cfg_addr_o => out_cfg_addr,
      out_cfg_data_i => out_cfg_data_in,
      out_cfg_data_o => out_cfg_data_out,
      out_cfg_wr_o => out_cfg_wr,
      pulses_evnt_tm_tai_i => pulses_evnt_tm_tai,
      pulses_evnt_tm_cyc_i => pulses_evnt_tm_cyc,
      pulses_evnt_evnt_i =>  pulses_evnt_redge(31 downto 0),
      pulses_evnt_sts_lost_i => pulses_evnt_lost,
      pulses_evnt_sts_nempty_i => pulses_evnt_nempty,
      pulses_evnt_ctl_wr_o => pulses_evnt_ctl_wr,
      pulses_evnt_ctl_rd_o => pulses_evnt_ctl_rd,
      pulsers_abort_o => pulsers_abort,
      pulsers_running_i => pulsers_running,
      pulsers_run_i => pulsers_running,
      pulsers_loaded_i => pulsers_loaded
    );

  --  Predefined outputs.
  --  As start and stop are defined from outputs, add the inputs, 0 and 1.
  gen_inputs_out: for i in 0 to 7 generate
    inputs (i) <= pg_out (i);
  end generate;
  gen_inputs_ext: for i in 0 to 13 generate
    inputs (8 + i) <= inputs_i (i);
  end generate;
  gen_inputs_clk: for i in 0 to 8 generate
    inputs (22 + i) <= clocks_i (i);
  end generate;
  inputs (31).v <= '0';

  gen_pg_out_d: process (clk_pg_i)
  begin
    if rising_edge(clk_pg_i) then
      for i in pg_out'range loop
        pg_out_d (i) <= pg_out (i).v;
      end loop;
    end if;
  end process;

  b_out_cfg: block
    signal out_cfgs : t_out_cfg_array;
    signal out_cfg_iaddr : natural range 0 to 63;
    signal ext_out : t_slv8_array(C_NBR_PULSER - 1 downto 0);
  begin
    --  Output configuration register CPU address as integer.
    out_cfg_iaddr <= to_integer(unsigned(out_cfg_addr));

    --  Output configuration registers.
    process (clk_wb_i)
    begin
      if rising_edge(clk_wb_i) then
        if rst_n_i = '0' then
          out_cfgs <= (others => (mask => (others => '0'),
                                  inv_in => '0',
                                  inv_out => '0'));
        elsif out_cfg_iaddr < C_NBR_PULSER then
          --  Read from CPU
          out_cfg_data_in <= x"0000_0000";
          out_cfg_data_in (7 downto 0) <= out_cfgs(out_cfg_iaddr).mask;
          out_cfg_data_in (8) <= out_cfgs(out_cfg_iaddr).inv_in;
          out_cfg_data_in (9) <= out_cfgs(out_cfg_iaddr).inv_out;
          if out_cfg_wr = '1' then
            --  Write from CPU
            out_cfgs(out_cfg_iaddr).mask <= out_cfg_data_out (7 downto 0);
            out_cfgs(out_cfg_iaddr).inv_in <= out_cfg_data_out (8);
            out_cfgs(out_cfg_iaddr).inv_out <= out_cfg_data_out (9);
          end if;
        end if;
      end if;
    end process;

    gen_ext_out: for i in t_pulser generate
    begin
      ext_out (i) <= extend_subpulse (pg_out (i), pg_out_d (i));
    end generate;

    --  Logic for the combined outputs:
    gen_comb_out: for i in t_pulser generate
    begin
      process (ext_out, out_cfgs)
        variable comb, sel_out : t_slv8;
      begin
        comb := (others => '0');
        for j in ext_out'range loop
          --  Pulser outputs can be inverted
          sel_out := ext_out(j) xor (t_slv8'range => out_cfgs(i).inv_in);
          --  Mask: OR the selected pulsers
          if out_cfgs(i).mask(j) = '1' then
            comb := comb or sel_out;
          end if;
        end loop;

        --  The result can be inverted
        comb := comb xor (t_slv8'range => out_cfgs(i).inv_out);
        comb_outputs_o (i) <= comb;
      end process;
    end generate;
  end block;

  gen_pulse_evnt: if C_WITH_PULSE_EVNTS generate
    signal pulses_evnt_fifo_wr : std_logic;
    signal pulses_evnt_fifo_full, pulses_evnt_fifo_empty : std_logic;
    signal pulses_evnt_ctl_rd_act : std_logic;

    signal pg_redge : std_logic_vector (t_pulser);
  begin
    --  Edge detector for pulser outputs.
    gen_redge: for i in pg_out'range generate
      pg_redge (i) <= (pg_out (i).v and not pg_out_d (i));
    end generate;

    pg_redge_ext(C_NBR_PULSER - 1 downto 0) <= pg_redge;
    pg_redge_ext(63 downto C_NBR_PULSER) <= (others => '0');

    --  Control pulse event fifo.
    process (clk_pg_i)
    begin
      if rising_edge(clk_pg_i) then
        pulses_evnt_fifo_wr <= '0';
          if pg_redge /= (pg_out'range => '0') then
            --  There is a rising edge.
            --  Store in the FIFO (if not full).
            if pulses_evnt_fifo_full = '1' then
              pulses_evnt_lost <= '1';
            else
              pulses_evnt_fifo_wr <= '1';
            end if;
          end if;
      end if;
    end process;

    pulses_evnt_nempty <= not pulses_evnt_fifo_empty;

    pulses_evnt_ctl_rd_act <= pulses_evnt_ctl_wr or pulses_evnt_ctl_rd;

    --  Pulse event fifo
    inst_pulses_evnt_fifo: entity work.inferred_async_fifo_dual_rst
      generic map (
        g_data_width => 122,
        g_size => 64,
        g_with_rd_empty => true,
        g_with_wr_full => true,
        g_almost_empty_threshold => 48,
        g_almost_full_threshold => 48
      )
      port map (
        rst_wr_n_i => rst_n_i,
        clk_wr_i => clk_pg_i,
        rst_rd_n_i => rst_n_i,
        clk_rd_i => clk_wb_i,
        d_i (63 downto 0) => pg_redge_ext,
        d_i (95 downto 64) => tm_tai_i,
        d_i (121 downto 96) => tm_cycles_i,
        we_i => pulses_evnt_fifo_wr,
        q_o (31 downto 0) => pulses_evnt_redge(31 downto 0),
        q_o (63 downto 32) => pulses_evnt_redge(63 downto 32),
        q_o (95 downto 64) => pulses_evnt_tm_tai,
        q_o (121 downto 96) => pulses_evnt_tm_cyc(25 downto 0),
        rd_i => pulses_evnt_ctl_rd_act,
        wr_full_o => pulses_evnt_fifo_full,
        wr_empty_o => open,
        wr_almost_empty_o => open,
        wr_almost_full_o => open,
        wr_count_o => open,
        rd_empty_o => pulses_evnt_fifo_empty,
        rd_full_o => open,
        rd_almost_full_o => open,
        rd_almost_empty_o => open,
        rd_count_o => open
      );

    pulses_evnt_tm_cyc(31 downto 26) <= (others => '0');
  end generate;

  gen_no_pulse_evnts: if not C_WITH_PULSE_EVNTS generate
    pulses_evnt_tm_tai <= (others => '0');
    pulses_evnt_tm_cyc <= (others => '0');
    pulses_evnt_redge <= (others => '0');
    pulses_evnt_lost <= '0';
    pulses_evnt_nempty <= '0';
  end generate;
end arch;
