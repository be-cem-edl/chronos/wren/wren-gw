files = [
    'board_map.vhd', 'host_map.vhd',
    'pci_map.vhd', 'vme_map.vhd',
    'pulser_group_map.vhd', "pulser_group_map_consts.vhd",
    'pulser_group.vhd',
    'pulser_pkg.vhd', 'pulser.vhd',
    'pulser_input.vhd', 'pulser_output.vhd',
    'iqdac_io.vhd',
    'wren_platform.vhd',
    'wren_core.vhd', 'timegen.vhd', 'freqgen.vhd',
    'nco_x32.vhd', 'nco_x7.vhd',
    'wrf_fifo.vhd',
    'ttc_biphase.vhd', 'ttc_frametx.vhd', 'ttc_demo.vhd',
    'ttc_modulator.vhd',
    'output_expander.vhd', 'output_shifter.vhd', 'io_expander.vhd',
    'sfp_argb_leds.vhd', 'lemos_argb_leds.vhd',
    'si5340_ctrl_map.vhd', 'si5340_ctrl.vhd',
]

modules = {
    'local': ['../../dependencies/RFFrameTransceiver'],
}
