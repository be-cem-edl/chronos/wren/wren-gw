library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ttc_modulator is
  port (
    clk_i : in std_logic;

    nco_i : in std_logic_vector(31 downto 0);
    nco_rst_i : in std_logic;
    nco_o : out std_logic_vector(31 downto 0);

    h1 : in std_logic_vector(15 downto 0);

    count_o : out std_logic_vector(15 downto 0);
    en_i : in std_logic
  );
end ttc_modulator;

architecture arch of ttc_modulator is
  signal bst_out_ddr, bst_prev_ddr : std_logic_vector(1 downto 0);
  signal ttc_data, ttc_addr       : std_logic_vector(7 downto 0);
  signal frev, load               : std_logic;
  signal mix_ddr, prev_nco_out    : std_logic;
  signal edge, clk_en             : std_logic;
  signal count                    : unsigned(15 downto 0);
begin
  inst_ttc_frame: entity work.ttc_frametx
    port map (
      clk_i => clk_i,
      rst_n_i => '1', -- TODO rst_80m_n,
      clk_en_i => clk_en,
      out_o => bst_out_ddr,
      data_i => ttc_data,
      subaddr_i => ttc_addr,
      frev_i => frev,
      load_imm_i => load,
      rdy_i => '0',
      ack_o => open
      );

  edge <= '1' when prev_nco_out /= nco_i(31) else '0';
  clk_en <= edge and not mix_ddr;

  count_o <= std_logic_vector(count);

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      load <= '0';
      frev <= '0';
      if nco_rst_i = '1' then
        count <= unsigned(h1) - 1;
        ttc_data <= x"ff";
        ttc_addr <= x"00";
        mix_ddr <= '0';
      else
        if edge = '1' then
          if mix_ddr = '0' then
            bst_prev_ddr <= bst_out_ddr;
          end if;
          mix_ddr <= not mix_ddr;
          if nco_i(31) = '1' then
            if count = unsigned(h1) then
              load <= '1';
              frev <= '1';
              count <= x"0001";
              ttc_addr <= std_logic_vector(unsigned(ttc_addr) + 1);
              ttc_data <= not ttc_addr;
              mix_ddr <= '0';
            else
              count <= count + 1;
            end if;
          end if;
        end if;
      end if;
    end if;
  end process;

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      --  Modulate the NCO clock with TTC encoding.
      if en_i = '1' then
        for j in nco_i'range loop
          if nco_i(j) = '1' then
            nco_o(j) <= bst_out_ddr(1);
          else
            if mix_ddr = '1' then
              nco_o(j) <= bst_prev_ddr(0);
            else
              nco_o(j) <= bst_out_ddr(0);
            end if;
          end if;
        end loop;
        prev_nco_out <= nco_i(31);
      else
        nco_o <= nco_i;
      end if;
    end if;
  end process;
end arch;
