--------------------------------------------------------------------------------
-- CERN BE-CO-HT
--------------------------------------------------------------------------------
--
-- unit name:   output_expander
--
-- description: Mini-core for a chain of sn74lv595
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity output_shifter is
  generic (
    g_WIDTH : positive := 1
  );
  port (
    clk_i : in std_logic;

    --  Signals to be serialized to the sn74lv595.
    --  Bit 0 is output A of the first chip (so the last serialized).
    data_i : in std_logic_vector(g_WIDTH - 1 downto 0);

    load_i  : in std_logic;
    shift_i : in std_logic;

    msb_o : out std_logic
  );
end;

architecture arch of output_shifter is
  signal regs : unsigned (g_WIDTH - 1 downto 0);
begin
  msb_o <= regs (regs'left);

  process (clk_i)
  begin
    if rising_edge(clk_i) then
      if load_i = '1' then
        regs <= unsigned (data_i);
      elsif shift_i = '1' then
        regs <= regs sll 1;
      end if;
    end if;
  end process;
end arch;