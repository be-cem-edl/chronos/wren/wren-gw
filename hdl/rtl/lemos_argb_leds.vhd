--------------------------------------------------------------------------------
-- CERN BE-CO-HT
-- Project    : General Cores Collection library
--------------------------------------------------------------------------------
--
-- unit name:   gc_argb_led_drv
--
-- description: Driver for argb (or intelligent) led like ws2812b
--
--------------------------------------------------------------------------------
-- Copyright CERN 2024
--------------------------------------------------------------------------------
-- Copyright and related rights are licensed under the Solderpad Hardware
-- License, Version 2.0 (the "License"); you may not use this file except
-- in compliance with the License. You may obtain a copy of the License at
-- http://solderpad.org/licenses/SHL-2.0.
-- Unless required by applicable law or agreed to in writing, software,
-- hardware and materials distributed under this License is distributed on an
-- "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
-- or implied. See the License for the specific language governing permissions
-- and limitations under the License.
--------------------------------------------------------------------------------

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity lemos_argb_leds is
  generic (
    g_clk_freq : natural;
    g_pulse_length : natural := 100
  );
  port (
    clk_i   : in std_logic;
    rst_n_i : in std_logic;

    din_i : in std_logic_vector(63 downto 0);
    dout_i : in std_logic_vector(63 downto 0);
    oe_i : in std_logic_vector(63 downto 0);

    --  If set, get color from memory (for each chain).
    force_i : in std_logic_vector(3 downto 0);

    --  Led color
    rgb_out_hi_i : in std_logic_vector(23 downto 0);
    rgb_out_lo_i : in std_logic_vector(23 downto 0);
    rgb_in_hi_i  : in std_logic_vector(23 downto 0);
    rgb_in_lo_i  : in std_logic_vector(23 downto 0);

    --  Access to the color memory
    leds_colors_adr_i    : in  std_logic_vector(7 downto 2);
    leds_colors_dato_o   : out std_logic_vector(31 downto 0);
    leds_colors_dati_i   : in  std_logic_vector(31 downto 0);
    leds_colors_rd_i     : in  std_logic;
    leds_colors_wr_i     : in  std_logic;
    leds_colors_rack_o   : out std_logic;
    leds_colors_wack_o   : out std_logic;

    --  Output to the first led.
    dout_o  : out std_logic_vector(3 downto 0)
  );
end lemos_argb_leds;

architecture arch of lemos_argb_leds is

  --  The memory for forced colors.
  subtype t_word is std_logic_vector(31 downto 0);
  type t_memory is array(63 downto 0) of t_word;
  signal colors_memory: t_memory;

  --  Counter to extend pulses
  subtype t_counter is natural range 0 to 255;
  type t_counter_array is array(63 downto 0) of t_counter;
  signal counter_memory: t_counter_array := (others => 0);

  signal oe_r : std_logic_vector(63 downto 0);

  signal has_changed, prev, cur : std_logic_vector(63 downto 0);

  signal cnt, cnt_d        : natural range 0 to 63;
  signal r, g, b           : std_logic_vector(7 downto 0);
  signal valid, ready, res : std_logic_vector(3 downto 0);

  signal counter_in, counter_out : t_counter;
  signal counter_we : std_logic;

  signal read_ack : std_logic;
  signal mem_val : t_word;

  type t_state is (S_RESET, S_READ, S_WAIT_READY);
  signal state : t_state;

begin
  p_memory: process(clk_i)
  begin
    if rising_edge(clk_i) then
      leds_colors_rack_o <= '0';
      leds_colors_wack_o <= '0';
      read_ack <= '0';

      if rst_n_i = '0' then
        null;
      else
        if leds_colors_wr_i = '1' then
          colors_memory(to_integer(unsigned(leds_colors_adr_i))) <= leds_colors_dati_i;
          leds_colors_wack_o <= '1';
        elsif leds_colors_rd_i = '1' then
          leds_colors_dato_o <= colors_memory(to_integer(unsigned(leds_colors_adr_i)));
          leds_colors_rack_o <= '1';
        elsif state = S_READ then
          mem_val <= colors_memory(cnt);
          read_ack <= '1';
        end if;

        counter_out <= counter_memory(cnt);
        if counter_we = '1' then
          counter_memory(cnt_d) <= counter_in;
        end if;
      end if;
    end if;
  end process;

  process(clk_i)
    variable col_sel : std_logic_vector(1 downto 0);
  begin
    if rising_edge(clk_i) then
      valid <= (others => '0');
      counter_we <= '0';
      oe_r <= oe_i;

      if rst_n_i = '0' then
        cnt <= 0;
        state <= S_RESET;

        prev <= (others => '0');
        cur <= (others => '0');
        has_changed <= (others => '0');
      else
        --  Select and register inputs
        cur <= (dout_i and oe_i) or (din_i and not oe_i);

        --  Accumulate inputs
        has_changed <= has_changed or (cur xor prev);

        --  Delayed count for writing counters.
        cnt_d <= cnt;

        case state is
          when S_RESET =>
            --  Wait until ARGB reset
            if res = (res'range => '1') then
              state <= S_READ;
            end if;
          when S_READ =>
            --  Read the color
            if read_ack = '1' then
              state <= S_WAIT_READY;
            end if;
          when S_WAIT_READY =>
            if ready (cnt / 16) = '1' then
              if force_i (cnt / 16) = '1' then
                r <= mem_val(23 downto 16);
                g <= mem_val(15 downto 8);
                b <= mem_val(7 downto 0);
              else
                if counter_out = g_pulse_length then
                  --  Can change the color.
                  if has_changed(cnt) = '1' then
                    col_sel (0) := not prev (cnt);
                    has_changed(cnt) <= '0';
                    prev(cnt) <= col_sel(0);
                    counter_in <= 0;
                    counter_we <= '1';
                  else
                    col_sel (0) := prev(cnt);
                  end if;
                else
                  --  The color has changed recently; still extending the pulse.
                  col_sel (0) := prev(cnt);
                  counter_in <= counter_out + 1;
                  counter_we <= '1';
                end if;

                col_sel(1) := oe_r(cnt);

                case col_sel is
                  when "00" =>
                    --  Input, not active
                    r <= rgb_in_lo_i(23 downto 16);
                    g <= rgb_in_lo_i(15 downto 8);
                    b <= rgb_in_lo_i(7 downto 0);
                  when "01" =>
                    --  Input, active
                    r <= rgb_in_hi_i(23 downto 16);
                    g <= rgb_in_hi_i(15 downto 8);
                    b <= rgb_in_hi_i(7 downto 0);
                  when "10" =>
                    --  Output, not active
                    r <= rgb_out_lo_i(23 downto 16);
                    g <= rgb_out_lo_i(15 downto 8);
                    b <= rgb_out_lo_i(7 downto 0);
                  when "11" =>
                    --  Output, active
                    r <= rgb_out_hi_i(23 downto 16);
                    g <= rgb_out_hi_i(15 downto 8);
                    b <= rgb_out_hi_i(7 downto 0);
                  when others =>
                    r <= (others => 'X');
                    g <= (others => 'X');
                    b <= (others => 'X');
                end case;
              end if;
              valid (cnt / 16) <= '1';

              if cnt = 63 then
                --  End of the leds
                cnt <= 0;
                state <= S_RESET;
              else
                if cnt >= 48 then
                  --  Next led in the raw
                  cnt <= cnt - 48 + 1;
                else
                  --  Next raw
                  cnt <= cnt + 16;
                end if;
                state <= S_READ;
              end if;
            end if;
        end case;
      end if;
    end if;
  end process;

  g_drv: for i in 0 to 3 generate
    inst_drv: entity work.gc_argb_led_drv
      generic map (
        g_clk_freq => g_clk_freq
        )
      port map (
        clk_i => clk_i,
        rst_n_i => rst_n_i,
        g_i => g,
        r_i => r,
        b_i => b,
        valid_i => valid (i),
        dout_o => dout_o (i),
        ready_o => ready (i),
        res_o => res (i)
        );
  end generate;
end arch;
