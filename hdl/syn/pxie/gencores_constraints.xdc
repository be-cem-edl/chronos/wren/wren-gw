# gc_sync
###########

### Cell b_wr.cmp_gth/U_Sync1/cmp_gc_sync
#DST_FF b_wr.cmp_gth/U_Sync1/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_user_clocking_internal.gen_single_instance.gtwiz_userclk_tx_inst/gen_gtwiz_userclk_tx_main.gtwiz_userclk_tx_active_sync_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_user_clocking_internal.gen_single_instance.gtwiz_userclk_tx_inst/gen_gtwiz_userclk_tx_main.gtwiz_userclk_tx_active_sync_reg/C } -to { b_wr.cmp_gth/U_Sync1/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.cmp_gth/U_Sync1/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.cmp_gth/U_Sync1/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.cmp_gth/U_Sync2/cmp_gc_sync
#DST_FF b_wr.cmp_gth/U_Sync2/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.gtwiz_userclk_rx_active_sync_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_user_clocking_internal.gen_single_instance.gtwiz_userclk_rx_inst/gen_gtwiz_userclk_rx_main.gtwiz_userclk_rx_active_sync_reg/C } -to { b_wr.cmp_gth/U_Sync2/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.cmp_gth/U_Sync2/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.cmp_gth/U_Sync2/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.cmp_gth/U_Sync_Reset/cmp_gc_sync
#DST_FF b_wr.cmp_gth/U_Sync_Reset/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C } -to { b_wr.cmp_gth/U_Sync_Reset/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.cmp_gth/U_Sync_Reset/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.cmp_gth/U_Sync_Reset/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_sync
#DST_FF b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.gtwiz_buffbypass_rx_done_out_reg/C
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_rx_done_inst/rst_in_out_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.gtwiz_buffbypass_rx_done_out_reg/C b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_rx_done_inst/rst_in_out_reg/C } -to { b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.cmp_gth/U_Sync_Serdes_RDY2/cmp_gc_sync
#DST_FF b_wr.cmp_gth/U_Sync_Serdes_RDY2/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.gtwiz_buffbypass_rx_done_out_reg/C
#SRC-CLK: b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_rx_done_inst/rst_in_out_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_tx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_tx_inst/gen_gtwiz_buffbypass_tx_main.gen_auto_mode.gtwiz_buffbypass_tx_done_out_reg/C {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_rx_buffer_bypass_internal.gen_single_instance.gtwiz_buffbypass_rx_inst/gen_gtwiz_buffbypass_rx_main.gen_auto_mode.gtwiz_buffbypass_rx_done_out_reg/C b_wr.cmp_gth/U_gtwizard_gthe4/inst/gen_gtwizard_gthe4_top.gtwizard_ultrascale_pxie_gtwizard_gthe4_inst/gen_gtwizard_gthe4.gen_reset_controller_internal.gen_single_instance.gtwiz_reset_inst/reset_synchronizer_rx_done_inst/rst_in_out_reg/C } -to { b_wr.cmp_gth/U_Sync_Serdes_RDY2/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.cmp_gth/U_Sync_Serdes_RDY2/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.cmp_gth/U_Sync_Serdes_RDY2/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/PPS_GEN/WRAPPED_PPSGEN/U_Sync_Link_OK
#DST_FF b_wr.inst_WR_CORE/WRPC/PPS_GEN/WRAPPED_PPSGEN/U_Sync_Link_OK/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/synced_d1_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/pcs_link_ok_o_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/synced_d1_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/pcs_link_ok_o_reg/C } -to { b_wr.inst_WR_CORE/WRPC/PPS_GEN/WRAPPED_PPSGEN/U_Sync_Link_OK/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/PPS_GEN/WRAPPED_PPSGEN/U_Sync_Link_OK/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/PPS_GEN/WRAPPED_PPSGEN/U_Sync_Link_OK/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_en_rxts
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_en_rxts/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_WB_SLAVE/ep_tscr_en_rxts_int_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_WB_SLAVE/ep_tscr_en_rxts_int_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_en_rxts/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_en_rxts/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_en_rxts/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_rx_cal_result
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_rx_cal_result/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/regs_o_tscr_rx_cal_result_rx_clk_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/regs_o_tscr_rx_cal_result_rx_clk_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_rx_cal_result/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_rx_cal_result/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/inst_sync_rx_cal_result/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_done_gen/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_sync_delay_reg[0]/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_sync_delay_reg[0]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_done_gen/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_f/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_f/cmp_gc_sync/sync_negedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_cal_pulse_a_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/timestamp_trigger_p_a_o_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_trigger_mask_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_cal_pulse_a_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/timestamp_trigger_p_a_o_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_trigger_mask_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_f/cmp_gc_sync/sync_negedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_f/cmp_gc_sync/sync_negedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_f/cmp_gc_sync/sync_negedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_r/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_r/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_cal_pulse_a_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/timestamp_trigger_p_a_o_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_trigger_mask_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_cal_pulse_a_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/timestamp_trigger_p_a_o_reg/C b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/rx_trigger_mask_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_r/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_r/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_rx_r/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_f/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_f/cmp_gc_sync/sync_negedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/timestamp_trigger_p_a_o_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/timestamp_trigger_p_a_o_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_f/cmp_gc_sync/sync_negedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_f/cmp_gc_sync/sync_negedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_f/cmp_gc_sync/sync_negedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_r/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_r/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/timestamp_trigger_p_a_o_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/timestamp_trigger_p_a_o_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_r/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_r/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/sync_ffs_tx_r/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_done_gen/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_sync_delay_reg[0]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_sync_delay_reg[0]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_done_gen/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_EP_TSU/tx_done_gen/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_LOS/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_LOS/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-PORT: sfp_los_i
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { sfp_los_i } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_LOS/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_LOS/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_LOS/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_TX_FAULT/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_TX_FAULT/cmp_gc_sync/sync_posedge.sync0_reg
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_SFP_TX_FAULT/cmp_gc_sync

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_idle_match/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_idle_match/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/an_idle_match_int_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/an_idle_match_int_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_idle_match/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_idle_match/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_idle_match/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_enable/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_enable/cmp_gc_sync/sync_posedge.sync0_reg
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_enable/cmp_gc_sync

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_ready/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_ready/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_ready_d_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_ready_d_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_ready/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_ready/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_an_rx_ready/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_los/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_los/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_SYNC_DET/synced_o_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_SYNC_DET/synced_o_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_los/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_los/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_los/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_mcr_reset/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_power_down/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_crst/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_crst/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/WR_SPEC_CAL_CRST_reg_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/WR_SPEC_CAL_CRST_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_crst/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_crst/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_crst/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_stat/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_stat/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/mdio_wr_spec_rx_cal_stat_rx_clk_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/mdio_wr_spec_rx_cal_stat_rx_clk_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_stat/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_stat/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_sync_rx_cal_stat/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_en/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_en/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_en_o_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_en_o_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_en/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_en/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_en/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_en_tx_cal/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_en_tx_cal/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/WR_SPEC_TX_CAL_reg_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/WR_SPEC_TX_CAL_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_en_tx_cal/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_en_tx_cal/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_en_tx_cal/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_mcr_reset/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_reset_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_mcr_reset/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_busy_o/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_busy_o/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/tx_busy_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/tx_busy_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_busy_o/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_busy_o/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_busy_o/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_error_o/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_error_o/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/tx_error_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/tx_error_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_error_o/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_error_o/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_pcs_error_o/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_power_down/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_MDIO_WB/MCR_pdown_reg_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_power_down/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_match_buff.U_Sync_Rst_match_buff/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_match_buff.U_Sync_Rst_match_buff/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_Bitslide/synced_o_reg/C
#SRC-CLK: b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_Bitslide/synced_o_reg/C b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_match_buff.U_Sync_Rst_match_buff/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_match_buff.U_Sync_Rst_match_buff/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_match_buff.U_Sync_Rst_match_buff/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/in_ext_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/in_ext_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_out2in_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_out2in_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync1_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_in2out_sync/sync_posedge.sync1_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_out2in_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_out2in_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_Sync_Done/cmp_out2in_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_sync_pfcr0_enable/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_sync_pfcr0_enable/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_WB_SLAVE/ep_pfcr0_enable_int_reg/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_WB_SLAVE/ep_pfcr0_enable_int_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_sync_pfcr0_enable/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_sync_pfcr0_enable/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/gen_with_packet_filter.U_packet_filter/U_sync_pfcr0_enable/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Sync_phy_rdy_sysclk/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Sync_phy_rdy_sysclk/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.cmp_gth/U_Bitslide/synced_o_reg/C
#SRC-CLK: b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
#WARNING: several inputs for cell
set_max_delay 16.000 -datapath_only -from { b_wr.cmp_gth/U_Bitslide/synced_o_reg/C b_wr.cmp_gth/U_Sync_Serdes_RDY1/cmp_gc_posedge/gen_async_rst.sync_posedge.dff_reg/C } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Sync_phy_rdy_sysclk/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Sync_phy_rdy_sysclk/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Sync_phy_rdy_sysclk/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Resync_Pulse/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/resync_p_o_reg/C
#CLK: wr_clk_helper_125m (period: 8.000)
set_max_delay 8.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/resync_p_o_reg/C} } -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Start_Pulse/cmp_in2out_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Start_Pulse/cmp_in2out_sync/sync_posedge.sync0_reg
#CLK: wr_clk_helper_125m (period: 8.000)
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_Sync_Start_Pulse/cmp_in2out_sync

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_sync_tag_strobe/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/new_edge_sreg_reg[0]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/new_edge_sreg_reg[0]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Resync_Pulse/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/resync_p_o_reg/C
#CLK: wr_clk_helper_125m (period: 8.000)
set_max_delay 8.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_feedback_dmtds[0].DMTD_FB/resync_p_o_reg/C} } -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Resync_Pulse/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Start_Pulse/cmp_in2out_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Start_Pulse/cmp_in2out_sync/sync_posedge.sync0_reg
#CLK: wr_clk_helper_125m (period: 8.000)
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_Sync_Start_Pulse/cmp_in2out_sync

### Cell b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_sync_tag_strobe/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/new_edge_sreg_reg[0]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/new_edge_sreg_reg[0]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_SOFTPLL/U_Wrapped_Softpll/gen_ref_dmtds[0].DMTD_REF/U_sync_tag_strobe/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_Sync_reset_refclk/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_Sync_reset_refclk/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Sync_reset_refclk/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Sync_reset_refclk/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Sync_reset_refclk/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_sync_reset_dmtd/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_sync_reset_dmtd/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#CLK: wr_clk_helper_125m (period: 8.000)
set_max_delay 8.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} } -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_dmtd/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_dmtd/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_dmtd/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#CLK: gtwiz_userclk_rx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} } -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_rxclk/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/U_sync_reset_txclk/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/U_sync_reset_txclk/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C
#CLK: gtwiz_userclk_tx_srcclk_out[0] (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/PERIPH/rst_net_n_chain_reg[0]_replica/C} } -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_txclk/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_txclk/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_sync_reset_txclk/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].U_Edge_Detect/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].clks_reg[0][clk_presc]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].clks_reg[0][clk_presc]/C} } -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[0].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].U_Edge_Detect/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].clks_reg[1][clk_presc]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].clks_reg[1][clk_presc]/C} } -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[1].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].U_Edge_Detect/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].clks_reg[2][clk_presc]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].clks_reg[2][clk_presc]/C} } -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[2].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].U_Edge_Detect/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].clks_reg[3][clk_presc]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].clks_reg[3][clk_presc]/C} } -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[3].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

### Cell b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].U_Edge_Detect/cmp_gc_sync
#DST_FF b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg
#SRC-CLK: b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].clks_reg[4][clk_presc]/C
#CLK: blk_clock.clk_sys_62m5_int (period: 16.000)
set_max_delay 16.000 -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].clks_reg[4][clk_presc]/C} } -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync0_reg/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/gen_with_clock_monitor.inst_clock_monitor/gen2[4].U_Edge_Detect/cmp_gc_sync/sync_posedge.sync1_reg/CLR }

# gc_sync_register
##################

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[0]/D
#SRC_CELL b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[1]/D
#SRC_CELL b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[2]/D
#SRC_CELL b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[3]/D
#SRC_CELL b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[4]/D
#SRC_CELL b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[4]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide, src {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[0]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[1]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[2]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[3]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[4]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[4]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[0]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[1]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[2]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[3]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[4]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[4]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[0]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[1]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[2]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[3]/C} {b_wr.cmp_gth/U_Bitslide/bitslide_o_reg[4]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[4]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_sync_bslide/sync1_reg[4]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[0]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[10]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[10]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[11]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[11]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[12]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[12]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[13]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[13]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[14]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[14]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[15]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[15]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[1]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[2]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[3]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[4]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[4]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[6]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[6]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[7]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[8]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[8]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[9]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[9]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[10]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[11]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[15]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[8]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[9]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[9]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[10]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[11]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[15]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[8]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[9]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[9]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[10]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[11]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[15]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[8]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/lcr_final_val_reg[9]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[9]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[10]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[11]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[12]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[13]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[14]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[15]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[8]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync0_reg[9]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[10]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[11]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[12]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[13]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[14]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[15]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[8]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_RX_PCS/U_Sync_an_rx_value/sync1_reg[9]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[12]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[12]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[13]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[13]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[14]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[14]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/D
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[7]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[8]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[8]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/D
#WARNING: no fan-in found for b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/D
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[8]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/D, clock gtwiz_userclk_tx_srcclk_out[0], period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[8]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[12]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[13]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[14]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[7]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/U_AUTONEGOTIATION/an_tx_val_o_reg[8]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[12]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[13]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[14]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[7]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[8]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[10]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[11]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[12]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[13]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[14]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[15]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[8]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync0_reg[9]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[10]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[11]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[12]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[13]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[14]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[15]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[8]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_sync_an_tx_val/sync1_reg[9]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[7]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D, clock gtwiz_userclk_tx_srcclk_out[0], period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/U_TX_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[7]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D, clock gtwiz_userclk_rx_srcclk_out[0], period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/rcb_reg[gray][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync1/sync1_reg[7]/CLR }

#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C
#DST_PINS: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D
#SRC_CELL b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C
#Cell: b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2, src {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C}, dst b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D }
set_bus_skew 16.000 -quiet -from { {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][0]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][1]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][2]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][3]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][4]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][5]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[gray][6]/C} {b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/wcb_reg[bin][7]/C} } -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/D b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/D }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync0_reg[7]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[4]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[5]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[6]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_Rx_Path/U_Rx_Clock_Align_FIFO/U_FIFO/U_Inferred_FIFO/U_Sync2/sync1_reg[7]/CLR }

#Cell: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[0]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][0]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[1]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][1]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[2]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][2]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[3]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][3]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[4]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][4]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[5]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][5]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[6]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][6]/C
#Cell: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1, src {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][6]/C}, dst inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[6]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][6]/C} } -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[6]/D }
set_bus_skew 16.000 -quiet -from { {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/rcb_reg[gray][6]/C} } -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[6]/D }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[0]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[1]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[2]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[3]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[4]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[5]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync0_reg[6]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[0]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[1]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[2]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[3]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[4]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[5]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync1/sync1_reg[6]/CLR }

#Cell: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[0]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][0]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[1]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][1]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[2]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][2]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[3]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][3]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[4]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][4]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[5]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][5]/C
#DST_PINS: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[6]/D
#SRC_CELL inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[bin][6]/C
#Cell: inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2, src {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[bin][6]/C}, dst inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[6]/D, clock blk_clock.clk_sys_62m5_int, period 16.000
set_max_delay 16.000 -quiet -datapath_only -from { {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[bin][6]/C} } -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[6]/D }
set_bus_skew 16.000 -quiet -from { {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][0]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][1]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][2]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][3]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][4]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[gray][5]/C} {inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/wcb_reg[bin][6]/C} } -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[0]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[1]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[2]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[3]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[4]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[5]/D inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[6]/D }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[0]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[1]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[2]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[3]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[4]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[5]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync0_reg[6]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[0]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[1]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[2]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[3]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[4]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[5]/CLR }
set_false_path -to { inst_wren/inst_cpu0_dp/gen_pulse_evnt.inst_pulses_evnt_fifo/U_Sync2/sync1_reg[6]/CLR }

# gc_reset_multi_aasd
#####################
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][0]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][10]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][11]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][12]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][13]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][14]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][15]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][1]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][2]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][3]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][4]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][5]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][6]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][7]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][8]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[1].rst_chains_reg[1][9]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][0]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][10]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][11]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][12]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][13]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][14]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][15]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][1]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][2]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][3]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][4]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][5]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][6]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][7]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][8]/CLR }
set_false_path -to { blk_clock.inst_rstlogic_reset/gen_rst_sync[2].rst_chains_reg[2][9]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[0].rst_chains_reg[0][0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[0].rst_chains_reg[0][1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[0].rst_chains_reg[0][2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[0].rst_chains_reg[0][3]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[1].rst_chains_reg[1][0]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[1].rst_chains_reg[1][1]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[1].rst_chains_reg[1][2]/CLR }
set_false_path -to { b_wr.inst_WR_CORE/WRPC/U_Endpoint/U_Wrapped_Endpoint/U_PCS_1000BASEX/gen_16bit.U_TX_PCS/inst_tx_fifo_resets/gen_rst_sync[1].rst_chains_reg[1][3]/CLR }

# gc_sync_word
##############
